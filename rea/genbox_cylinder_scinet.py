#!/usr/bin/python
#import numpy as np

"""
genbox_cylinder_scinet.py

This file creates a structured box mesh, with a single cell removed, evenly divided into 
groups of elements for distributed computing
"""

from sys import argv

def genbox():

    N = 1

    N_proc = 59

    outFilename = 'cylinder_' + str(N_proc) +'group.rea'
    outFile = open(outFilename,'wb')

    x_list = [0,4,6,8,9,11,15,20,25,30,35,40,45]
    n = 12
    y_list = [0,2,4,5,7,9]
    m = 5

    N_cells = 59

     #First line
    outFile.write(" **MESH DATA** 1st line is X of corner 1,2,3,4. 2nd line is Y." + "\n")
    outFile.write(str(N_cells).rjust(10)+"2".rjust(10)+str(N_proc).rjust(10)+" NEL,NDIM,NGROUPS" + "\n")


    elem = []
    group = []
    x1 = []
    x2 = []
    y1 = []
    y2 = []
    BC = []
    neighbour = []
    neighbour_side = []
    G_neighbour = []

    for j in xrange(1,m+1):
        for i in xrange(1,n+1):
            G = i + (j-1)*n
            elem.append(1)
            group.append(G)

            x1.append(x_list[i-1])
            x2.append(x_list[i])
            y1.append(y_list[j-1])
            y2.append(y_list[j])

            #side 1
            if j == 1:
                BC.append('v  ')
                neighbour.append(0)
                neighbour_side.append(0)
                G_neighbour.append(0)
            else:
                BC.append('GE ')
                neighbour.append(1)
                neighbour_side.append(3)
                G_neighbour.append(i+(j-2)*n)

            #side 2
            if i == n:
                BC.append('O  ')
                neighbour.append(0)
                neighbour_side.append(0)
                G_neighbour.append(0)
            else:
                BC.append('GE ')
                neighbour.append(1)
                neighbour_side.append(4)
                G_neighbour.append(i+1+(j-1)*n)

            #side 3
            if j == m:
                BC.append('v  ')
                neighbour.append(0)
                neighbour_side.append(0)
                G_neighbour.append(0)
            else:
                BC.append('GE ')
                neighbour.append(1)
                neighbour_side.append(1)
                G_neighbour.append(i+(j)*n)

            #side 4
            if i == 1:
                BC.append('v  ')
                neighbour.append(0)
                neighbour_side.append(0)
                G_neighbour.append(0)
            else:
                BC.append('GE ')
                neighbour.append(1)
                neighbour_side.append(2)
                G_neighbour.append(i-1+(j-1)*n)


    #element 28 is the cylinder so we now remove it.
    BC[4*26+1] = 'W  '  
    neighbour[4*26+1] = 0
    G_neighbour[4*26+1] = 0
    neighbour_side[4*26+1] = 0  

    BC[4*28+3] = 'W  '  
    neighbour[4*28+3] = 0
    G_neighbour[4*28+3] = 0
    neighbour_side[4*28+3] = 0      

    BC[4*15+2] = 'W  '  
    neighbour[4*15+2] = 0
    G_neighbour[4*15+2] = 0
    neighbour_side[4*15+2] = 0   

    BC[4*39+0] = 'W  '  
    neighbour[4*39+0] = 0
    G_neighbour[4*39+0] = 0
    neighbour_side[4*39+0] = 0   

    #and change element 60 to element 28
    x1[27] = x1[59]
    x2[27] = x2[59]
    y1[27] = y1[59]
    y2[27] = y2[59]

    BC[4*27+0] = 'GE '  
    neighbour[4*27+0] = 1
    G_neighbour[4*27+0] = 48
    neighbour_side[4*27+0] = 3

    BC[4*27+1] = 'O  '  
    neighbour[4*27+1] = 0
    G_neighbour[4*27+1] = 0
    neighbour_side[4*27+1] = 0

    BC[4*27+2] = 'v  '  
    neighbour[4*27+2] = 0
    G_neighbour[4*27+2] = 0
    neighbour_side[4*27+2] = 0

    BC[4*27+3] = 'GE '  
    neighbour[4*27+3] = 1
    G_neighbour[4*27+3] = 59
    neighbour_side[4*27+3] = 2  
   
    BC[4*47+2] = 'GE '  
    neighbour[4*47+2] = 1
    G_neighbour[4*47+2] = 28
    neighbour_side[4*39+2] = 1

    BC[4*58+1] = 'GE '  
    neighbour[4*58+1] = 1
    G_neighbour[4*58+1] = 28
    neighbour_side[4*58+1] = 4 

    for i in xrange(0,N_cells):
        G = group[i]
        E = elem[i]

        outFile.write("      ELEMENT"+str(E).rjust(11)+ " [    1A]    GROUP"+str(G).rjust(6)+"\n")
        outFile.write("  %.7f" % x1[i])
        outFile.write("      %.7f" % x2[i])
        outFile.write("      %.7f" % x2[i])
        outFile.write("      %.7f  \n" % x1[i])
        outFile.write("  %.7f" % y1[i])
        outFile.write("      %.7f" % y1[i])
        outFile.write("      %.7f" % y2[i])
        outFile.write("      %.7f  \n" % y2[i])


    #Dummey lines for curved elements
    outFile.write("  ***** CURVED SIDE DATA ***** \n")
    outFile.write("        0  Curved sides follow IEDGE,IEL,CURVE(I),I=1,5, CCURVE    \n")

    outFile.write("  ***** GROUP CONNECTIVITY ***** \n")
    for I in xrange(1,N_cells+1):
        proc = I

        for side in xrange(0,4):
            Neighbour = G_neighbour[4*(I-1)+side]

            if BC[4*(I-1)+side] == 'GE ':
                if side == 0:
                    Neighbour_side = 3
                elif side == 1: 
                    Neighbour_side = 4
                elif side == 2: 
                    Neighbour_side = 1
                elif side == 3: 
                    Neighbour_side = 2
            else:
                Neighbour_side = 0

            outFile.write(str(proc).rjust(6)+str(side+1).rjust(6)+str(Neighbour).rjust(6)+str(Neighbour_side).rjust(6)+"\n")

    #Boundary conditions
    outFile.write("  ***** BOUNDARY CONDITIONS ***** \n")
    outFile.write("  ***** FLUID   BOUNDARY CONDITIONS ***** \n")

    if N_cells < 1000:
    	ele_just = 3
    elif N_cells <10000:
	   ele_just =5
    elif N_cells<1000000:
	   ele_just =6	

    for i in xrange(0,N_cells):
        G = group[i]
        E = elem[i]

        #side 1
        outFile.write(" "+BC[4*i+0]+str(G).rjust(3)+str(E).rjust(ele_just)+str(1).rjust(3)+"  "
                    +("%9.5f     "% neighbour[4*i+0])+("%9.5f     "% neighbour_side[4*i+0])
                    +("%9.5f     "% G_neighbour[4*i+0])+("%9.5f     "% 0)
                    +("%9.5f     "% 0)+"\n")

        #side 2
        outFile.write(" "+BC[4*i+1]+str(G).rjust(3)+str(E).rjust(ele_just)+str(2).rjust(3)+"  "
                    +("%9.5f     "% neighbour[4*i+1])+("%9.5f     "% neighbour_side[4*i+1])
                    +("%9.5f     "% G_neighbour[4*i+1])+("%9.5f     "% 0)
                    +("%9.5f     "% 0)+"\n")
        
        #side 3
        outFile.write(" "+BC[4*i+2]+str(G).rjust(3)+str(E).rjust(ele_just)+str(3).rjust(3)+"  "
                    +("%9.5f     "% neighbour[4*i+2])+("%9.5f     "% neighbour_side[4*i+2])
                    +("%9.5f     "% G_neighbour[4*i+2])+("%9.5f     "% 0)
                    +("%9.5f     "% 0)+"\n")

        #side 4
        outFile.write(" "+BC[4*i+3]+str(G).rjust(3)+str(E).rjust(ele_just)+str(4).rjust(3)+"  "
                    +("%9.5f     "% neighbour[4*i+3])+("%9.5f     "% neighbour_side[4*i+3])
                    +("%9.5f     "% G_neighbour[4*i+3])+("%9.5f     "% 0)
                    +("%9.5f     "% 0)+"\n")
     

    outFile.close()


if __name__ == "__main__":
        genbox()
