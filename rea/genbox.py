#!/usr/bin/python
#import numpy as np

"""
genbox.py

This file creates a structured box mesh, evenly divided into 
groups of elements for distributed computing
"""

from sys import argv

def genbox():

    #Number of groups in x direction
    N = 1
    #Number of groups in y direction
    M = 1

    #Number of cells in x direction per group
    n = 8
    #Number of cells in y direction per group
    m = 8

    #Origin
    X0 = -1.0
    Y0 = -1.0

    #Box length and height
    L = 2.0
    H = 2.0

    N_proc  = N*M
    N_cells = N*M*n*m

    DX = L/N
    DY = H/M

    dx = DX/n
    dy = DY/m

    outFilename = str(N_cells) + 'cell_' + str(N_proc) +'group.rea'
    outFile = open(outFilename,'wb')

    #First line
    outFile.write(" **MESH DATA** 1st line is X of corner 1,2,3,4. 2nd line is Y." + "\n")
    outFile.write(str(N_cells).rjust(10)+"2".rjust(10)+str(N_proc).rjust(10)+" NEL,NDIM,NGROUPS" + "\n")

    for J in xrange(1,M+1):
        for I in xrange(1,N+1):
            proc = I+(J-1)*N
            X = X0 + (I-1)*DX
            Y = Y0 + (J-1)*DY

            for j in xrange(1,m+1):
                for i in xrange(1,n+1):
                    x = X + (i-1)*dx
                    y = Y + (j-1)*dy
                    elem = i + (j-1)*n
                    outFile.write("      ELEMENT"+str(elem).rjust(11)+ " [    1A]    GROUP"+str(proc).rjust(6)+"\n")
                    outFile.write("  %.7f" % x)
                    outFile.write("      %.7f" % (x+dx))
                    outFile.write("      %.7f" % (x+dx))
                    outFile.write("      %.7f  \n" % x)
                    outFile.write("  %.7f" % y)
                    outFile.write("      %.7f" % y)
                    outFile.write("      %.7f" % (y+dy))
                    outFile.write("      %.7f  \n" % (y+dy))


    #Dummey lines for curved elements
    outFile.write("  ***** CURVED SIDE DATA ***** \n")
    outFile.write("        0  Curved sides follow IEDGE,IEL,CURVE(I),I=1,5, CCURVE    \n")

    outFile.write("  ***** GROUP CONNECTIVITY ***** \n")
    for J in xrange(1,M+1):
        for I in xrange(1,N+1):
            proc = I+(J-1)*N
            #side 1
            if J == 1:
                neighbour = 0
                neighbour_side = 0
            else:
                neighbour = I+(J-2)*N
                neighbour_side = 3

            outFile.write(str(proc).rjust(6)+str(1).rjust(6)+str(neighbour).rjust(6)+str(neighbour_side).rjust(6)+"\n")

            #side 2
            if I == N:
                neighbour = 0
                neighbour_side = 0
            else:
                neighbour = I+1+(J-1)*N
                neighbour_side = 4

            outFile.write(str(proc).rjust(6)+str(2).rjust(6)+str(neighbour).rjust(6)+str(neighbour_side).rjust(6)+"\n")

            #side 3
            if J == M:
                neighbour = 0
                neighbour_side = 0
            else:
                neighbour = I+(J)*N
                neighbour_side = 1

            outFile.write(str(proc).rjust(6)+str(3).rjust(6)+str(neighbour).rjust(6)+str(neighbour_side).rjust(6)+"\n")

            #side 4
            if I == 1:
                neighbour = 0
                neighbour_side = 0
            else:
                neighbour = I-1+(J-1)*N
                neighbour_side = 2

            outFile.write(str(proc).rjust(6)+str(4).rjust(6)+str(neighbour).rjust(6)+str(neighbour_side).rjust(6)+"\n")

    #Boundary conditions
    outFile.write("  ***** BOUNDARY CONDITIONS ***** \n")
    outFile.write("  ***** FLUID   BOUNDARY CONDITIONS ***** \n")

    if N_cells < 1000:
    	ele_just = 3
    elif N_cells <10000:
	   ele_just =5
    elif N_cells<1000000:
	   ele_just =6	

    for J in xrange(1,M+1):
        for I in xrange(1,N+1):
            proc = I+(J-1)*N

            for j in xrange(1,m+1):
                for i in xrange(1,n+1):
                    elem = i + (j-1)*n

                    #side 1
                    if j == 1:
                        if J == 1:
                            #if (I-1)*n+i-0.5 <= 0.5*N*n:
                            #    BC = 'O  '
                            #else:
                            #    BC = 'v  '
                            BC = 'v  '
                            neighbour = 0
                            neighbour_side = 0
                            G_neighbour = 0
                        else:
                            BC = 'GE '
                            neighbour = i+(m-1)*n
                            neighbour_side = 3
                            G_neighbour = I+(J-2)*N
                    else:
                        BC = 'E  '
                        neighbour = i+(j-2)*n
                        neighbour_side = 3
                        G_neighbour = 0

                    outFile.write(" "+BC+str(proc).rjust(3)+str(elem).rjust(ele_just)+str(1).rjust(3)+"  "
                                +("%9.5f     "% neighbour)+("%9.5f     "% neighbour_side)
                                +("%9.5f     "% G_neighbour)+("%9.5f     "% 0)
                                +("%9.5f     "% 0)+"\n")

                    #side 2
                    if i == n:
                        if I == N:
                            #if (J-1)*m+j-0.5 <= 0.5*M*m:
                            #    BC = 'O  '
                            #else:
                            #    BC = 'v  '
                            BC = 'v  '
                            neighbour = 0
                            neighbour_side = 0
                            G_neighbour = 0
                        else:
                            BC = 'GE '
                            neighbour = 1+(j-1)*n
                            neighbour_side = 4
                            G_neighbour = I+1+(J-1)*N
                    else:
                        BC = 'E  '
                        neighbour = i+1+(j-1)*n
                        neighbour_side = 4
                        G_neighbour = 0

                    outFile.write(" "+BC+str(proc).rjust(3)+str(elem).rjust(ele_just)+str(2).rjust(3)+"  "
                                +("%9.5f     "% neighbour)+("%9.5f     "% neighbour_side)
                                +("%9.5f     "% G_neighbour)+("%9.5f     "% 0)
                                +("%9.5f     "% 0)+"\n")
                    
                    #side 3
                    if j == m:
                        if J == M:
                            #if (I-1)*n+i-0.5 <= 0.5*N*n:
                            #    BC = 'v  '
                            #else:
                            #    BC = 'O  '
                            BC = 'v  '
                            neighbour = 0
                            neighbour_side = 0
                            G_neighbour = 0
                        else:
                            BC = 'GE '
                            neighbour = i
                            neighbour_side = 1
                            G_neighbour = I+(J)*N
                    else:
                        BC = 'E  '
                        neighbour = i+(j)*n
                        neighbour_side = 1
                        G_neighbour = 0

                    outFile.write(" "+BC+str(proc).rjust(3)+str(elem).rjust(ele_just)+str(3).rjust(3)+"  "
                                +("%9.5f     "% neighbour)+("%9.5f     "% neighbour_side)
                                +("%9.5f     "% G_neighbour)+("%9.5f     "% 0)
                                +("%9.5f     "% 0)+"\n")

                    #side 4
                    if i == 1:
                        if I == 1:
                            #if (J-1)*m+j-0.5 <= 0.5*M*m:
                            #    BC = 'v  '
                            #else:
                            #    BC = 'O  '
                            BC = 'v  '
                            neighbour = 0
                            neighbour_side = 0
                            G_neighbour = 0
                        else:
                            BC = 'GE '
                            neighbour = n+(j-1)*n
                            neighbour_side = 2
                            G_neighbour = I-1+(J-1)*N
                    else:
                        BC = 'E  '
                        neighbour = i-1+(j-1)*n
                        neighbour_side = 2
                        G_neighbour = 0

                    outFile.write(" "+BC+str(proc).rjust(3)+str(elem).rjust(ele_just)+str(4).rjust(3)+"  "
                                +("%9.5f     "% neighbour)+("%9.5f     "% neighbour_side)
                                +("%9.5f     "% G_neighbour)+("%9.5f     "% 0)
                                +("%9.5f     "% 0)+"\n")
                    

    outFile.close()


if __name__ == "__main__":
        genbox()
