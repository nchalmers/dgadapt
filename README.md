# README #

### Key Features ###

* 2D (quadralateral cells)
* Fully parallel on CPU using MPI and OpenMP

### Installation ###

* Dependencies
    * OpenMPI
    * gfortran
    * Python + NumPy
* Install dependencies using 
    `sudo apt-get install python python-numpy, openmpi'