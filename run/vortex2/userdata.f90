!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!C
!!C             Incompressible Navier Stokes vortex problem
!!C             
!!C
!!C
!!C
!!C
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Initial condition
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

    SUBROUTINE INITIAL_CONDITION(U,P,X,Y)
    USE PARAM, ONLY:PI, NU
    IMPLICIT NONE

    DOUBLE PRECISION X, Y 
    DOUBLE PRECISION U(2), P

    U(1) = -DCOS(PI*X)*DSIN(PI*Y)
    U(2) =  DSIN(PI*X)*DCOS(PI*Y)

    P    = -0.25d0*(DCOS(2.0d0*PI*X)+DCOS(2.0d0*PI*Y))

    END SUBROUTINE

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Inflow boundary
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

    SUBROUTINE U_BC_INFLOW(U_R,X1,Y1,X2,Y2,K,SIDE,N,T)
    USE BASIS, ONLY: QUAD
    USE PARAM, ONLY: PI,NU
    IMPLICIT NONE

    INTEGER I, K, SIDE, N
    DOUBLE PRECISION X(0:N), Y(0:N),T
    DOUBLE PRECISION X1,Y1,X2,Y2
    DOUBLE PRECISION U_R(2,0:N)

    DO I = 0,N
        X(I) = X1 + (X2-X1)*0.5d0*(QUAD(I,N)+1.0d0)
        Y(I) = Y1 + (Y2-Y1)*0.5d0*(QUAD(I,N)+1.0d0)
    ENDDO

    U_R(1,:) = -DCOS(PI*X)*DSIN(PI*Y)*DEXP(-NU*2.0d0*PI*PI*T)
    U_R(2,:) =  DSIN(PI*X)*DCOS(PI*Y)*DEXP(-NU*2.0d0*PI*PI*T)

    END SUBROUTINE

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Outflow boundary
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

    SUBROUTINE U_QN_BC_OUTFLOW(Q_R,X1,Y1,X2,Y2,K,SIDE,N,T)
    USE BASIS, ONLY: QUAD
    USE PARAM, ONLY: PI,NU
    USE MESH, ONLY:NX,NY
    IMPLICIT NONE

    INTEGER I, K, SIDE, N
    DOUBLE PRECISION Q_R(2,0:N)
    DOUBLE PRECISION X(0:N), Y(0:N),T
    DOUBLE PRECISION X1,Y1,X2,Y2

    !No outflow for this problem
    Q_R = 0.0d0

    END SUBROUTINE



!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Outflow pressure boundary
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

    SUBROUTINE P_BC_OUTFLOW(P_R,X1,Y1,X2,Y2,K,SIDE,N,T)
    USE BASIS, ONLY: QUAD
    USE PARAM, ONLY: PI,NU
    IMPLICIT NONE

    INTEGER I, IND, K, SIDE, N
    DOUBLE PRECISION P_R(0:N)
    DOUBLE PRECISION X(0:N), Y(0:N), T
    DOUBLE PRECISION X1,Y1,X2,Y2

    !No outflow for this problem
    P_R = 0.0d0

    END

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Exact solution
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

    SUBROUTINE EXACT_SOLUTION(U,P,X,Y,T)
    USE PARAM, ONLY:PI, NU
    IMPLICIT NONE

    DOUBLE PRECISION X, Y, T 
    DOUBLE PRECISION U(2), P

    U(1) = -DCOS(PI*X)*DSIN(PI*Y)*DEXP(-NU*2.0d0*PI*PI*T)
    U(2) =  DSIN(PI*X)*DCOS(PI*Y)*DEXP(-NU*2.0d0*PI*PI*T)

    P    = -0.25d0*(DCOS(2.0d0*PI*X)+DCOS(2.0d0*PI*Y))*DEXP(-NU*4.0d0*PI*PI*T)

    END SUBROUTINE