!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!C
!!C             Linear adection equation, square pulse 
!!C             
!!C
!!C
!!C
!!C
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC


!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Flow direction
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

        SUBROUTINE GET_VELOCITY(A,B)
        IMPLICIT NONE

        DOUBLE PRECISION A, B 

        A = 0.5d0
        B = 0.4d0

        END SUBROUTINE

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Initial condition
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

        SUBROUTINE INITIAL_CONDITION(U,X,Y)
        USE PARAM, ONLY: PI
        IMPLICIT NONE

        DOUBLE PRECISION X, Y 
        DOUBLE PRECISION U(1)

        !Square pulse
        IF ( (X.LE.0.475d0).AND.(X.GT.0.175d0) .AND. (Y.LE.0.475d0).AND.(Y.GT.0.175d0) ) THEN
            U(1) = 1.0d0
        ELSE
            U(1) = 0.0d0
        ENDIF

        !Gaussian
        !U(1) = DEXP(-50.0d0*((X-0.25d0)**2 + (Y-0.25d0)**2))

        !DOUBLE PRECISION R
        !R = min(1.d0,4.0d0*dsqrt((X-0.25d0)**2 + (Y-0.25d0)**2))
        !U(1) = 0.5d0*(1.0d0+dcos(PI*R))

        END SUBROUTINE

!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!C
!C         Inflow boundary
!C
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

        SUBROUTINE U_BC_INFLOW(U_R,X1,Y1,X2,Y2,K,SIDE,N,T)
        IMPLICIT NONE

        DOUBLE PRECISION X1,Y1,X2,Y2, T
        INTEGER N, K, SIDE

        DOUBLE PRECISION U_R(1,0:N)

        U_R = 0.0d0        

        END SUBROUTINE
