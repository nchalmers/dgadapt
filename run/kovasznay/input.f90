
    !Size and orders
    INTEGER,PARAMETER:: NUM_EQN =2    !System size
    INTEGER,PARAMETER:: N       =8   !Initial polynomial orders
    INTEGER,PARAMETER:: M       =8
    INTEGER,PARAMETER:: NMAX    =9   !Maximum degree during adaptation
    INTEGER,PARAMETER:: MMAX    =9     
    INTEGER,PARAMETER:: NMIN    =1   !Minimum degree during adaptation
    INTEGER,PARAMETER:: MMIN    =1     

    !Mesh file
    CHARACTER(LEN=*),PARAMETER:: MESHFILE = "4cell_1group.rea"

    !Adaptation
    LOGICAL,PARAMETER:: IFADAPT_PROJ = .FALSE. !Adaptive initial projection
    LOGICAL,PARAMETER:: IFADAPT      = .FALSE. !Runtime adaptation

    INTEGER,PARAMETER:: NELMAX  =25     !Maximum number of elements during adaptation
    INTEGER,PARAMETER:: LEVEL   =0      !Maximum refinement depth

    INTEGER,PARAMETER::          NASTEPS =1         !Number of steps btw adaptaion
    INTEGER,PARAMETER::          NAINIT  =0         !Initial number of steps before adapating
    DOUBLE PRECISION,PARAMETER:: NATIME  =45.0      !Initial time before adapating

    DOUBLE PRECISION,PARAMETER:: SIGMA_MEAN_THRSH = 1.0d0   !Sigma threshold
    DOUBLE PRECISION,PARAMETER:: ERRTRSH_REF      = 0.8d0   !Relative error threshold for refinement
    DOUBLE PRECISION,PARAMETER:: ERRTRSH_COARSE   = 0.05d0  !Relative error threshold for coarsening

    !Non-dimensional parameters
    DOUBLE PRECISION,PARAMETER:: RE = 4.0d01   !Reynolds number
    DOUBLE PRECISION,PARAMETER:: PR = 1.0d0    !Prandtl number
    DOUBLE PRECISION,PARAMETER:: RA = 1.0d0    !Rayleight number

    !Time stepping
    DOUBLE PRECISION,PARAMETER:: DT_MAX    = 1.0d-4  !Maximum allowed time step
    DOUBLE PRECISION,PARAMETER:: CFL       = 1.0d-1   !CFL number
    DOUBLE PRECISION,PARAMETER:: MAXTIME   = 5.00    !Maximum simulation time (ignored if negative)
    INTEGER,PARAMETER::          MAXSTEPS  = 100    !Maximum time steps      (ignored if negative)
    INTEGER,PARAMETER::          MAXFRAMES = 100     !Number of frames to output

    !Preconditioned conjugate gradient solver
    LOGICAL, PARAMETER::         IFPRECOND   = .FALSE. !Preconditioner switch
    LOGICAL, PARAMETER::         IFMULTIGRID = .FALSE. !Multigrid preconditioner switch
    DOUBLE PRECISION,PARAMETER:: TOLP        = 1.0d-13  !Pressure solve tolerance
    DOUBLE PRECISION,PARAMETER:: TOLVEL      = 1.0d-13  !Velocity solve tolerance
    INTEGER, PARAMETER::         ITERMAX     = 5000    !Maximum solver iterations
