
    !Size and orders
    INTEGER,PARAMETER:: NUM_EQN =1    !System size
    INTEGER,PARAMETER:: N       =4   !Initial polynomial orders
    INTEGER,PARAMETER:: M       =4
    
    !Mesh file
    CHARACTER(LEN=*),PARAMETER:: MESHFILE = "16cell_1group.rea"

    !OpenMP threads
    INTEGER,PARAMETER:: NTHREADS = 8

    !Adaptation
    LOGICAL,PARAMETER:: IFADAPT_PROJ = .FALSE. !Adaptive initial projection
    LOGICAL,PARAMETER:: IFADAPT      = .FALSE. !Runtime adaptation

    INTEGER,PARAMETER::          NASTEPS =1         !Number of steps btw adaptaion
    INTEGER,PARAMETER::          NAINIT  =0         !Initial number of steps before adapating
    DOUBLE PRECISION,PARAMETER:: NATIME  =0.0       !Initial time before adapating

    INTEGER,PARAMETER:: NELMAX  =600     !Maximum number of elements during adaptation
    INTEGER,PARAMETER:: LEVEL   =5      !Maximum refinement depth
    INTEGER,PARAMETER:: NMAX    =10   !Maximum degree during adaptation
    INTEGER,PARAMETER:: MMAX    =10     
    INTEGER,PARAMETER:: NMIN    =4  !Minimum degree during adaptation
    INTEGER,PARAMETER:: MMIN    =4    

    DOUBLE PRECISION,PARAMETER:: SIGMA_MEAN_THRSH = 1.0d0   !Sigma threshold
    DOUBLE PRECISION,PARAMETER:: ERRTRSH_REF      = 0.5d0   !Relative error threshold for refinement
    DOUBLE PRECISION,PARAMETER:: ERRTRSH_COARSE   = 0.3d0  !Relative error threshold for coarsening

    !Non-dimensional parameters
    DOUBLE PRECISION,PARAMETER:: RE = 4.0d01   !Reynolds number
    DOUBLE PRECISION,PARAMETER:: PR = 1.0d0    !Prandtl number
    DOUBLE PRECISION,PARAMETER:: RA = 1.0d0    !Rayleight number

    !Time stepping
    DOUBLE PRECISION,PARAMETER:: DT_MAX    = 1.0d-2  !Maximum allowed time step
    DOUBLE PRECISION,PARAMETER:: CFL       = 1.0d0   !CFL number
    DOUBLE PRECISION,PARAMETER:: MAXTIME   = 5.00    !Maximum simulation time (ignored if negative)
    INTEGER,PARAMETER::          MAXSTEPS  = 5000    !Maximum time steps      (ignored if negative)
    INTEGER,PARAMETER::          MAXFRAMES = 10     !Number of frames to output

    !Preconditioned conjugate gradient solver
    LOGICAL, PARAMETER::         IFPRECOND   = .FALSE. !Preconditioner switch
    LOGICAL, PARAMETER::         IFMULTIGRID = .FALSE. !Multigrid preconditioner switch
    DOUBLE PRECISION,PARAMETER:: TOLP        = 1.0d-8  !Pressure solve tolerance
    DOUBLE PRECISION,PARAMETER:: TOLVEL      = 1.0d-8  !Velocity solve tolerance
    INTEGER, PARAMETER::         ITERMAX     = 2000    !Maximum solver iterations
