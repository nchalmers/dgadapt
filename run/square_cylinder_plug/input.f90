
    !Size and orders
    INTEGER,PARAMETER:: NUM_EQN =2   !System size
    INTEGER,PARAMETER:: N       =4   !Polynomial orders
    INTEGER,PARAMETER:: M       =4
    
    !Mesh file
    CHARACTER(LEN=*),PARAMETER:: MESHFILE = "cylinder_1group.rea"

    !OpenMP threads
    INTEGER,PARAMETER:: NTHREADS = 8

    !Adaptation
    LOGICAL,PARAMETER:: IFADAPT_PROJ = .FALSE. !Adaptive initial projection
    LOGICAL,PARAMETER:: IFADAPT      = .FALSE.  !Runtime adaptation

    INTEGER,PARAMETER::          NASTEPS =1         !Number of steps btw adaptaion
    INTEGER,PARAMETER::          NAINIT  =0         !Initial number of steps before adapating
    DOUBLE PRECISION,PARAMETER:: NATIME  =200.0      !Initial time before adapating

    INTEGER,PARAMETER:: NELMAX  =250   !Maximum number of elements during adaptation
    INTEGER,PARAMETER:: LEVEL   =2     !Maximum refinement depth
    INTEGER,PARAMETER:: NMAX    =8     !Maximum degree during adaptation
    INTEGER,PARAMETER:: MMAX    =8     
    INTEGER,PARAMETER:: NMIN    =4     !Minimum degree during adaptation
    INTEGER,PARAMETER:: MMIN    =4     

    DOUBLE PRECISION,PARAMETER:: SIGMA_MEAN_THRSH = 5.0d-2   !Sigma threshold
    DOUBLE PRECISION,PARAMETER:: ERRTRSH_REF      = 1.0d-3   !Error threshold for refinement
    DOUBLE PRECISION,PARAMETER:: ERRTRSH_COARSE   = 1.0d-5   !Error threshold for coarsening

    !Non-dimensional parameters
    DOUBLE PRECISION,PARAMETER:: RE = 1.0d2   !Reynolds number
    DOUBLE PRECISION,PARAMETER:: PR = 1.0d0    !Prandtl number
    DOUBLE PRECISION,PARAMETER:: RA = 1.0d0    !Rayleight number

    !Time stepping
    DOUBLE PRECISION,PARAMETER:: DT_MAX    = 5.0d-2  !Maximum allowed time step
    DOUBLE PRECISION,PARAMETER:: CFL       = 1.0d0   !CFL number
    DOUBLE PRECISION,PARAMETER:: MAXTIME   = 350.00    !Maximum simulation time (ignored if negative)
    INTEGER,PARAMETER::          MAXSTEPS  = 100     !Maximum time steps      (ignored if negative)
    INTEGER,PARAMETER::          MAXFRAMES = 100     !Number of frames to output

    !Preconditioned conjugate gradient solver
    LOGICAL, PARAMETER::         IFPRECOND   = .FALSE. !Preconditioner switch
    LOGICAL, PARAMETER::         IFMULTIGRID = .FALSE. !Multigrid preconditioner switch
    DOUBLE PRECISION,PARAMETER:: TOLP        = 1.0d-5  !Pressure solve tolerance
    DOUBLE PRECISION,PARAMETER:: TOLVEL      = 1.0d-8  !Velocity solve tolerance
    INTEGER, PARAMETER::         ITERMAX     = 1000000    !Maximum solver iterations
