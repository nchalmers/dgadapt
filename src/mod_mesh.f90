
MODULE MESH
IMPLICIT NONE

!Element vertices

DOUBLE PRECISION,ALLOCATABLE::X(:,:)
DOUBLE PRECISION,ALLOCATABLE::Y(:,:)

!Boundary data

CHARACTER*3,     ALLOCATABLE::BDRY_TYPE(:,:)
DOUBLE PRECISION,ALLOCATABLE::BDRY_DATA(:,:,:)

INTEGER,ALLOCATABLE::NEIGHBOUR_ID(:,:,:)
INTEGER,ALLOCATABLE::NEIGHBOUR_SIDE_ID(:,:,:)

INTEGER, PARAMETER, DIMENSION(16) :: SIDEMAP= (/ 1, 0,-1, 0, &
                                               & 0, 1, 0,-1, &
                                               & 0, 0, 1, 0, &
                                               & 0, 0, 0, 1 /)
INTEGER, PARAMETER, DIMENSION(8) :: SIDEMAP2= (/ 0, 1, 1, 0 ,&
                                               & 1, 0, 1, 0 /)

!Boundary buffer data

INTEGER :: GLOBAL_NEIGHBOUR_ID(4)
INTEGER :: GLOBAL_NEIGHBOUR_SIDE_ID(4)
INTEGER :: BDRY_BUFFER_SENDSIZE(4)
INTEGER :: BDRY_BUFFER_RECVSIZE(4)
INTEGER MAX_BUFFER_LENGTH
INTEGER, ALLOCATABLE :: BDRY_BUFFER_NEIGHBOUR_ID(:,:,:)
INTEGER, ALLOCATABLE :: BDRY_BUFFER_ELEM_MAP(:,:)
INTEGER, ALLOCATABLE :: BDRY_BUFFER_SIDE_MAP(:,:)

!Computed mesh data

DOUBLE PRECISION,ALLOCATABLE::JAC(:,:)
DOUBLE PRECISION,ALLOCATABLE::DXDR(:,:)
DOUBLE PRECISION,ALLOCATABLE::DXDS(:,:)
DOUBLE PRECISION,ALLOCATABLE::DYDR(:,:)
DOUBLE PRECISION,ALLOCATABLE::DYDS(:,:)
DOUBLE PRECISION,ALLOCATABLE::SCAL(:,:)
DOUBLE PRECISION,ALLOCATABLE::NX(:,:)
DOUBLE PRECISION,ALLOCATABLE::NY(:,:)

CONTAINS
    SUBROUTINE READ_MESH
    !------------------------------------------------------------------------   
    !       Reads the mesh file (.rea) and distributes the data to the 
    !       processors
    !------------------------------------------------------------------------
    USE SIZE, ONLY: NEL,NELMAX,NGROUPS,NPROC,LEVEL,NMMAX,NUM_EQN,RANK, NEL_TOTAL
    USE PARAM, ONLY: MESHFILE
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IEL,IC,GROUP
    INTEGER, ALLOCATABLE::NEL_GLOBAL(:)
    INTEGER IERROR
    DOUBLE PRECISION, ALLOCATABLE::X_GLOBAL(:,:,:),Y_GLOBAL(:,:,:)

    INTEGER ISIDE,ID1,ID2,NCSIDES, NDIM
    CHARACTER CHTEMP0
    CHARACTER*3 CHTEMP
    DOUBLE PRECISION DPTEMP(5)

    INTEGER K,L,SIDE, G_NEIGHBOUR, NB, ELEM, IND, NEIGHBOUR_SIDE, NEIGHBOUR_ELEM
    CHARACTER*3,     ALLOCATABLE :: BDRY_TYPE_TOTAL(:,:,:)
    DOUBLE PRECISION,ALLOCATABLE :: BDRY_DATA_TOTAL(:,:,:,:)
    INTEGER,         ALLOCATABLE :: NEIGHBOUR_ID_TOTAL(:,:,:,:)
    INTEGER,         ALLOCATABLE :: NEIGHBOUR_SIDE_ID_TOTAL(:,:,:,:)
    INTEGER,         ALLOCATABLE :: BDRY_BUFFER_SENDSIZE_TOTAL(:,:)
    INTEGER,         ALLOCATABLE :: BDRY_BUFFER_RECVSIZE_TOTAL(:,:)

    INTEGER GLOBAL_NEIGHBOUR_ID_TOTAL(4,NPROC), GLOBAL_NEIGHBOUR_SIDE_ID_TOTAL(4,NPROC)

    INTEGER, ALLOCATABLE :: BDRY_BUFFER_NEIGHBOUR_ID_TOTAL(:,:,:,:)
    INTEGER, ALLOCATABLE :: BDRY_BUFFER_ELEM_MAP_TOTAL(:,:,:)
    INTEGER, ALLOCATABLE :: BDRY_BUFFER_SIDE_MAP_TOTAL(:,:,:)


    IF (RANK.EQ.0) THEN !Only root process reads
        WRITE(*,*)'Opening .rea file: ', MESHFILE
        OPEN (UNIT=9,FILE=meshfile,STATUS='OLD')
        WRITE(*,'(32A)',advance='no')'Reading .rea file...'
        READ(9,*)! READ dummy line

        READ(9,*)  NEL_TOTAL,NDIM,NGROUPS !Read mesh size and dimension
        
        !TODO: In the future we should allow more/less processors than groups and partition if necessary
        IF (NGROUPS.GT.NPROC)THEN
            PRINT '("ERROR: Mesh file has ",I0," groups and requires at least ",&
                    & I0," processors. Only ", I0," launched")', NGROUPS,NGROUPS,NPROC
            CALL MPI_FINALIZE(IERROR)
            STOP 
        ENDIF

        ALLOCATE(NEL_GLOBAL(NPROC))
        ALLOCATE(X_GLOBAL(4,NELMAX,NPROC))
        ALLOCATE(Y_GLOBAL(4,NELMAX,NPROC))

        NEL_GLOBAL = 0
        IF (NEL_TOTAL.LE.NELMAX*NPROC) THEN
            DO IEL=1,NEL_TOTAL
                READ(9,30) GROUP !Read group number of current element
                NEL_GLOBAL(GROUP) = NEL_GLOBAL(GROUP)+1
                READ(9,*) (X_GLOBAL(IC,NEL_GLOBAL(GROUP),GROUP),IC=1,4) !Read x coordinates
                READ(9,*) (Y_GLOBAL(IC,NEL_GLOBAL(GROUP),GROUP),IC=1,4) !Read y coordinates
            END DO
            30  FORMAT(43X,I5)
        ELSE
            PRINT '("Number of elements in .rea file is too large (Total)")'
            CALL MPI_FINALIZE(IERROR)
            STOP
        ENDIF

        DO IC=1,NPROC
            IF (NEL_GLOBAL(IC).GT.NELMAX) THEN
                PRINT '("Number of elements in .rea file is too large (Elem/proc)")'
                CALL MPI_FINALIZE(IERROR)
                STOP 
            ENDIF
        ENDDO
    ENDIF

    ALLOCATE(X(4,NELMAX))
    ALLOCATE(Y(4,NELMAX))

    CALL MPI_BCAST(NDIM,1,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(NEL_GLOBAL,1,MPI_INTEGER,NEL,1,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(X_GLOBAL,4*NELMAX,MPI_DOUBLE_PRECISION,X,4*NELMAX,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(Y_GLOBAL,4*NELMAX,MPI_DOUBLE_PRECISION,Y,4*NELMAX,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERROR)

    CALL MPI_BARRIER (MPI_COMM_WORLD,IERROR)

    IF (RANK.EQ.0) THEN
        DEALLOCATE(X_GLOBAL)
        DEALLOCATE(Y_GLOBAL)
    ENDIF


    !Now read the boundaries

    IF (RANK.EQ.0) THEN
        ALLOCATE(BDRY_DATA_TOTAL(5,4,NELMAX,NPROC))
        ALLOCATE(BDRY_TYPE_TOTAL(4,NELMAX,NPROC))

        BDRY_DATA_TOTAL = 0.0d0 
        BDRY_TYPE_TOTAL = ' '

        READ(9,*)                ! ***** CURVED SIDE DATA *****
        READ(9,*) NCSIDES        ! NCSIDES Curved sides follow

        !Assume no curved sides

        GLOBAL_NEIGHBOUR_ID_TOTAL = 0
        GLOBAL_NEIGHBOUR_SIDE_ID_TOTAL = 0
        READ(9,*)               !***** GROUP CONNECTIVITY *****
        DO GROUP = 1,NGROUPS
            DO ISIDE = 1,4
                READ(9,49)  ID1, ID2, GLOBAL_NEIGHBOUR_ID_TOTAL(ISIDE,GROUP), &
                            GLOBAL_NEIGHBOUR_SIDE_ID_TOTAL(ISIDE,GROUP)
            49  FORMAT(4I6)
            ENDDO
        ENDDO

        READ(9,*)               !  ***** BOUNDARY CONDITIONS *****
        READ(9,*)               !  ***** FLUID   BOUNDARY CONDITIONS *****

        DO IEL=1,NEL_TOTAL
            DO ISIDE=1,4
                IF (NEL_TOTAL.LT.1000) THEN
                    READ(9,50) CHTEMP0,CHTEMP, GROUP, ID1, ID2, DPTEMP 
           50       FORMAT(A1,A3,I3,2I3,5G14.6)
                ELSEIF (NEL_TOTAL.LT.100000) THEN
                    READ(9,51) CHTEMP0,CHTEMP, GROUP, ID1, ID2, DPTEMP 
           51       FORMAT(A1,A3,I3,I5,I3,5G14.6)
                ELSEIF (NEL_TOTAL.LT.1000000) THEN
                    READ(9,52) CHTEMP0,CHTEMP, GROUP, ID1, ID2, DPTEMP 
           52       FORMAT(A1,A3,I3,I6,I3,5G14.6)
                ENDIF
                BDRY_TYPE_TOTAL(ID2,ID1,GROUP) = CHTEMP
                BDRY_DATA_TOTAL(:,ID2,ID1,GROUP) = DPTEMP
            ENDDO
        ENDDO
    !!
    !!     END OF BC READ
    !!
        CLOSE(9)


        ALLOCATE(NEIGHBOUR_ID_TOTAL(4,2,NELMAX,NPROC))
        ALLOCATE(NEIGHBOUR_SIDE_ID_TOTAL(4,2,NELMAX,NPROC))
        ALLOCATE(BDRY_BUFFER_SENDSIZE_TOTAL(4,NPROC))
        ALLOCATE(BDRY_BUFFER_RECVSIZE_TOTAL(4,NPROC))

        !establish internal face connectivity, and assign local index to inter-process edges
        NEIGHBOUR_ID_TOTAL = -1
        NEIGHBOUR_SIDE_ID_TOTAL  = 0
        BDRY_BUFFER_SENDSIZE_TOTAL = 0
        BDRY_BUFFER_RECVSIZE_TOTAL = 0

        DO GROUP = 1, NGROUPS 
            DO L=1,NEL_GLOBAL(GROUP)
                DO ISIDE = 1,4    
                    IF(BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'E  '.OR. BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'J  ') THEN !Local interior edge
                        NEIGHBOUR_ID_TOTAL      (ISIDE,1,L,GROUP) = INT(BDRY_DATA_TOTAL(1,ISIDE,L,GROUP))     !neighbours index
                        NEIGHBOUR_SIDE_ID_TOTAL (ISIDE,1,L,GROUP) = INT(BDRY_DATA_TOTAL(2,ISIDE,L,GROUP))  !neighbours side number
                    ELSEIF(BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'SP ') THEN  !Local split edge
                        !First check
                        IF (INT(BDRY_DATA_TOTAL(5,ISIDE,L,GROUP)).NE.2) THEN
                            PRINT '("Degree of non-conformity of cell ",I0,", side ",I0, ", on processor ",I0,", is not 2")'&
                                    &, L, ISIDE, GROUP    
                            CALL MPI_FINALIZE(IERROR)
                            STOP
                        ENDIF

                        !Find the 2 neighbours of this edge
                        NB = 0
                        DO K=1,NEL_GLOBAL(GROUP)
                            DO SIDE=1,4
                                IF(            (INT(BDRY_DATA_TOTAL(1,SIDE,K,GROUP)).EQ.L) &
                                        & .AND.(INT(BDRY_DATA_TOTAL(2,SIDE,K,GROUP)).EQ.ISIDE) &
                                        & .AND.(INT(BDRY_DATA_TOTAL(3,SIDE,K,GROUP)).EQ.0) ) THEN
                                    NB = NB+1
                                    
                                    IF(INT(BDRY_DATA_TOTAL(5,SIDE,K,GROUP)).EQ.0) THEN  !Left or right element
                                        NEIGHBOUR_ID_TOTAL      (ISIDE,1,L,GROUP)= K
                                        NEIGHBOUR_SIDE_ID_TOTAL (ISIDE,1,L,GROUP)= SIDE 
                                    ELSE
                                        NEIGHBOUR_ID_TOTAL      (ISIDE,2,L,GROUP)= K
                                        NEIGHBOUR_SIDE_ID_TOTAL (ISIDE,2,L,GROUP)= SIDE 
                                    ENDIF
                                ENDIF
                            ENDDO
                        ENDDO

                        !Second check
                        IF (NB.NE.2) THEN
                            PRINT '("Degree of non-conformity of cell ",I0,", side ",I0,&
                                    & ", on processo&
                                    &r ",I0,", is not 2")',&
                                    & L, ISIDE, GROUP
                            CALL MPI_FINALIZE(IERROR)
                            STOP
                        ENDIF

                    ELSEIF ((BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'GE ').OR.(BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'GJ ') &
                                & .OR.(BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'GSP')) THEN !Inter-process edge
                        DO SIDE = 1,4
                            IF (INT(BDRY_DATA_TOTAL(3,ISIDE,L,GROUP)).EQ.GLOBAL_NEIGHBOUR_ID_TOTAL(SIDE,GROUP))THEN !Find what SIDE this edge is on
                                BDRY_BUFFER_SENDSIZE_TOTAL(SIDE,GROUP) = BDRY_BUFFER_SENDSIZE_TOTAL(SIDE,GROUP)+1    !Local index of boundary edge
                                !Store local index in NEIGHBOUR_ID
                                NEIGHBOUR_ID_TOTAL(ISIDE,1,L,GROUP) = BDRY_BUFFER_SENDSIZE_TOTAL(SIDE,GROUP)
                                NEIGHBOUR_SIDE_ID_TOTAL (ISIDE,1,L,GROUP) = SIDE
                            ENDIF
                        ENDDO
                    ENDIF
                ENDDO
            ENDDO
        ENDDO


        !At this point, internal edges have their neighbour's index and 
        ! inter-process edges have a local index on that boundary. 
        ! Now we need a map from the local indicies to the element edges
        ! and a map between the local edge indicies along an inter-process boundary
        
        MAX_BUFFER_LENGTH = MAXVAL(BDRY_BUFFER_SENDSIZE_TOTAL)
        
        ALLOCATE(BDRY_BUFFER_NEIGHBOUR_ID_TOTAL(MAX_BUFFER_LENGTH*(2**LEVEL),4,2,NPROC))
        ALLOCATE(BDRY_BUFFER_ELEM_MAP_TOTAL(MAX_BUFFER_LENGTH*(2**LEVEL),4,NPROC))
        ALLOCATE(BDRY_BUFFER_SIDE_MAP_TOTAL(MAX_BUFFER_LENGTH*(2**LEVEL),4,NPROC))
        
        !First the map from local index to element edges
        IND = 0
        DO GROUP = 1,NGROUPS
            DO L=1,NEL
                DO ISIDE = 1,4     
                    IF ((BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'GE ').OR.(BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'GJ ') &
                            & .OR.(BDRY_TYPE_TOTAL(ISIDE,L,GROUP).EQ.'GSP')) THEN
                        !Get local index, and global side
                        IND  = NEIGHBOUR_ID_TOTAL      (ISIDE,1,L,GROUP)
                        SIDE = NEIGHBOUR_SIDE_ID_TOTAL (ISIDE,1,L,GROUP)

                        BDRY_BUFFER_ELEM_MAP_TOTAL(IND,SIDE,GROUP) = L      !Map local IND to element index
                        BDRY_BUFFER_SIDE_MAP_TOTAL(IND,SIDE,GROUP) = ISIDE  !Map local IND to side index
                    ENDIF
                ENDDO
            ENDDO
        ENDDO

        !Finally, construct the mapping between local indicies along the inter-process boundary
        DO GROUP = 1, NGROUPS
            DO SIDE = 1,4
                IF (GLOBAL_NEIGHBOUR_ID_TOTAL(SIDE,GROUP).NE.0) THEN
                    !Loop through the local index
                    DO L = 1,BDRY_BUFFER_SENDSIZE_TOTAL(SIDE,GROUP)
                        !Use local index to find what element and edge we're looking at
                        ELEM  = BDRY_BUFFER_ELEM_MAP_TOTAL(L,SIDE,GROUP)
                        ISIDE = BDRY_BUFFER_SIDE_MAP_TOTAL(L,SIDE,GROUP)

                        IF ((BDRY_TYPE_TOTAL(ISIDE,ELEM,GROUP).EQ.'GE ').OR.(BDRY_TYPE_TOTAL(ISIDE,ELEM,GROUP).EQ.'GJ ')) THEN
                            !Find neighbour in neighbouring group
                            NEIGHBOUR_ELEM = INT(BDRY_DATA_TOTAL(1,ISIDE,ELEM,GROUP))
                            NEIGHBOUR_SIDE = INT(BDRY_DATA_TOTAL(2,ISIDE,ELEM,GROUP))
                            G_NEIGHBOUR    = INT(BDRY_DATA_TOTAL(3,ISIDE,ELEM,GROUP))

                            !Map to the local index of this element
                            BDRY_BUFFER_NEIGHBOUR_ID_TOTAL(L,SIDE,1,GROUP) =&
                                    & NEIGHBOUR_ID_TOTAL(NEIGHBOUR_SIDE,1,NEIGHBOUR_ELEM,G_NEIGHBOUR)
                        ELSEIF (BDRY_TYPE_TOTAL(ISIDE,ELEM,GROUP).EQ.'GSP') THEN
                            !First check
                            IF (INT(BDRY_DATA_TOTAL(5,ISIDE,ELEM,GROUP)).NE.2) THEN
                                PRINT '("Degree of non-conformity of cell ",I0,", side ",I0, ", on processor ",I0,&
                                        &", is not 2")',&
                                        & ELEM, ISIDE, GROUP    
                                CALL MPI_FINALIZE(IERROR)
                                STOP
                            ENDIF

                            G_NEIGHBOUR = INT(BDRY_DATA_TOTAL(3,ISIDE,ELEM,GROUP))

                            !Find the 2 neighbours of this edge
                            NB = 0
                            DO NEIGHBOUR_ELEM=1,NEL_GLOBAL(G_NEIGHBOUR)
                                DO NEIGHBOUR_SIDE = 1,4
                                    IF(    (INT(BDRY_DATA_TOTAL(1,NEIGHBOUR_SIDE,NEIGHBOUR_ELEM,G_NEIGHBOUR)).EQ.ELEM) &
                                    & .AND.(INT(BDRY_DATA_TOTAL(2,NEIGHBOUR_SIDE,NEIGHBOUR_ELEM,G_NEIGHBOUR)).EQ.ISIDE) &
                                    & .AND.(INT(BDRY_DATA_TOTAL(3,NEIGHBOUR_SIDE,NEIGHBOUR_ELEM,G_NEIGHBOUR)).EQ.GROUP))THEN
                                        NB = NB+1
                                        IF(INT(BDRY_DATA_TOTAL(5,NEIGHBOUR_SIDE,NEIGHBOUR_ELEM,G_NEIGHBOUR)).EQ.0) THEN  !Left or right element
                                            BDRY_BUFFER_NEIGHBOUR_ID_TOTAL(L,SIDE,1,GROUP) =&
                                                & NEIGHBOUR_ID_TOTAL(NEIGHBOUR_SIDE,1,NEIGHBOUR_ELEM,G_NEIGHBOUR)
                                        ELSE
                                            BDRY_BUFFER_NEIGHBOUR_ID_TOTAL(L,SIDE,2,GROUP) =&
                                                & NEIGHBOUR_ID_TOTAL(NEIGHBOUR_SIDE,1,NEIGHBOUR_ELEM,G_NEIGHBOUR)
                                        ENDIF
                                    ENDIF
                                ENDDO
                            ENDDO

                            !Second check
                            IF (NB.NE.2) THEN
                                PRINT '("Degree of non-conformity of cell ",I0,", side ",I0, ", on processor ",I0,", &
                                      & is not 2")', L, ISIDE, GROUP
                                CALL MPI_FINALIZE(IERROR)
                                STOP
                            ENDIF
                        ENDIF
                    ENDDO


                    !Lastly, copy the size of the recieve buffer to the neighbours
                    G_NEIGHBOUR = GLOBAL_NEIGHBOUR_ID_TOTAL(SIDE,GROUP)
                    ISIDE       = GLOBAL_NEIGHBOUR_SIDE_ID_TOTAL(SIDE,GROUP)
                    BDRY_BUFFER_RECVSIZE_TOTAL(SIDE,GROUP) = BDRY_BUFFER_SENDSIZE_TOTAL(ISIDE,G_NEIGHBOUR)
                ENDIF
            ENDDO
        ENDDO
    ENDIF

    CALL MPI_BCAST(MAX_BUFFER_LENGTH,1,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)

    ALLOCATE(NEIGHBOUR_ID(4,2,NELMAX))
    ALLOCATE(NEIGHBOUR_SIDE_ID(4,2,NELMAX))
    ALLOCATE(BDRY_DATA(5,4,NELMAX))
    ALLOCATE(BDRY_TYPE(4,NELMAX))

    ALLOCATE(BDRY_BUFFER_NEIGHBOUR_ID(MAX_BUFFER_LENGTH*(2**LEVEL),4,2))
    ALLOCATE(BDRY_BUFFER_ELEM_MAP    (MAX_BUFFER_LENGTH*(2**LEVEL),4))
    ALLOCATE(BDRY_BUFFER_SIDE_MAP    (MAX_BUFFER_LENGTH*(2**LEVEL),4))

    CALL MPI_SCATTER(BDRY_TYPE_TOTAL,3*4*NELMAX,MPI_CHARACTER,&
                     BDRY_TYPE,3*4*NELMAX,MPI_CHARACTER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(BDRY_DATA_TOTAL,5*4*NELMAX,MPI_DOUBLE_PRECISION, &
                     BDRY_DATA,5*4*NELMAX,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERROR)


    CALL MPI_SCATTER(GLOBAL_NEIGHBOUR_SIDE_ID_TOTAL,4,MPI_INTEGER,&
                     GLOBAL_NEIGHBOUR_SIDE_ID,4,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(GLOBAL_NEIGHBOUR_ID_TOTAL,4,MPI_INTEGER,&
                     GLOBAL_NEIGHBOUR_ID,4,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)

    CALL MPI_SCATTER(NEIGHBOUR_SIDE_ID_TOTAL,4*2*NELMAX,MPI_INTEGER,&
                     NEIGHBOUR_SIDE_ID,4*2*NELMAX,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(NEIGHBOUR_ID_TOTAL,4*2*NELMAX,MPI_INTEGER,&
                     NEIGHBOUR_ID,4*2*NELMAX,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)

    CALL MPI_SCATTER(BDRY_BUFFER_SENDSIZE_TOTAL,4,MPI_INTEGER,&
                     BDRY_BUFFER_SENDSIZE,4,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(BDRY_BUFFER_RECVSIZE_TOTAL,4,MPI_INTEGER,&
                     BDRY_BUFFER_RECVSIZE,4,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(BDRY_BUFFER_NEIGHBOUR_ID_TOTAL,MAX_BUFFER_LENGTH*(2**LEVEL)*4*2,MPI_INTEGER,&
                     BDRY_BUFFER_NEIGHBOUR_ID,MAX_BUFFER_LENGTH*(2**LEVEL)*4*2,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(BDRY_BUFFER_ELEM_MAP_TOTAL,MAX_BUFFER_LENGTH*(2**LEVEL)*4,MPI_INTEGER,&
                     BDRY_BUFFER_ELEM_MAP,MAX_BUFFER_LENGTH*(2**LEVEL)*4,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)
    CALL MPI_SCATTER(BDRY_BUFFER_SIDE_MAP_TOTAL,MAX_BUFFER_LENGTH*(2**LEVEL)*4,MPI_INTEGER,&
                     BDRY_BUFFER_SIDE_MAP,MAX_BUFFER_LENGTH*(2**LEVEL)*4,MPI_INTEGER,0,MPI_COMM_WORLD,IERROR)

    IF (RANK.EQ.0) THEN
        DEALLOCATE(BDRY_DATA_TOTAL)
        DEALLOCATE(BDRY_TYPE_TOTAL)
        DEALLOCATE(NEIGHBOUR_ID_TOTAL)
        DEALLOCATE(NEIGHBOUR_SIDE_ID_TOTAL)
        DEALLOCATE(BDRY_BUFFER_SENDSIZE_TOTAL)
        DEALLOCATE(BDRY_BUFFER_NEIGHBOUR_ID_TOTAL)
        DEALLOCATE(BDRY_BUFFER_ELEM_MAP_TOTAL)
        DEALLOCATE(BDRY_BUFFER_SIDE_MAP_TOTAL)
        DEALLOCATE(NEL_GLOBAL)
        WRITE(*,*) 'done.'
    ENDIF

    END SUBROUTINE

!---------------------------------------------------------------------------------

    SUBROUTINE DEALLOCATE_MESH
    IMPLICIT NONE

    DEALLOCATE(X)
    DEALLOCATE(Y)
    DEALLOCATE(BDRY_DATA)
    DEALLOCATE(BDRY_TYPE)

    DEALLOCATE(NEIGHBOUR_ID)
    DEALLOCATE(NEIGHBOUR_SIDE_ID)

    DEALLOCATE(BDRY_BUFFER_NEIGHBOUR_ID)
    DEALLOCATE(BDRY_BUFFER_ELEM_MAP)
    DEALLOCATE(BDRY_BUFFER_SIDE_MAP)

    END SUBROUTINE DEALLOCATE_MESH

!---------------------------------------------------------------------------------

    SUBROUTINE ALLOCATE_METRICS
    USE SIZE, ONLY: NPMAX, NELMAX
    IMPLICIT NONE

    ALLOCATE(DXDR(0:NPMAX,NELMAX))
    ALLOCATE(DYDR(0:NPMAX,NELMAX))
    ALLOCATE(DXDS(0:NPMAX,NELMAX))
    ALLOCATE(DYDS(0:NPMAX,NELMAX))
    ALLOCATE(JAC(0:NPMAX,NELMAX))
    ALLOCATE(SCAL(4,NELMAX))
    ALLOCATE(NX(4,NELMAX))
    ALLOCATE(NY(4,NELMAX))

    END SUBROUTINE ALLOCATE_METRICS

!---------------------------------------------------------------------------------

    SUBROUTINE DEALLOCATE_METRICS
    IMPLICIT NONE

    DEALLOCATE(DXDR)
    DEALLOCATE(DYDR)
    DEALLOCATE(DXDS)
    DEALLOCATE(DYDS)
    DEALLOCATE(JAC)
    DEALLOCATE(SCAL)
    DEALLOCATE(NX)
    DEALLOCATE(NY)

    END SUBROUTINE DEALLOCATE_METRICS

!---------------------------------------------------------------------------------
    
    SUBROUTINE INIT_METRICS
    !-----------------------------------------------------------
    !     Generate mesh metrics
    !-----------------------------------------------------------
    USE SIZE, ONLY: NEL,NG,MG
    USE BASIS, ONLY: QUAD
    IMPLICIT NONE

    INTEGER I,J,K,N,M
    DOUBLE PRECISION XR, XS, YR, YS,JJ

    DO K=1,NEL
        N = NG(K)
        M = MG(K)
        DO J=0,M
            DO I=0,N 
                CALL QUADMAP(DXDR(I+(N+1)*J,K),DXDS(I+(N+1)*J,K), &
                             DYDR(I+(N+1)*J,K),DYDS(I+(N+1)*J,K), &
                             QUAD(I,N),QUAD(J,M),X(:,K),Y(:,K))    
                JAC(I+(N+1)*J,K) = DXDR(I+(N+1)*J,K)*DYDS(I+(N+1)*J,K) &
                                  -DXDS(I+(N+1)*J,K)*DYDR(I+(N+1)*J,K)
            ENDDO
        ENDDO

        !Side 1
        CALL QUADMAP(XR,XS,YR,YS,0.0d0,-1.0d0,X(:,K),Y(:,K))
        JJ = XR*YS-XS*YR
        SCAL(1,K) = DSQRT(YR**2 + XR**2)
        NX(1,K)= SIGN(1.0d0,JJ)*YR/SCAL(1,K)
        NY(1,K)=-SIGN(1.0d0,JJ)*XR/SCAL(1,K)

        !Side 2
        CALL QUADMAP(XR,XS,YR,YS,1.0d0,0.0d0,X(:,K),Y(:,K))
        JJ = XR*YS-XS*YR
        SCAL(2,K) = DSQRT(YS**2 + XS**2)
        NX(2,K)= SIGN(1.0d0,JJ)*YS/SCAL(2,K)
        NY(2,K)=-SIGN(1.0d0,JJ)*XS/SCAL(2,K)

        !Side 3
        CALL QUADMAP(XR,XS,YR,YS,0.0d0,1.0d0,X(:,K),Y(:,K))
        JJ = XR*YS-XS*YR
        SCAL(3,K) = DSQRT(YR**2 + XR**2)
        NX(3,K)=-SIGN(1.0d0,JJ)*YR/SCAL(3,K)
        NY(3,K)= SIGN(1.0d0,JJ)*XR/SCAL(3,K)

        !Side 4
        CALL QUADMAP(XR,XS,YR,YS,-1.0d0,0.0d0,X(:,K),Y(:,K))
        JJ = XR*YS-XS*YR
        SCAL(4,K) = DSQRT(YS**2 + XS**2)
        NX(4,K)=-SIGN(1.0d0,JJ)*YS/SCAL(4,K)
        NY(4,K)= SIGN(1.0d0,JJ)*XS/SCAL(4,K)
    ENDDO

    END SUBROUTINE INIT_METRICS

!---------------------------------------------------------------------------------

    SUBROUTINE QUADMAP(DXDR,DXDS,DYDR,DYDS,R,S,X,Y)
    IMPLICIT NONE

    DOUBLE PRECISION,INTENT(OUT):: DXDR,DXDS,DYDR,DYDS
    DOUBLE PRECISION,INTENT(IN):: R,S
    DOUBLE PRECISION,INTENT(IN):: X(4),Y(4)

    DXDR = 0.25d0*( (1-S)*(X(2)-X(1)) + (1+S)*(X(3)-X(4)) )
    DYDR = 0.25d0*( (1-S)*(Y(2)-Y(1)) + (1+S)*(Y(3)-Y(4)) )
    DXDS = 0.25d0*( (1-R)*(X(4)-X(1)) + (1+R)*(X(3)-X(2)) )
    DYDS = 0.25d0*( (1-R)*(Y(4)-Y(1)) + (1+R)*(Y(3)-Y(2)) )

    END SUBROUTINE QUADMAP
    

!-------------------------------------------------------------------------------
!
!   Adaptation subroutines
!
!-----------------------------------------------------------------------------

    SUBROUTINE UPDATE_METRICS(K)
    USE SIZE, ONLY: NG, MG
    USE BASIS, ONLY: QUAD
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K
    INTEGER I,J,N,M

    N = NG(K)
    M = MG(K)

    DO J=0,M
        DO I=0,N
            CALL QUADMAP(DXDR(I+(N+1)*J,K),DXDS(I+(N+1)*J,K), &
                         DYDR(I+(N+1)*J,K),DYDS(I+(N+1)*J,K), &
                         QUAD(I,N),QUAD(J,M),X(:,K),Y(:,K))    
            JAC(I+(N+1)*J,K) = DXDR(I+(N+1)*J,K)*DYDS(I+(N+1)*J,K) &
                              -DXDS(I+(N+1)*J,K)*DYDR(I+(N+1)*J,K)
        ENDDO
    ENDDO

    END SUBROUTINE UPDATE_METRICS

!----------------------------------------------------------------------------

    SUBROUTINE COARSEN_METRICS_X(K1)
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1
    INTEGER K2

    K2 = NEIGHBOUR_ID(2,1,K1)

    X(2,K1) = X(2,K2)
    Y(2,K1) = Y(2,K2)
    X(3,K1) = X(3,K2)
    Y(3,K1) = Y(3,K2)

    SCAL(1,K1) = 2.0d0*SCAL(1,K1)
    SCAL(3,K1) = 2.0d0*SCAL(3,K1)

    CALL UPDATE_METRICS(K1)

    END SUBROUTINE COARSEN_METRICS_X
    
!----------------------------------------------------------------------------

    SUBROUTINE COARSEN_METRICS_Y(K1)
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1
    INTEGER K2

    K2 = NEIGHBOUR_ID(3,1,K1)

    X(3,K1) = X(3,K2)
    Y(3,K1) = Y(3,K2)
    X(4,K1) = X(4,K2)
    Y(4,K1) = Y(4,K2)

    SCAL(2,K1) = 2.0d0*SCAL(2,K1)
    SCAL(4,K1) = 2.0d0*SCAL(4,K1)

    CALL UPDATE_METRICS(K1)

    END SUBROUTINE COARSEN_METRICS_Y

!---------------------------------------------------------------------------

    SUBROUTINE MOVE_METRICS(K1,K2)
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2

    X(:,K2) = X(:,K1)
    Y(:,K2) = Y(:,K1)

    DXDR(:,K2) = DXDR(:,K1)
    DXDS(:,K2) = DXDS(:,K1)
    DYDR(:,K2) = DYDR(:,K1)
    DYDS(:,K2) = DYDS(:,K1)

    JAC(:,K2) = JAC(:,K1)
    SCAL(:,K2) = SCAL(:,K1)
    NX(:,K2) = NX(:,K1)
    NY(:,K2) = NY(:,K1)

    END SUBROUTINE MOVE_METRICS

!----------------------------------------------------------------------------

    SUBROUTINE REFINE_METRICS_X(K1,K2)
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2

    X(1,K2) = 0.5d0*(X(1,K1)+X(2,K1))
    Y(1,K2) = 0.5d0*(Y(1,K1)+Y(2,K1))
    X(2,K2) = X(2,K1)
    Y(2,K2) = Y(2,K1)
    X(3,K2) = X(3,K1)
    Y(3,K2) = Y(3,K1)
    X(4,K2) = 0.5d0*(X(4,K1)+X(3,K1))
    Y(4,K2) = 0.5d0*(Y(4,K1)+Y(3,K1))

    X(2,K1) = X(1,K2)
    Y(2,K1) = Y(1,K2)
    X(3,K1) = X(4,K2)
    Y(3,K1) = Y(4,K2)

    SCAL(1,K1) = 0.5d0*SCAL(1,K1)
    SCAL(3,K1) = 0.5d0*SCAL(3,K1)

    SCAL(:,K2) = SCAL(:,K1)

    NX(:,K2) = NX(:,K1)
    NY(:,K2) = NY(:,K1)

    CALL UPDATE_METRICS(K1)
    CALL UPDATE_METRICS(K2)

    END SUBROUTINE REFINE_METRICS_X

!----------------------------------------------------------------------------

    SUBROUTINE REFINE_METRICS_Y(K1,K2)
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2

    X(1,K2) = 0.5d0*(X(1,K1)+X(4,K1))
    Y(1,K2) = 0.5d0*(Y(1,K1)+Y(4,K1))
    X(2,K2) = 0.5d0*(X(2,K1)+X(3,K1))
    Y(2,K2) = 0.5d0*(Y(2,K1)+Y(3,K1))
    X(3,K2) = X(3,K1)
    Y(3,K2) = Y(3,K1)
    X(4,K2) = X(4,K1)
    Y(4,K2) = Y(4,K1)

    X(3,K1) = X(2,K2)
    Y(3,K1) = Y(2,K2)
    X(4,K1) = X(1,K2)
    Y(4,K1) = Y(1,K2)

    SCAL(2,K1) = 0.5d0*SCAL(2,K1)
    SCAL(4,K1) = 0.5d0*SCAL(4,K1)

    SCAL(:,K2) = SCAL(:,K1)

    NX(:,K2) = NX(:,K1)
    NY(:,K2) = NY(:,K1)

    CALL UPDATE_METRICS(K1)
    CALL UPDATE_METRICS(K2)

    END SUBROUTINE REFINE_METRICS_Y

END MODULE MESH
