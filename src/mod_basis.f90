MODULE BASIS
IMPLICIT NONE

DOUBLE PRECISION,ALLOCATABLE::QUAD(:,:),W(:,:)

DOUBLE PRECISION,ALLOCATABLE::LEGENDRE_P(:,:,:)

DOUBLE PRECISION,ALLOCATABLE::QUAD_LOBATTO(:,:)
DOUBLE PRECISION,ALLOCATABLE::INTERP_LOBATTO(:,:,:)

DOUBLE PRECISION,ALLOCATABLE::DER(:,:)
DOUBLE PRECISION,ALLOCATABLE::INTERP(:,:,:,:)
DOUBLE PRECISION,ALLOCATABLE::INTERP_ENDPOINTS(:,:,:)
DOUBLE PRECISION,ALLOCATABLE::INTERP_LEFT(:,:,:,:)
DOUBLE PRECISION,ALLOCATABLE::INTERP_RIGHT(:,:,:,:)

PRIVATE :: GL, GLL, BARW, LEGENDRE_POLYNOMIAL_AND_DERIVATIVE, &
           ALMOSTEQUAL, MATDER, AL024, INTERP_BASIS

CONTAINS
    SUBROUTINE ALLOCATE_BASIS
    USE SIZE, ONLY: NMMAX, NPMAX, NELMAX
    IMPLICIT NONE

    ALLOCATE(QUAD      (0:NMMAX,NMMAX))
    ALLOCATE(W         (0:NMMAX,NMMAX))
    ALLOCATE(DER       (0:NPMAX,NMMAX))
    ALLOCATE(INTERP      (0:NMMAX,0:NMMAX,NMMAX,NMMAX))
    ALLOCATE(INTERP_ENDPOINTS(0:NMMAX,0:1,NMMAX))
    ALLOCATE(INTERP_LEFT (0:NMMAX,0:NMMAX,NMMAX,NMMAX))
    ALLOCATE(INTERP_RIGHT(0:NMMAX,0:NMMAX,NMMAX,NMMAX))
    ALLOCATE(QUAD_LOBATTO(0:NMMAX,NMMAX))
    ALLOCATE(INTERP_LOBATTO(0:NMMAX,0:NMMAX,NMMAX))
    ALLOCATE(LEGENDRE_P(0:NMMAX,0:NMMAX,NMMAX))

    END SUBROUTINE ALLOCATE_BASIS

!----------------------------------------------------------------------

    SUBROUTINE DEALLOCATE_BASIS
    IMPLICIT NONE

    DEALLOCATE(QUAD)
    DEALLOCATE(W)
    DEALLOCATE(DER)
    DEALLOCATE(INTERP)
    DEALLOCATE(INTERP_ENDPOINTS)
    DEALLOCATE(INTERP_LEFT)
    DEALLOCATE(INTERP_RIGHT)
    DEALLOCATE(QUAD_LOBATTO)
    DEALLOCATE(INTERP_LOBATTO)
    DEALLOCATE(LEGENDRE_P)

    END SUBROUTINE DEALLOCATE_BASIS

!----------------------------------------------------------------------

    SUBROUTINE INIT_BASIS
    !!--------------------------------------------------------------
    !!        Compute the Gauss Legendre points and their 
    !!        corresponding derivative matrices, and interpolate the lagrange
    !!        polynomails at the Lobatto points 
    !!
    !! -------------------------------------------------------------
    USE SIZE, ONLY: NMMAX
    IMPLICIT NONE

    INTEGER I, J, K
    DOUBLE PRECISION QUAD_LEFT (0:NMMAX)
    DOUBLE PRECISION QUAD_RIGHT(0:NMMAX)

    DOUBLE PRECISION DQ !Dummy variable

    DOUBLE PRECISION W_GLL(0:NMMAX) !Filler array. Stores the Lobatto quadrature weights which are no longer needed

    DOUBLE PRECISION BAR_QUAD(0:NMMAX,NMMAX)

    DO I = 1,NMMAX
        !Gauss Legendre quadrature nodes and weights
        CALL GL(I,QUAD(:,I),W(:,I))

        !Gauss Lobatto quadrature nodes and weights
        CALL GLL(I,QUAD_LOBATTO(:,I),W_GLL)

        CALL BARW(I,BAR_QUAD(:,I),QUAD(:,I))

        !Derivtive matrix
        ! DER(i+(N+1)*j,N) = L_j'(x_i)   0<=i,j<=N
        CALL MATDER(QUAD(:,I),BAR_QUAD(:,I),I,DER(:,I), 1)
    ENDDO

    !Evaluate the legendre polynomials at the quadrature points. Used to transform to modal form
    ! LEGENDRE_P(K,I,N) = L_k(xi_i),    0<=i<=N
    DO K = 0,NMMAX
        DO I = 1,NMMAX
            DO J = 0,I
                CALL LEGENDRE_POLYNOMIAL_AND_DERIVATIVE(K,QUAD(J,I),LEGENDRE_P(K,J,I),DQ)
            ENDDO
        ENDDO
    ENDDO

    DO I = 1,NMMAX
        !Interpolation matrix to Lobatto nodes for plotting
        CALL INTERP_BASIS(INTERP_LOBATTO(:,:,I),I,QUAD_LOBATTO(:,I),I,QUAD(:,I),BAR_QUAD(:,I))

        !Interpolate basis at endpoints for surface integrals
        CALL INTERP_BASIS(INTERP_ENDPOINTS(:,0,I),0,(/-1.0D0/),I,QUAD(:,I),BAR_QUAD(:,I))
        CALL INTERP_BASIS(INTERP_ENDPOINTS(:,1,I),0,(/ 1.0D0/),I,QUAD(:,I),BAR_QUAD(:,I))

        DO J = 1,NMMAX
            !Interpolate at other polynomial orders for non-uniform p approximations
            CALL INTERP_BASIS(INTERP(:,:,I,J),J,QUAD(:,J),I,QUAD(:,I),BAR_QUAD(:,I))
            
            QUAD_LEFT  = 0.5d0*QUAD(:,J) - 0.5d0
            QUAD_RIGHT = 0.5d0*QUAD(:,J) + 0.5d0

            !Interpolate to half intervals for non-conforming mesh interfaces
            CALL INTERP_BASIS(INTERP_LEFT(:,:,I,J),J,QUAD_LEFT,I,QUAD(:,I),BAR_QUAD(:,I))
            CALL INTERP_BASIS(INTERP_RIGHT(:,:,I,J),J,QUAD_RIGHT,I,QUAD(:,I),BAR_QUAD(:,I))
        ENDDO
    ENDDO  
    
    END SUBROUTINE INIT_BASIS  

!----------------------------------------------------------------------
    
    SUBROUTINE GL(N,QUAD,W)
    !!----------------------------------------------------------------------
    !!
    !!       Compute Gauss Legendre nodes and weights. 
    !!       Algorithm taken from algorithm 23 in Kopriva 
    !!----------------------------------------------------------------------
    USE PARAM, ONLY: PI
    IMPLICIT NONE

    INTEGER N
    INTEGER J
    DOUBLE PRECISION,DIMENSION(0:N)::QUAD,W
    DOUBLE PRECISION DELTA,Q,DQ
    DOUBLE PRECISION TOL
            
    TOL = 4.0D0*EPSILON(1.0D0)
    Q = 0.0D0
    DQ = 0.0d0

    IF(N==0)THEN
        QUAD(0)=0.0D0
        W(0)=2.0D0
    ELSEIF (N==1) THEN
        QUAD(0)=-DSQRT(1.0D0/3.0D0)
        W(0)=1.0D0
        QUAD(1)= DSQRT(1.0d0/3.0d0)
        W(1)=1.0D0  
    ELSE
        DO J=0,((N+1)/2) -1
            !Initial guess
            QUAD(J)=-DCOS(PI*DBLE(2*J+1)/DBLE(2*N+2))
              
            !Iterative method
            DELTA=1.0D30
            DO WHILE(DABS(DELTA).GE.TOL*DABS(QUAD(J)))
                CALL LEGENDRE_POLYNOMIAL_AND_DERIVATIVE(N+1,QUAD(J),Q,DQ)
                DELTA=-Q/DQ
                QUAD(J)=QUAD(J)+DELTA
            END DO
              
            CALL LEGENDRE_POLYNOMIAL_AND_DERIVATIVE(N+1,QUAD(J),Q,DQ)
            QUAD(N-J)=-QUAD(J)
            W(J)=2.0D0/((1.0d0 - QUAD(J)**2)*(DQ**2))
            W(N-J) = W(J)
        END DO
    END IF

    IF(MOD(N,2)==0)THEN
        CALL LEGENDRE_POLYNOMIAL_AND_DERIVATIVE(N+1,0.0d0,Q,DQ)
        QUAD(N/2)=0.0D0
        W(N/2)=2.0D0/(DQ**2)
    END IF

    END SUBROUTINE GL

!----------------------------------------------------------------------

    SUBROUTINE LEGENDRE_POLYNOMIAL_AND_DERIVATIVE(N,X,Q,DQ)
    !!--------------------------------------------------------------------
    !!       Algorithm 22 Kopriva - compute L_n (Q) and L_n' (DQ)
    !!--------------------------------------------------------------------
    IMPLICIT NONE

    INTEGER N,K
    DOUBLE PRECISION X
    DOUBLE PRECISION Q,DQ

    DOUBLE PRECISION Q_M1, Q_M2
    DOUBLE PRECISION DQ_M1, DQ_M2

    IF (N==0) THEN
        Q = 1.0D0
        DQ = 0.0D0
    ELSEIF (N==1) THEN
        Q = X
        DQ = 1.0D0
    ELSE
        Q_M2 = 1.0D0
        Q_M1 = X
        DQ_M2 = 0.0D0
        DQ_M1 = 1.0D0

        DO K = 2,N
            Q = DBLE(2*K-1)*X*Q_M1/DBLE(K) - DBLE(K-1)*Q_M2/DBLE(K)
            DQ = DQ_M2 + DBLE(2*K-1)*Q_M1
            Q_M2 = Q_M1
            Q_M1 = Q
            DQ_M2 = DQ_M1
            DQ_M1 = DQ
        END DO
    END IF

    END SUBROUTINE LEGENDRE_POLYNOMIAL_AND_DERIVATIVE

!----------------------------------------------------------------------

    SUBROUTINE GLL(N,QUAD,W)
    !!----------------------------------------------------------------------
    !!
    !!       Compute Gauss Legendre Lobatto nodes and weights. 
    !!       Algorithm taken from algorithm 25 in Kopriva 
    !!----------------------------------------------------------------------
    USE PARAM, ONLY: PI
    IMPLICIT NONE

    INTEGER N
    INTEGER J
    DOUBLE PRECISION,DIMENSION(0:N):: QUAD,W
    DOUBLE PRECISION DELTA,VAL,DVAL,Q,DQ
    DOUBLE PRECISION FACTOR
    DOUBLE PRECISION,PARAMETER:: TOL=1.0D-12
    INTEGER ITER

    IF(N==1)THEN
        QUAD(0)=-1.0D0;W(0)=1.0D0
        QUAD(1)= 1.0D0;W(1)=W(0)
    ELSE
        QUAD(0)=-1.0D0;W(0)=2.0D0/(DBLE(N*(N+1)))
        QUAD(N)= 1.0D0;W(N)=W(0)
        DO J=1,(N+1)/2-1
            !Initial guess
            FACTOR=PI*(DBLE(J)+0.25D0)
            QUAD(J)=-DCOS(FACTOR/DBLE(N)-3.0D0/8.0D0/DBLE(N)/FACTOR)
      
            !Iterative method
            DELTA=1.0D30
            ITER=0
            DO WHILE(DABS(DELTA).GE.TOL*DABS(QUAD(J)))
                ITER=ITER+1
                CALL AL024(N,QUAD(J),VAL,DVAL,Q,DQ)
                DELTA=-Q/DQ
                QUAD(J)=QUAD(J)+DELTA
            ENDDO

            CALL AL024(N,QUAD(J),VAL,DVAL,Q,DQ)
            QUAD(N-J)=-QUAD(J)
            W(J)=2.0D0/(DBLE(N*(N+1))*VAL**2)
            W(N-J)=W(J)
        ENDDO
    ENDIF

    IF(MOD(N,2)==0)THEN
        CALL AL024(N,0.0D0,VAL,DVAL,Q,DQ)
        QUAD(N/2)=0.0D0
        W(N/2)=2.0D0/(DBLE(N*(N+1))*VAL**2)
    ENDIF

    END SUBROUTINE GLL

!----------------------------------------------------------------------

    SUBROUTINE AL024(N,X,POL,DPOL,Q,DQ)
    !!--------------------------------------------------------------------
    !!       Algorithm 24 Kopriva - combined algorithm to compute L_n (POL) 
    !!       L'_n (DPOL), Q = L_n+1 - L_n-1, and Q'
    !!
    !!--------------------------------------------------------------------
    IMPLICIT NONE

    INTEGER N,K
    DOUBLE PRECISION X
    DOUBLE PRECISION,DIMENSION(0:N+1)::VAL,DVAL
    DOUBLE PRECISION POL,DPOL,Q,DQ

    K=2
    VAL(K-2)=1.0D0
    VAL(K-1)=X
    DVAL(K-2)=0.0D0
    DVAL(K-1)=1.0D0

    DO K=2,N+1
        VAL(K) = DBLE(2*K-1)/DBLE(K)*X*VAL(K-1)-DBLE(K-1)/DBLE(K)*VAL(K-2)
        DVAL(K)= DVAL(K-2)+DBLE(2*K-1)*VAL(K-1)
    ENDDO
    
    POL=VAL(N)
    DPOL=DVAL(N)

    Q=VAL(N+1)-VAL(N-1)
    DQ=DVAL(N+1)-DVAL(N-1)

    END SUBROUTINE AL024
    
!----------------------------------------------------------------------
    
    SUBROUTINE INTERP_BASIS(INTERP,M,X,N,QUAD,BAR_QUAD)
    !!----------------------------------------------------------------------
    !!
    !!       Interpolate the Lagrange polynomails interpolating the order I 
    !!       Gauss Legendre nodes at the points in X 
    !!       Algorithm taken from algorithm 32 in Kopriva 
    !!----------------------------------------------------------------------
    USE SIZE,ONLY: NMMAX
    IMPLICIT NONE

    INTEGER N,M
    DOUBLE PRECISION INTERP(0:NMMAX,0:M) !! INTERP(J,K) = L_J(X_K)
    DOUBLE PRECISION X(0:M)
    DOUBLE PRECISION QUAD(0:N), BAR_QUAD(0:N)

    INTEGER K,J
    LOGICAL MATCH, FLAG
    DOUBLE PRECISION SUM

    DO K = 0,M 
        MATCH = .FALSE.
        DO J = 0,N
            INTERP(J,K) = 0.0d0
            CALL ALMOSTEQUAL(FLAG,X(K),QUAD(J))
            IF (FLAG.EQV..TRUE.) THEN
                MATCH = .TRUE.
                INTERP(J,K)  = 1.0D0
            ENDIF
        ENDDO

        IF (.NOT.MATCH) THEN
            SUM = 0.0D0
            DO J = 0,N
                INTERP(J,K)  = BAR_QUAD(J)/(X(K)-QUAD(J))
                SUM = SUM + INTERP(J,K) 
            ENDDO

            DO J = 0,N
                INTERP(J,K)  = INTERP(J,K) /SUM
            ENDDO
        ENDIF
    ENDDO

    END SUBROUTINE

!----------------------------------------------------------------------

    SUBROUTINE ALMOSTEQUAL(FLAG,A,B)
    IMPLICIT NONE

    LOGICAL FLAG
    DOUBLE PRECISION A,B

    IF ((A.EQ.0.0d0).OR.(B.EQ.0.0d0)) THEN
        IF (DABS(A-B).LE.2.0d0*EPSILON(A)) THEN
            FLAG = .TRUE.
        ELSE
            FLAG = .FALSE.
        ENDIF
    ELSE
        IF ((DABS(A-B).LE.EPSILON(A)*DABS(A)).AND.(DABS(A-B).LE.EPSILON(A)*DABS(B))) THEN
            FLAG = .TRUE.
        ELSE
            FLAG = .FALSE.
        ENDIF
    ENDIF

    END SUBROUTINE ALMOSTEQUAL

!----------------------------------------------------------------------

    SUBROUTINE BARW(N,BARY,QUAD)
    !--------------------------------------------------------------------
    !       Barycentric weights
    !-------------------------------------------------------------------
    IMPLICIT NONE

    INTEGER N,I,J,K
    DOUBLE PRECISION,DIMENSION(0:N)::QUAD,BARY

    DO I=0,N
        BARY(I)=1.0D0
    ENDDO

    DO J=1,N
        DO K=0,J-1
            BARY(K)=BARY(K)*(QUAD(K)-QUAD(J))
            BARY(J)=BARY(J)*(QUAD(J)-QUAD(K))
        ENDDO
    ENDDO
    
    DO J=0,N
        BARY(J)=1.0D0/BARY(J)
    ENDDO
    
    END SUBROUTINE BARW

!--------------------------------------------------------------------

    SUBROUTINE MATDER(QUAD,W,N,DER,M)
    !----------------------------------------------------------------------
    !       M-th order derivative matrix
    !       Algorithm 37-38 in Kopriva
    !--------------------------------------------------------------------        
    IMPLICIT NONE

    INTEGER I,J,K
    INTEGER M
    INTEGER N
    DOUBLE PRECISION,DIMENSION(0:N)::QUAD,W
    DOUBLE PRECISION,DIMENSION(0:N,0:N)::DER,AUX

    DER=0.0D0
    
    DO I=0,N
        DER(I,I)=0.0D0
        DO J=0,N
            IF(J/=I)THEN
                DER(I,J)=W(J)/W(I)/(QUAD(I)-QUAD(J))
                DER(I,I)=DER(I,I)-DER(I,J)
            ENDIF
        ENDDO
    ENDDO
    
    IF(M==1)RETURN
    
    AUX=DER
    DO K=2,M
        DO I=0,N
            DER(I,I)=0.0D0
            DO J=0,N
                IF(J/=I)THEN
                    DER(I,J)=DBLE(K)/(QUAD(I)-QUAD(J))*(W(J)/W(I)*AUX(I,I)-AUX(I,J))
                    DER(I,I)=DER(I,I)-DER(I,J)
                ENDIF
            ENDDO
        ENDDO
    ENDDO

    END SUBROUTINE MATDER

!--------------------------------------------------------------------


END MODULE BASIS