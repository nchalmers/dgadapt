
    SUBROUTINE ERROR_ESTIM_MAVRIPLIS(U,NUM_EQN)
    USE SIZE,  ONLY: NEL,NMMAX, NG, MG, NPMAX, NELMAX
    USE BASIS, ONLY: LEGENDRE_P, W
    USE ADAPT, ONLY: ERROR_ESTIM_X, ERROR_ESTIM_Y, SIGMA_X, SIGMA_Y, &
                    & MAX_ERROR, MIN_ERROR
    USE MESH, ONLY: SCAL
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER, INTENT(IN):: NUM_EQN
    DOUBLE PRECISION, INTENT(IN):: U(0:NPMAX,NELMAX,NUM_EQN)

    INTEGER IERROR
     
    INTEGER I,J, K, II,JJ,KK, N, M
    DOUBLE PRECISION U_HAT(0:NPMAX)

    DOUBLE PRECISION ERR, SIGMA, C, R
    DOUBLE PRECISION ERR_XT(NUM_EQN), SIGMA_XT(NUM_EQN)
    DOUBLE PRECISION ERR_YT(NUM_EQN), SIGMA_YT(NUM_EQN)
    DOUBLE PRECISION MIN_ERROR_X, MAX_ERROR_X
    DOUBLE PRECISION MIN_ERROR_Y, MAX_ERROR_Y

    INTEGER, PARAMETER::NP=4 !Number of points to use in regression
    DOUBLE PRECISION, PARAMETER::TOL=1.0D-14

    !$OMP DO PRIVATE(K,N,M,I,J,II,JJ,KK,U_HAT,ERR,SIGMA,ERR_XT,SIGMA_XT,ERR_YT,SIGMA_YT) SCHEDULE(DYNAMIC) 
    DO K=1,NEL
        N = NG(K)
        M = MG(K)

        SIGMA_XT = 0.0D0
        SIGMA_YT = 0.0d0
        ERR_XT = 0.0d0
        ERR_YT = 0.0d0
        DO KK = 1,NUM_EQN
            !Convert to modal form
            U_HAT = 0.0d0
            DO J=0,M     
                DO I = 0,N
                    DO JJ = 0,M
                        DO II=0,N
                            U_HAT(I+(N+1)*J) = U_HAT(I+(N+1)*J) + DBLE(2*I+1)*DBLE(2*J+1)*W(II,N)*W(JJ,M)&
                                                    *U(II+(N+1)*JJ,K,KK)*LEGENDRE_P(I,II,N)*LEGENDRE_P(J,JJ,M)/4.0d0
                        ENDDO
                    ENDDO
                ENDDO
            ENDDO

            !X direction
            DO J = 0,M
                IF (MINVAL(DABS(U_HAT(N-NP+1+(N+1)*J:N+(N+1)*J))).LT.TOL) THEN !Modes are too small to attempt extrapolation
                    ERR =  MINVAL(U_HAT(N-NP+1+(N+1)*J:N+(N+1)*J)**2)!Use a simple mode estimator
                    !ERR = U_HAT(1+(N+1)*J)**2  !Use a gradient estimator
                    SIGMA = 0.0d0 ! mark as not smooth
                ELSE
                    CALL FIT(U_HAT((N+1)*J:N+(N+1)*J),N,NP,SIGMA,C,R)

                    IF((R.LT.0.8D0).OR.(SIGMA.LT.0.0d0))THEN !can't trust extrapolation
                        !ERR = U_HAT(1+(N+1)*J)**2  !Use a gradient estimator
                        ERR = U_HAT(N+(N+1)*J)**2  !Use highest mode
                        SIGMA = 0.0d0
                    ELSE
                        ERR = ((C**2)/(2.0D0*SIGMA))*DEXP(-2.D0*SIGMA*DBLE(N+1))*2.0D0/DBLE(2*N+1)
                    END IF
                ENDIF

                !Add quadrature error
                ERR = ERR + 2.0d0*(U_HAT(N+(N+1)*J)**2)/DBLE(2*N+1)

                !ERR_T(J) = ERR                    
                !SIGMA_T(J) = SIGMA
                ERR_XT(KK)   = ERR_XT(KK) + ERR
                SIGMA_XT(KK) = SIGMA_XT(KK) + SIGMA
            ENDDO
            !SIGMA_XT(KK) = MINVAL(SIGMA_T(0:M))
            ERR_XT(KK) = ERR_XT(KK)/DBLE(M+1)
            SIGMA_XT(KK) = SIGMA_XT(KK)/DBLE(M+1)
            !ERR_XT(KK)   = MAXVAL(ERR_T(0:M))
        

            !Error estimation in y direction
            DO I=0,N
                IF (MINVAL(DABS(U_HAT(I+(N+1)*(M-NP+1):I+(N+1)*M:(N+1)))).LT.TOL) THEN !Modes are too small to attempt extrapolation
                    ERR =  MINVAL(U_HAT(I+(N+1)*(M-NP+1):I+(N+1)*M:(N+1))**2)!Use a simple mode estimator
                    !ERR = U_HAT(I+(N+1))**2  !Use a gradient estimator
                    SIGMA = 0.0d0 ! mark as not smooth
                ELSE
                    CALL FIT(U_HAT(I:I+(N+1)*M:(N+1)),M,NP,SIGMA,C,R)

                    IF((R.LT.0.8D0).OR.(SIGMA.LT.0.0d0))THEN !can't trust extrapolation
                        ERR = U_HAT(I+(N+1)*M)**2  !Use highest mode
                        !ERR = U_HAT(I+(N+1))**2  !Use a gradient estimator
                        SIGMA = 0.0d0
                    ELSE
                        ERR = (C**2/(2.0D0*SIGMA))*DEXP(-2.D0*SIGMA*DBLE(M+1))*2.0D0/DBLE(2*M+1)
                    END IF
                ENDIF

                !Add quadrature error
                ERR = ERR + 2.0d0*(U_HAT(I+(N+1)*M)**2)/DBLE(2*M+1)

                !ERR_T(I) = ERR                    
                !SIGMA_T(I) = SIGMA
                ERR_YT(KK) = ERR_YT(KK) + ERR
                SIGMA_YT(KK) = SIGMA_YT(KK) + SIGMA
            ENDDO
            !SIGMA_YT(KK) = MINVAL(SIGMA_T(0:N))
            !ERR_YT(KK)   = MAXVAL(ERR_T(0:N))
            ERR_YT(KK) = ERR_YT(KK)/DBLE(N+1)
            SIGMA_YT(KK) = SIGMA_YT(KK)/DBLE(N+1)
        ENDDO

        !Norm of error over all equations
        ERROR_ESTIM_X(K) = DSQRT(MAXVAL(ERR_XT))*SCAL(2,K)
        ERROR_ESTIM_Y(K) = DSQRT(MAXVAL(ERR_YT))*SCAL(1,K)
        SIGMA_X(K) = MINVAL(SIGMA_XT) !Take minimum sigma over all equations
        SIGMA_Y(K) = MINVAL(SIGMA_YT) !Take minimum sigma over all equations
    ENDDO
    !$OMP ENDDO
    !$OMP BARRIER

    !$OMP MASTER
    MIN_ERROR_X = MINVAL(ERROR_ESTIM_X(1:NEL))
    MIN_ERROR_Y = MINVAL(ERROR_ESTIM_Y(1:NEL))
    MAX_ERROR_X = MAXVAL(ERROR_ESTIM_X(1:NEL))
    MAX_ERROR_Y = MAXVAL(ERROR_ESTIM_Y(1:NEL))
    MIN_ERROR = MIN(MIN_ERROR_X,MIN_ERROR_Y)
    MAX_ERROR = MAX(MAX_ERROR_X,MAX_ERROR_Y)
    CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
    CALL MPI_ALLREDUCE (MIN_ERROR,MIN_ERROR,1,MPI_DOUBLE_PRECISION,&
                        & MPI_MIN,MPI_COMM_WORLD,IERROR)
    CALL MPI_ALLREDUCE (MAX_ERROR,MAX_ERROR,1,MPI_DOUBLE_PRECISION,&
                        & MPI_MAX,MPI_COMM_WORLD,IERROR)
    !$OMP END MASTER
    !$OMP BARRIER
    END SUBROUTINE ERROR_ESTIM_MAVRIPLIS
    

    SUBROUTINE ERROR_ESTIM()
    USE SIZE,  ONLY: NEL,NMMAX, NUM_EQN, NG, MG
    USE BASIS, ONLY: LEGENDRE_P, W
    USE ADAPT, ONLY: ERROR_ESTIM_X, ERROR_ESTIM_Y, SIGMA_X, SIGMA_Y, &
                    & MAX_ERROR, MIN_ERROR
    USE MESH, ONLY: SCAL
    USE FIELDS, ONLY: U
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IERROR
     
    INTEGER I,J, K, II,JJ,KK, N, M
    DOUBLE PRECISION U_HAT(0:NMMAX)

    DOUBLE PRECISION ERR, SIGMA, C, R
    DOUBLE PRECISION SIGMA_T(0:NMMAX), ERR_T(0:NMMAX)
    DOUBLE PRECISION ERR_TT(NUM_EQN), SIGMA_TT(NUM_EQN)
    DOUBLE PRECISION MIN_ERROR_X, MAX_ERROR_X
    DOUBLE PRECISION MIN_ERROR_Y, MAX_ERROR_Y

    INTEGER, PARAMETER::NP=6 !Number of points to use in regression
    DOUBLE PRECISION, PARAMETER::TOL=1.0D-14


    !$OMP DO PRIVATE(K,N,M,I,J,II,JJ,KK,ERR_T,SIGMA_T,U_HAT,ERR,SIGMA,ERR_TT,SIGMA_TT) SCHEDULE(DYNAMIC) 
    DO K=1,NEL
        N = NG(K)
        M = MG(K)

        !Error estimation in x direction
        DO KK = 1,NUM_EQN
            DO J=0,M
                !Convert to modal form at y_j
                U_HAT(0:N) = 0.0D0
                DO I = 0,N
                    DO II=0,N
                        U_HAT(I) = U_HAT(I) + DBLE(2*I+1)*W(II,N)*U(KK,II+(N+1)*J,K)*LEGENDRE_P(I,II,N)/2.0d0
                    ENDDO
                ENDDO

                IF (MINVAL(DABS(U_HAT(N-NP:N))).LT.TOL) THEN !Modes are too small to attempt extrapolation
                    !ERR =  MAXVAL(U_HAT(N-NP:N)**2)!Use a simple mode estimator
                    ERR = U_HAT(1)**2  !Use a gradient estimator
                    SIGMA = 0.0d0 ! mark as not smooth
                ELSE
                    CALL FIT(U_HAT,N,NP,SIGMA,C,R)

                    IF((R.LT.0.8D0).OR.(SIGMA.LT.0.0d0))THEN !can't trust extrapolation
                        ERR = U_HAT(1)**2  !Use a gradient estimator
                        SIGMA = 0.0d0
                    ELSE
                        ERR = (C**2/(2.0D0*SIGMA))*DEXP(-2.D0*SIGMA*DBLE(N+1))*2.0D0/DBLE(2*N+1)
                    END IF
                ENDIF

                !Add quadrature error
                ERR = ERR + 2.0d0*(U_HAT(N)**2)/DBLE(2*N+1)

                ERR_T(J) = ERR                    
                SIGMA_T(J) = SIGMA
            ENDDO
            SIGMA_TT(KK) = MINVAL(SIGMA_T(0:M))
            ERR_TT(KK)   = MAXVAL(ERR_T(0:M))
        ENDDO

        !Norm of error over all equations
        ERROR_ESTIM_X(K) = DSQRT(MAXVAL(ERR_TT)*SCAL(1,K))
        SIGMA_X(K) = MINVAL(SIGMA_TT) !Take minimum sigma over y_j and all equations

        !Error estimation in y direction
        DO KK = 1,NUM_EQN
            DO I=0,N
                !Convert to modal form at y_j
                U_HAT(0:M) = 0.0D0
                DO J = 0,M
                    DO JJ=0,M
                        U_HAT(J) = U_HAT(J) + DBLE(2*J+1)*W(JJ,M)*U(KK,I+(N+1)*JJ,K)*LEGENDRE_P(J,JJ,M)/2.0d0
                    ENDDO
                ENDDO

                IF (MINVAL(DABS(U_HAT(M-NP:M))).LT.TOL) THEN !Modes are too small to attempt extrapolation
                    !ERR =  MAXVAL(U_HAT(M-NP:M)**2)!Use a simple mode estimator
                    ERR = U_HAT(1)**2  !Use a gradient estimator
                    SIGMA = 0.0d0 ! mark as not smooth
                ELSE
                    CALL FIT(U_HAT,M,NP,SIGMA,C,R)

                    IF((R.LT.0.8D0).OR.(SIGMA.LT.0.0d0))THEN !can't trust extrapolation
                        ERR = U_HAT(1)**2  !Use a gradient estimator
                        SIGMA = 0.0d0
                    ELSE
                        ERR = (C**2/(2.0D0*SIGMA))*DEXP(-2.D0*SIGMA*DBLE(M+1))*2.0D0/DBLE(2*M+1)
                    END IF
                ENDIF

                !Add quadrature error
                ERR = ERR + 2.0d0*(U_HAT(M)**2)/DBLE(2*M+1)

                ERR_T(I) = ERR                    
                SIGMA_T(I) = SIGMA
            ENDDO
            SIGMA_TT(KK) = MINVAL(SIGMA_T(0:N))
            ERR_TT(KK)   = MAXVAL(ERR_T(0:N))
        ENDDO

         !Norm of error over all equations
        ERROR_ESTIM_Y(K) = DSQRT(MAXVAL(ERR_TT)*SCAL(2,K))
        SIGMA_Y(K) = MINVAL(SIGMA_TT) !Take minimum sigma over y_j and all equations
    ENDDO
    !$OMP ENDDO
    !$OMP BARRIER

    !$OMP MASTER
    MIN_ERROR_X = MINVAL(ERROR_ESTIM_X(1:NEL))
    MIN_ERROR_Y = MINVAL(ERROR_ESTIM_Y(1:NEL))
    MAX_ERROR_X = MAXVAL(ERROR_ESTIM_X(1:NEL))
    MAX_ERROR_Y = MAXVAL(ERROR_ESTIM_Y(1:NEL))
    MIN_ERROR = MIN(MIN_ERROR_X,MIN_ERROR_Y)
    MAX_ERROR = MAX(MAX_ERROR_X,MAX_ERROR_Y)
    CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
    CALL MPI_ALLREDUCE (MIN_ERROR,MIN_ERROR,1,MPI_DOUBLE_PRECISION,&
                        & MPI_MIN,MPI_COMM_WORLD,IERROR)
    CALL MPI_ALLREDUCE (MAX_ERROR,MAX_ERROR,1,MPI_DOUBLE_PRECISION,&
                        & MPI_MAX,MPI_COMM_WORLD,IERROR)
    !$OMP END MASTER
    !$OMP BARRIER
    END SUBROUTINE ERROR_ESTIM

!
!-----------------------------------------------------------------------
!------------    REGRESSION (BEST FIT) LINE ----------------------------
!!               (Linear least squares)
! THE BEST FIT LINE ASSOCIATED WITH THE N POINTS 
! (X1, Y1), (X2, Y2), . . . , (XN, YN) HAS THE FORM
!
!    Y = MX + B 
!
! WHERE
!
!    SLOPE = M  = [N(XY) - (X)(Y)]/[N(X2) - (X)2]
!
!    INTERCEPT = B = [(Y) - M(X)]/N
!
! WHERE
!
!    (XY) = SUM OF PRODUCTS = X1Y1 + X2Y2 + . . . + XNYN
!    (X) = SUM OF X-VALUES = X1 + X2 + . . . + XN
!    (Y) = SUM OF Y-VALUES = Y1 + Y2 + . . . + YN
!    (X2) = SUM OF SQUARES OF X-VALUES = X1^2 + X2^2+ . . . + XN^2 
!-----------------------------------------------------------------------      

    SUBROUTINE FIT(U,N,NP,SIGMA,C,R)
    IMPLICIT NONE

    INTEGER N, NP
    DOUBLE PRECISION U(0:N)
    DOUBLE PRECISION SIGMA, C, R

    INTEGER I
    DOUBLE PRECISION X(NP), Y(NP)
    DOUBLE PRECISION SUM1,SUM2,SUM3,SUM4
    DOUBLE PRECISION SQUARE_ERR,SQUARE_TOT,Y_REG,LOG_C,Y_AVE

    SUM1=0.0D0
    SUM2=0.0D0
    SUM3=0.0D0
    SUM4=0.0D0
    SQUARE_ERR=0.0D0
    SQUARE_TOT=0.0D0

    ! Y = C*EXP(-SIGMA*X)
    ! => 
    ! LOG Y = LOG C - SIGMA*X

    DO I = 1,NP
        X(I) = DBLE(N-NP+I)
        Y(I) = DLOG(DABS(U(N-NP+I)))
    ENDDO

    DO I=1,NP
        SUM1=SUM1+Y(I)
        SUM2=SUM2+X(I)
        SUM3=SUM3+X(I)*X(I)
        SUM4=SUM4+Y(I)*X(I)
    ENDDO

    Y_AVE = SUM1/DBLE(NP)
    SIGMA = (SUM1*SUM2-SUM4*DBLE(NP))/(SUM3*DBLE(NP)-SUM2*SUM2)
    LOG_C = (SUM1+SIGMA*SUM2)/DBLE(NP)
    C = DEXP(LOG_C)

    DO I=1,NP
        Y_REG = LOG_C-SIGMA*X(I)
        SQUARE_ERR = SQUARE_ERR + (Y_REG-Y(I))**2
        SQUARE_TOT = SQUARE_TOT + (Y(I)-Y_AVE)**2
    END DO

    R=1.0D0-SQUARE_ERR/SQUARE_TOT

    END SUBROUTINE FIT

!--------------------------------------------------------------------------

    SUBROUTINE ERROR_ESTIM_GRAD()
    USE SIZE,  ONLY: NEL,NMMAX, NUM_EQN, NG, MG
    USE BASIS, ONLY: LEGENDRE_P, W
    USE ADAPT, ONLY: ERROR_ESTIM_X, ERROR_ESTIM_Y, SIGMA_X, SIGMA_Y, &
                    & MIN_ERROR, MAX_ERROR
    USE MESH, ONLY: SCAL
    USE FIELDS, ONLY: U
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IERROR
     
    INTEGER I,J, K, II,JJ,KK, N, M
    DOUBLE PRECISION U_HAT(0:NMMAX)

    DOUBLE PRECISION ERR
    DOUBLE PRECISION ERR_T
    DOUBLE PRECISION ERR_TT(NUM_EQN), SIGMA_TT(NUM_EQN)
    DOUBLE PRECISION MIN_ERROR_X, MAX_ERROR_X
    DOUBLE PRECISION MIN_ERROR_Y, MAX_ERROR_Y

    INTEGER, PARAMETER::NP=6 !Number of points to use in regression
    DOUBLE PRECISION, PARAMETER::TOL=1.0D-14


    !$OMP DO PRIVATE(K,N,M,I,J,II,JJ,KK,ERR_T,U_HAT,ERR,ERR_TT,SIGMA_TT) SCHEDULE(DYNAMIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K)

        !Error estimation in x direction
        ERR_TT = 0.0D0
        SIGMA_TT = 0.0D0
        DO KK = 1,NUM_EQN
            DO J=0,M
                !Convert to modal form at y_j
                U_HAT(0:N) = 0.0D0
                DO I = 0,N
                    DO II=0,N
                        U_HAT(I) = U_HAT(I) + DBLE(2*I+1)*W(II,N)*U(KK,II+(N+1)*J,K)*LEGENDRE_P(I,II,N)/2.0d0
                    ENDDO
                ENDDO
            
                !Gradient error estimator
                ERR = abs(U_HAT(1))

                ERR_TT(KK) = MAX(ERR_TT(KK),ERR) 
            ENDDO
        ENDDO

        !Norm of error over all equations
        ERR_T = 0.0d0
        DO KK = 1,NUM_EQN
            ERR_T = ERR_T + ERR_TT(KK)**2
        ENDDO
        ERROR_ESTIM_X(K) = DSQRT(ERR_T*SCAL(1,K))
        SIGMA_X(K) = MINVAL(SIGMA_TT(1:NUM_EQN))/DBLE(M+1) !Take minimum average sigma over y_j and all equations

        !Error estimation in y direction
        ERR_TT = 0.0d0
        SIGMA_TT = 0.0d0
        DO KK = 1,NUM_EQN
            DO I=0,N
                !Convert to modal form at y_j
                U_HAT(0:M) = 0.0D0
                DO J = 0,M
                    DO JJ=0,M
                        U_HAT(J) = U_HAT(J) + DBLE(2*J+1)*W(JJ,M)*U(KK,I+(N+1)*JJ,K)*LEGENDRE_P(J,JJ,M)/2.0d0
                    ENDDO
                ENDDO

                !Gradient error estimator
                ERR = abs(U_HAT(1))

                ERR_TT(KK) = MAX(ERR_TT(KK),ERR) 
            ENDDO
        ENDDO

        !Average error estimators and smoothness indicators over all x values
        !ERROR_ESTIM_Y(K) = DSQRT(ERR_T/DBLE(N+1))
        !SIGMA_Y(K) = SIGMA_T/DBLE(N+1)

        !Norm of error over all equations
        ERR_T = 0.0d0
        DO KK = 1,NUM_EQN
            ERR_T = ERR_T + ERR_TT(KK)**2
        ENDDO
        ERROR_ESTIM_Y(K) = DSQRT(ERR_T*SCAL(2,K))
        SIGMA_Y(K) = MINVAL(SIGMA_TT(1:NUM_EQN))/DBLE(N+1) !Take minimum average sigma over x_j and all equations
    ENDDO
    !$OMP ENDDO
    !$OMP BARRIER

    !$OMP MASTER
    MIN_ERROR_X = MINVAL(ERROR_ESTIM_X(1:NEL))
    MIN_ERROR_Y = MINVAL(ERROR_ESTIM_Y(1:NEL))
    MAX_ERROR_X = MAXVAL(ERROR_ESTIM_X(1:NEL))
    MAX_ERROR_Y = MAXVAL(ERROR_ESTIM_Y(1:NEL))
    MIN_ERROR = MIN(MIN_ERROR_X,MIN_ERROR_Y)
    MAX_ERROR = MAX(MAX_ERROR_X,MAX_ERROR_Y)
    CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
    CALL MPI_ALLREDUCE (MIN_ERROR,MIN_ERROR,1,MPI_DOUBLE_PRECISION,&
                        & MPI_MIN,MPI_COMM_WORLD,IERROR)
    CALL MPI_ALLREDUCE (MAX_ERROR,MAX_ERROR,1,MPI_DOUBLE_PRECISION,&
                        & MPI_MAX,MPI_COMM_WORLD,IERROR)
    !$OMP END MASTER
    !$OMP BARRIER
    END