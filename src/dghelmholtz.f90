    
    SUBROUTINE DGHELMHOLTZ(AU,U,SIGMA,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)
    USE SIZE, ONLY: NEL
    USE BASIS, ONLY: W
    USE FIELDS, ONLY: UQ_HOMOGENEOUS_BOUNDARY
    IMPLICIT NONE

    INTEGER,INTENT(IN):: NPMAX,NG(NEL),MG(NEL)
    DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: SIGMA

    DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    INTEGER I,J,K,N,M

    CALL DGPOISSON(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,.FALSE.,UQ_HOMOGENEOUS_BOUNDARY)

    !$OMP DO PRIVATE(N,M,I,J,K)&
    !$OMP&     SCHEDULE(DYNAMIC)
    DO K =1,NEL
        N = NG(K)
        M = MG(K)
        DO I = 0,N
            DO J = 0,M
                AU(I+(N+1)*J,K) = AU(I+(N+1)*J,K) + &
                            & SIGMA*U(I+(N+1)*J,K)*W(I,N)*W(J,M)*JAC(I+(N+1)*J,K)
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE DGHELMHOLTZ