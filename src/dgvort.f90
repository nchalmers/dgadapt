
    SUBROUTINE DGVORT(VORT,U,T)
    USE SIZE, ONLY: NG,MG,NEL,NPMAX,NMMAX,NELMAX
    USE MESH, ONLY: DXDR,DXDS,DYDR,DYDS, NX,NY,JAC
    USE BASIS, ONLY: W,DER, INTERP_ENDPOINTS
    USE COMMS, ONLY: SHARE_U_BDRY_BUFFERS
    IMPLICIT NONE

    DOUBLE PRECISION,INTENT(OUT):: VORT(0:NPMAX,NELMAX)
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NELMAX,2)
    DOUBLE PRECISION,INTENT(IN):: T

    INTEGER K,I,J,KK, N,M
    DOUBLE PRECISION RHS_SURFACE(2,0:NMMAX)

    !Before we start, we need the boundary information from our neighbours
    CALL SHARE_U_BDRY_BUFFERS(U)

    !$OMP WORKSHARE
    VORT = 0.0d0
    !$OMP END WORKSHARE

    !$OMP DO PRIVATE(N,M,I,J,KK,RHS_SURFACE)&            
    !$OMP&   SCHEDULE(STATIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K)
        
        !Compute Volume integrals
        DO J=0,M
            DO I=0,N
                DO KK = 0,N
                    VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K) &
                                        - U(KK+(N+1)*J,K,1)*DXDS(KK+(N+1)*J,K)*DER(KK+(N+1)*I,N)*W(KK,N)*W(J,M) & 
                                        - U(KK+(N+1)*J,K,2)*DYDS(KK+(N+1)*J,K)*DER(KK+(N+1)*I,N)*W(KK,N)*W(J,M)
                ENDDO
                DO KK = 0,M
                    VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K) &
                                        + U(I+(N+1)*KK,K,1)*DXDR(I+(N+1)*KK,K)*DER(KK+(M+1)*J,M)*W(I,N)*W(KK,M) &
                                        + U(I+(N+1)*KK,K,2)*DYDR(I+(N+1)*KK,K)*DER(KK+(M+1)*J,M)*W(I,N)*W(KK,M)
                ENDDO
            ENDDO
        ENDDO

        !Surface integrals
        CALL VORT_SURFACE_INTEGRAL(RHS_SURFACE,U,K,1,N,T) !Side 1        
        DO J = 0,M
            DO I = 0,N
                VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K) + NX(1,K)*RHS_SURFACE(2,I)*INTERP_ENDPOINTS(J,0,M) &
                                                      - NY(1,K)*RHS_SURFACE(1,I)*INTERP_ENDPOINTS(J,0,M) 
            ENDDO
        ENDDO 

        CALL VORT_SURFACE_INTEGRAL(RHS_SURFACE,U,K,3,N,T) !Side 3        
        DO J = 0,M
            DO I = 0,N
                VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K) + NX(3,K)*RHS_SURFACE(2,N-I)*INTERP_ENDPOINTS(J,1,M) &
                                                      - NY(3,K)*RHS_SURFACE(1,N-I)*INTERP_ENDPOINTS(J,1,M) 
            ENDDO
        ENDDO 

        CALL VORT_SURFACE_INTEGRAL(RHS_SURFACE,U,K,2,M,T) !Side 2       
        DO J = 0,M
            DO I = 0,N
                VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K) + NX(2,K)*RHS_SURFACE(2,J)*INTERP_ENDPOINTS(I,1,N) &
                                                      - NY(2,K)*RHS_SURFACE(1,J)*INTERP_ENDPOINTS(I,1,N) 
            ENDDO
        ENDDO 

        CALL VORT_SURFACE_INTEGRAL(RHS_SURFACE,U,K,4,M,T) !Side 4        
        DO J = 0,M
            DO I = 0,N
                VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K) + NX(4,K)*RHS_SURFACE(2,M-J)*INTERP_ENDPOINTS(I,0,N) &
                                                      - NY(4,K)*RHS_SURFACE(1,M-J)*INTERP_ENDPOINTS(I,0,N) 
            ENDDO
        ENDDO 

        !Scale
        DO J=0,M
            DO I=0,N
                VORT(I+(N+1)*J,K) = VORT(I+(N+1)*J,K)/(JAC(I+(N+1)*J,K)*W(I,N)*W(J,M))
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE DGVORT


!!----------------------------------------------------------------
!!
!!         Evaluate the surface integral at the IND-th point 
!!                along on edge SIDE of element K
!!
!!---------------------------------------------------------------

    SUBROUTINE VORT_SURFACE_INTEGRAL(RHS_SURFACE,U,K,SIDE,NN,T)
    USE SIZE, ONLY:  NPMAX, NELMAX, NG, MG, NMMAX
    USE MESH, ONLY: X, Y,SCAL, NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, &
                    & SIDEMAP, SIDEMAP2, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_TYPE, BDRY_DATA
    USE BASIS, ONLY: W, INTERP, INTERP_LEFT, INTERP_RIGHT, INTERP_ENDPOINTS
    USE COMMS, ONLY: U_BDRY_BUFFER, ORDER_BDRY_BUFFER
    IMPLICIT NONE

    INTEGER, INTENT(IN):: K,SIDE,NN

    INTEGER I, J, IND, IND2, ISIDE, MM, KK, K_R, SIDE_R, D
    INTEGER N_LEFT, M_LEFT, N_RIGHT, M_RIGHT 
    INTEGER DEG_LEFT, DEG_RIGHT, DEG_MAX, DEG_RIGHT2, DEG_MAX2, VL,VR
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NELMAX,2), T
    DOUBLE PRECISION U_L(2,0:NMMAX),U_R(2,0:NMMAX)
    DOUBLE PRECISION U_L2(2,0:NMMAX),U_R2(2,0:NMMAX)
    DOUBLE PRECISION U_TEMP(2,0:NMMAX)
    DOUBLE PRECISION U_EDGE(2,0:NMMAX), U_EDGE2(2,0:NMMAX)
    DOUBLE PRECISION,INTENT(OUT):: RHS_SURFACE(2,0:NN)

    !Get order of element
    N_LEFT = NG(K)
    M_LEFT = MG(K)
    DEG_LEFT = NN

    VL = SIDE
    VR = SIDE+1
    IF (SIDE .EQ. 4) VR = 1

    U_L = 0.0d0
    !These are extra indicies to evaluate U_l. IND2 is what end o the interval [-1,1] this
    ! edge is on, D is a shift, and MM is the order of the element in the sum direction
    ! if   SIDE=1    SIDE=2      SIDE=3     SIDE=4
    !      IND2=0    IND2=1      IND2=1     IND2=0
    !      D=N+1     D=1         D=N+1      D=1
    !      MM=M      MM=N        MM=M       MM=N
    IND2 = SIDEMAP2(SIDE)
    D = 1+SIDEMAP2(SIDE+4)*N_LEFT
    MM = (1-SIDEMAP2(SIDE+4))*N_LEFT + SIDEMAP2(SIDE+4)*M_LEFT
    DO I = 0,NN
        IND = SIDEMAP(SIDE)*I + SIDEMAP(SIDE+4)*I*(N_LEFT+1) + &
                 & SIDEMAP(SIDE+8)*N_LEFT + SIDEMAP(SIDE+12)*M_LEFT*(N_LEFT+1) 
        !Note, this finds the local index of this point based on the side number,
        !i.e. if SIDE=1      SIDE=2          SIDE=3        SIDE=4
        !    then IND=I      IND=I*(N+1)     IND=N-I       IND=(M-I)*(N+1)

        DO KK = 0, MM
            U_L(:,I) = U_L(:,I) + U(IND+D*KK,K,:)*INTERP_ENDPOINTS(KK,IND2,MM)
        ENDDO
    ENDDO

    IF (BDRY_TYPE(SIDE,K).EQ.'GSP'.OR. (BDRY_TYPE(SIDE,K).EQ.'SP ')) THEN !Split edge, two neighbours
        IF (BDRY_TYPE(SIDE,K).EQ.'GSP') THEN     !Interprocess split edge
            IND   = NEIGHBOUR_ID(SIDE,1,K) !Get local index of boundary edge
            ISIDE = NEIGHBOUR_SIDE_ID(SIDE,1,K)
            K_R   = BDRY_BUFFER_NEIGHBOUR_ID(IND,ISIDE,1) !Get index on other side of boundary
            DEG_RIGHT = ORDER_BDRY_BUFFER(K_R,ISIDE)    !Get degree of polynomial

            DO I = 0,DEG_RIGHT
                U_R(:,I) = U_BDRY_BUFFER(:,I,K_R,ISIDE)
            ENDDO

            K_R   = BDRY_BUFFER_NEIGHBOUR_ID(IND,ISIDE,2) !Get index on other side of boundary
            DEG_RIGHT2 = ORDER_BDRY_BUFFER(K_R,ISIDE)    !Get degree of polynomial

            DO I = 0,DEG_RIGHT2
                U_R2(:,I) = U_BDRY_BUFFER(:,I,K_R,ISIDE)
            ENDDO
        ELSE                     !Interiour split edge
            !Get information about neighbour
            SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,1,K)
            K_R    = NEIGHBOUR_ID(SIDE,1,K)
            N_RIGHT = NG(K_R)
            M_RIGHT = MG(K_R)

            !Get degree of polynomial on right
            IF ((SIDE_R.EQ.1).OR.(SIDE_R.EQ.3)) THEN
                DEG_RIGHT = N_RIGHT
            ELSE 
                DEG_RIGHT = M_RIGHT
            ENDIF

            !Get neighbouring values
            U_R = 0.0d0

            IND2 = SIDEMAP2(SIDE_R)
            D = 1+SIDEMAP2(SIDE_R+4)*N_RIGHT
            MM = (1-SIDEMAP2(SIDE_R+4))*N_RIGHT + SIDEMAP2(SIDE_R+4)*M_RIGHT

            DO I =0,DEG_RIGHT
                 IND = SIDEMAP(SIDE_R)*(N_RIGHT-I) + SIDEMAP(SIDE_R+4)*(M_RIGHT-I)*(N_RIGHT+1) + &
                     & SIDEMAP(SIDE_R+8)*N_RIGHT + SIDEMAP(SIDE_R+12)*M_RIGHT*(N_RIGHT+1) 
                !Note, this finds the local index of this point based on the side number,
                ! this time in reverse direction
                !i.e. if SIDE_R=1         SIDE_R=2              SIDE_R=3        SIDE_R=4
                !    then IND=N-I         InD=(M-I)*(N+1)+N      IND=I+(N+1)*M     IND=I*(N+1)    
                DO KK =0,MM
                    U_R(:,I) = U_R(:,I) + U(IND+D*KK,K_R,:)*INTERP_ENDPOINTS(KK,IND2,MM)
                ENDDO
            ENDDO

            SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,2,K)
            K_R    = NEIGHBOUR_ID(SIDE,2,K)
            N_RIGHT = NG(K_R)
            M_RIGHT = MG(K_R)

            !Get degree of polynomial on right
            IF ((SIDE_R.EQ.1).OR.(SIDE_R.EQ.3)) THEN
                DEG_RIGHT2 = N_RIGHT
            ELSE 
                DEG_RIGHT2 = M_RIGHT
            ENDIF

            !Get neighbouring values
            U_R2 = 0.0d0

            IND2 = SIDEMAP2(SIDE_R)
            D = 1+SIDEMAP2(SIDE_R+4)*N_RIGHT
            MM = (1-SIDEMAP2(SIDE_R+4))*N_RIGHT + SIDEMAP2(SIDE_R+4)*M_RIGHT

            DO I =0,DEG_RIGHT2
                 IND = SIDEMAP(SIDE_R)*(N_RIGHT-I) + SIDEMAP(SIDE_R+4)*(M_RIGHT-I)*(N_RIGHT+1) + &
                     & SIDEMAP(SIDE_R+8)*N_RIGHT + SIDEMAP(SIDE_R+12)*M_RIGHT*(N_RIGHT+1) 
                !Note, this finds the local index of this point based on the side number,
                ! this time in reverse direction
                !i.e. if SIDE_R=1         SIDE_R=2              SIDE_R=3    SIDE_R=4
                !    then IND=N-I         InD=(M-I)*(N+1)       IND=I       IND=I*(N+1)    
                DO KK =0,MM
                    U_R2(:,I) = U_R2(:,I) + U(IND+D*KK,K_R,:)*INTERP_ENDPOINTS(KK,IND2,MM)
                ENDDO
            ENDDO
        ENDIF

        !Two integrals must be performed. We need to interpolate U_l to left and right half intervals
        ! and interpolate U_r and U_r2 if necessary

        IF (DEG_LEFT.GT.DEG_RIGHT) THEN
            DEG_MAX = DEG_LEFT

            DO I = 0,DEG_RIGHT
                U_TEMP(:,I) = U_R(:,I)
            ENDDO

            DO I = 0,DEG_MAX
                U_R(:,I) = 0.0d0
                DO J = 0,DEG_RIGHT
                    U_R(:,I) = U_R(:,I) + INTERP(J,I,DEG_RIGHT,DEG_MAX)*U_TEMP(:,J)
                ENDDO
            ENDDO
        ELSE 
            DEG_MAX = DEG_RIGHT    
        ENDIF

        IF (DEG_LEFT.GT.DEG_RIGHT2) THEN
            DEG_MAX2 = DEG_LEFT

            DO I = 0,DEG_RIGHT2
                U_TEMP(:,I) = U_R2(:,I)
            ENDDO

            DO I = 0,DEG_MAX2
                U_R2(:,I) = 0.0d0
                DO J = 0,DEG_RIGHT2
                    U_R2(:,I) = U_R2(:,I) + INTERP(J,I,DEG_RIGHT2,DEG_MAX2)*U_TEMP(:,J)
                ENDDO
            ENDDO
        ELSE 
            DEG_MAX2 = DEG_RIGHT2    
        ENDIF

        !Interpolate the left polynomial to a half intervals
        DO I = 0,DEG_LEFT
            U_TEMP(:,I) = U_L(:,I)
        ENDDO

        DO I = 0,DEG_MAX
            U_L(:,I) = 0.0d0
            DO J = 0,DEG_LEFT
                U_L(:,I) = U_L(:,I)  + INTERP_LEFT(J,I,DEG_LEFT,DEG_MAX)*U_TEMP(:,J)
            ENDDO
        ENDDO

        DO I = 0,DEG_MAX2
            U_L2(:,I) = 0.0d0
            DO J = 0,DEG_LEFT
                U_L2(:,I) = U_L2(:,I) + INTERP_RIGHT(J,I,DEG_LEFT,DEG_MAX2)*U_TEMP(:,J)
            ENDDO
        ENDDO

        DO I = 0,DEG_MAX
            U_EDGE(:,I) = 0.5d0*(U_R(:,I)+U_L(:,I))
        ENDDO
        DO I = 0,DEG_MAX2
            U_EDGE2(:,I) = 0.5d0*(U_R2(:,I)+U_L2(:,I))
        ENDDO

        DO I = 0,DEG_LEFT
            RHS_SURFACE(:,I) = 0.0d0
            DO J = 0,DEG_MAX
                RHS_SURFACE(:,I) = RHS_SURFACE(:,I) + 0.5d0*SCAL(SIDE,K)*U_EDGE(:,J)&
                                    &*W(J,DEG_MAX)*INTERP_LEFT(I,J,DEG_LEFT,DEG_MAX)
            ENDDO
            DO J = 0,DEG_MAX2
                RHS_SURFACE(:,I) = RHS_SURFACE(:,I) + 0.5d0*SCAL(SIDE,K)*U_EDGE2(:,J)&
                                    &*W(J,DEG_MAX2)*INTERP_RIGHT(I,J,DEG_LEFT,DEG_MAX2)
            ENDDO
        ENDDO
    ELSE                  !Single neighbour
        !Check if boundary is physical
        IF (NEIGHBOUR_ID(SIDE,1,K).LT.0) THEN
            !Evaluate right state based on the boundary data
            DEG_RIGHT = DEG_LEFT
            CALL U_BOUNDARY_CONDITION(U_R,U_L,X(VL,K),Y(VL,K),X(VR,K),Y(VR,K),K,SIDE,NN,T) 
        ELSEIF (BDRY_TYPE(SIDE,K).EQ.'GE '.OR. (BDRY_TYPE(SIDE,K).EQ.'GJ ')) THEN !Interprocess boundary
            IND   = NEIGHBOUR_ID(SIDE,1,K) !Get local index of boundary edge
            ISIDE = NEIGHBOUR_SIDE_ID(SIDE,1,K)
            K_R   = BDRY_BUFFER_NEIGHBOUR_ID(IND,ISIDE,1) !Get index on other side of boundary
            DEG_RIGHT = ORDER_BDRY_BUFFER(K_R,ISIDE)    !Get degree of polynomial

            DO I = 0,DEG_RIGHT
                U_R(:,I) = U_BDRY_BUFFER(:,I,K_R,ISIDE)
            ENDDO
        ELSE !Interior edge
            !Get information about neighbour
            SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,1,K)
            K_R    = NEIGHBOUR_ID(SIDE,1,K)
            N_RIGHT = NG(K_R)
            M_RIGHT = MG(K_R)

            !Get degree of polynomial on right
            IF ((SIDE_R.EQ.1).OR.(SIDE_R.EQ.3)) THEN
                DEG_RIGHT = N_RIGHT
            ELSE 
                DEG_RIGHT = M_RIGHT
            ENDIF

            !Get neighbouring values
            U_R = 0.0d0

            IND2 = SIDEMAP2(SIDE_R)
            D = 1+SIDEMAP2(SIDE_R+4)*N_RIGHT
            MM = (1-SIDEMAP2(SIDE_R+4))*N_RIGHT + SIDEMAP2(SIDE_R+4)*M_RIGHT

            DO I =0,DEG_RIGHT
                 IND = SIDEMAP(SIDE_R)*(N_RIGHT-I) + SIDEMAP(SIDE_R+4)*(M_RIGHT-I)*(N_RIGHT+1) + &
                     & SIDEMAP(SIDE_R+8)*N_RIGHT + SIDEMAP(SIDE_R+12)*M_RIGHT*(N_RIGHT+1) 
                !Note, this finds the local index of this point based on the side number,
                ! this time in reverse direction
                !i.e. if SIDE_R=1         SIDE_R=2              SIDE_R=3        SIDE_R=4
                !    then IND=N-I         InD=(M-I)*(N+1)       IND=I           IND=I*(N+1)    
                DO KK =0,MM
                    U_R(:,I) = U_R(:,I) + U(IND+D*KK,K_R,:)*INTERP_ENDPOINTS(KK,IND2,MM)
                ENDDO
            ENDDO
        ENDIF

        IF ((BDRY_TYPE(SIDE,K).EQ.'J  ') .OR. (BDRY_TYPE(SIDE,K).EQ.'GJ ')) THEN
            !Non-conforming edge, U_r must be interpolated to a half interval quadrature
            !If the degree of U_r is larger, U_l must also be interpolated
            IF (DEG_RIGHT.GT.DEG_LEFT) THEN
                DEG_MAX = DEG_RIGHT

                DO I = 0,DEG_LEFT
                    U_TEMP(:,I) = U_L(:,I)
                ENDDO

                DO I = 0,DEG_MAX
                    U_L(:,I) = 0.0d0
                    DO J = 0,DEG_LEFT
                        U_L(:,I) = U_L(:,I) + INTERP(J,I,DEG_LEFT,DEG_MAX)*U_TEMP(:,J)
                    ENDDO
                ENDDO
            ELSE 
                DEG_MAX = DEG_LEFT    
            ENDIF

            !Interpolate the right polynomial to a half interval quadrature
            DO I = 0,DEG_RIGHT
                U_TEMP(:,I) = U_R(:,I)
            ENDDO

            IF (INT(BDRY_DATA(5,SIDE,K)).EQ.0) THEN !Left half
                DO I = 0,DEG_MAX
                    U_R(:,I) = 0.0d0
                    DO J = 0,DEG_RIGHT
                        U_R(:,I) = U_R(:,I) + INTERP_RIGHT(J,I,DEG_RIGHT,DEG_MAX)*U_TEMP(:,J)
                    ENDDO
                ENDDO
            ELSE                             !Right half
                DO I = 0,DEG_MAX
                    U_R(:,I) = 0.0d0
                    DO J = 0,DEG_RIGHT
                        U_R(:,I) = U_R(:,I) + INTERP_LEFT(J,I,DEG_RIGHT,DEG_MAX)*U_TEMP(:,J)
                    ENDDO
                ENDDO
            ENDIF
        ELSE                             !Conforming edge
            !If the degrees do not match, one polynomial must be interpolated
            IF (DEG_RIGHT.EQ.DEG_LEFT) THEN
                DEG_MAX = DEG_LEFT
            ELSEIF (DEG_LEFT.GT.DEG_RIGHT) THEN !Interpolate the right polynomial to the quadrature of the left
                DEG_MAX = DEG_LEFT

                DO I = 0,DEG_RIGHT
                    U_TEMP(:,I) = U_R(:,I)
                ENDDO

                DO I = 0,DEG_MAX
                    U_R(:,I) = 0.0d0
                    DO J = 0,DEG_RIGHT
                        U_R(:,I) = U_R(:,I) + INTERP(J,I,DEG_RIGHT,DEG_MAX)*U_TEMP(:,J)
                    ENDDO
                ENDDO
            ELSE !Interpolate the left polynomial to the quadrature of the right
                DEG_MAX = DEG_RIGHT

                DO I = 0,DEG_LEFT
                    U_TEMP(:,I) = U_L(:,I)
                ENDDO

                DO I = 0,DEG_MAX
                    U_L(:,I) = 0.0d0
                    DO J = 0,DEG_LEFT
                        U_L(:,I) = U_L(:,I) + INTERP(J,I,DEG_LEFT,DEG_MAX)*U_TEMP(:,J)
                    ENDDO
                ENDDO
            ENDIF
        ENDIF

        DO I = 0,DEG_MAX
            U_EDGE(:,I) = 0.5d0*(U_R(:,I)+U_L(:,I))
        ENDDO

        IF (DEG_MAX.EQ.DEG_LEFT) THEN 
            !Basis aligns with quadrature and the integral is simplified
            DO I = 0,DEG_LEFT
                RHS_SURFACE(:,I) = SCAL(SIDE,K)*U_EDGE(:,I)*W(I,DEG_LEFT)
            ENDDO
        ELSE 
            !U_l has been interpolated and a full integral is needed
            DO I = 0,DEG_LEFT
                RHS_SURFACE(:,I) = 0.0d0
                DO J = 0,DEG_MAX
                    RHS_SURFACE(:,I) = RHS_SURFACE(:,I) + SCAL(SIDE,K)*U_EDGE(:,J)*W(J,DEG_MAX)*INTERP(I,J,DEG_LEFT,DEG_MAX)
                ENDDO
            ENDDO
        ENDIF
    ENDIF

    END SUBROUTINE VORT_SURFACE_INTEGRAL