
    SUBROUTINE MAIN_LOOP
    USE SIZE,ONLY: NUM_EQN, NELMAX, NPMAX, NMMAX, RANK, NG, MG
    USE PARAM, ONLY: MAXTIME, MAXSTEPS, IFADAPT, MAXFRAMES, FRAME,NASTEPS
    USE FIELDS, ONLY: U, WRITE_FIELDS
    USE TIMESTEP, ONLY: T,DT,TSTEP,RK_A,RK_B,RK_C,U_RK_STORAGE, RK_RHS_STORAGE, &
                        & NUM_STAGES, BUTCHER_TABLEAU
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS
    !$ USE OMP_LIB
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER K, M, LV, IERROR
    DOUBLE PRECISION CPU_START,CPU_FINAL
    REAL PERCENT

    CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)

    CALL BUTCHER_TABLEAU(NMMAX)

    ALLOCATE(U_RK_STORAGE(0:NPMAX,NELMAX,NUM_EQN,1))
    ALLOCATE(RK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,NUM_STAGES))

    T = 0.0d0
    TSTEP = 0
    !FRAME = 0
    PERCENT = 0.0

    IF (RANK.EQ.0) WRITE(*,'("Computing...")')
    CALL WRITE_FIELDS(0.0)


    !.......MAIN LOOP
    CALL CPU_TIME(CPU_START)
    !$ CPU_START = OMP_GET_WTIME()

    !$OMP PARALLEL DEFAULT(SHARED)
    DO      
        CALL CFL_CONDITION     !.......Compute stable time step 
        !$OMP MASTER
        CALL MPI_ALLREDUCE(DT, DT, 1, MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, IERROR)
        IF (MAXTIME .GE. 0.0d0) THEN
            IF (T+DT .GT. MAXTIME) DT = MAXTIME - T
        ENDIF
        !$OMP END MASTER
        !$OMP BARRIER

        DO K=1,NUM_STAGES
            !Compute the intermediate U to eval the RHS at
            !$OMP WORKSHARE
            U_RK_STORAGE(:,:,:,1) = U
            !$OMP END WORKSHARE
            DO M = 1,K-1 !K-1 is assumed since these are explicit RK methods
                IF (RK_A((K-1)*NUM_STAGES + M).NE. 0.0d0) THEN !Don't do unnecessary adds
                    !$OMP BARRIER
                    !$OMP WORKSHARE
                    U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) + DT*RK_A((K-1)*NUM_STAGES + M)*RK_RHS_STORAGE(:,:,:,M)
                    !$OMP END WORKSHARE
                ENDIF
            ENDDO

            !Compute RHS at this stage
            CALL DGCONVECT(RK_RHS_STORAGE(:,:,:,K),U_RK_STORAGE(:,:,:,1),T+RK_C(K)*DT)
        ENDDO

        !FINAL STAGE
        DO K = 1,NUM_STAGES
            IF (RK_B(K).NE.0.0d0) THEN !Don't do unnecessary adds
                !$OMP BARRIER
                !$OMP WORKSHARE
                U = U + DT*RK_B(K)*RK_RHS_STORAGE(:,:,:,K)
                !$OMP END WORKSHARE
            ENDIF
        ENDDO

        !$OMP MASTER
        T = T + DT
        TSTEP = TSTEP+1
        IF (MAXTIME.LT.0) THEN
            PERCENT = REAL(TSTEP*100)/REAL(MAXSTEPS)
        ELSEIF (MAXSTEPS.LT.0) THEN
            PERCENT = REAL(T/MAXTIME)*100.0
        ELSE
            PERCENT = MAX(REAL(TSTEP*100)/REAL(MAXSTEPS),REAL(T/MAXTIME)*100)
        ENDIF

        IF (PERCENT.GE. REAL(FRAME*100)/REAL(MAXFRAMES)) CALL WRITE_FIELDS(PERCENT)
        !$OMP END MASTER
        !$OMP BARRIER
        IF (PERCENT.GE.100.0) EXIT

        !Adapt
        IF (IFADAPT.AND.MOD(TSTEP,NASTEPS).EQ.0) THEN
            CALL ADAPTMESH
            CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
        ENDIF
    END DO
    !$OMP END PARALLEL

    CALL CPU_TIME(CPU_FINAL)
    !$ CPU_FINAL = OMP_GET_WTIME()
    IF (RANK.EQ.0)PRINT '("Time = ",f10.3," seconds.")',CPU_FINAL-CPU_START

    RETURN
    END
