!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!!C
!!C
!!C         linear equation for durran test
!!C             u_t + a*u_x + b*u_y = 0
!!C
!!C         a =  sin^2(pi x)sin(2pi y)cos(pi t/5)
!!C         b = -sin^2(pi y)sin(2pi x)cos(pi t/5)
!!C
!!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

!C------------------------------------------------------------
!C
!C         Linear Flux FX(U) = a*U, FY(U) = b*U
!C
!C------------------------------------------------------------

    SUBROUTINE FLUX(FX,FY,U,X,Y,T)
    IMPLICIT NONE

    DOUBLE PRECISION FX(1), FY(1)
    DOUBLE PRECISION U(1)
    DOUBLE PRECISION X, Y, T
    DOUBLE PRECISION A, B

    !Flow direction
    CALL GET_VELOCITY(A,B, X, Y, T)

    FX = A*U
    FY = B*U

    END

!C------------------------------------------------------------
!C
!C         Max wavespeed - Used in CFL condition
!C
!C------------------------------------------------------------

    SUBROUTINE MAX_WAVESPEED(A,B,U,X,Y,T)
    IMPLICIT NONE

    DOUBLE PRECISION U(1)
    DOUBLE PRECISION A, B
    DOUBLE PRECISION X, Y, T

    !Flow direction
    CALL GET_VELOCITY(A,B,X,Y,T)

    END

!C------------------------------------------------------------
!C
!C         Max local wavespeed - Used in LLF flux
!C
!C------------------------------------------------------------

    SUBROUTINE MAX_LOCAL_WAVESPEED(LAMBDA,U_L,U_R,NX,NY,X,Y,T)
    IMPLICIT NONE

    DOUBLE PRECISION U_L(1), U_R(1)
    DOUBLE PRECISION NX, NY
    DOUBLE PRECISION LAMBDA

    DOUBLE PRECISION A,B
    DOUBLE PRECISION X, Y, T

    !Flow direction
    CALL GET_VELOCITY(A,B,X,Y,T)

    LAMBDA = DSQRT(A*A+B*B)

    END SUBROUTINE


!!--------------------------------------------------------------------------
!!
!!       CFL_CONDITION
!!       Compute the max DT based on CFL condition
!!       Uses the CFL condition for rectangular grids quoted in 
!!       Cockburn, Shu - The Runge–Kutta Discontinuous Galerkin
!!                       Method for Conservation Laws V
!!       (Mapped for non-uniform quads)
!!--------------------------------------------------------------------------


    SUBROUTINE CFL_CONDITION()
    USE PARAM, ONLY: CFL,DT_MAX 
    USE SIZE, ONLY: NEL,NG,MG, NPMAX
    USE MESH, ONLY: X, Y, DXDR,DXDS,DYDR,DYDS, JAC
    USE BASIS, ONLY: QUAD
    USE TIMESTEP, ONLY: T, DT
    IMPLICIT NONE

    INTEGER I, J, K, N, M
    DOUBLE PRECISION A,B, AlPHA(0:NPMAX), BETA(0:NPMAX), C
    DOUBLE PRECISION XX, YY

    !$OMP MASTER
    DT = DT_MAX
    !$OMP END MASTER
    !$OMP BARRIER
    
    !$OMP DO PRIVATE(N,M,I,J,XX,YY,A,B,ALPHA,BETA,C) SCHEDULE(STATIC) REDUCTION(MIN:DT)
    DO K = 1,NEL
        N = NG(K)
        M = MG(K)
        ALPHA = 0.0d0
        BETA = 0.0d0
        DO J = 0,M
            DO I = 0,N
                !This assumes parallelogram cells
                XX = X(1,K)+0.5d0*(X(2,K)-X(1,K))*(QUAD(I,N)+1.0d0)
                YY = Y(1,K)+0.5d0*(Y(4,K)-Y(1,K))*(QUAD(J,M)+1.0d0)
                !Find max wavespeed at point (I,J) in cell K, !!! Flux dependant
                CALL GET_VELOCITY(A,B,XX,YY,T)

                !Map the wavespeed to the computational cell and take ABS
                ALPHA(I+(N+1)*J) = DABS(( A*DYDS(I+(N+1)*J,K)-B*DXDS(I+(N+1)*J,K))/JAC(I+(N+1)*J,K))
                BETA(I+(N+1)*J)  = DABS((-A*DYDR(I+(N+1)*J,K)+B*DXDR(I+(N+1)*J,K))/JAC(I+(N+1)*J,K))
            ENDDO
        ENDDO

        !Compute min of the inverse in the cell K
        C = MAXVAL(ALPHA+BETA)*(2.0d0*MAX(N,M)+1.0d0)
        C = CFL/C  !CFL number in cell K = CFL * 1/(2N+1) * 1/(abs(alpha) +abs(beta))
        DT = MIN(DT,C)
    ENDDO
    !$OMP END DO

    END SUBROUTINE