MODULE TIMESTEP
IMPLICIT NONE

DOUBLE PRECISION, ALLOCATABLE::RK_A(:)
DOUBLE PRECISION, ALLOCATABLE::RK_B(:)
DOUBLE PRECISION, ALLOCATABLE::RK_C(:)
DOUBLE PRECISION, ALLOCATABLE::DIRK_A(:)
DOUBLE PRECISION, ALLOCATABLE::U_RK_STORAGE(:,:,:,:)     
DOUBLE PRECISION, ALLOCATABLE::RK_RHS_STORAGE(:,:,:,:)
DOUBLE PRECISION, ALLOCATABLE::DIRK_RHS_STORAGE(:,:,:,:)

INTEGER NUM_STAGES

DOUBLE PRECISION T
DOUBLE PRECISION DT
INTEGER TSTEP

CONTAINS
    SUBROUTINE BUTCHER_TABLEAU(ORDER)
    !-----------------------------------------------------------------
    !        Returns the Butcher tableaus of some explicit RK methods 
    !        for a given order        
    !
    !----------------------------------------------------------------
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: ORDER

    IF (ORDER.EQ.1) THEN !Forward Euler
        ALLOCATE(RK_A (1))
        ALLOCATE(RK_B (1))
        ALLOCATE(RK_C (1))

        NUM_STAGES = 1
        RK_A = (/ 0.0d0 /)
        RK_B = (/ 1.0d0 /)
        RK_C = (/ 0.0d0 /)
    ELSEIF (ORDER.EQ.2) THEN !RK2-Midpoint 
        ALLOCATE(RK_A (4))
        ALLOCATE(RK_B (2))
        ALLOCATE(RK_C (2))

        NUM_STAGES = 2
        RK_A = (/ 0.0d0, 0.0d0,  &
               0.5d0, 0.0d0 /)
        RK_B = (/ 0.0d0, 1.0d0 /)
        RK_C = (/ 0.0d0, 0.5d0 /)
    ELSEIF (ORDER.EQ.3) THEN !Kutta's third order method
        ALLOCATE(RK_A (9))
        ALLOCATE(RK_B (3))
        ALLOCATE(RK_C (3))

        NUM_STAGES = 3
        RK_A = (/ 0.0d0, 0.0d0, 0.0d0, &
               0.5d0, 0.0d0, 0.0d0, &
              -1.0d0, 2.0d0, 0.0d0 /)
        RK_B = (/ 1.0d0/6.0d0, 2.0d0/3.0d0, 1.0d0/6.0d0 /)
        RK_C = (/ 0.0d0, 0.5d0, 1.0d0 /)
    ELSE                !RK4
        ALLOCATE(RK_A (16))
        ALLOCATE(RK_B (4))
        ALLOCATE(RK_C (4))

        NUM_STAGES = 4
        RK_A = (/ 0.0d0, 0.0d0, 0.0d0, 0.0d0, &
               0.5d0, 0.0d0, 0.0d0, 0.0d0, &
               0.0d0, 0.5d0, 0.0d0, 0.0d0, &
               0.0d0, 0.0d0, 1.0d0, 0.0d0  /)
        RK_B = (/ 1.0d0/6.0d0, 1.0d0/3.0d0, 1.0d0/3.0d0, 1.0d0/6.0d0 /)
        RK_C = (/ 0.0d0, 0.5d0, 0.5d0, 1.0d0 /)

    ENDIF

    END SUBROUTINE BUTCHER_TABLEAU

!------------------------------------------------------------------------------------

    SUBROUTINE IMEX_RK_TABLEAU(ORDER)
    !-----------------------------------------------------------------
    !  Returns the Butcher tableaus of some implicit-explicit RK methods 
    !  for a given order. Methods are chosen to be stiffly table 
    !  to avoid extra pressure solves. See "Implicit-explicit Runge-Kutta
    !  methods for time-dependent partial diferential equations" by Ascher, Ruuth, Spiteri        
    !
    !----------------------------------------------------------------
    IMPLICIT NONE

    INTEGER, INTENT(IN):: ORDER
    DOUBLE PRECISION GAMMA, DELTA

    IF (ORDER.EQ.1) THEN 
        ALLOCATE(RK_A (1))
        ALLOCATE(DIRK_A (1))
        ALLOCATE(RK_C (2))

        NUM_STAGES = 1
        
        RK_A = (/  1.0d0 /)
        DIRK_A = (/ 1.0d0 /)
        RK_C = (/ 0.0d0, 1.0d0 /)
    ELSEIF (ORDER.EQ.2) THEN
        ALLOCATE(RK_A (4))
        ALLOCATE(DIRK_A (4))
        ALLOCATE(RK_C (3))

        NUM_STAGES = 2
        
        GAMMA = (2.0d0-DSQRT(2.0d0))/2.0d0
        DELTA = 1.0d0 - 1.0d0/(2.0d0*GAMMA)

        RK_A = (/ GAMMA, 0.0d0,  &
                   DELTA, 1.0d0-DELTA /)
        DIRK_A = (/ GAMMA, 0.0d0, &
                    1.0d0-GAMMA, GAMMA /)
        RK_C = (/ 0.0d0, GAMMA, 1.0d0 /)
    ELSE 
        ALLOCATE(RK_A (16))
        ALLOCATE(DIRK_A (16))
        ALLOCATE(RK_C (5))

        NUM_STAGES = 4
        
        RK_A = (/  1.0d0/ 2.0d0,        0.0d0,       0.0d0, 0.0d0, &
                   11.0d0/18.0d0, 1.0d0/18.0d0,       0.0d0, 0.0d0, &
                    5.0d0/ 6.0d0,-5.0d0/ 6.0d0, 1.0d0/2.0d0, 0.0d0, &
                    1.0d0/ 4.0d0, 7.0d0/ 4.0d0, 3.0d0/4.0d0,-7.0d0/4.0d0 /)
        
        DIRK_A = (/ 1.0d0/2.0d0,       0.0d0,       0.0d0, 0.0d0, &
                    1.0d0/6.0d0, 1.0d0/2.0d0,       0.0d0, 0.0d0, &
                   -1.0d0/2.0d0, 1.0d0/2.0d0, 1.0d0/2.0d0, 0.0d0, &
                    3.0d0/2.0d0,-3.0d0/2.0d0, 1.0d0/2.0d0, 1.0d0/2.0d0 /)
        RK_C = (/ 0.0d0, 1.0d0/2.0d0, 2.0d0/3.0d0, 1.0d0/2.0d0, 1.0d0 /)
    ENDIF

    END SUBROUTINE IMEX_RK_TABLEAU
END MODULE TIMESTEP