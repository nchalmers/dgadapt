
    SUBROUTINE DGDIFFUSION(RHS,U,T)
    USE SIZE, ONLY: NUM_EQN,NG,MG,NEL,NPMAX,NELMAX
    USE MESH, ONLY: JAC, DXDR,DXDS,DYDR,DYDS,NEIGHBOUR_ID
    USE BASIS, ONLY: W
    USE FIELDS, ONLY: UQ_HOMOGENEOUS_BOUNDARY
    IMPLICIT NONE

    DOUBLE PRECISION,INTENT(IN):: T
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION,INTENT(OUT):: RHS(0:NPMAX,NELMAX,NUM_EQN)
 
    DOUBLE PRECISION UQ_SURFACE(0:NPMAX)

    INTEGER K,I,J,N,M, MM, SIDE


    DO MM = 1,NUM_EQN
        CALL DGPOISSON(RHS(:,:,MM),U(:,:,MM),NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,.FALSE.,UQ_HOMOGENEOUS_BOUNDARY)
    
        !$OMP DO PRIVATE(N,M,I,J,K,SIDE,UQ_SURFACE)&            
        !$OMP&   SCHEDULE(DYNAMIC)
        DO K=1,NEL
            N = NG(K)
            M = MG(K)

            DO SIDE = 1,4
                !Check if boundary is physical and add boundary contribution if so
                IF (NEIGHBOUR_ID(SIDE,1,K).LT.0) THEN
                    CALL VELOCITY_BDRY_INTEGRAL(UQ_SURFACE,K,SIDE,T,MM)

                    DO J = 0,M
                        DO I = 0,N
                            RHS(I+(N+1)*J,K,MM) = RHS(I+(N+1)*J,K,MM) + UQ_SURFACE(I+(N+1)*J) 
                        ENDDO
                    ENDDO 
                END IF
            END DO

            !Scale
            DO J=0,M
                DO I=0,N
                    RHS(I+(N+1)*J,K,MM) = RHS(I+(N+1)*J,K,MM)/(JAC(I+(N+1)*J,K)*W(I,N)*W(J,M))
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
    ENDDO

    END SUBROUTINE
