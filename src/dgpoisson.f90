
    SUBROUTINE DGPOISSON(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS, &
                        & NPMAX,ALL_NEUMANN,HOMOGENEOUS_BOUNDARY)
    USE SIZE, ONLY: NEL
    USE BASIS, ONLY: W,DER
    USE FIELDS, ONLY: DUDX, DUDY,SS
    USE COMMS, ONLY: SHARE_UQ_BDRY_BUFFERS
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER,INTENT(IN):: NPMAX,NG(NEL),MG(NEL)
    DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)

    DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    LOGICAL,INTENT(IN):: ALL_NEUMANN

    INTERFACE 
        SUBROUTINE HOMOGENEOUS_BOUNDARY(Q_R,Q_L,U_R,U_L,K,SIDE,N)
        USE MESH, ONLY:BDRY_TYPE
        IMPLICIT NONE

        INTEGER,INTENT(IN):: N, K, SIDE
        DOUBLE PRECISION,INTENT(IN)::  U_L(0:N), Q_L(0:N)
        DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)
        END SUBROUTINE 
    END INTERFACE

    INTEGER K,I,J,KK, N,M, IERROR
    DOUBLE PRECISION DUDR, DUDS

    !$OMP WORKSHARE
    DUDX = 0.0d0
    DUDY = 0.0d0
    AU = 0.0d0
    !$OMP END WORKSHARE
    
    !Volume integrals
    !$OMP DO PRIVATE(N,M,I,J,KK,DUDR,DUDS)&            
    !$OMP&   SCHEDULE(DYNAMIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K)
        !Compute grad U and grad V
        DO J=0,M
            DO I=0,N
                DUDR = 0.0d0
                DUDS = 0.0d0

                DO KK = 0,N
                    DUDR = DUDR + U(KK+(N+1)*J,K)*DER(I+(N+1)*KK,N)
                ENDDO
                DO KK = 0,M
                    DUDS = DUDS + U(I+(N+1)*KK,K)*DER(J+(M+1)*KK,M)
                ENDDO
                DUDX(I+(N+1)*J,K) = ( DYDS(I+(N+1)*J,K)*DUDR-DYDR(I+(N+1)*J,K)*DUDS)/JAC(I+(N+1)*J,K)
                DUDY(I+(N+1)*J,K) = (-DXDS(I+(N+1)*J,K)*DUDR+DXDR(I+(N+1)*J,K)*DUDS)/JAC(I+(N+1)*J,K)
            ENDDO
        ENDDO

        !Compute and add Volume integrals
        DO J=0,M
            DO I=0,N
                DO KK = 0,N
                    AU(I+(N+1)*J,K) = AU(I+(N+1)*J,K) -(DUDX(KK+(N+1)*J,K)*DYDS(KK+(N+1)*J,K) &
                                    -DUDY(KK+(N+1)*J,K)*DXDS(KK+(N+1)*J,K))*DER(KK+(N+1)*I,N)*W(KK,N)*W(J,M)
                ENDDO
                DO KK = 0,M
                    AU(I+(N+1)*J,K) = AU(I+(N+1)*J,K) -(-DUDX(I+(N+1)*KK,K)*DYDR(I+(N+1)*KK,K) &
                                    +DUDY(I+(N+1)*KK,K)*DXDR(I+(N+1)*KK,K))*DER(KK+(M+1)*J,M)*W(I,N)*W(KK,M)
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    !Transfer boundary information from our neighbours
    CALL SHARE_UQ_BDRY_BUFFERS(U,NG,MG,NPMAX)

    !Surface integrals
    CALL UQ_SURFACE_INTEGRALS(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                                    & NPMAX,HOMOGENEOUS_BOUNDARY)


    IF (ALL_NEUMANN) THEN
    !Impose a global cell average = 0 constraint if every boundary is of neumann type
    ! This constraint is added algebraically to the system to make A non-signular. 
    ! See "On the finite element soution of the pure Neumann problem" Bochev & Lehoucq 
        !$OMP MASTER
        SS = 0.0d0
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP DO PRIVATE(N,M,I,J,K) REDUCTION(+:SS) SCHEDULE(DYNAMIC)
        DO K=1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    SS = SS + JAC(I+(N+1)*J,K)*W(I,N)*W(J,M)*U(I+(N+1)*J,K)
                ENDDO
            ENDDO
        ENDDO
        !$OMP ENDDO

        !$OMP MASTER
        CALL MPI_ALLREDUCE(SS,SS, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP DO PRIVATE(N,M,I,J,K) SCHEDULE(DYNAMIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)

            DO I = 0,N
                DO J = 0,M
                    AU(I+(N+1)*J,K) = AU(I+(N+1)*J,K) + SS*JAC(I+(N+1)*J,K)*W(I,N)*W(J,M)
                ENDDO
            ENDDO
        ENDDO
        !$OMP ENDDO
    ENDIF

    END SUBROUTINE

!-------------------------------------------------------------------------------------------

    SUBROUTINE UQ_SURFACE_INTEGRALS(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                                    & NPMAX, HOMOGENEOUS_BOUNDARY)
    !!----------------------------------------------------------------
    !!
    !!         Evaluate the grad U and Q surface integrals at the IND-th point 
    !!                along on edge SIDE of element K
    !!
    !!---------------------------------------------------------------
    USE SIZE, ONLY: NEL, NMMAX
    USE MESH, ONLY: SCAL, NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, NX, NY,SIDEMAP, SIDEMAP2, SCAL, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_TYPE, BDRY_DATA
    USE BASIS, ONLY: W, INTERP, INTERP_LEFT, INTERP_RIGHT, INTERP_ENDPOINTS, DER
    USE FIELDS, ONLY: TAU, DUDX, DUDY
    USE COMMS, ONLY: U_BDRY_BUFFER, Q_BDRY_BUFFER,TAU_BDRY_BUFFER, ORDER_BDRY_BUFFER
    IMPLICIT NONE

    INTEGER,INTENT(IN):: NG(NEL), MG(NEL), NPMAX
    DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    INTERFACE 
        SUBROUTINE HOMOGENEOUS_BOUNDARY(Q_R,Q_L,U_R,U_L,K,SIDE,N)
        USE MESH, ONLY:BDRY_TYPE
        IMPLICIT NONE

        INTEGER,INTENT(IN):: N, K, SIDE
        DOUBLE PRECISION,INTENT(IN)::  U_L(0:N), Q_L(0:N)
        DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)
        END SUBROUTINE 
    END INTERFACE

    INTEGER I, J, IND, K, SIDE, ISIDE, MM,KK,D,IND2,K_R, SIDE_R
    INTEGER N, M, N_R, M_R 
    INTEGER DEG_L, DEG_R, DEG_MAX, DEG_R2, DEG_MAX2
    DOUBLE PRECISION TAU_L,TAU_R,TAU_R2
    DOUBLE PRECISION Q_L(0:NMMAX), Q_R(0:NMMAX)
    DOUBLE PRECISION Q_L2(0:NMMAX),Q_R2(0:NMMAX)
    DOUBLE PRECISION U_L(0:NMMAX), U_R(0:NMMAX)
    DOUBLE PRECISION U_L2(0:NMMAX),U_R2(0:NMMAX)
    DOUBLE PRECISION QN_TEMP(0:NMMAX)
    DOUBLE PRECISION U_TEMP(0:NMMAX)
    DOUBLE PRECISION U_EDGE(0:NMMAX), U_EDGE2(0:NMMAX)
    DOUBLE PRECISION Q_EDGE(0:NMMAX), Q_EDGE2(0:NMMAX)
    DOUBLE PRECISION Q_SURFACE(0:NMMAX)
    DOUBLE PRECISION U_SURFACE(0:NMMAX)
    DOUBLE PRECISION UQ_SURFACE(0:NPMAX)

    U_TEMP = 0.0d0  !These are just to avoid the compiler complaining that they may be uninitialized
    QN_TEMP = 0.0d0

    !$OMP DO PRIVATE(N,M,I,J,K,SIDE,UQ_SURFACE)&            
    !$OMP&   SCHEDULE(DYNAMIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K)
        
        !Surface integrals
        DO SIDE =1,4
            IF ((SIDE==1).OR.(SIDE==3)) THEN
                DEG_L = N
            ELSE
                DEG_L = M
            ENDIF

            Q_L = 0.0d0
            U_L = 0.0d0

            IND2 = SIDEMAP2(SIDE)
            D = 1+SIDEMAP2(SIDE+4)*N
            MM = (1-SIDEMAP2(SIDE+4))*N + SIDEMAP2(SIDE+4)*M
            DO I = 0,DEG_L
                IND = SIDEMAP(SIDE)*I + SIDEMAP(SIDE+4)*I*(N+1) + &
                         & SIDEMAP(SIDE+8)*N + SIDEMAP(SIDE+12)*M*(N+1) 
                !Note, this finds the local index of this point based on the side number,
                !i.e. if SIDE=1         SIDE=2          SIDE=3                SIDE=4
                !    then IND=I             IND=N+I*(N+1)      IND=N-I+(N+1)*M     IND=(M-I)*(N+1)
                DO KK = 0, MM
                    Q_L(I) = Q_L(I) + NX(SIDE,K)*DUDX(IND+D*KK,K)*INTERP_ENDPOINTS(KK,IND2,MM) &
                                            &   + NY(SIDE,K)*DUDY(IND+D*KK,K)*INTERP_ENDPOINTS(KK,IND2,MM)
                    U_L(I) = U_L(I) + U(IND+D*KK,K)*INTERP_ENDPOINTS(KK,IND2,MM)
                ENDDO
            ENDDO

            TAU_L = TAU(K)

            IF (BDRY_TYPE(SIDE,K).EQ.'GSP'.OR. (BDRY_TYPE(SIDE,K).EQ.'SP ')) THEN !Split edge, two neighbours
                IF (BDRY_TYPE(SIDE,K).EQ.'GSP') THEN     !Interprocess split edge
                    IND   = NEIGHBOUR_ID(SIDE,1,K) !Get local index of boundary edge
                    ISIDE = NEIGHBOUR_SIDE_ID(SIDE,1,K)
                    K_R   = BDRY_BUFFER_NEIGHBOUR_ID(IND,ISIDE,1) !Get index on other side of boundary
                    DEG_R = ORDER_BDRY_BUFFER(K_R,ISIDE)    !Get degree of polynomial

                    DO I = 0,DEG_R
                        Q_R(I) = Q_BDRY_BUFFER(I,K_R,ISIDE)
                        U_R(I) = U_BDRY_BUFFER(1,I,K_R,ISIDE)
                    ENDDO

                    TAU_R = TAU_BDRY_BUFFER(K_R,ISIDE)

                    K_R   = BDRY_BUFFER_NEIGHBOUR_ID(IND,ISIDE,2) !Get index on other side of boundary
                    DEG_R2 = ORDER_BDRY_BUFFER(K_R,ISIDE)    !Get degree of polynomial

                    DO I = 0,DEG_R2
                        Q_R2(I) = Q_BDRY_BUFFER(I,K_R,ISIDE)
                        U_R2(I) = U_BDRY_BUFFER(1,I,K_R,ISIDE)
                    ENDDO

                    TAU_R2 = TAU_BDRY_BUFFER(K_R,ISIDE)
                ELSE                     !Interiour split edge
                    !Get information about neighbour
                    SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,1,K)
                    K_R    = NEIGHBOUR_ID(SIDE,1,K)
                    N_R = NG(K_R)
                    M_R = MG(K_R)

                    !Get degree of polynomial on right
                    IF ((SIDE_R.EQ.1).OR.(SIDE_R.EQ.3)) THEN
                        DEG_R = N_R
                    ELSE 
                        DEG_R = M_R
                    ENDIF

                    !Get neighbouring values
                    Q_R = 0.0d0
                    U_R = 0.0d0

                    IND2 = SIDEMAP2(SIDE_R)
                    D = 1+SIDEMAP2(SIDE_R+4)*N_R
                    MM = (1-SIDEMAP2(SIDE_R+4))*N_R + SIDEMAP2(SIDE_R+4)*M_R

                    DO I =0,DEG_R
                         IND = SIDEMAP(SIDE_R)*(N_R-I) + SIDEMAP(SIDE_R+4)*(M_R-I)*(N_R+1) + &
                             & SIDEMAP(SIDE_R+8)*N_R + SIDEMAP(SIDE_R+12)*M_R*(N_R+1) 
                        !Note, this finds the local index of this point based on the side number,
                        ! this time in reverse direction
                        !i.e. if SIDE_R=1         SIDE_R=2              SIDE_R=3        SIDE_R=4
                        !    then IND=N-I         InD=(M-I)*(N+1)+N      IND=I+(N+1)*M     IND=I*(N+1)    
                        DO KK =0,MM
                            Q_R(I) = Q_R(I) + NX(SIDE,K)*DUDX(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM) &
                                                        & + NY(SIDE,K)*DUDY(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM) 
                            U_R(I) = U_R(I) + U(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM)
                        ENDDO
                    ENDDO

                    TAU_R = TAU(K_R)

                    SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,2,K)
                    K_R    = NEIGHBOUR_ID(SIDE,2,K)
                    N_R = NG(K_R)
                    M_R = MG(K_R)

                    !Get degree of polynomial on right
                    IF ((SIDE_R.EQ.1).OR.(SIDE_R.EQ.3)) THEN
                        DEG_R2 = N_R
                    ELSE 
                        DEG_R2 = M_R
                    ENDIF

                    !Get neighbouring values
                    Q_R2 = 0.0d0
                    U_R2 = 0.0d0

                    IND2 = SIDEMAP2(SIDE_R)
                    D = 1+SIDEMAP2(SIDE_R+4)*N_R
                    MM = (1-SIDEMAP2(SIDE_R+4))*N_R + SIDEMAP2(SIDE_R+4)*M_R

                    DO I =0,DEG_R2
                         IND = SIDEMAP(SIDE_R)*(N_R-I) + SIDEMAP(SIDE_R+4)*(M_R-I)*(N_R+1) + &
                             & SIDEMAP(SIDE_R+8)*N_R + SIDEMAP(SIDE_R+12)*M_R*(N_R+1) 
                        !Note, this finds the local index of this point based on the side number,
                        ! this time in reverse direction
                        !i.e. if SIDE_R=1         SIDE_R=2              SIDE_R=3        SIDE_R=4
                        !    then IND=N-I         InD=(M-I)*(N+1)+N      IND=I+(N+1)*M     IND=I*(N+1)    
                        DO KK =0,MM
                            Q_R2(I) = Q_R2(I) + NX(SIDE,K)*DUDX(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM) &
                                                          & + NY(SIDE,K)*DUDY(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM) 
                            U_R2(I) = U_R2(I) + U(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM)
                        ENDDO
                    ENDDO

                    TAU_R2 = TAU(K_R)
                ENDIF

                !Two integrals must be performed. We need to interpolate U_l to left and right half intervals
                ! and interpolate U_r and U_r2 if necessary

                IF (DEG_L.GT.DEG_R) THEN
                    DEG_MAX = DEG_L

                    DO I = 0,DEG_R
                        QN_TEMP(I) = Q_R(I)
                        U_TEMP(I) = U_R(I)
                    ENDDO

                    DO I = 0,DEG_MAX
                        Q_R(I) = 0.0d0
                        U_R(I) = 0.0d0
                        DO J = 0,DEG_R
                            Q_R(I) = Q_R(I) + INTERP(J,I,DEG_R,DEG_MAX)*QN_TEMP(J)
                            U_R(I)  =  U_R(I) + INTERP(J,I,DEG_R,DEG_MAX)*U_TEMP(J)
                        ENDDO
                    ENDDO
                ELSE 
                    DEG_MAX = DEG_R    
                ENDIF

                IF (DEG_L.GT.DEG_R2) THEN
                    DEG_MAX2 = DEG_L

                    DO I = 0,DEG_R2
                        QN_TEMP(I) = Q_R2(I)
                        U_TEMP(I) = U_R2(I)
                    ENDDO

                    DO I = 0,DEG_MAX2
                        Q_R2(I) = 0.0d0
                        U_R2(I) = 0.0d0
                        DO J = 0,DEG_R2
                            Q_R2(I) = Q_R2(I) + INTERP(J,I,DEG_R2,DEG_MAX2)*QN_TEMP(J)
                            U_R2(I)  =  U_R2(I) + INTERP(J,I,DEG_R2,DEG_MAX2)*U_TEMP(J)
                        ENDDO
                    ENDDO
                ELSE 
                    DEG_MAX2 = DEG_R2    
                ENDIF

                !Interpolate the left polynomial to a half intervals
                DO I = 0,DEG_L
                    QN_TEMP(I) = Q_L(I)
                    U_TEMP(I) = U_L(I)
                ENDDO

                DO I = 0,DEG_MAX
                    Q_L(I) = 0.0d0
                    U_L(I) = 0.0d0
                    DO J = 0,DEG_L
                        Q_L(I) = Q_L(I)  + INTERP_LEFT(J,I,DEG_L,DEG_MAX)*QN_TEMP(J)
                        U_L(I) = U_L(I)  + INTERP_LEFT(J,I,DEG_L,DEG_MAX)*U_TEMP(J)
                    ENDDO
                ENDDO

                DO I = 0,DEG_MAX2
                    Q_L2(I) = 0.0d0
                    U_L2(I) = 0.0d0
                    DO J = 0,DEG_L
                        Q_L2(I) = Q_L2(I) + INTERP_RIGHT(J,I,DEG_L,DEG_MAX2)*QN_TEMP(J)
                        U_L2(I) = U_L2(I) + INTERP_RIGHT(J,I,DEG_L,DEG_MAX2)*U_TEMP(J)
                    ENDDO
                ENDDO

                !Evaluate the flux at these points using the Internal penalty flux
                DO I = 0,DEG_MAX
                    U_EDGE(I) = 0.5d0*(U_R(I)+U_L(I)) - U_L(I)
                    Q_EDGE(I) = 0.5d0*(Q_L(I)+Q_R(I)) &
                                    &+ MAX(TAU_L,TAU_R)*(U_R(I)-U_L(I))
                ENDDO
                DO I = 0,DEG_MAX2
                    U_EDGE2(I) = 0.5d0*(U_R2(I)+U_L2(I)) - U_L2(I)
                    Q_EDGE2(I) = 0.5d0*(Q_L2(I)+Q_R2(I)) &
                                    &+ MAX(TAU_L,TAU_R2)*(U_R2(I)-U_L2(I))
                ENDDO

                DO I = 0,DEG_L
                    U_SURFACE(I) = 0.0d0
                    Q_SURFACE(I) = 0.0d0
                    DO J = 0,DEG_MAX
                        U_SURFACE(I) = U_SURFACE(I) + 0.5d0*U_EDGE(J)*W(J,DEG_MAX)*INTERP_LEFT(I,J,DEG_L,DEG_MAX)
                        Q_SURFACE(I) = Q_SURFACE(I) + 0.5d0*Q_EDGE(J)*W(J,DEG_MAX)*INTERP_LEFT(I,J,DEG_L,DEG_MAX)
                    ENDDO
                    DO J = 0,DEG_MAX2
                        U_SURFACE(I) = U_SURFACE(I) + 0.5d0*U_EDGE2(J)*W(J,DEG_MAX2)*INTERP_RIGHT(I,J,DEG_L,DEG_MAX2)
                        Q_SURFACE(I) = Q_SURFACE(I) + 0.5d0*Q_EDGE2(J)*W(J,DEG_MAX2)*INTERP_RIGHT(I,J,DEG_L,DEG_MAX2)
                    ENDDO
                ENDDO
            ELSE                  !Single neighbour
                !Check if boundary is physical
                IF (NEIGHBOUR_ID(SIDE,1,K).LT.0) THEN
                    !Evaluate right state based on the boundary data
                    DEG_R = DEG_L
                    CALL HOMOGENEOUS_BOUNDARY(Q_R,Q_L,U_R,U_L,K,SIDE,DEG_L)    
                    TAU_R = TAU(K)
                ELSEIF (BDRY_TYPE(SIDE,K).EQ.'GE '.OR. (BDRY_TYPE(SIDE,K).EQ.'GJ ')) THEN !Interprocess boundary
                    IND   = NEIGHBOUR_ID(SIDE,1,K) !Get local index of boundary edge
                    ISIDE = NEIGHBOUR_SIDE_ID(SIDE,1,K)
                    K_R   = BDRY_BUFFER_NEIGHBOUR_ID(IND,ISIDE,1) !Get index on other side of boundary
                    DEG_R = ORDER_BDRY_BUFFER(K_R,ISIDE)    !Get degree of polynomial

                    DO I = 0,DEG_R
                        Q_R(I) = Q_BDRY_BUFFER(I,K_R,ISIDE)
                        U_R(I) = U_BDRY_BUFFER(1,I,K_R,ISIDE)
                    ENDDO

                    TAU_R = TAU_BDRY_BUFFER(K_R,ISIDE)
                ELSE !Interior edge
                    !Get information about neighbour
                    SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,1,K)
                    K_R    = NEIGHBOUR_ID(SIDE,1,K)
                    N_R = NG(K_R)
                    M_R = MG(K_R)

                    !Get degree of polynomial on right
                    IF ((SIDE_R.EQ.1).OR.(SIDE_R.EQ.3)) THEN
                        DEG_R = N_R
                    ELSE 
                        DEG_R = M_R
                    ENDIF

                    !Get neighbouring values
                    Q_R = 0.0d0
                    U_R = 0.0d0

                    IND2 = SIDEMAP2(SIDE_R)
                    D = 1+SIDEMAP2(SIDE_R+4)*N_R
                    MM = (1-SIDEMAP2(SIDE_R+4))*N_R + SIDEMAP2(SIDE_R+4)*M_R

                    DO I =0,DEG_R
                         IND = SIDEMAP(SIDE_R)*(N_R-I) + SIDEMAP(SIDE_R+4)*(M_R-I)*(N_R+1) + &
                             & SIDEMAP(SIDE_R+8)*N_R + SIDEMAP(SIDE_R+12)*M_R*(N_R+1) 
                        !Note, this finds the local index of this point based on the side number,
                        ! this time in reverse direction
                        !i.e. if SIDE_R=1         SIDE_R=2              SIDE_R=3        SIDE_R=4
                        !    then IND=N-I         InD=(M-I)*(N+1)+N      IND=I+(N+1)*M     IND=I*(N+1)    
                        DO KK =0,MM
                            Q_R(I) = Q_R(I) + NX(SIDE,K)*DUDX(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM) &
                                                        & + NY(SIDE,K)*DUDY(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM) 
                            U_R(I) = U_R(I) + U(IND+D*KK,K_R)*INTERP_ENDPOINTS(KK,IND2,MM)
                        ENDDO
                    ENDDO

                    TAU_R = TAU(K_R)
                ENDIF

                IF ((BDRY_TYPE(SIDE,K).EQ.'J  ') .OR. (BDRY_TYPE(SIDE,K).EQ.'GJ ')) THEN
                    !Non-conforming edge, U_r must be interpolated to a half interval quadrature
                    !If the degree of U_r is larger, U_l must also be interpolated
                    IF (DEG_R.GT.DEG_L) THEN
                        DEG_MAX = DEG_R

                        DO I = 0,DEG_L
                            QN_TEMP(I) = Q_L(I)
                            U_TEMP(I)  = U_L(I)
                        ENDDO

                        DO I = 0,DEG_MAX
                            Q_L(I) = 0.0d0
                            U_L(I) = 0.0d0
                            DO J = 0,DEG_L
                                Q_L(I) = Q_L(I) + INTERP(J,I,DEG_L,DEG_MAX)*QN_TEMP(J)
                                U_L(I) = U_L(I) + INTERP(J,I,DEG_L,DEG_MAX)*U_TEMP(J)
                            ENDDO
                        ENDDO
                    ELSE 
                        DEG_MAX = DEG_L    
                    ENDIF

                    !Interpolate the right polynomial to a half interval quadrature
                    DO I = 0,DEG_R
                        QN_TEMP(I) = Q_R(I)
                        U_TEMP(I) = U_R(I)
                    ENDDO

                    IF (INT(BDRY_DATA(5,SIDE,K)).EQ.0) THEN !Left half
                        DO I = 0,DEG_MAX
                            Q_R(I) = 0.0d0
                            U_R(I) = 0.0d0
                            DO J = 0,DEG_R
                                Q_R(I) = Q_R(I) + INTERP_RIGHT(J,I,DEG_R,DEG_MAX)*QN_TEMP(J)
                                U_R(I) = U_R(I) + INTERP_RIGHT(J,I,DEG_R,DEG_MAX)*U_TEMP(J)
                            ENDDO
                        ENDDO
                    ELSE                             !Right half
                        DO I = 0,DEG_MAX
                            Q_R(I) = 0.0d0
                            U_R(I) = 0.0d0
                            DO J = 0,DEG_R
                                Q_R(I) = Q_R(I) + INTERP_LEFT(J,I,DEG_R,DEG_MAX)*QN_TEMP(J)
                                U_R(I) = U_R(I) + INTERP_LEFT(J,I,DEG_R,DEG_MAX)*U_TEMP(J)
                            ENDDO
                        ENDDO
                    ENDIF
                ELSE                             !Conforming edge
                    !If the degrees do not match, one polynomial must be interpolated
                    IF (DEG_R.EQ.DEG_L) THEN
                        DEG_MAX = DEG_L
                    ELSEIF (DEG_L.GT.DEG_R) THEN !Interpolate the right polynomial to the quadrature of the left
                        DEG_MAX = DEG_L

                        DO I = 0,DEG_R
                            QN_TEMP(I) = Q_R(I)
                            U_TEMP(I) = U_R(I)
                        ENDDO

                        DO I = 0,DEG_MAX
                            Q_R(I) = 0.0d0
                            U_R(I) = 0.0d0
                            DO J = 0,DEG_R
                                Q_R(I) = Q_R(I) + INTERP(J,I,DEG_R,DEG_MAX)*QN_TEMP(J)
                                U_R(I) = U_R(I) + INTERP(J,I,DEG_R,DEG_MAX)*U_TEMP(J)
                            ENDDO
                        ENDDO
                    ELSE !Interpolate the left polynomial to the quadrature of the right
                        DEG_MAX = DEG_R

                        DO I = 0,DEG_L
                            QN_TEMP(I) = Q_L(I)
                            U_TEMP(I) = U_L(I)
                        ENDDO

                        DO I = 0,DEG_MAX
                            Q_L(I) = 0.0d0
                            U_L(I) = 0.0d0
                            DO J = 0,DEG_L
                                Q_L(I) = Q_L(I) + INTERP(J,I,DEG_L,DEG_MAX)*QN_TEMP(J)
                                U_L(I) = U_L(I) + INTERP(J,I,DEG_L,DEG_MAX)*U_TEMP(J)
                            ENDDO
                        ENDDO
                    ENDIF
                ENDIF

                !Evaluate the flux at these points using the Internal penalty flux
                DO I = 0,DEG_MAX
                    U_EDGE(I) = 0.5d0*(U_L(I)+U_R(I))-U_L(I)
                    Q_EDGE(I) = 0.5d0*(Q_L(I)+Q_R(I)) &
                                    &+ MAX(TAU_L,TAU_R)*(U_R(I)-U_L(I))
                ENDDO

                IF (DEG_MAX.EQ.DEG_L) THEN 
                    !Basis aligns with quadrature and the integral is simplified
                    DO I = 0,DEG_L
                        U_SURFACE(I) = U_EDGE(I)*W(I,DEG_L)
                        Q_SURFACE(I) = Q_EDGE(I)*W(I,DEG_L)
                    ENDDO
                ELSE 
                    !U_l has been interpolated and a full integral is needed
                    DO I = 0,DEG_L
                        U_SURFACE(I) = 0.0d0
                        Q_SURFACE(I) = 0.0d0
                        DO J = 0,DEG_MAX
                            U_SURFACE(I) = U_SURFACE(I) + U_EDGE(J)*W(J,DEG_MAX)*INTERP(I,J,DEG_L,DEG_MAX)
                            Q_SURFACE(I) = Q_SURFACE(I) + Q_EDGE(J)*W(J,DEG_MAX)*INTERP(I,J,DEG_L,DEG_MAX)
                        ENDDO
                    ENDDO
                ENDIF
            ENDIF


            !Construct the entire term
            SELECT CASE (SIDE)
            CASE (1)
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(I)*INTERP_ENDPOINTS(J,0,M)
                        DO KK = 0,N
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(NX(1,K)*DYDS(KK+(N+1)*J,K)-NY(1,K)*DXDS(KK+(N+1)*J,K)) &
                                             *DER(KK+(N+1)*I,N)*U_SURFACE(KK)*INTERP_ENDPOINTS(J,0,M)/JAC(KK+(N+1)*J,K)
                        ENDDO
                        DO KK = 0,M
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(-NX(1,K)*DYDR(I+(N+1)*KK,K)+NY(1,K)*DXDR(I+(N+1)*KK,K))&
                                             *DER(KK+(M+1)*J,M)*U_SURFACE(I)*INTERP_ENDPOINTS(KK,0,M)/JAC(I+(N+1)*KK,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(1,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            CASE (2) 
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(J)*INTERP_ENDPOINTS(I,1,N)
                        DO KK = 0,N
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(NX(2,K)*DYDS(KK+(N+1)*J,K)-NY(2,K)*DXDS(KK+(N+1)*J,K))&
                                             *DER(KK+(N+1)*I,N)*U_SURFACE(J)*INTERP_ENDPOINTS(KK,1,N)/JAC(KK+(N+1)*J,K)
                        ENDDO
                        DO KK = 0,M
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(-NX(2,K)*DYDR(I+(N+1)*KK,K)+NY(2,K)*DXDR(I+(N+1)*KK,K))&
                                             *DER(KK+(M+1)*J,M)*U_SURFACE(KK)*INTERP_ENDPOINTS(I,1,N)/JAC(I+(N+1)*KK,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(2,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            CASE (3)
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(N-I)*INTERP_ENDPOINTS(J,1,M)
                        DO KK = 0,N
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(NX(3,K)*DYDS(KK+(N+1)*J,K)-NY(3,K)*DXDS(KK+(N+1)*J,K))&
                                             *DER(KK+(N+1)*I,N)*U_SURFACE(N-KK)*INTERP_ENDPOINTS(J,1,M)/JAC(KK+(N+1)*J,K)
                        ENDDO
                        DO KK = 0,M
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(-NX(3,K)*DYDR(I+(N+1)*KK,K)+NY(3,K)*DXDR(I+(N+1)*KK,K))&
                                             *DER(KK+(M+1)*J,M)*U_SURFACE(N-I)*INTERP_ENDPOINTS(KK,1,M)/JAC(I+(N+1)*KK,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(3,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            CASE (4) 
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(M-J)*INTERP_ENDPOINTS(I,0,N)
                        DO KK = 0,N
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(NX(4,K)*DYDS(KK+(N+1)*J,K)-NY(4,K)*DXDS(KK+(N+1)*J,K))&
                                             *DER(KK+(N+1)*I,N)*U_SURFACE(M-J)*INTERP_ENDPOINTS(KK,0,N)/JAC(KK+(N+1)*J,K)
                        ENDDO
                        DO KK = 0,M
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            -(-NX(4,K)*DYDR(I+(N+1)*KK,K)+NY(4,K)*DXDR(I+(N+1)*KK,K))&
                                             *DER(KK+(M+1)*J,M)*U_SURFACE(M-KK)*INTERP_ENDPOINTS(I,0,N)/JAC(I+(N+1)*KK,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(4,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            END SELECT

            !Add integral to RHS
            DO J = 0,M
                DO I = 0,N
                    AU(I+(N+1)*J,K) = AU(I+(N+1)*J,K) + UQ_SURFACE(I+(N+1)*J) 
                ENDDO
            ENDDO 
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE