MODULE MULTIGRID
IMPLICIT NONE

ABSTRACT INTERFACE
    SUBROUTINE DG_BC(Q_R,Q_L,U_R,U_L,K,SIDE,N)
    USE MESH, ONLY:BDRY_TYPE
    IMPLICIT NONE

    INTEGER,INTENT(IN):: N, K, SIDE
    DOUBLE PRECISION,INTENT(IN)::  U_L(0:N), Q_L(0:N)
    DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)
    END SUBROUTINE DG_BC   
END INTERFACE

ABSTRACT INTERFACE 
    SUBROUTINE DG_A(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,ALL_NEUMANN,BC)
    USE SIZE, ONLY: NEL
    IMPORT 
    IMPLICIT NONE

    INTEGER,INTENT(IN):: NPMAX,NG(NEL),MG(NEL)
    DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)
    
    DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    LOGICAL,INTENT(IN):: ALL_NEUMANN

    PROCEDURE (DG_BC) BC

    END SUBROUTINE DG_A
END INTERFACE

PRIVATE
INTEGER, ALLOCATABLE::NG_MULTIGRID(:),MG_MULTIGRID(:)

DOUBLE PRECISION, ALLOCATABLE :: RR(:)
DOUBLE PRECISION, ALLOCATABLE :: DELTA(:)
DOUBLE PRECISION, ALLOCATABLE :: MULTIGRID_SMOOTHER(:)
DOUBLE PRECISION, ALLOCATABLE :: MULTIGRID_STORAGE(:)

PROCEDURE (DG_A),  POINTER ::  pA
PROCEDURE (DG_BC), POINTER ::  pBC


DOUBLE PRECISION, ALLOCATABLE:: CG_R(:,:), CG_P(:,:)
DOUBLE PRECISION CG_R_DOT_R, CG_PP_DOT_APP
DOUBLE PRECISION CG_R_DOT_R_NEW


PUBLIC :: ALLOCATE_MULTIGRID, DEALLOCATE_MULTIGRID, &
         & INIT_MULTIGRID, MULTIGRID_PRECONDITIONER

CONTAINS
    SUBROUTINE ALLOCATE_MULTIGRID
    USE SIZE, ONLY: NPMAX, NELMAX
    USE MESH, ONLY: JAC,DXDR,DXDS,DYDR,DYDS
    USE PARAM, ONLY: IFMULTIGRID,IFPRECOND
    IMPLICIT NONE

    IF (IFPRECOND) THEN
        IF (IFMULTIGRID) THEN
            ALLOCATE(NG_MULTIGRID(NELMAX))
            ALLOCATE(MG_MULTIGRID(NELMAX))

            ALLOCATE(RR((NPMAX+1)*NELMAX)) 
            ALLOCATE(DELTA((NPMAX+1)*NELMAX)) 
            ALLOCATE(MULTIGRID_STORAGE((NPMAX+1)*NELMAX)) 
            ALLOCATE(MULTIGRID_SMOOTHER((NPMAX+1)*NELMAX*2))

            !Need double the room for metrics of different levels of meshes
            DEALLOCATE(DXDR)
            DEALLOCATE(DYDR)
            DEALLOCATE(DXDS)
            DEALLOCATE(DYDS)
            DEALLOCATE(JAC)

            ALLOCATE(DXDR(0:NPMAX,NELMAX*2))
            ALLOCATE(DYDR(0:NPMAX,NELMAX*2))
            ALLOCATE(DXDS(0:NPMAX,NELMAX*2))
            ALLOCATE(DYDS(0:NPMAX,NELMAX*2))
            ALLOCATE(JAC(0:NPMAX,NELMAX*2))

            ALLOCATE(CG_R(0:NPMAX,NELMAX))
            ALLOCATE(CG_P(0:NPMAX,NELMAX))
        ELSE
            ALLOCATE(MULTIGRID_SMOOTHER((NPMAX+1)*NELMAX))
            ALLOCATE(MULTIGRID_STORAGE((NPMAX+1)*NELMAX)) 
        ENDIF
    ENDIF

    END SUBROUTINE ALLOCATE_MULTIGRID

!---------------------------------------------------------------------    

    SUBROUTINE DEALLOCATE_MULTIGRID
    USE PARAM, ONLY: IFMULTIGRID, IFPRECOND
    IMPLICIT NONE

    IF (IFPRECOND) THEN
        IF (IFMULTIGRID) THEN
            DEALLOCATE(NG_MULTIGRID)
            DEALLOCATE(MG_MULTIGRID)

            DEALLOCATE(RR) 
            DEALLOCATE(DELTA) 
        ENDIF

        DEALLOCATE(MULTIGRID_SMOOTHER)
        DEALLOCATE(MULTIGRID_STORAGE) 
    ENDIF

    END SUBROUTINE DEALLOCATE_MULTIGRID

!---------------------------------------------------------------------

    SUBROUTINE INIT_MULTIGRID(A, BC)
    USE SIZE, ONLY: NPMAX, NG, MG, NMMAX
    USE PARAM, ONLY: IFMULTIGRID, IFPRECOND
    USE MESH, ONLY: JAC,DXDR,DXDS,DYDR,DYDS
    IMPLICIT NONE 

    PROCEDURE (DG_A)  A
    PROCEDURE (DG_BC) BC

    IF (IFPRECOND) THEN
        !Set pointers to the operators that we are going to precondition
        pA  => A
        pBC => BC

        IF (IFMULTIGRID) THEN
            NG_MULTIGRID = NG
            MG_MULTIGRID = MG

            CALL MULTIGRID_BUILD_SMOOTHERS_AND_METRICS(JAC,DXDR,DXDS,DYDR,DYDS)

            NG_MULTIGRID = NG
            MG_MULTIGRID = MG
        ELSE
            CALL MULTIGRID_SMOOTHER_JACOBI(MULTIGRID_SMOOTHER,NG,MG,&
                                    JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)
        ENDIF
    ENDIF

    END SUBROUTINE INIT_MULTIGRID
    
!---------------------------------------------------------------------

    SUBROUTINE MULTIGRID_PRECONDITIONER(U,RHS)
    USE SIZE, ONLY: NMMAX, NPMAX, NEL,NG,MG
    USE PARAM, ONLY: IFMULTIGRID,IFPRECOND
    USE MESH, ONLY: JAC, DXDR,DXDS,DYDR,DYDS
    IMPLICIT NONE

    DOUBLE PRECISION U(0:NPMAX,NEL)
    DOUBLE PRECISION RHS(0:NPMAX,NEL)

    IF (IFPRECOND) THEN
        IF (IFMULTIGRID) THEN
            !$OMP WORKSHARE
            RR = 0.0d0
            DELTA = 0.0d0
            MULTIGRID_STORAGE = 0.0d0
            !$OMP END WORKSHARE
        
            !CALL MULTIGRID_W_CYCLE(U,RHS,DELTA,RR,MULTIGRID_STORAGE,MULTIGRID_SMOOTHER,&
            !                       & JAC,DXDR,DXDS,DYDR,DYDS, &
            !                       & NMMAX,2,2,2,NPMAX)

            CALL FULL_MULTIGRID_CYCLE(U,RHS,DELTA,RR,MULTIGRID_STORAGE,MULTIGRID_SMOOTHER,&
                                   & JAC,DXDR,DXDS,DYDR,DYDS, &
                                   & NMMAX,2,2,2,NPMAX)
        ELSE
            CALL SMOOTH(U,RHS,MULTIGRID_SMOOTHER,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                        & NPMAX,2,MULTIGRID_STORAGE)
        ENDIF
    ELSE
        !$OMP WORKSHARE
        U = RHS
        !$OMP END WORKSHARE
    ENDIF

    END SUBROUTINE

!---------------------------------------------------------------------

    RECURSIVE SUBROUTINE FULL_MULTIGRID_CYCLE(U,RHS,DELTA,RR,STORAGE,SMOOTHER, &
                                & JAC, DXDR, DXDS, DYDR, DYDS, &
                                & P,PRE_ITER,POST_ITER,CYCLE_ITER,NPMAX)
    USE SIZE, ONLY: NEL
    USE PARAM, ONLY: ALL_NEUMANN,ITER_PRES
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER,INTENT(IN):: NPMAX, P
    INTEGER,INTENT(IN):: PRE_ITER, POST_ITER, CYCLE_ITER

    DOUBLE PRECISION,INTENT(INOUT):: U((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: RHS((NPMAX+1)*NEL)

    DOUBLE PRECISION,INTENT(IN):: RR((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(INOUT):: DELTA((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(OUT):: STORAGE((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: SMOOTHER((NPMAX+1)*NEL)

    DOUBLE PRECISION,INTENT(IN):: JAC((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR((NPMAX+1)*NEL), DXDS((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR((NPMAX+1)*NEL), DYDS((NPMAX+1)*NEL)

    INTEGER K, I,J,N,M, P_NEXT, NPMAX_NEXT, IERROR
    DOUBLE PRECISION, PARAMETER:: TOL = 1.0d-8

    !Check for base of cycle
    IF (P.EQ.1) THEN
        !$OMP MASTER
        ITER_PRES = 0
        !$OMP END MASTER
        !Exact solve
        CALL MULTIGRID_CONJ_GRAD_SOLVE(U,RHS,CG_R,CG_P,NG_MULTIGRID,MG_MULTIGRID,JAC,DXDR,DXDS,DYDR,DYDS,STORAGE,NPMAX)
        RETURN
    ENDIF

    !Next order
    P_NEXT = FLOOR( REAL(P+1)/SQRT(2.0) -1.0)
    NPMAX_NEXT = (P_NEXT+1)**2 -1

    !Compute residule
    CALL pA(STORAGE,U,NG_MULTIGRID,MG_MULTIGRID, &
                    & JAC,DXDR,DXDS,DYDR,DYDS,NPMAX,ALL_NEUMANN,pBC)
    !$OMP WORKSHARE
    STORAGE = RHS-STORAGE
    !$OMP END WORKSHARE

    !Restriction
    !CALL MULTIGRID_RESTRICTION(RR,RHS,P_NEXT,NPMAX_NEXT,NPMAX)
    CALL MULTIGRID_RESTRICTION(RR,STORAGE,P_NEXT,NPMAX_NEXT,NPMAX)

    !Cycle
    CALL FULL_MULTIGRID_CYCLE(DELTA,RR, &
                            & DELTA((NPMAX+1)*NEL/2+1), RR((NPMAX+1)*NEL/2+1), &
                            & STORAGE, SMOOTHER((NPMAX+1)*NEL+1), &
                            & JAC((NPMAX+1)*NEL+1), DXDR((NPMAX+1)*NEL+1),DXDS((NPMAX+1)*NEL+1), &
                            & DYDR((NPMAX+1)*NEL+1), DYDS((NPMAX+1)*NEL+1), & 
                            & P_NEXT,PRE_ITER,POST_ITER,CYCLE_ITER,NPMAX_NEXT)

    !Prolongate
    !CALL MULTIGRID_PROLONGATION(U,DELTA,P,NPMAX,NPMAX_NEXT)
    CALL MULTIGRID_PROLONGATION(STORAGE,DELTA,P,NPMAX,NPMAX_NEXT)

    !Correct
    !$OMP WORKSHARE
    U = U + STORAGE
    !$OMP END WORKSHARE

    DO 
        !Compute residual
        CALL pA(STORAGE,U,NG_MULTIGRID,MG_MULTIGRID, &
                        & JAC,DXDR,DXDS,DYDR,DYDS,NPMAX,ALL_NEUMANN,pBC)

        !$OMP WORKSHARE
        STORAGE = RHS-STORAGE
        !$OMP END WORKSHARE

        !$OMP MASTER 
        CG_R_DOT_R = 0.0d0
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP DO PRIVATE(I,J,N,M) REDUCTION(+:CG_R_DOT_R) SCHEDULE(DYNAMIC)
        DO K = 1,NEL
            N = NG_MULTIGRID(K)
            M = MG_MULTIGRID(K)
            DO I = 0,N
                DO J=0,M
                    CG_R_DOT_R = CG_R_DOT_R + STORAGE((I+(N+1)*J+1)*K)**2
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO

        !$OMP MASTER
        CALL MPI_ALLREDUCE(CG_R_DOT_R, CG_R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        !$OMP END MASTER
        !$OMP BARRIER

        IF (DSQRT(CG_R_DOT_R).LT.TOL) EXIT
        !V Cycle
        !DO K = 1,CYCLE_ITER
        !    CALL MULTIGRID_W_CYCLE(U,RHS, DELTA, RR, STORAGE, SMOOTHER, &
        !                        & JAC, DXDR,DXDS, DYDR, DYDS, & 
        !                        & P,PRE_ITER,POST_ITER,CYCLE_ITER,NPMAX)
        !ENDDO
        
        CALL MULTIGRID_CONJ_GRAD_SOLVE(U,RHS,CG_R,CG_P,NG_MULTIGRID,MG_MULTIGRID,JAC,DXDR,DXDS,DYDR,DYDS,STORAGE,NPMAX)
    ENDDO

    END SUBROUTINE FULL_MULTIGRID_CYCLE

!---------------------------------------------------------------------

    RECURSIVE SUBROUTINE MULTIGRID_W_CYCLE(U,RHS,DELTA,RR,STORAGE,SMOOTHER, &
                                & JAC, DXDR, DXDS, DYDR, DYDS, &
                                & P,PRE_ITER,POST_ITER,CYCLE_ITER,NPMAX)
    USE SIZE, ONLY: NEL
    USE PARAM,ONLY: ALL_NEUMANN
    IMPLICIT NONE

    INTEGER,INTENT(IN):: NPMAX
    INTEGER,INTENT(IN):: PRE_ITER, POST_ITER, CYCLE_ITER

    DOUBLE PRECISION,INTENT(INOUT):: U((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: RHS((NPMAX+1)*NEL)

    DOUBLE PRECISION,INTENT(IN):: RR((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(INOUT):: DELTA((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(OUT):: STORAGE((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: SMOOTHER((NPMAX+1)*NEL)

    DOUBLE PRECISION,INTENT(IN):: JAC((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR((NPMAX+1)*NEL), DXDS((NPMAX+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR((NPMAX+1)*NEL), DYDS((NPMAX+1)*NEL)

    INTEGER K, P, P_NEXT, NPMAX_NEXT

    !Check for base of cycle
    IF (P.EQ.1) THEN
        !Do only smoothing and return
        !CALL SMOOTH(U,RHS,SMOOTHER,NG_MULTIGRID,MG_MULTIGRID,JAC,DXDR,DXDS,DYDR,DYDS,&
        !            & NPMAX,PRE_ITER+POST_ITER,STORAGE)
        
        !Exact solve
        CALL MULTIGRID_CONJ_GRAD_SOLVE(U,RHS,CG_R,CG_P,NG_MULTIGRID,MG_MULTIGRID,JAC,DXDR,DXDS,DYDR,DYDS,STORAGE,NPMAX)
        RETURN
    ENDIF

    !Next order
    P_NEXT = FLOOR( REAL(P+1)/SQRT(2.0) -1.0)
    NPMAX_NEXT = (P_NEXT+1)**2 -1

    !Pre smoothing
    CALL SMOOTH(U,RHS,SMOOTHER,NG_MULTIGRID,MG_MULTIGRID,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,PRE_ITER,STORAGE)

    !Compute residule
    CALL pA(STORAGE,U,NG_MULTIGRID,MG_MULTIGRID, &
                    & JAC,DXDR,DXDS,DYDR,DYDS,NPMAX,ALL_NEUMANN,pBC)
    !$OMP WORKSHARE
    STORAGE = RHS-STORAGE
    !$OMP END WORKSHARE

    !Restriction
    CALL MULTIGRID_RESTRICTION(RR,STORAGE,P_NEXT,NPMAX_NEXT,NPMAX)

    !Cycle
    DO K = 1,CYCLE_ITER
        CALL MULTIGRID_W_CYCLE(DELTA,RR, &
                            & DELTA((NPMAX+1)*NEL/2+1), RR((NPMAX+1)*NEL/2+1), &
                            & STORAGE, SMOOTHER((NPMAX+1)*NEL+1), &
                            & JAC((NPMAX+1)*NEL+1), DXDR((NPMAX+1)*NEL+1),DXDS((NPMAX+1)*NEL+1), &
                            & DYDR((NPMAX+1)*NEL+1), DYDS((NPMAX+1)*NEL+1), & 
                            & P_NEXT,PRE_ITER,POST_ITER,CYCLE_ITER,NPMAX_NEXT)
    ENDDO

    !Prolongate and store in RR
    CALL MULTIGRID_PROLONGATION(STORAGE,DELTA,P,NPMAX,NPMAX_NEXT)

    !Correct
    !$OMP WORKSHARE
    U = U + STORAGE
    !$OMP END WORKSHARE

    !Post smoothing
    CALL SMOOTH(U,RHS,SMOOTHER,NG_MULTIGRID,MG_MULTIGRID,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,POST_ITER,STORAGE)

    END SUBROUTINE MULTIGRID_W_CYCLE

!--------------------------------------------------------------------------------------------

    SUBROUTINE SMOOTH(U,RHS,SMOOTHER,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NP,ITER,STORAGE)
    USE SIZE, ONLY:NEL
    USE PARAM,ONLY: ALL_NEUMANN
    IMPLICIT NONE

    INTEGER, INTENT(IN)::NG(NEL),MG(NEL),NP,ITER

    DOUBLE PRECISION,INTENT(INOUT):: U((NP+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: RHS((NP+1)*NEL)

    DOUBLE PRECISION,INTENT(INOUT):: STORAGE((NP+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: SMOOTHER((NP+1)*NEL)

    DOUBLE PRECISION,INTENT(IN):: JAC((NP+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR((NP+1)*NEL), DXDS((NP+1)*NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR((NP+1)*NEL), DYDS((NP+1)*NEL)

    INTEGER K

    DO K = 1,ITER
        CALL pA(STORAGE,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                & NP, ALL_NEUMANN, pBC)
        !$OMP WORKSHARE
        U = U + SMOOTHER*(RHS-STORAGE)
        !$OMP END WORKSHARE
    ENDDO

    END SUBROUTINE

!--------------------------------------------------------------------------------------------

    SUBROUTINE MULTIGRID_RESTRICTION(U_C,U,P,NPMAX_C,NPMAX)
    USE SIZE, ONLY: NEL
    USE BASIS, ONLY: INTERP
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS
    IMPLICIT NONE

    INTEGER P, NPMAX, NPMAX_C
    DOUBLE PRECISION U_C(0:NPMAX_C,NEL)
    DOUBLE PRECISION U(0:NPMAX,NEL)

    INTEGER K,N,M, I, J, II, JJ, N_C, M_C

    !$OMP DO PRIVATE(K,I,J,N,M,II,JJ,N_C,M_C)
    DO K = 1, NEL
        N = NG_MULTIGRID(K)
        M = MG_MULTIGRID(K)

        U_C(:,K) = 0.0d0
        IF ((P.LT.N).AND.(P.LT.M)) THEN
            N_C = P
            M_C = P

            DO J=0,M_C
                DO I=0,N_C
                    DO JJ = 0,M
                        DO II = 0,N
                            U_C(I+(N_C+1)*J,K) = U_C(I+(N_C+1)*J,K) + &
                                            U(II+(N+1)*JJ,K)*INTERP(I,II,N_C,N)*INTERP(J,JJ,M_C,M)
                        ENDDO
                    ENDDO
                ENDDO
            ENDDO

            NG_MULTIGRID(K) = N_C
            MG_MULTIGRID(K) = M_C
        ELSEIF (P.LT.N) THEN
            N_C = P
            M_C = M

            DO J=0,M_C
                DO I=0,N_C
                    DO II = 0,N
                        U_C(I+(N_C+1)*J,K) = U_C(I+(N_C+1)*J,K) + &
                                            U(II+(N+1)*J,K)*INTERP(I,II,N_C,N)
                    ENDDO
                ENDDO
            ENDDO

            NG_MULTIGRID(K) = N_C
        ELSEIF (P.LT.M) THEN
            M_C = P
            N_C = N

            DO J=0,M_C
                DO I=0,N_C
                    DO JJ = 0,M
                        U_C(I+(N_C+1)*J,K) = U_C(I+(N_C+1)*J,K) + &
                                            U(I+(N+1)*JJ,K)*INTERP(J,JJ,M_C,M)
                    ENDDO
                ENDDO
            ENDDO

            MG_MULTIGRID(K) = M_C
        ELSE
            M_C = M
            N_C = N
            DO J = 0,M_C
                DO I = 0,N_C
                    U_C(I+(N_C+1)*J,K) = U(I+(N+1)*J,K)
                ENDDO
            ENDDO
        ENDIF
    ENDDO
    !$OMP ENDDO

    CALL SHARE_ORDER_BDRY_BUFFERS(NG_MULTIGRID,MG_MULTIGRID)

    END SUBROUTINE

!-----------------------------------------------------------------------------------------------------------

    SUBROUTINE MULTIGRID_PROLONGATION(U,U_C,P,NPMAX,NPMAX_C)
    USE SIZE, ONLY: NEL, NG, MG 
    USE BASIS, ONLY: INTERP
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS
    IMPLICIT NONE
    
    INTEGER P, NPMAX, NPMAX_C
    DOUBLE PRECISION U(0:NPMAX,NEL)
    DOUBLE PRECISION U_C(0:NPMAX_C,NEL)

    INTEGER K,N,M, I, J, II, JJ, N_C, M_C 

    !$OMP DO PRIVATE(K,I,J,N,M,II,JJ,N_C,M_C)
    DO K = 1, NEL
        N_C = NG_MULTIGRID(K)
        M_C = MG_MULTIGRID(K)

        U(:,K) = 0.0d0
        IF (((M_C.LT.P).AND.(M_C.LT.MG(K))).AND.((N_C.LT.P).AND.(N_C.LT.NG(K)))) THEN
            N = MIN(P,NG(K)) !Prolongate, but not beyond original degree
            M = MIN(P,MG(K))

            !Interpolate to the higher order basis
            DO J = 0,M
                DO I = 0,N
                    DO JJ = 0,M_C
                        DO II = 0,N_C
                            U(I+(N+1)*J,K) = U(I+(N+1)*J,K) & 
                                & + U_C(II+(N_C+1)*JJ,K)*INTERP(II,I,N_C,N)*INTERP(JJ,J,M_C,M)
                        ENDDO
                    ENDDO
                ENDDO
            ENDDO

            NG_MULTIGRID(K) = N
            MG_MULTIGRID(K) = M
        ELSEIF ((N_C.LT.P).AND.(N_C.LT.NG(K))) THEN
            N = MIN(P,NG(K)) !Prolongate, but not beyond original degree
            M = M_C

            !Interpolate to the higher order basis
            DO J = 0,M
                DO I = 0,N
                    DO II = 0,N_C
                        U(I+(N+1)*J,K) = U(I+(N+1)*J,K) + U(II+(N_C+1)*J,K)*INTERP(II,I,N_C,N)
                    ENDDO
                ENDDO
            ENDDO

            NG_MULTIGRID(K) = N
        ELSEIF ((M_C.LT.P).AND.(M_C.LT.MG(K))) THEN
            M = MIN(P,MG(K)) !Prolongate, but not beyond original degree
            N = N_C

            !Interpolate to the higher order basis
            DO J = 0,M
                DO I = 0,N
                    DO JJ = 0,M_C
                        U(I+(N+1)*J,K) = U(I+(N+1)*J,K) + U(I+(N+1)*JJ,K)*INTERP(JJ,J,M_C,M)
                    ENDDO
                ENDDO
            ENDDO

            MG_MULTIGRID(K) = M
        ELSE
            N = N_C
            M = M_C

            DO J = 0,M
                DO I = 0,N
                    U(I+(N+1)*J,K) = U_C(I+(N_C+1)*J,K)
                ENDDO
            ENDDO
        ENDIF
    ENDDO
    !$OMP ENDDO

    CALL SHARE_ORDER_BDRY_BUFFERS(NG_MULTIGRID,MG_MULTIGRID)

    END SUBROUTINE

!------------------------------------------------------------------------------------------------------------

    SUBROUTINE MULTIGRID_BUILD_SMOOTHERS_AND_METRICS(JAC,DXDR,DXDS,DYDR,DYDS)
    USE SIZE, ONLY: NEL, NMMAX, NPMAX
    IMPLICIT NONE

    INTEGER P, INDEX, NPMAX_NEXT
    INTEGER K

    DOUBLE PRECISION JAC((NPMAX+1)*NEL*2)
    DOUBLE PRECISION DXDR((NPMAX+1)*NEL*2), DXDS((NPMAX+1)*NEL*2)
    DOUBLE PRECISION DYDR((NPMAX+1)*NEL*2), DYDS((NPMAX+1)*NEL*2)

    P = NMMAX
    CALL MULTIGRID_SMOOTHER_JACOBI(MULTIGRID_SMOOTHER,NG_MULTIGRID,MG_MULTIGRID,&
                                    JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)
    !CALL MULTIGRID_SMOOTHER_MASS(MULTIGRID_SMOOTHER,NG_MULTIGRID,MG_MULTIGRID,JAC,NPMAX)
    INDEX = (NPMAX+1)*NEL + 1

    DO
        IF (P==1) EXIT

        P = FLOOR(REAL(P+1)/SQRT(2.0)-1.0)
        NPMAX_NEXT = (P+1)**2 -1
        
        !Restrict orders for next level
        !$OMP DO PRIVATE(K)
        DO K = 1,NEL
            IF (P.LT.NG_MULTIGRID(K)) NG_MULTIGRID(K) = P
            IF (P.LT.MG_MULTIGRID(K)) MG_MULTIGRID(K) = P
        ENDDO
        !$OMP ENDDO
        
        CALL MULTIGRID_METRICS(JAC(INDEX),DXDR(INDEX),DXDS(INDEX),DYDR(INDEX),DYDS(INDEX), &
                               & NG_MULTIGRID, MG_MULTIGRID, NPMAX_NEXT)
        CALL MULTIGRID_SMOOTHER_JACOBI(MULTIGRID_SMOOTHER(INDEX),NG_MULTIGRID,MG_MULTIGRID,&
                            & JAC(INDEX),DXDR(INDEX),DXDS(INDEX),DYDR(INDEX),DYDS(INDEX),NPMAX_NEXT)
        !CALL MULTIGRID_SMOOTHER_MASS(MULTIGRID_SMOOTHER(INDEX),NG_MULTIGRID,MG_MULTIGRID,JAC(INDEX),NPMAX_NEXT)
        
        INDEX = INDEX + (NPMAX_NEXT+1)*NEL 
    ENDDO

    END SUBROUTINE

!---------------------------------------------------------------------------------

    SUBROUTINE MULTIGRID_METRICS(JAC,DXDR,DXDS,DYDR,DYDS,NG,MG,NPMAX)
    USE SIZE, ONLY: NEL
    USE MESH, ONLY: X, Y, QUADMAP
    USE BASIS, ONLY: QUAD
    IMPLICIT NONE

    INTEGER NG(NEL), MG(NEL), NPMAX
    DOUBLE PRECISION JAC(0:NPMAX,NEL)
    DOUBLE PRECISION DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    INTEGER K, I, J, N ,M

    !$OMP DO PRIVATE(K,I,J,N,M)
    DO K =1,NEL
        N = NG(K)
        M = MG(K)
        DO J=0,M
            DO I=0,N
                CALL QUADMAP(DXDR(I+(N+1)*J,K),DXDS(I+(N+1)*J,K), &
                             DYDR(I+(N+1)*J,K),DYDS(I+(N+1)*J,K), &
                             QUAD(I,N),QUAD(J,M),X(:,K),Y(:,K))    
                JAC(I+(N+1)*J,K) = DXDR(I+(N+1)*J,K)*DYDS(I+(N+1)*J,K) &
                                  -DXDS(I+(N+1)*J,K)*DYDR(I+(N+1)*J,K)
            ENDDO
        ENDDO
    ENDDO
    !$OMP ENDDO

    END SUBROUTINE


!--------------------------------------------------------------------------------

    SUBROUTINE MULTIGRID_SMOOTHER_JACOBI(R,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)
    USE SIZE, ONLY: NEL, NMMAX
    USE MESH, ONLY: SCAL, NX,NY, NEIGHBOUR_ID
    USE FIELDS, ONLY: TAU
    USE BASIS, ONLY: W, INTERP_ENDPOINTS, DER
    IMPLICIT NONE

    INTEGER NG(NEL), MG(NEL), NPMAX
    DOUBLE PRECISION R(0:NPMAX,NEL)

    DOUBLE PRECISION JAC(0:NPMAX,NEL)
    DOUBLE PRECISION DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    INTEGER I,J,N,M,K, KK, SIDE
    DOUBLE PRECISION TAU_L

    DOUBLE PRECISION AU(0:NPMAX), UQ_SURFACE(0:NPMAX)
    DOUBLE PRECISION FACTOR(1), DUMMY(1)

    DOUBLE PRECISION OMEGA 

    !Weighted interations. Weighting is dependent on the polynomial order. 
    ! This value of omega was determined experimentally. 
    OMEGA = 2.0d0/DBLE(NMMAX+2)

    !$OMP DO PRIVATE(K,I,J,N,M,KK,TAU_L,SIDE,UQ_SURFACE,AU)
    DO K = 1,NEL
        N = NG(K)
        M = MG(K)

        DO J = 0,M
            DO I = 0,N
                !Compute and Volume integral
                AU(I+(N+1)*J) = 2.0d0*(DYDS(I+(N+1)*J,K)*DYDR(I+(N+1)*J,K)+DXDS(I+(N+1)*J,K)*DXDR(I+(N+1)*J,K))&
                                            *DER(I+(N+1)*I,N)*DER(J+(M+1)*J,M)*W(I,N)*W(J,M)/JAC(I+(N+1)*J,K)

                DO KK = 0,N
                    AU(I+(N+1)*J) = AU(I+(N+1)*J) -(DYDS(KK+(N+1)*J,K)**2 + DXDS(KK+(N+1)*J,K)**2) &
                                    *(DER(KK+(N+1)*I,N)**2)*W(KK,N)*W(J,M)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    AU(I+(N+1)*J) = AU(I+(N+1)*J) -(DYDR(I+(N+1)*KK,K)**2 + DXDR(I+(N+1)*KK,K)**2) &
                                    *(DER(KK+(M+1)*J,M)**2)*W(I,N)*W(KK,M)/JAC(I+(N+1)*KK,K)
                ENDDO
            ENDDO 
        ENDDO

        TAU_L = TAU(K)

        DO SIDE = 1,4
            SELECT CASE (SIDE)
            CASE (1)
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = -TAU_L*W(I,N)*(INTERP_ENDPOINTS(J,0,M)**2) &
                                             +(NX(1,K)*DYDS(I+(N+1)*J,K)-NY(1,K)*DXDS(I+(N+1)*J,K)) &
                                             *DER(I+(N+1)*I,N)*W(I,N)*(INTERP_ENDPOINTS(J,0,M)**2)/JAC(I+(N+1)*J,K)
                        DO KK = 0,M
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            +(-NX(1,K)*DYDR(I+(N+1)*KK,K)+NY(1,K)*DXDR(I+(N+1)*KK,K))&
                                             *DER(KK+(M+1)*J,M)*INTERP_ENDPOINTS(KK,0,M)&
                                             *W(I,N)*INTERP_ENDPOINTS(J,0,M)/JAC(I+(N+1)*KK,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(1,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            CASE (2) 
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = -TAU_L*W(J,M)*(INTERP_ENDPOINTS(I,1,N)**2) &
                                             +(-NX(2,K)*DYDR(I+(N+1)*J,K)+NY(2,K)*DXDR(I+(N+1)*J,K))&
                                             *DER(J+(M+1)*J,M)*W(J,M)*(INTERP_ENDPOINTS(I,1,N)**2)/JAC(I+(N+1)*J,K)
                        DO KK = 0,N
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                             +(NX(2,K)*DYDS(KK+(N+1)*J,K)-NY(2,K)*DXDS(KK+(N+1)*J,K))&
                                             *DER(KK+(N+1)*I,N)*INTERP_ENDPOINTS(KK,1,N) &
                                             *W(J,M)*INTERP_ENDPOINTS(I,1,N)/JAC(KK+(N+1)*J,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(2,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            CASE (3)
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = -TAU_L*W(N-I,N)*(INTERP_ENDPOINTS(J,1,M)**2) &
                                             +(NX(3,K)*DYDS(I+(N+1)*J,K)-NY(3,K)*DXDS(I+(N+1)*J,K))&
                                             *DER(I+(N+1)*I,N)*W(N-I,N)*(INTERP_ENDPOINTS(J,1,M)**2)/JAC(I+(N+1)*J,K)
                        DO KK = 0,M
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                            +(-NX(3,K)*DYDR(I+(N+1)*KK,K)+NY(3,K)*DXDR(I+(N+1)*KK,K))&
                                             *DER(KK+(M+1)*J,M)*INTERP_ENDPOINTS(KK,1,M)&
                                             *W(N-I,N)*INTERP_ENDPOINTS(J,1,M)/JAC(I+(N+1)*KK,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(3,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            CASE (4) 
                DO J = 0,M
                    DO I = 0,N
                        UQ_SURFACE(I+(N+1)*J) = -TAU_L*W(M-J,M)*(INTERP_ENDPOINTS(I,0,N)**2) &
                                             +(-NX(4,K)*DYDR(I+(N+1)*J,K)+NY(4,K)*DXDR(I+(N+1)*J,K))&
                                             *DER(J+(M+1)*J,M)*W(M-J,M)*(INTERP_ENDPOINTS(I,0,N)**2)/JAC(I+(N+1)*J,K)
                        DO KK = 0,N
                            UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                             +(NX(4,K)*DYDS(KK+(N+1)*J,K)-NY(4,K)*DXDS(KK+(N+1)*J,K))&
                                             *DER(KK+(N+1)*I,N)*INTERP_ENDPOINTS(KK,0,N)&
                                             *W(M-J,M)*INTERP_ENDPOINTS(I,0,N)/JAC(KK+(N+1)*J,K)
                        ENDDO
                        UQ_SURFACE(I+(N+1)*J) = SCAL(4,K)*UQ_SURFACE(I+(N+1)*J)
                    ENDDO
                ENDDO
            END SELECT

            IF (NEIGHBOUR_ID(SIDE,1,K).LT.0) THEN
                !Call the prescribed homogeneous boundary condition with U_L = 1 and Q_L = 0
                !On Dirchlet boundary this will return FACTOR=-1
                !On Nuemann boundary this will return FACTOR=1
                CALL pBC(DUMMY,[0.0d0],FACTOR,[1.0d0],K,SIDE,0)
                FACTOR = (1.0d0-FACTOR) !Dirichlet BCs are doubled, Neumann BCs are zero

                UQ_SURFACE = FACTOR(1)*UQ_SURFACE !Double the edge contributions on all Dirichlet boundaries
            ENDIF

            DO J = 0,M
                DO I = 0,N
                    AU(I+(N+1)*J) = AU(I+(N+1)*J) + UQ_SURFACE(I+(N+1)*J) 
                ENDDO
            ENDDO 
        ENDDO

        DO J = 0,M
            DO I =0,N
                R(I+(N+1)*J,K) = OMEGA/(AU(I+(N+1)*J))
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE MULTIGRID_SMOOTHER_JACOBI

!--------------------------------------------------------------------------

!    SUBROUTINE MULTIGRID_SMOOTHER_MASS(R,NG,MG,JAC,NPMAX)
!    USE SIZE, ONLY: NEL
!    USE PARAM, ONLY: DT_MAX 
!    USE MESH, ONLY: SCAL
!    USE BASIS, ONLY: W
!    USE TIMESTEP, ONLY: DT
!    IMPLICIT NONE
!
!    INTEGER NG(NEL),MG(NEL),NPMAX
!    DOUBLE PRECISION R(0:NPMAX,NEL)
!    DOUBLE PRECISION JAC(0:NPMAX,NEL)
!
!    INTEGER I,J,N,M,K
!    DOUBLE PRECISION D
!
!    !$OMP MASTER
!    DT = DT_MAX
!    !$OMP END MASTER
!    !$OMP BARRIER
!
!    !$OMP DO PRIVATE(N,M,I,J,K,D) SCHEDULE(DYNAMIC) REDUCTION(MIN:DT)
!    DO K = 1,NEL
!        N = NG(K)
!        M = MG(K)
!
!        D = MAX((1.0d0/SCAL(1,K))**2,(1.0d0/SCAL(2,K))**2)
!
!        !Compute min of the inverse in the cell K
!        D = 32.0d0*DBLE((N+1)*(M+1))**(1.5d0)*D
!        DT = MIN(DT,1.0d0/D)  
!    ENDDO
!    !$OMP END DO
!
!    !$OMP DO PRIVATE(I,J,K,N,M) SCHEDULE(DYNAMIC)
!    DO K = 1,NEL
!        N = NG(K)
!        M = MG(K)
!        DO I =0,N
!            DO J = 0,M
!                R(I+(N+1)*J,K) = -DT/(JAC(I+(N+1)*J,K)*W(I,N)*W(J,M))
!            ENDDO
!        ENDDO
!    ENDDO
!    !$OMP ENDDO
!
!    END SUBROUTINE MULTIGRID_SMOOTHER_MASS

!------------------------------------------------------------------------------

    SUBROUTINE MULTIGRID_CONJ_GRAD_SOLVE(U,RHS,CG_R,CG_P,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,STORAGE,NP)
    USE SIZE, ONLY: NEL
    USE PARAM, ONLY: ALL_NEUMANN, ITER_PRES
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER, INTENT(IN)::NG(NEL),MG(NEL), NP

    DOUBLE PRECISION,INTENT(INOUT):: U(0:NP,NEL)
    DOUBLE PRECISION,INTENT(IN):: RHS(0:NP,NEL)

    DOUBLE PRECISION,INTENT(INOUT):: STORAGE(0:NP,NEL)
    DOUBLE PRECISION,INTENT(INOUT):: CG_R(0:NP,NEL)
    DOUBLE PRECISION,INTENT(INOUT):: CG_P(0:NP,NEL)

    DOUBLE PRECISION,INTENT(IN):: JAC(0:NP,NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR(0:NP,NEL), DXDS(0:NP,NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR(0:NP,NEL), DYDS(0:NP,NEL)

    DOUBLE PRECISION, PARAMETER:: TOL = 1.0d-8
    INTEGER K,I,J,N,M, IERROR

    !Compute residual
    CALL pA(STORAGE,U,NG,MG, &
                    & JAC,DXDR,DXDS,DYDR,DYDS,NP,ALL_NEUMANN,pBC)

    !$OMP WORKSHARE
    CG_R = RHS-STORAGE
    !$OMP END WORKSHARE

    !$OMP WORKSHARE
    CG_P = CG_R
    !$OMP END WORKSHARE

    !$OMP MASTER 
    CG_R_DOT_R = 0.0d0
    CG_PP_DOT_APP = 0.0d0
    CG_R_DOT_R_NEW = 0.0d0
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO PRIVATE(I,J,N,M) REDUCTION(+:CG_R_DOT_R) SCHEDULE(DYNAMIC)
    DO K = 1,NEL
        N = NG(K)
        M = MG(K)
        DO I = 0,N
            DO J=0,M
                CG_R_DOT_R = CG_R_DOT_R + CG_R(I+(N+1)*J,K)**2
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    !$OMP MASTER
    CALL MPI_ALLREDUCE(CG_R_DOT_R, CG_R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
    !$OMP END MASTER
    !$OMP BARRIER

    !........MAIN LOOP
    !Conjugate Gradient method
    DO  
        !Compute residule
        CALL pA(STORAGE,CG_P,NG,MG, &
                    & JAC,DXDR,DXDS,DYDR,DYDS,NP,ALL_NEUMANN,pBC)

        !$OMP DO PRIVATE(I,J,N,M) REDUCTION(+:CG_PP_DOT_APP) SCHEDULE(STATIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    CG_PP_DOT_APP = CG_PP_DOT_APP + CG_P(I+(N+1)*J,K)*STORAGE(I+(N+1)*J,K)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP MASTER
        CALL MPI_ALLREDUCE(CG_PP_DOT_APP, CG_PP_DOT_APP, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP WORKSHARE
        U    =    U + (CG_R_DOT_R/CG_PP_DOT_APP)*CG_P
        CG_R = CG_R - (CG_R_DOT_R/CG_PP_DOT_APP)*STORAGE
        !$OMP END WORKSHARE

        !$OMP DO PRIVATE(I,J,N,M) REDUCTION(+:CG_R_DOT_R_NEW) SCHEDULE(STATIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    CG_R_DOT_R_NEW = CG_R_DOT_R_NEW + CG_R(I+(N+1)*J,K)**2
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP MASTER
        CALL MPI_ALLREDUCE(CG_R_DOT_R_NEW, CG_R_DOT_R_NEW, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        ITER_PRES = ITER_PRES +1
        !$OMP END MASTER
        !$OMP BARRIER

         IF (DSQRT(CG_R_DOT_R_NEW) .LT. TOL) EXIT

        !$OMP WORKSHARE
        CG_P = CG_R + (CG_R_DOT_R_NEW/CG_R_DOT_R)*CG_P
        !$OMP END WORKSHARE

        !$OMP MASTER
        CG_R_DOT_R = CG_R_DOT_R_NEW
        CG_PP_DOT_APP = 0.0d0
        CG_R_DOT_R_NEW = 0.0d0
        !$OMP END MASTER   
        !$OMP BARRIER         
    ENDDO

    END SUBROUTINE MULTIGRID_CONJ_GRAD_SOLVE

END MODULE MULTIGRID