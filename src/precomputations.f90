
    SUBROUTINE PRECOMPUTATIONS
    USE SIZE, ONLY: INIT_SIZE
    USE BASIS, ONLY: INIT_BASIS
    USE MESH, ONLY: INIT_METRICS
    USE FIELDS, ONLY: INIT_FIELDS
    USE ADAPT, ONLY: INIT_ADAPT
    USE MULTIGRID, ONLY: INIT_MULTIGRID
    IMPLICIT NONE

    CALL INIT_SIZE
    CALL INIT_BASIS       !.......NODES AND WEIGHTS AND DERIVATIVE MATRICES
    CALL INIT_METRICS     !.......COMPUTE THE METRICS
    CALL INIT_ADAPT       !.......INITIALIZE ADAPTIVITY ARRAYS
    CALL INIT_FIELDS      !.......SET/LOAD INITIAL FLOW CONDITIONS 

    END SUBROUTINE PRECOMPUTATIONS
