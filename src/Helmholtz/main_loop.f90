
    SUBROUTINE MAIN_LOOP
    IMPLICIT NONE

    CALL PRECOND_CONJ_GRAD

    !CALL ITERATIVE

    END SUBROUTINE MAIN_LOOP

    SUBROUTINE PRECOND_CONJ_GRAD
    USE SIZE,ONLY: NEL, NPMAX,NMMAX,NG,MG, RANK, NUM_EQN
    USE PARAM, ONLY: ITERMAX, TOLVEL, ITER_VEL,FRAME,MAXFRAMES,IFADAPT
    USE FIELDS, ONLY: U, AU, RHS, R, Z, PP, &
                & R_DOT_R, R_DOT_R_INIT, PP_DOT_APP, &
                & R_DOT_Z, R_DOT_Z_NEW, UQ_HOMOGENEOUS_BOUNDARY, SET_TAU, WRITE_FIELDS, &
                & SET_ADAPT_BACKUP
    USE MESH, ONLY: JAC,DXDR,DXDS,DYDR,DYDS
    USE MULTIGRID, ONLY: INIT_MULTIGRID, MULTIGRID_PRECONDITIONER
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS, SHARE_TAU_BDRY_BUFFERS
    !$ USE OMP_LIB
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTERFACE 
        SUBROUTINE DGPOISSON(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                        & NPMAX,ALL_NEUMANN,BC)
        USE SIZE, ONLY: NEL
        IMPLICIT NONE

        INTEGER,INTENT(IN):: NPMAX,NG(NEL),MG(NEL)
        DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
        DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)
        
        DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
        DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
        DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

        LOGICAL,INTENT(IN):: ALL_NEUMANN

        INTERFACE
            SUBROUTINE BC(Q_R,Q_L,U_R,U_L,K,SIDE,N)
            USE MESH, ONLY:BDRY_TYPE
            IMPLICIT NONE

            INTEGER,INTENT(IN):: N, K, SIDE
            DOUBLE PRECISION,INTENT(IN)::  U_L(0:N), Q_L(0:N)
            DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)
            END SUBROUTINE BC
        END INTERFACE 

        END SUBROUTINE DGPOISSON
    END INTERFACE

    INTEGER I, J, K, N, M, MM, IERROR, ADAPT_ITER, ADAPT_ITERMAX
    REAL CPU_START,CPU_FINAL,PERCENT

    DOUBLE PRECISION SIGMA

    ADAPT_ITERMAX = 10 !Maximum Adaptation iterations

    CALL GET_SIGMA(SIGMA)

    !$OMP PARALLEL DEFAULT(SHARED)

    !$OMP MASTER
    IF (RANK.EQ.0) WRITE(*,'("Computing...")')
    FRAME = 0
    ADAPT_ITER = 0
    CALL CPU_TIME(CPU_START)
    !$ CPU_START = REAL(OMP_GET_WTIME())
    !$OMP END MASTER
    !$OMP BARRIER
    
    DO  
        FRAME = MAXFRAMES*ADAPT_ITER+1
        PERCENT = 0.0      
        CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
        CALL SHARE_TAU_BDRY_BUFFERS

        CALL INIT_MULTIGRID(DGPOISSON,UQ_HOMOGENEOUS_BOUNDARY)

        DO MM = 1,NUM_EQN
            !Compute RHS
            CALL DGHELMHOLTZ_RHS(RHS,MM)

            !Compute residual
            CALL DGHELMHOLTZ(AU,U(:,:,MM),SIGMA,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

            !$OMP WORKSHARE
            R = RHS-AU
            Z = 0.0d0
            !$OMP END WORKSHARE
            CALL MULTIGRID_PRECONDITIONER(Z,R)

            !$OMP WORKSHARE
            PP = Z
            !$OMP END WORKSHARE

            !$OMP MASTER 
            R_DOT_R = 0.0d0
            R_DOT_Z = 0.0d0
            PP_DOT_APP = 0.0d0
            R_DOT_Z_NEW = 0.0d0
            !$OMP END MASTER
            !$OMP BARRIER

            !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_R) REDUCTION(+:R_DOT_Z) SCHEDULE(DYNAMIC)
            DO K = 1,NEL
                N = NG(K)
                M = MG(K)
                DO I = 0,N
                    DO J = 0,M
                        R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                        R_DOT_Z = R_DOT_Z + R(I+(N+1)*J,K)*Z(I+(N+1)*J,K)
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO

            !$OMP MASTER
            CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            CALL MPI_ALLREDUCE(R_DOT_Z, R_DOT_Z, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            ITER_VEL = 0
            R_DOT_R_INIT = DSQRT(R_DOT_R)
            R_DOT_R = 0.0d0
            CALL WRITE_FIELDS(0,R_DOT_R_INIT)
            !$OMP END MASTER
            !$OMP BARRIER

            !........MAIN LOOP
            !Preconditioned Conjugate Gradient method
            DO  
                CALL DGHELMHOLTZ(AU,PP,SIGMA,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

                !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:PP_DOT_APP) SCHEDULE(STATIC)
                DO K = 1,NEL
                    N = NG(K)
                    M = MG(K)
                    DO I = 0,N
                        DO J = 0,M
                            PP_DOT_APP = PP_DOT_APP + PP(I+(N+1)*J,K)*AU(I+(N+1)*J,K)
                        ENDDO
                    ENDDO
                ENDDO
                !$OMP END DO
                !$OMP MASTER
                CALL MPI_ALLREDUCE(PP_DOT_APP, PP_DOT_APP, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
                !$OMP END MASTER
                !$OMP BARRIER

                !$OMP WORKSHARE
                U(:,:,MM) = U(:,:,MM) + (R_DOT_Z/PP_DOT_APP)*PP
                R = R - (R_DOT_Z/PP_DOT_APP)*AU
                !$OMP END WORKSHARE
                
                !Apply preconditioner
                !$OMP WORKSHARE
                Z= 0.0d0
                !$OMP END WORKSHARE
                CALL MULTIGRID_PRECONDITIONER(Z,R)

                !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_Z_NEW) REDUCTION(+:R_DOT_R) SCHEDULE(STATIC)
                DO K = 1,NEL
                    N = NG(K)
                    M = MG(K)
                    DO I = 0,N
                        DO J = 0,M
                            R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                            R_DOT_Z_NEW = R_DOT_Z_NEW + R(I+(N+1)*J,K)*Z(I+(N+1)*J,K)
                        ENDDO
                    ENDDO
                ENDDO
                !$OMP END DO
                !$OMP MASTER
                CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
                CALL MPI_ALLREDUCE(R_DOT_Z_NEW, R_DOT_Z_NEW, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
                ITER_VEL = ITER_VEL + 1
                !$OMP END MASTER
                !$OMP BARRIER

                IF (DSQRT(DABS(R_DOT_R)) .LT. TOLVEL) THEN
                    !$OMP MASTER 
                    CALL WRITE_FIELDS(ITER_VEL,DSQRT(R_DOT_R))           !.......POST-PROCESS THE RESULTS
                    !$OMP END MASTER
                    !$OMP BARRIER
                    EXIT
                ENDIF
                IF (ITER_VEL .GT. ITERMAX) STOP "ITERMAX Reached"

                !$OMP WORKSHARE
                PP = Z + (R_DOT_Z_NEW/R_DOT_Z)*PP
                !$OMP END WORKSHARE

                !$OMP MASTER
                PERCENT = REAL(ITER_VEL*100)/REAL(ITERMAX)
                IF (PERCENT.GE. REAL((FRAME-MAXFRAMES*ADAPT_ITER)*100)/REAL(MAXFRAMES)) CALL WRITE_FIELDS(ITER_VEL,DSQRT(R_DOT_R))   
                R_DOT_Z = R_DOT_Z_NEW
                PP_DOT_APP = 0.0d0
                R_DOT_Z_NEW = 0.0d0
                R_DOT_R = 0.0d0
                !$OMP END MASTER   
                !$OMP BARRIER         
            ENDDO
        ENDDO

        IF (IFADAPT) THEN
            CALL SET_ADAPT_BACKUP
            CALL ADAPTMESH
            CALL SET_TAU(NG,MG)
            !$OMP MASTER
            ADAPT_ITER = ADAPT_ITER + 1
            !$OMP END MASTER
            !$OMP BARRIER
            IF (ADAPT_ITER.EQ.ADAPT_ITERMAX) EXIT
        ELSE
            EXIT
        ENDIF
    ENDDO
    !$OMP END PARALLEL

    CALL CPU_TIME(CPU_FINAL)
    !$ CPU_FINAL = REAL(OMP_GET_WTIME())
    IF (RANK.EQ.0) THEN
        WRITE(*,*)
        PRINT '("Time = ",f6.3," seconds.")',CPU_FINAL-CPU_START
    ENDIF

    RETURN
    END SUBROUTINE PRECOND_CONJ_GRAD

!------------------------------------------------------------------------

    SUBROUTINE ITERATIVE
    USE SIZE,ONLY: NEL, NPMAX,NMMAX,NG,MG, RANK, NUM_EQN
    USE PARAM, ONLY: ITERMAX, TOLVEL, ITER_VEL,FRAME,MAXFRAMES,IFADAPT
    USE FIELDS, ONLY: U, AU, RHS, R, &
                & R_DOT_R, &
                & UQ_HOMOGENEOUS_BOUNDARY, SET_TAU, WRITE_FIELDS
    USE MESH, ONLY: JAC,DXDR,DXDS,DYDR,DYDS
    USE MULTIGRID, ONLY: INIT_MULTIGRID, MULTIGRID_PRECONDITIONER
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS, SHARE_TAU_BDRY_BUFFERS
    !$ USE OMP_LIB
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTERFACE 
        SUBROUTINE DGPOISSON(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                        & NPMAX,ALL_NEUMANN,BC)
        USE SIZE, ONLY: NEL
        IMPLICIT NONE

        INTEGER,INTENT(IN):: NPMAX,NG(NEL),MG(NEL)
        DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
        DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)
        
        DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
        DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
        DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

        LOGICAL,INTENT(IN):: ALL_NEUMANN

        INTERFACE
            SUBROUTINE BC(Q_R,Q_L,U_R,U_L,K,SIDE,N)
            USE MESH, ONLY:BDRY_TYPE
            IMPLICIT NONE

            INTEGER,INTENT(IN):: N, K, SIDE
            DOUBLE PRECISION,INTENT(IN)::  U_L(0:N), Q_L(0:N)
            DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)
            END SUBROUTINE BC
        END INTERFACE 

        END SUBROUTINE DGPOISSON
    END INTERFACE

    INTEGER I, J, K, N, M, MM, IERROR, ADAPT_ITER, ADAPT_ITERMAX
    REAL CPU_START,CPU_FINAL,PERCENT

    DOUBLE PRECISION SIGMA

    ADAPT_ITERMAX = 10 !Adaptation iterations

    CALL GET_SIGMA(SIGMA)

    !$OMP PARALLEL DEFAULT(SHARED)

    !$OMP MASTER
    IF (RANK.EQ.0) WRITE(*,'("Computing...")')
    ADAPT_ITER = 0
    CALL CPU_TIME(CPU_START)
    !$ CPU_START = REAL(OMP_GET_WTIME())
    !$OMP END MASTER
    !$OMP BARRIER
    DO
        FRAME = MAXFRAMES*ADAPT_ITER
        PERCENT = 0.0      
        CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
        CALL SHARE_TAU_BDRY_BUFFERS
        CALL INIT_MULTIGRID(DGPOISSON,UQ_HOMOGENEOUS_BOUNDARY)

        DO MM = 1,NUM_EQN
            !Compute RHS
            CALL DGHELMHOLTZ_RHS(RHS,MM)

            !Compute RHS
            CALL DGHELMHOLTZ(AU,U(:,:,MM),SIGMA,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

            !$OMP WORKSHARE
            R = RHS-AU
            !$OMP END WORKSHARE

            !$OMP MASTER 
            R_DOT_R = 0.0d0
            !$OMP END MASTER
            !$OMP BARRIER

            !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_R) SCHEDULE(DYNAMIC)
            DO K = 1,NEL
                N = NG(K)
                M = MG(K)
                DO I = 0,N
                    DO J = 0,M
                        R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO

            !$OMP MASTER
            CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            ITER_VEL = 0
            CALL WRITE_FIELDS(0,DSQRT(R_DOT_R))
            !$OMP END MASTER
            !$OMP BARRIER

            !Basic relaxation iterative method for testing purposes
            DO 
                IF ((DSQRT(R_DOT_R) .LT. TOLVEL).OR.(ITER_VEL .GT. ITERMAX)) THEN
                    !$OMP MASTER 
                    CALL WRITE_FIELDS(ITER_VEL,DSQRT(R_DOT_R))           !.......POST-PROCESS THE RESULTS
                    !$OMP END MASTER
                    !$OMP BARRIER
                    EXIT
                ENDIF

                CALL MULTIGRID_PRECONDITIONER(U(:,:,MM),RHS)

                CALL DGHELMHOLTZ(AU,U(:,:,MM),SIGMA,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

                !$OMP WORKSHARE
                R = RHS-AU
                !$OMP END WORKSHARE

                !DO K =1,NEL
                !    N = NG(K)
                !    M = MG(K)
                !    DO J = 0,M
                !        DO I =0,N
                !            U(I+(N+1)*J,K) = U(I+(N+1)*J,K) + SMOOTHER(I+(N+1)*J,K)*R(I+(N+1)*J,K)
                !        ENDDO
                !    ENDDO
                !ENDDO

                !$OMP MASTER
                R_DOT_R = 0.0d0
                !$OMP END MASTER
                !$OMP BARRIER
                !$OMP DO PRIVATE(N,M) REDUCTION(+:R_DOT_R) SCHEDULE(STATIC)
                DO K = 1,NEL
                    N = NG(K)
                    M = MG(K)
                    DO I = 0,N
                        DO J = 0,M
                            R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                        ENDDO
                    ENDDO
                ENDDO
                !$OMP END DO
                !$OMP MASTER
                CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
                ITER_VEL = ITER_VEL + 1
                !$OMP END MASTER
                !$OMP BARRIER

                !$OMP MASTER
                PERCENT = REAL(ITER_VEL*100)/REAL(ITERMAX)
                IF (PERCENT.GE. REAL((FRAME-MAXFRAMES*ADAPT_ITER)*100)/REAL(MAXFRAMES)) CALL WRITE_FIELDS(ITER_VEL,DSQRT(R_DOT_R))   
                !$OMP END MASTER   
                !$OMP BARRIER         
            ENDDO
        ENDDO

        IF (IFADAPT) THEN
            IF (ADAPT_ITER.EQ.ADAPT_ITERMAX) EXIT
            !$OMP MASTER
            ADAPT_ITER = ADAPT_ITER + 1
            !$OMP END MASTER
            !$OMP BARRIER
            CALL ADAPTMESH
            CALL SET_TAU(NG,MG)
        ELSE
            EXIT
        ENDIF
    ENDDO
    !$OMP END PARALLEL

    CALL CPU_TIME(CPU_FINAL)
    !$ CPU_FINAL = REAL(OMP_GET_WTIME())
    IF (RANK.EQ.0) THEN
        WRITE(*,*)
        PRINT '("Time = ",f6.3," seconds.")',CPU_FINAL-CPU_START
    ENDIF

    END SUBROUTINE ITERATIVE


