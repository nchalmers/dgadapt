MODULE FIELDS
IMPLICIT NONE

DOUBLE PRECISION, ALLOCATABLE::U(:,:,:)

DOUBLE PRECISION, ALLOCATABLE::AU(:,:)
DOUBLE PRECISION, ALLOCATABLE::RHS(:,:)
DOUBLE PRECISION, ALLOCATABLE::DUDX(:,:), DUDY(:,:)
DOUBLE PRECISION, ALLOCATABLE::TAU(:)

DOUBLE PRECISION, ALLOCATABLE::R (:,:)
DOUBLE PRECISION, ALLOCATABLE::Z (:,:)
DOUBLE PRECISION, ALLOCATABLE::PP(:,:)

DOUBLE PRECISION R_DOT_R, R_DOT_Z, R_DOT_Z_NEW
DOUBLE PRECISION R_DOT_R_INIT, PP_DOT_APP, SS

DOUBLE PRECISION, ALLOCATABLE::U_ADAPT_STORAGE(:,:,:)
CONTAINS
    SUBROUTINE ALLOCATE_FIELDS
    USE SIZE, ONLY: NUM_EQN, NELMAX, NPMAX
    USE PARAM, ONLY: IFADAPT,IFADAPT_PROJ
    IMPLICIT NONE

    ALLOCATE(U(0:NPMAX,NELMAX,NUM_EQN)) 

    ALLOCATE(AU(0:NPMAX,NELMAX))
    ALLOCATE(RHS(0:NPMAX,NELMAX))
    ALLOCATE(DUDX(0:NPMAX,NELMAX))
    ALLOCATE(DUDY(0:NPMAX,NELMAX))
    ALLOCATE(TAU(NELMAX))     

    ALLOCATE(R(0:NPMAX,NELMAX))
    ALLOCATE(Z(0:NPMAX,NELMAX))
    ALLOCATE(PP(0:NPMAX,NELMAX))

    IF (IFADAPT.OR.IFADAPT_PROJ) ALLOCATE(U_ADAPT_STORAGE(0:NPMAX,NELMAX,NUM_EQN))  

    END SUBROUTINE ALLOCATE_FIELDS

!---------------------------------------------------------------------------------------

    SUBROUTINE DEALLOCATE_FIELDS
    USE PARAM, ONLY: IFADAPT, IFADAPT_PROJ
    IMPLICIT NONE

    DEALLOCATE(U)  

    DEALLOCATE(AU)
    DEALLOCATE(RHS)
    DEALLOCATE(DUDX)
    DEALLOCATE(DUDY)
    DEALLOCATE(TAU)   

    DEALLOCATE(R)
    DEALLOCATE(Z)
    DEALLOCATE(PP)

    IF (IFADAPT.OR.IFADAPT_PROJ) DEALLOCATE(U_ADAPT_STORAGE)  

    END SUBROUTINE DEALLOCATE_FIELDS

!--------------------------------------------------------------------------
	
    SUBROUTINE INIT_FIELDS
    !!-----------------------------------------------------------------------
    !!
    !!       Set Initial condition
    !!
    !!-----------------------------------------------------------------------
    USE SIZE, ONLY: NEL,NG,MG,NMAX,MMAX,NMIN,MMIN,LEVEL,RANK
    USE BASIS, ONLY: QUAD
    USE MESH, ONLY: X, Y
    USE PARAM, ONLY: IFADAPT, IFADAPT_PROJ
    IMPLICIT NONE

    INTEGER N,M,I,J,K,LV,ITER
    DOUBLE PRECISION XX, YY

    IF (RANK.EQ.0) WRITE(*,'(32A)',advance='no') "Computing initial projection...."

    !$OMP PARALLEL PRIVATE(LV,ITER) DEFAULT(SHARED)

    !$OMP DO PRIVATE(K,N,M,XX,YY,I,J) SCHEDULE(DYNAMIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K) 
        DO J=0,M
            DO I=0,N
                XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N)+1.0d0)
                YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M)+1.0d0)

                 CALL INITIAL_CONDITION(U(I+(N+1)*J,K,:),XX,YY)
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    IF (IFADAPT_PROJ) THEN
        ITER = MAX(2*LEVEL,2*(NMAX-NMIN),2*(MMAX-MMIN))

        DO LV = 1,ITER
            CALL ADAPTMESH
            !$OMP BARRIER
            
            !Re-project
            !$OMP DO PRIVATE(K,N,M,XX,YY,I,J) SCHEDULE(DYNAMIC)
            DO K=1,NEL
                N = NG(K)
                M = MG(K) 
                DO J=0,M
                    DO I=0,N
                        XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N)+1.0d0)
                        YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M)+1.0d0)
                       CALL INITIAL_CONDITION(U(I+(N+1)*J,K,:),XX,YY)
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO
        ENDDO
    ENDIF

    CALL SET_TAU(NG,MG)

    IF (IFADAPT.OR.IFADAPT_PROJ) THEN
        !$OMP WORKSHARE
        U_ADAPT_STORAGE = U
        !$OMP END WORKSHARE
    ENDIF
    !$OMP END PARALLEL

    IF (RANK.EQ.0) WRITE(*,*) "done"
    IF (RANK.EQ.0) WRITE(*,*)

    END SUBROUTINE INIT_FIELDS

!-----------------------------------------------------------------------

    SUBROUTINE SET_TAU(NG,MG)
    !!----------------------------------------------------------------
	!!
	!!      Set tau paramter in cell K 
	!!      Taken from K. Shahbazi "An explicit expression for the         
	!!      penalty paramter in the internal penalty method"
	!!---------------------------------------------------------------
    USE SIZE, ONLY: NEL
    USE MESH, ONLY: SCAL
    IMPLICIT NONE

    INTEGER, INTENT(IN):: NG(NEL), MG(NEL)

    INTEGER K,NN

    !$OMP DO PRIVATE(K,NN) SCHEDULE(DYNAMIC)
    DO K = 1, NEL
        NN = MAX(NG(K),MG(K))

        !This is a rough bound, there is a tighter one if you check wheither the side is a boundary
        !!!!! THIS ASSUMES PARALLELOGRAM CELLS
        TAU(K) = 0.5d0*DBLE((NN+1)*(NN+2))*(SCAL(1,K)+SCAL(2,K)+SCAL(3,K)+SCAL(4,K))/(2.0d0*SCAL(1,K)*SCAL(2,K))
    ENDDO
    !$OMP END DO

    END SUBROUTINE

!-------------------------------------------------------------------------------------------

    SUBROUTINE UQ_HOMOGENEOUS_BOUNDARY(Q_R,Q_L,U_R,U_L,K,SIDE,N)
    !----------------------------------------------------------------
    !   Homogeneous U boundary conditions for use in DGPOISSON
    !---------------------------------------------------------------
    USE MESH, ONLY:BDRY_TYPE
    IMPLICIT NONE

    INTEGER,INTENT(IN):: N, K, SIDE
    DOUBLE PRECISION,INTENT(IN):: U_L(0:N), Q_L(0:N)
    DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)

    IF (BDRY_TYPE(SIDE,K).EQ.'O  ') THEN !Neumann
        U_R =  U_L
        Q_R = -Q_L
    ELSE !Homogenous Diriclet BCs
        U_R = -U_L 
        Q_R =  Q_L
    ENDIF

    END SUBROUTINE

!---------------------------------------------------------------------------------

    SUBROUTINE WRITE_FIELDS(ITER,ERR)
    USE SIZE, ONLY: RANK, NUM_EQN, NPMAX, NELMAX, NEL, NG, MG, NPROC 
    USE BASIS, ONLY: QUAD_LOBATTO, INTERP_LOBATTO
    USE MESH, ONLY: X,Y
    USE PARAM, ONLY: FRAME
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER, INTENT(IN)::ITER
    DOUBLE PRECISION, INTENT(IN)::ERR

    INTEGER I,J,K,IEL
    INTEGER N,M,IJ,II,JJ

    INTEGER IERROR, STATS(MPI_STATUS_SIZE), PROC, ELEM

    DOUBLE PRECISION XX, YY, U_INTERP(NUM_EQN)

    CHARACTER(LEN=16)FILENAME

    INTEGER NEL_LOCAL
    INTEGER,ALLOCATABLE :: NG_LOCAL(:), MG_LOCAL(:)
    DOUBLE PRECISION,ALLOCATABLE :: U_LOCAL(:,:,:)
    DOUBLE PRECISION,ALLOCATABLE :: X_LOCAL(:,:), Y_LOCAL(:,:)

    FRAME = FRAME +1

    IF (RANK.EQ.0) THEN !Only root process writes
        !.....WRITE TO SCREEN
        WRITE(*, '(60A,''('' I0 '') err = '' F0.8 )',ADVANCE='NO') (CHAR(8),J=1,60), ITER,ERR
      
        !.....WRITE TO FILE
        WRITE(FILENAME,FMT='(''aoutput'',I5.5,''.dat'')') FRAME
        OPEN(18,FILE=FILENAME)

        WRITE(18,FMT='(''variables="X","Y",'')',advance='no')

        ALLOCATE(NG_LOCAL(NELMAX))
        ALLOCATE(MG_LOCAL(NELMAX))
        ALLOCATE(U_LOCAL(0:NPMAX,NELMAX,NUM_EQN))
        ALLOCATE(X_LOCAL(4,NELMAX))
        ALLOCATE(Y_LOCAL(4,NELMAX))

        NEL_LOCAL = NEL
        NG_LOCAL = NG
        MG_LOCAL = MG
        U_LOCAL = U
        X_LOCAL = X
        Y_LOCAL = Y
        
        IF (NUM_EQN.GT.1) THEN
            DO K = 1,NUM_EQN-1
                WRITE(18,FMT='(''"U'',I1,''",'')',advance='no') K
            ENDDO
        ENDIF
        WRITE(18,FMT='(''"U'',I1,''"'')') NUM_EQN

        ELEM = 1
        DO PROC = 0,NPROC-1
            IF (PROC.NE.0) THEN
                CALL MPI_RECV(NEL_LOCAL, 1, MPI_INTEGER, PROC, 0, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(NG_LOCAL, NEL_LOCAL, MPI_INTEGER, PROC, 1, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(MG_LOCAL, NEL_LOCAL, MPI_INTEGER, PROC, 2, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(U_LOCAL, NUM_EQN*(NPMAX+1)*NELMAX, MPI_DOUBLE_PRECISION, &
                                                    PROC, 3, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(X_LOCAL, 4*NEL_LOCAL, MPI_DOUBLE_PRECISION, PROC, 4, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(Y_LOCAL, 4*NEL_LOCAL, MPI_DOUBLE_PRECISION, PROC, 5, MPI_COMM_WORLD, STATS, IERROR)
            ENDIF

            DO IEL=1,NEL_LOCAL
                N = NG_LOCAL(IEL)
                M = MG_LOCAL(IEL)

                WRITE(18,FMT='(''zone t="IEL'',I6,''",i='',I2,'',j='',I2, &
                             & '',SOLUTIONTIME='',I0)') ELEM,N+1,M+1,ITER
                ELEM = ELEM+1

                DO J=0,M
                    DO I=0,N
                        XX = X_LOCAL(1,IEL) + (X_LOCAL(2,IEL)-X_LOCAL(1,IEL))*0.5d0*(QUAD_LOBATTO(I,N)+1.0d0)
                        YY = Y_LOCAL(1,IEL) + (Y_LOCAL(3,IEL)-Y_LOCAL(1,IEL))*0.5d0*(QUAD_LOBATTO(J,M)+1.0d0)
                        IJ = I + (N+1)*J

                        U_INTERP = 0.0d0
                        DO JJ = 0,M
                            DO II = 0,N
                                U_INTERP(:) = U_INTERP(:) & 
                                        + U_LOCAL(II+(N+1)*JJ,IEL,:)*INTERP_LOBATTO(II,I,N)*INTERP_LOBATTO(JJ,J,M)
                            ENDDO
                        ENDDO

                        WRITE(18,FMT='(2F20.12)',advance='no') XX, YY
                        IF (NUM_EQN.GT.1) THEN
                            DO K = 1,NUM_EQN-1
                                WRITE(18,FMT='(F20.12)',advance='no') U_INTERP(K)
                            ENDDO
                        ENDIF
                        WRITE(18,FMT='(F20.12)') U_INTERP(NUM_EQN)
                    END DO
                END DO
            END DO
        ENDDO
        CLOSE(18)

        DEALLOCATE(NG_LOCAL)
        DEALLOCATE(MG_LOCAL)
        DEALLOCATE(U_LOCAL)
        DEALLOCATE(X_LOCAL)
        DEALLOCATE(Y_LOCAL)
    ELSE
        CALL MPI_SEND(NEL, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(NG, NEL, MPI_INTEGER, 0, 1, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(MG, NEL, MPI_INTEGER, 0, 2, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(U, NUM_EQN*(NPMAX+1)*NELMAX, MPI_DOUBLE_PRECISION, 0, 3, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(X, 4*NEL, MPI_DOUBLE_PRECISION, 0, 4, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(Y, 4*NEL, MPI_DOUBLE_PRECISION, 0, 5, MPI_COMM_WORLD, IERROR)
    ENDIF

    END SUBROUTINE WRITE_FIELDS

!-------------------------------------------------------------------
!
!   Adaptation subroutines
!
!-------------------------------------------------------------
    
    SUBROUTINE SET_ADAPT_BACKUP()
    IMPLICIT NONE

    !$OMP WORKSHARE
    U_ADAPT_STORAGE = U
    !$OMP END WORKSHARE

    END SUBROUTINE SET_ADAPT_BACKUP

!-------------------------------------------------------------------

    SUBROUTINE RESTORE_ADAPT_BACKUP()
    IMPLICIT NONE

    !$OMP WORKSHARE
    U = U_ADAPT_STORAGE
    !$OMP END WORKSHARE

    END SUBROUTINE RESTORE_ADAPT_BACKUP

!-------------------------------------------------------------------

    SUBROUTINE PADAPT_FIELDS_X(K,STEP) 
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP
    USE MESH, ONLY: UPDATE_METRICS
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K, STEP 
    INTEGER I, J, KK, N, M 

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION B_TEMP(0:NPMAX,NUM_EQN)

    N = NG(K)
    M = MG(K)

    U_TEMP = 0.0d0
    B_TEMP = 0.0d0
    DO J = 0,M
        DO I = 0,N+STEP
            DO KK = 0,N
                U_TEMP(I+(N+1+STEP)*J,:) = U_TEMP(I+(N+1+STEP)*J,:) + INTERP(KK,I,N,N+STEP)*U(KK+(N+1)*J,K,:)
                B_TEMP(I+(N+1+STEP)*J,:) = B_TEMP(I+(N+1+STEP)*J,:) + INTERP(KK,I,N,N+STEP)*U_ADAPT_STORAGE(KK+(N+1)*J,K,:)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K,:) = U_TEMP
    U_ADAPT_STORAGE(0:NPMAX,K,:) = B_TEMP

    NG(K) = N+STEP

    CALL UPDATE_METRICS(K) 

    END SUBROUTINE PADAPT_FIELDS_X

!---------------------------------------------------------------------------

    SUBROUTINE PADAPT_FIELDS_Y(K,STEP) 
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP
    USE MESH, ONLY: UPDATE_METRICS
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K, STEP
    INTEGER I, J, KK, N, M 

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION B_TEMP(0:NPMAX,NUM_EQN)

    N = NG(K)
    M = MG(K)

    U_TEMP = 0.0d0
    B_TEMP = 0.0d0
    DO J = 0,M+STEP
        DO I = 0,N
            DO KK = 0,M
                U_TEMP(I+(N+1)*J,:) = U_TEMP(I+(N+1)*J,:) + INTERP(KK,J,M,M+STEP)*U(I+(N+1)*KK,K,:)
                B_TEMP(I+(N+1)*J,:) = B_TEMP(I+(N+1)*J,:) + INTERP(KK,J,M,M+STEP)*U_ADAPT_STORAGE(I+(N+1)*KK,K,:)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K,:) = U_TEMP
    U_ADAPT_STORAGE(0:NPMAX,K,:) = B_TEMP

    MG(K) = M+STEP

    CALL UPDATE_METRICS(K) 

    END SUBROUTINE PADAPT_FIELDS_Y

!------------------------------------------------------------------------------------------

    SUBROUTINE COARSEN_FIELDS_X(K1) 
    USE SIZE, ONLY: NG,MG,NPMAX,NMMAX, NUM_EQN
    USE BASIS, ONLY: W, INTERP, INTERP_LEFT, INTERP_RIGHT
    USE MESH, ONLY: NEIGHBOUR_ID
    IMPLICIT NONE

    INTEGER K1, K2, I, J,II,JJ, KK, N1, M1, N2, M2, N_MAX, M_MAX

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION U1(NUM_EQN,0:NMMAX), U2(NUM_EQN,0:NMMAX)

    !Because of the way the cells are refined, and the fact that child 1 calls the coarsening,
    ! the cell to the right is the cell which coarsens with this one
    K2 = NEIGHBOUR_ID(2,1,K1)

    N1 = NG(K1)
    M1 = MG(K1)
    N2 = NG(K2)
    M2 = MG(K2)

    !The degree of the coarsened cell will be the max of the children cells
    N_MAX = MAX(N1,N2)
    M_MAX = MAX(M1,M2)

    !This projection assumes paralleogram cells. A more general projection should include the jacobians
    U_TEMP = 0.0d0
    DO J = 0,M_MAX
        !Interpolate N_MAX points in Cell 1 along eta_j
        U1 = 0.0d0
        DO JJ=0,M1
            DO II = 0,N1
                DO KK = 0,N_MAX
                    U1(:,KK) = U1(:,KK) + U(II+(N1+1)*JJ,K1,:)*INTERP(II,KK,N1,N_MAX)*INTERP(JJ,J,M1,M_MAX)
                ENDDO
            ENDDO
        ENDDO
       
        !And in cell 2
        U2 = 0.0d0
        DO JJ=0,M2
            DO II = 0,N2
                DO KK = 0,N_MAX
                    U2(:,KK) = U2(:,KK) + U(II+(N2+1)*JJ,K2,:)*INTERP(II,KK,N2,N_MAX)*INTERP(JJ,J,M2,M_MAX)
                ENDDO
            ENDDO
        ENDDO

        DO I = 0,N_MAX
            DO KK = 0,N_MAX
                U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:) + U1(:,KK)*INTERP_LEFT(I,KK,N_MAX,N_MAX)*W(KK,N_MAX) &
                                                            & + U2(:,KK)*INTERP_RIGHT(I,KK,N_MAX,N_MAX)*W(KK,N_MAX)
            ENDDO
            U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:)/(2.0d0*W(I,N_MAX))
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U_TEMP

    NG(K1) = N_MAX
    MG(K1) = M_MAX

    NG(K2) = -1
    MG(K2) = -1

    END SUBROUTINE COARSEN_FIELDS_X

!-----------------------------------------------------------------------------------------------------

    SUBROUTINE COARSEN_FIELDS_Y(K1) 
    USE SIZE, ONLY: NG,MG,NPMAX,NMMAX,NUM_EQN
    USE BASIS, ONLY: W, INTERP, INTERP_LEFT, INTERP_RIGHT
    USE MESH, ONLY: NEIGHBOUR_ID
    IMPLICIT NONE

    INTEGER K1, K2, I, J,II,JJ, KK, N1, M1, N2, M2, N_MAX, M_MAX

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION U1(NUM_EQN,0:NMMAX), U2(NUM_EQN,0:NMMAX)

    !Because of the way the cells are refined, and the fact that child 1 calls the coarsening,
    ! the cell to the right is the cell which coarsens with this one
    K2 = NEIGHBOUR_ID(3,1,K1)

    N1 = NG(K1)
    M1 = MG(K1)
    N2 = NG(K2)
    M2 = MG(K2)

    !The degree of the coarsened cell will be the max of the children cells
    N_MAX = MAX(N1,N2)
    M_MAX = MAX(M1,M2)

    !This projection assumes paralleogram cells. A more general projection should include the jacobians
    U_TEMP = 0.0d0
    DO I = 0,N_MAX
        !Interpolate N_MAX points in Cell 1 along xi_i
        U1 = 0.0d0
        DO JJ=0,M1
            DO II = 0,N1
                DO KK = 0,M_MAX
                    U1(:,KK) = U1(:,KK) + U(II+(N1+1)*JJ,K1,:)*INTERP(II,I,N1,N_MAX)*INTERP(JJ,KK,M1,M_MAX)
                ENDDO
            ENDDO
        ENDDO
       
        !And in cell 2
        U2 = 0.0d0
        DO JJ=0,M2
            DO II = 0,N2
                DO KK = 0,M_MAX
                    U2(:,KK) = U2(:,KK) + U(II+(N2+1)*JJ,K2,:)*INTERP(II,I,N2,N_MAX)*INTERP(JJ,KK,M2,M_MAX)
                ENDDO
            ENDDO
        ENDDO

        DO J = 0,M_MAX
            DO KK = 0,M_MAX
                U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:) + U1(:,KK)*INTERP_LEFT(J,KK,M_MAX,M_MAX)*W(KK,M_MAX) &
                                                            & + U2(:,KK)*INTERP_RIGHT(J,KK,M_MAX,M_MAX)*W(KK,M_MAX)
            ENDDO
            U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:)/(2.0d0*W(J,M_MAX))
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U_TEMP

    NG(K1) = N_MAX
    MG(K1) = M_MAX

    NG(K2) = -1
    MG(K2) = -1

    END SUBROUTINE COARSEN_FIELDS_Y

!-------------------------------------------------------------------------------

    SUBROUTINE MOVE_FIELDS(K1,K2)
    USE SIZE, ONLY:NG,MG
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2

    NG(K2) = NG(K1)
    MG(K2) = MG(K1)

    U(:,K2,:) = U(:,K1,:)

    NG(K1) = -1
    MG(K1) = -1

    END SUBROUTINE MOVE_FIELDS

!-------------------------------------------------------------------------------

    SUBROUTINE REFINE_FIELDS_X(K1,K2)
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP_LEFT, INTERP_RIGHT
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2
    INTEGER I, J, KK, N, M

    DOUBLE PRECISION U1(0:NPMAX,NUM_EQN), U2(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION B1(0:NPMAX,NUM_EQN), B2(0:NPMAX,NUM_EQN)

    N = NG(K1)
    M = MG(K1)

    !Since we're splitting a cell, we can project through simple interpolation
    U1 = 0.0d0
    U2 = 0.0d0
    B1 = 0.0d0
    B2 = 0.0d0
    DO J = 0,M
        DO I = 0,N
            DO KK = 0,N
                U1(I+(N+1)*J,:) = U1(I+(N+1)*J,:) + U(KK+(N+1)*J,K1,:)*INTERP_LEFT(KK,I,N,N)
                U2(I+(N+1)*J,:) = U2(I+(N+1)*J,:) + U(KK+(N+1)*J,K1,:)*INTERP_RIGHT(KK,I,N,N)
                B1(I+(N+1)*J,:) = B1(I+(N+1)*J,:) + U_ADAPT_STORAGE(KK+(N+1)*J,K1,:)*INTERP_LEFT(KK,I,N,N)
                B2(I+(N+1)*J,:) = B2(I+(N+1)*J,:) + U_ADAPT_STORAGE(KK+(N+1)*J,K1,:)*INTERP_RIGHT(KK,I,N,N)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U1
    U(0:NPMAX,K2,:) = U2
    U_ADAPT_STORAGE(0:NPMAX,K1,:) = B1
    U_ADAPT_STORAGE(0:NPMAX,K2,:) = B2

    NG(K2) = N
    MG(K2) = M

    END SUBROUTINE REFINE_FIELDS_X

!-----------------------------------------------------------------------------

    SUBROUTINE REFINE_FIELDS_Y(K1,K2)
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP_LEFT, INTERP_RIGHT
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2
    INTEGER I, J, KK, N, M

    DOUBLE PRECISION U1(0:NPMAX,NUM_EQN), U2(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION B1(0:NPMAX,NUM_EQN), B2(0:NPMAX,NUM_EQN)

    N = NG(K1)
    M = MG(K1)

    !Since we're splitting a cell, we can project through simple interpolation
    U1 = 0.0d0
    U2 = 0.0d0
    B1 = 0.0d0
    B2 = 0.0d0
    DO J = 0,M
        DO I = 0,N
            DO KK = 0,M
                U1(I+(N+1)*J,:) = U1(I+(N+1)*J,:) + U(I+(N+1)*KK,K1,:)*INTERP_LEFT(KK,J,M,M)
                U2(I+(N+1)*J,:) = U2(I+(N+1)*J,:) + U(I+(N+1)*KK,K1,:)*INTERP_RIGHT(KK,J,M,M)
                B1(I+(N+1)*J,:) = B1(I+(N+1)*J,:) + U_ADAPT_STORAGE(I+(N+1)*KK,K1,:)*INTERP_LEFT(KK,J,M,M)
                B2(I+(N+1)*J,:) = B2(I+(N+1)*J,:) + U_ADAPT_STORAGE(I+(N+1)*KK,K1,:)*INTERP_RIGHT(KK,J,M,M)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U1
    U(0:NPMAX,K2,:) = U2
    U_ADAPT_STORAGE(0:NPMAX,K1,:) = B1
    U_ADAPT_STORAGE(0:NPMAX,K2,:) = B2

    NG(K2) = N
    MG(K2) = M
 
    END SUBROUTINE REFINE_FIELDS_Y

END MODULE FIELDS

