    SUBROUTINE DGPRESSURE_SOLVE(P,UAUX,T,S)
    USE SIZE,ONLY: NUM_EQN, NEL, NELMAX, NPMAX,NG,MG
    USE PARAM, ONLY: ITERMAX, TOLP, ITER_PRES, NU, ALL_NEUMANN
    USE MESH, ONLY: JAC, DXDR,DXDS,DYDR,DYDS
    USE FIELDS, ONLY: AU, RHS, R, Z, PP, &
                & R_DOT_R, R_DOT_R_INIT, PP_DOT_APP,&
                & R_DOT_Z, R_DOT_Z_NEW, PQ_HOMOGENEOUS_BOUNDARY
    !USE MULTIGRID, ONLY: MULTIGRID_PRECONDITIONER
    IMPLICIT NONE
    INCLUDE "mpif.h"

    DOUBLE PRECISION,INTENT(OUT):: P(0:NPMAX,NELMAX)
    DOUBLE PRECISION,INTENT(IN):: UAUX(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION,INTENT(IN):: T

    INTEGER I, J, K, N, M, IERROR,S

    !$OMP WORKSHARE
    P = 0.0d0
    !$OMP END WORKSHARE

    !Compute RHS
    CALL DGPRESSURE_RHS(RHS,UAUX,T,S)

    !Compute residual
    CALL DGPOISSON(AU,P,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,ALL_NEUMANN,PQ_HOMOGENEOUS_BOUNDARY)
    
    !$OMP WORKSHARE
    R = RHS-AU
    Z = R
    !Z = 0.0d0
    !$OMP END WORKSHARE
    !CALL MULTIGRID_PRECONDITIONER(Z,R)

    !$OMP WORKSHARE
    PP = Z
    !$OMP END WORKSHARE

    !$OMP MASTER 
    R_DOT_R = 0.0d0
    R_DOT_Z = 0.0d0
    PP_DOT_APP = 0.0d0
    R_DOT_Z_NEW = 0.0d0
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_R) REDUCTION(+:R_DOT_Z) SCHEDULE(DYNAMIC)
    DO K = 1,NEL
        N = NG(K)
        M = MG(K)
        DO I = 0,N
            DO J = 0,M
                R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                R_DOT_Z = R_DOT_Z + R(I+(N+1)*J,K)*Z(I+(N+1)*J,K)
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    !$OMP MASTER
    CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
    CALL MPI_ALLREDUCE(R_DOT_Z, R_DOT_Z, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
    ITER_PRES = 0
    R_DOT_R_INIT = DSQRT(R_DOT_R)
    !$OMP END MASTER
    !$OMP BARRIER
    

    !........MAIN LOOP
    !Conjugate Gradient method
    DO
        IF ((DSQRT(R_DOT_R)).LT. TOLP) EXIT

        CALL DGPOISSON(AU,PP,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                & NPMAX,ALL_NEUMANN,PQ_HOMOGENEOUS_BOUNDARY)

        !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:PP_DOT_APP) SCHEDULE(DYNAMIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    PP_DOT_APP = PP_DOT_APP + PP(I+(N+1)*J,K)*AU(I+(N+1)*J,K)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP MASTER
        CALL MPI_ALLREDUCE(PP_DOT_APP, PP_DOT_APP, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        R_DOT_R = 0.0d0
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP WORKSHARE
        P = P + (R_DOT_Z/PP_DOT_APP)*PP
        R = R - (R_DOT_Z/PP_DOT_APP)*AU
        !$OMP END WORKSHARE
        
        !$OMP WORKSHARE
        Z = R
        !Z=0.0d0
        !$OMP END WORKSHARE
        !CALL MULTIGRID_PRECONDITIONER(Z,R)

        !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_Z_NEW) REDUCTION(+:R_DOT_R) SCHEDULE(DYNAMIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                    R_DOT_Z_NEW = R_DOT_Z_NEW + R(I+(N+1)*J,K)*Z(I+(N+1)*J,K)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP MASTER
        CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        CALL MPI_ALLREDUCE(R_DOT_Z_NEW, R_DOT_Z_NEW, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        ITER_PRES = ITER_PRES + 1
        !$OMP END MASTER
        !$OMP BARRIER

        IF (ITER_PRES .GT. ITERMAX) THEN
            WRITE(*,*) "Warning: ITERMAX Reached (ITER_PRES)"
            EXIT
        ENDIF

        !$OMP WORKSHARE
        PP = Z + (R_DOT_Z_NEW/R_DOT_Z)*PP
        !$OMP END WORKSHARE

        !$OMP MASTER 
        R_DOT_Z = R_DOT_Z_NEW
        PP_DOT_APP = 0.0d0
        R_DOT_Z_NEW = 0.0d0
        !$OMP END MASTER 
        !$OMP BARRIER           
    ENDDO

    END SUBROUTINE DGPRESSURE_SOLVE

