    SUBROUTINE DGPRESSURE_RHS(RHS,UAUX,T,S)
    USE SIZE, ONLY: NPMAX,NEL, NG,MG, NUM_EQN, NELMAX
    USE PARAM, ONLY: NU, ALL_NEUMANN
    USE MESH, ONLY: BDRY_TYPE
    USE TIMESTEP, ONLY: DT, RK_C
    IMPLICIT NONE
    INCLUDE "mpif.h"

    DOUBLE PRECISION,INTENT(OUT):: RHS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: UAUX(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION, INTENT(IN):: T

    INTEGER I,J,K, N,M, SIDE, IERROR,S

    DOUBLE PRECISION PQ_SURFACE(0:NPMAX)

    !$OMP MASTER 
    ALL_NEUMANN=.TRUE.
    !$OMP END MASTER

    CALL DGDIV(RHS,UAUX,T)

    !$OMP WORKSHARE
    RHS = RHS/(RK_C(S+1)*DT)
    !$OMP END WORKSHARE
    
    !$OMP DO PRIVATE(N,M,K,I,J,SIDE,PQ_SURFACE) REDUCTION(.AND.:ALL_NEUMANN) SCHEDULE(DYNAMIC)
    DO K = 1, NEL
        N = NG(K)
        M = MG(K)

        DO SIDE = 1,4
            !Check if boundary is physical
            IF (BDRY_TYPE(SIDE,K).EQ.'O  ') THEN 
                ALL_NEUMANN = .FALSE.

                CALL PRESSURE_BDRY_INTEGRAL(PQ_SURFACE,S,K,SIDE,T)

                DO J = 0,M
                    DO I = 0,N
                        RHS(I+(N+1)*J,K) = RHS(I+(N+1)*J,K) - PQ_SURFACE(I+(N+1)*J) 
                    ENDDO
                ENDDO 
            END IF
        END DO
    ENDDO
    !$OMP END DO    

    !$OMP MASTER
    CALL MPI_ALLREDUCE(ALL_NEUMANN,ALL_NEUMANN, 1, MPI_LOGICAL, MPI_LAND, MPI_COMM_WORLD, IERROR)
    !$OMP END MASTER
    !$OMP BARRIER

    END SUBROUTINE

!------------------------------------------------------------------------------------------------

    SUBROUTINE PRESSURE_BDRY_INTEGRAL(PQ_SURFACE,S,K,SIDE,T)
    USE SIZE, ONLY: NPMAX, NELMAX, NG, MG, NMMAX, NUM_EQN
    USE MESH, ONLY: X,Y, SCAL, NX, NY, JAC, SCAL, DXDR,DXDS,DYDR,DYDS
    USE BASIS, ONLY: W, INTERP_ENDPOINTS, DER
    USE FIELDS, ONLY: TAU
    USE TIMESTEP, ONLY: DT, RK_C
    IMPLICIT NONE

    DOUBLE PRECISION, INTENT(OUT)::PQ_SURFACE(0:NPMAX)
    INTEGER,INTENT(IN):: S, K, SIDE
    DOUBLE PRECISION,INTENT(IN):: T
    
    INTEGER I, J, KK, N, M, DEG 
    DOUBLE PRECISION TAU_L
    DOUBLE PRECISION P_R(0:NMMAX), P_EDGE(0:NMMAX)
    DOUBLE PRECISION P_R2(0:NMMAX)
    DOUBLE PRECISION Q_EDGE(0:NMMAX)
    DOUBLE PRECISION Q_SURFACE(0:NMMAX), P_SURFACE(0:NMMAX)

    INTEGER V1, V2

    V1 = SIDE
    V2 = SIDE+1
    IF (SIDE==4) V2=1

    !Get order of element
    N = NG(K)
    M = MG(K)

    TAU_L = TAU(K)

    IF ((SIDE==1).OR.(SIDE==3)) THEN
        DEG = N
    ELSE
        DEG = M
    ENDIF

    !Evaluate right state of the pressure increment based on the boundary data 
    CALL P_BC_OUTFLOW(P_R,X(V1,K),Y(V1,K),X(V2,K),Y(V2,K),K,SIDE,DEG,T)
    CALL P_BC_OUTFLOW(P_R2,X(V1,K),Y(V1,K),X(V2,K),Y(V2,K),K,SIDE,DEG,T-RK_C(S+1)*DT)
    P_R = P_R - P_R2

    !Evaluate the flux at these points using the central and Internal penalty flux
    DO I = 0,DEG
        !Strong form of boundary conditions
        P_EDGE(I) = P_R(I)
        Q_EDGE(I) = TAU_L*(2.0d0*P_R(I))
    ENDDO

    !Perform the integral
    DO I = 0,DEG
        Q_SURFACE(I) =  W(I,DEG)*Q_EDGE(I)
        P_SURFACE(I) =  W(I,DEG)*P_EDGE(I)
    ENDDO

    !Construct the entire term
    SELECT CASE (SIDE)
    CASE (1)
        DO J = 0,M
            DO I = 0,N
                PQ_SURFACE(I+(N+1)*J) = Q_SURFACE(I)*INTERP_ENDPOINTS(J,0,M)
                DO KK = 0,N
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(NX(1,K)*DYDS(KK+(N+1)*J,K)-NY(1,K)*DXDS(KK+(N+1)*J,K)) &
                                     *DER(KK+(N+1)*I,N)*P_SURFACE(KK)*INTERP_ENDPOINTS(J,0,M)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(1,K)*DYDR(I+(N+1)*KK,K)+NY(1,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*P_SURFACE(I)*INTERP_ENDPOINTS(KK,0,M)/JAC(I+(N+1)*KK,K)
                ENDDO
                PQ_SURFACE(I+(N+1)*J) = SCAL(1,K)*PQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    CASE (2) 
        DO J = 0,M
            DO I = 0,N
                PQ_SURFACE(I+(N+1)*J) = Q_SURFACE(J)*INTERP_ENDPOINTS(I,1,N)
                DO KK = 0,N
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(NX(2,K)*DYDS(KK+(N+1)*J,K)-NY(2,K)*DXDS(KK+(N+1)*J,K))&
                                     *DER(KK+(N+1)*I,N)*P_SURFACE(J)*INTERP_ENDPOINTS(KK,1,N)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(2,K)*DYDR(I+(N+1)*KK,K)+NY(2,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*P_SURFACE(KK)*INTERP_ENDPOINTS(I,1,N)/JAC(I+(N+1)*KK,K)
                ENDDO
                PQ_SURFACE(I+(N+1)*J) = SCAL(2,K)*PQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    CASE (3)
        DO J = 0,M
            DO I = 0,N
                PQ_SURFACE(I+(N+1)*J) = Q_SURFACE(N-I)*INTERP_ENDPOINTS(J,1,M)
                DO KK = 0,N
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(NX(3,K)*DYDS(KK+(N+1)*J,K)-NY(3,K)*DXDS(KK+(N+1)*J,K))&
                                     *DER(KK+(N+1)*I,N)*P_SURFACE(N-KK)*INTERP_ENDPOINTS(J,1,M)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(3,K)*DYDR(I+(N+1)*KK,K)+NY(3,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*P_SURFACE(N-I)*INTERP_ENDPOINTS(KK,1,M)/JAC(I+(N+1)*KK,K)
                ENDDO
                PQ_SURFACE(I+(N+1)*J) = SCAL(3,K)*PQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    CASE (4) 
        DO J = 0,M
            DO I = 0,N
                PQ_SURFACE(I+(N+1)*J) = Q_SURFACE(M-J)*INTERP_ENDPOINTS(I,0,N)
                DO KK = 0,N
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(NX(4,K)*DYDS(KK+(N+1)*J,K)-NY(4,K)*DXDS(KK+(N+1)*J,K))&
                                     *DER(KK+(N+1)*I,N)*P_SURFACE(M-J)*INTERP_ENDPOINTS(KK,0,N)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    PQ_SURFACE(I+(N+1)*J) = PQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(4,K)*DYDR(I+(N+1)*KK,K)+NY(4,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*P_SURFACE(M-KK)*INTERP_ENDPOINTS(I,0,N)/JAC(I+(N+1)*KK,K)
                ENDDO
                PQ_SURFACE(I+(N+1)*J) = SCAL(4,K)*PQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    END SELECT

    END SUBROUTINE

