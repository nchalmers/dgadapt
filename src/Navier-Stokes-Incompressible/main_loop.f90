
    SUBROUTINE MAIN_LOOP
    USE SIZE,ONLY: NUM_EQN, NPMAX, NELMAX, RANK,NG,MG, NMMAX,NEL
    USE PARAM, ONLY: MAXTIME, MAXSTEPS, FRAME, MAXFRAMES, &
                    & NU, IFADAPT, NAINIT, NASTEPS, NATIME
    USE FIELDS, ONLY: U, P, VORT, WRITE_FIELDS,SET_TAU,SET_ADAPT_BACKUP,CALC_VORT
    USE MESH, ONLY: X, Y
    USE ADAPT, ONLY: ADAPT_START_TIME
    USE BASIS, ONLY: QUAD
    USE TIMESTEP, ONLY: T,DT, TSTEP, RK_A, DIRK_A, RK_C, &
                        & NUM_STAGES, IMEX_RK_TABLEAU, &
                        & U_RK_STORAGE, RK_RHS_STORAGE, DIRK_RHS_STORAGE
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS, SHARE_TAU_BDRY_BUFFERS
    !$ USE OMP_LIB
    IMPLICIT NONE
    INCLUDE "mpif.h"


    INTEGER I, S, IERROR, J, K, N, M
    DOUBLE PRECISION CPU_START,CPU_FINAL
    DOUBLE PRECISION ERROR_U, ERROR_P, UU(NUM_EQN), PP,XX,YY
    REAL PERCENT

    DOUBLE PRECISION,ALLOCATABLE:: UAUX(:,:,:)
    DOUBLE PRECISION,ALLOCATABLE:: PRES_INC(:,:)
    DOUBLE PRECISION,ALLOCATABLE:: GRADP(:,:,:)

    !WARNING: Order > 2 is not currently supported. 
    CALL IMEX_RK_TABLEAU(1)

    ALLOCATE(U_RK_STORAGE(0:NPMAX,NELMAX,NUM_EQN,1))
    ALLOCATE(PRES_INC(0:NPMAX,NELMAX))
    ALLOCATE(RK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,NUM_STAGES))
    ALLOCATE(DIRK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,NUM_STAGES))
    ALLOCATE(UAUX(0:NPMAX,NELMAX,NUM_EQN))
    ALLOCATE(GRADP(0:NPMAX,NELMAX,NUM_EQN))

    T = 0.0d0
    TSTEP = 0
    FRAME = 0
    PERCENT = 0.0

    CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
    CALL SHARE_TAU_BDRY_BUFFERS

    IF (RANK.EQ.0) WRITE(*,'("Computing...")')
    !CALL DGVORT(VORT,U,0.0d0)
    CALL CALC_VORT
    CALL WRITE_FIELDS(0.0)

    !.......MAIN LOOP
    CALL CPU_TIME(CPU_START)
    !$ CPU_START = OMP_GET_WTIME()

    !$OMP PARALLEL DEFAULT(SHARED)
    !..............Main Loop .......................
    DO WHILE( (T.LT.MAXTIME) .AND. &
               & ( (TSTEP.LT.MAXSTEPS).AND.(MAXSTEPS.NE.-1) ) )     
        CALL CFL_CONDITION
        !$OMP MASTER
        CALL MPI_ALLREDUCE(DT, DT, 1, MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, IERROR)
        IF (T+DT .GT. MAXTIME) DT = MAXTIME - T
        !$OMP END MASTER
        !$OMP BARRIER

        CALL DGCONVECT(RK_RHS_STORAGE(:,:,:,1),U,T)

        DO S=1,NUM_STAGES
            !Calculate RHS for Helmholtz problem
            !$OMP WORKSHARE
            U_RK_STORAGE(:,:,:,1)  = U
            !$OMP END WORKSHARE
            DO I = 1,S
                !$OMP WORKSHARE
                U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) + DT*RK_A(I+(S-1)*NUM_STAGES)*RK_RHS_STORAGE(:,:,:,I)
                !$OMP END WORKSHARE
            ENDDO
            DO I = 1,S-1
                !$OMP WORKSHARE
                U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) + NU*DT*DIRK_A(I+(S-1)*NUM_STAGES)*DIRK_RHS_STORAGE(:,:,:,I)
                !$OMP END WORKSHARE
            ENDDO

            CALL DGGRAD(GRADP,P,T,1)
            !$OMP WORKSHARE
            U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) - RK_C(S+1)*DT*GRADP
            !$OMP END WORKSHARE

            !Solve Holemholtz problem for UAUX
            CALL DGVELOCITY_SOLVE(UAUX,U_RK_STORAGE(:,:,:,1),T+RK_C(S+1)*DT)

            !Solve Poisson problem for pressure increment
            CALL DGPRESSURE_SOLVE(PRES_INC,UAUX,T+RK_C(S+1)*DT,S)
            CALL DGGRAD(GRADP,PRES_INC,T+RK_C(S+1)*DT,S+1)

            IF (S==NUM_STAGES) EXIT

            !$OMP WORKSHARE
            U_RK_STORAGE(:,:,:,1) = UAUX - 0.5d0*RK_C(S+1)*DT*GRADP
            !$OMP END WORKSHARE        

            !Calculate convective term at U_i
            CALL DGCONVECT(RK_RHS_STORAGE(:,:,:,S+1),U_RK_STORAGE(:,:,:,1),T+RK_C(S+1)*DT)

            !Calculate diffusive term at U_i
            CALL DGDIFFUSION(DIRK_RHS_STORAGE(:,:,:,S),U_RK_STORAGE(:,:,:,1),T+RK_C(S+1)*DT)
        ENDDO

        !$OMP WORKSHARE
        U = UAUX - 0.5d0*DT*GRADP
        P = P + PRES_INC
        !$OMP END WORKSHARE   

        !$OMP MASTER
        T = T + DT
        TSTEP = TSTEP+1
        !$OMP END MASTER
        !$OMP BARRIER

        !Adapt
        IF (T.LE.NATIME) THEN
            ADAPT_START_TIME = T
            CALL SET_ADAPT_BACKUP
        ENDIF

        IF ((IFADAPT.AND.MOD(TSTEP,NASTEPS).EQ.0).AND.(TSTEP.GE.NAINIT)&
            .AND.(T.GT.NATIME)) THEN
            CALL ADAPTMESH_VORT
            CALL SET_TAU
            CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
            CALL SHARE_TAU_BDRY_BUFFERS
        ENDIF

        !$OMP MASTER
        IF (MAXTIME.LT.0) THEN
            PERCENT = REAL(TSTEP*100)/REAL(MAXSTEPS)
        ELSEIF (MAXSTEPS.LT.0) THEN
            PERCENT = REAL(T/MAXTIME)*100.0
        ELSE
            PERCENT = MAX(REAL(TSTEP*100)/REAL(MAXSTEPS),REAL(T/MAXTIME)*100)
        ENDIF
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP DO PRIVATE(K,N,M,XX,YY,I,J,UU,PP)
        DO K=1,NEL
            N = NG(K)
            M = MG(K) 
            DO J=0,M
                DO I=0,N
                    XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N)+1.0d0)
                    YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M)+1.0d0)

                    !CALL EXACT_SOLUTION(UU,PP,XX,YY,T)

                    !U(I+(N+1)*J,K,:) = U(I+(N+1)*J,K,:) - UU
                    !P(I+(N+1)*J,K) = P(I+(N+1)*J,K) - PP
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO

        IF (PERCENT.GE. REAL(FRAME*100)/REAL(MAXFRAMES)) THEN
            !CALL DGVORT(VORT,U,T)
            CALL CALC_VORT
            !$OMP BARRIER
            !$OMP MASTER
            CALL WRITE_FIELDS(PERCENT)
            !$OMP END MASTER
            !$OMP BARRIER
        ENDIF

        IF (PERCENT.GE.100.0) EXIT
    ENDDO

    CALL EVAL_ERROR(ERROR_U, ERROR_P)
    !$OMP END PARALLEL

    CALL CPU_TIME(CPU_FINAL)
    !$ CPU_FINAL = OMP_GET_WTIME()
    IF (RANK.EQ.0)THEN
        WRITE(*,*)
        PRINT '("ERROR_U = ",E12.5," ERROR_P = ",E12.5 )', ERROR_U, ERROR_P
        PRINT '("Time = ",f8.3," seconds.")',CPU_FINAL-CPU_START
    ENDIF

    DEALLOCATE(U_RK_STORAGE)
    DEALLOCATE(PRES_INC)
    DEALLOCATE(RK_RHS_STORAGE)
    DEALLOCATE(DIRK_RHS_STORAGE)
    DEALLOCATE(UAUX)
    DEALLOCATE(GRADP)
    DEALLOCATE(RK_A)
    DEALLOCATE(DIRK_A)
    DEALLOCATE(RK_C)

    END


    SUBROUTINE EVAL_ERROR(ERROR_U,ERROR_P)
    USE FIELDS, ONLY: U, P
    USE BASIS, ONLY: W, QUAD,INTERP
    USE SIZE, ONLY: NG, MG, NEL
    USE MESH, ONLY: X, Y, QUADMAP
    USE TIMESTEP, ONLY: T
    IMPLICIT NONE

    INTEGER K, I, J, II,JJ, N, M
    DOUBLE PRECISION XX, YY
    DOUBLE PRECISION UU_NUM(2), PP_NUM
    DOUBLE PRECISION UU(2), PP

    DOUBLE PRECISION DXDR,DXDS,DYDR,DYDS, JAC

    DOUBLE PRECISION ERROR_U, ERROR_P

    !$OMP MASTER
    ERROR_U = 0.0d0
    ERROR_P = 0.0d0
    !$OMP END MASTER

    !$OMP DO PRIVATE(K,N,M,XX,YY,I,J,II,JJ,UU_NUM,PP_NUM,UU,PP,DXDR,DXDS,DYDR,DYDS,JAC) &
    !$OMP&  REDUCTION(+:ERROR_U) REDUCTION(+:ERROR_P)
    DO K=1,NEL
        N = NG(K)
        M = MG(K) 
        DO J=0,M+1
            DO I=0,N+1
                XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N+1)+1.0d0)
                YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M+1)+1.0d0)

                CALL QUADMAP(DXDR,DXDS, DYDR,DYDS, &
                             QUAD(I,N+1),QUAD(J,M+1),X(:,K),Y(:,K))    
                JAC = DXDR*DYDS - DXDS*DYDR

                !CALL EXACT_SOLUTION(UU,PP,XX,YY,T)

                UU_NUM = 0.0d0
                PP_NUM = 0.0d0
                DO JJ = 0,M
                    DO II = 0,N
                        UU_NUM = UU_NUM + U(II+(N+1)*JJ,K,:)*INTERP(II,I,N,N+1)*INTERP(JJ,J,M,M+1)
                        PP_NUM = PP_NUM + P(II+(N+1)*JJ,K)*INTERP(II,I,N,N+1)*INTERP(JJ,J,M,M+1)
                    ENDDO
                ENDDO

                ERROR_U = ERROR_U + DABS(UU_NUM(1)-UU(1))*W(I,N+1)*W(J,M+1)*JAC &
                                & + DABS(UU_NUM(2)-UU(2))*W(I,N+1)*W(J,M+1)*JAC
                ERROR_P = ERROR_P + DABS(PP_NUM   -PP   )*W(I,N+1)*W(J,M+1)*JAC                     
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE EVAL_ERROR
