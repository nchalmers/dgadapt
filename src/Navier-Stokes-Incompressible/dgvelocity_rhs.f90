    SUBROUTINE DGVELOCITY_RHS(RHS,F,T,MM)
    USE SIZE, ONLY: NPMAX,NEL, NG,MG, NELMAX,NUM_EQN
    USE PARAM, ONLY: NU
    USE MESH, ONLY: NEIGHBOUR_ID,JAC
    USE BASIS, ONLY: W
    USE TIMESTEP, ONLY: DIRK_A, DT
    IMPLICIT NONE

    DOUBLE PRECISION,INTENT(OUT):: RHS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: F(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION,INTENT(IN):: T
    INTEGER,INTENT(IN):: MM

    INTEGER I,J,K,N,M,SIDE

    DOUBLE PRECISION UQ_SURFACE(0:NPMAX)

    !Scale the RHS for the Helmholtz solve
    !$OMP DO PRIVATE(N,M,I,J)&
    !$OMP&     SCHEDULE(DYNAMIC)
    DO K =1,NEL
        N = NG(K)
        M = MG(K)
        DO I = 0,N
            DO J = 0,M
                !!!RHS = -f/(DT*NU*a_11)
                RHS(I+(N+1)*J,K) = -F(I+(N+1)*J,K,MM)*W(I,N)*W(J,M)*JAC(I+(N+1)*J,K)&
                                    &/(DIRK_A(1)*DT*NU)
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    !Add boudary contributions from Laplace operator
    !$OMP DO PRIVATE(N,M,K,I,J,SIDE,UQ_SURFACE) SCHEDULE(DYNAMIC)
    DO K = 1, NEL
        N = NG(K)
        M = MG(K)

        DO SIDE = 1,4
            !Check if boundary is physical
            IF (NEIGHBOUR_ID(SIDE,1,K).LT.0) THEN
                CALL VELOCITY_BDRY_INTEGRAL(UQ_SURFACE,K,SIDE,T,MM)

                DO J = 0,M
                    DO I = 0,N
                        RHS(I+(N+1)*J,K) = RHS(I+(N+1)*J,K) - UQ_SURFACE(I+(N+1)*J) 
                    ENDDO
                ENDDO 
            END IF
        END DO
    ENDDO
    !$OMP END DO    


    END SUBROUTINE DGVELOCITY_RHS

!-------------------------------------------------------------------------------------

    SUBROUTINE VELOCITY_BDRY_INTEGRAL(UQ_SURFACE,K,SIDE,T,MM)
    USE SIZE, ONLY: NPMAX, NG, MG, NMMAX, NUM_EQN
    USE MESH, ONLY: SCAL, NX, NY, JAC, SCAL, DXDR,DXDS,DYDR,DYDS
    USE BASIS, ONLY: W, INTERP_ENDPOINTS, DER
    USE FIELDS, ONLY: TAU
    IMPLICIT NONE

    DOUBLE PRECISION,INTENT(OUT):: UQ_SURFACE(0:NPMAX)
    DOUBLE PRECISION,INTENT(IN):: T
    INTEGER,INTENT(IN):: K,SIDE,MM
    
    INTEGER I, J, KK, N, M, DEG 
    DOUBLE PRECISION TAU_L
    DOUBLE PRECISION U_R(NUM_EQN,0:NMMAX), U_EDGE(0:NMMAX)
    DOUBLE PRECISION Q_R(NUM_EQN,0:NMMAX), Q_EDGE(0:NMMAX)
    DOUBLE PRECISION Q_SURFACE(0:NMMAX),U_SURFACE(0:NMMAX)

    !Get order of element
    N = NG(K)
    M = MG(K)

    TAU_L = TAU(K)

    IF ((SIDE==1).OR.(SIDE==3)) THEN
        DEG = N
    ELSE
        DEG = M
    ENDIF

    !Evaluate right state based on the boundary data
    CALL UQ_BOUNDARY(Q_R,U_R,K,SIDE,DEG,T)    

    !Evaluate the flux at these points using the central and Internal penalty flux
    DO I = 0,DEG
        !Strong form of boundary conditions
        U_EDGE(I) = U_R(MM,I)
        Q_EDGE(I) = Q_R(MM,I) + TAU_L*(2.0d0*U_R(MM,I))
    ENDDO

    !Perform the integral
    DO I = 0,DEG
        Q_SURFACE(I) =  W(I,DEG)*Q_EDGE(I)
        U_SURFACE(I) =  W(I,DEG)*U_EDGE(I)
    ENDDO

    !Construct the entire term
    SELECT CASE (SIDE)
    CASE (1)
        DO J = 0,M
            DO I = 0,N
                UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(I)*INTERP_ENDPOINTS(J,0,M)
                DO KK = 0,N
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(NX(1,K)*DYDS(KK+(N+1)*J,K)-NY(1,K)*DXDS(KK+(N+1)*J,K)) &
                                     *DER(KK+(N+1)*I,N)*U_SURFACE(KK)*INTERP_ENDPOINTS(J,0,M)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(1,K)*DYDR(I+(N+1)*KK,K)+NY(1,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*U_SURFACE(I)*INTERP_ENDPOINTS(KK,0,M)/JAC(I+(N+1)*KK,K)
                ENDDO
                UQ_SURFACE(I+(N+1)*J) = SCAL(1,K)*UQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    CASE (2) 
        DO J = 0,M
            DO I = 0,N
                UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(J)*INTERP_ENDPOINTS(I,1,N)
                DO KK = 0,N
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(NX(2,K)*DYDS(KK+(N+1)*J,K)-NY(2,K)*DXDS(KK+(N+1)*J,K))&
                                     *DER(KK+(N+1)*I,N)*U_SURFACE(J)*INTERP_ENDPOINTS(KK,1,N)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(2,K)*DYDR(I+(N+1)*KK,K)+NY(2,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*U_SURFACE(KK)*INTERP_ENDPOINTS(I,1,N)/JAC(I+(N+1)*KK,K)
                ENDDO
                UQ_SURFACE(I+(N+1)*J) = SCAL(2,K)*UQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    CASE (3)
        DO J = 0,M
            DO I = 0,N
                UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(N-I)*INTERP_ENDPOINTS(J,1,M)
                DO KK = 0,N
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(NX(3,K)*DYDS(KK+(N+1)*J,K)-NY(3,K)*DXDS(KK+(N+1)*J,K))&
                                     *DER(KK+(N+1)*I,N)*U_SURFACE(N-KK)*INTERP_ENDPOINTS(J,1,M)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(3,K)*DYDR(I+(N+1)*KK,K)+NY(3,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*U_SURFACE(N-I)*INTERP_ENDPOINTS(KK,1,M)/JAC(I+(N+1)*KK,K)
                ENDDO
                UQ_SURFACE(I+(N+1)*J) = SCAL(3,K)*UQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    CASE (4) 
        DO J = 0,M
            DO I = 0,N
                UQ_SURFACE(I+(N+1)*J) = Q_SURFACE(M-J)*INTERP_ENDPOINTS(I,0,N)
                DO KK = 0,N
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(NX(4,K)*DYDS(KK+(N+1)*J,K)-NY(4,K)*DXDS(KK+(N+1)*J,K))&
                                     *DER(KK+(N+1)*I,N)*U_SURFACE(M-J)*INTERP_ENDPOINTS(KK,0,N)/JAC(KK+(N+1)*J,K)
                ENDDO
                DO KK = 0,M
                    UQ_SURFACE(I+(N+1)*J) = UQ_SURFACE(I+(N+1)*J) &
                                    -(-NX(4,K)*DYDR(I+(N+1)*KK,K)+NY(4,K)*DXDR(I+(N+1)*KK,K))&
                                     *DER(KK+(M+1)*J,M)*U_SURFACE(M-KK)*INTERP_ENDPOINTS(I,0,N)/JAC(I+(N+1)*KK,K)
                ENDDO
                UQ_SURFACE(I+(N+1)*J) = SCAL(4,K)*UQ_SURFACE(I+(N+1)*J)
            ENDDO
        ENDDO
    END SELECT

    END SUBROUTINE

!-----------------------------------------------------------------

    SUBROUTINE UQ_BOUNDARY(Q_R,U_R,K,SIDE,N,T)
    USE SIZE, ONLY:NUM_EQN    
    USE MESH, ONLY:BDRY_TYPE, BDRY_DATA, X, Y
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K, SIDE, N
    DOUBLE PRECISION,INTENT(OUT):: U_R(NUM_EQN,0:N), Q_R(NUM_EQN,0:N)
    DOUBLE PRECISION,INTENT(IN):: T

    INTEGER I, V1, V2

    V1 = SIDE
    V2 = SIDE+1
    IF (SIDE==4) V2=1

    IF (BDRY_TYPE(SIDE,K).EQ.'V  ') THEN !Constant Dirichlet 
        DO I = 0,N
            U_R(1:NUM_EQN,I) = BDRY_DATA(1:NUM_EQN,SIDE,K)
        ENDDO
        Q_R = 0.0d0
    ELSEIF (BDRY_TYPE(SIDE,K).EQ.'v  ') THEN !User prescribed Dirichlet
        CALL U_BC_INFLOW(U_R,X(V1,K),Y(V1,K),X(V2,K),Y(V2,K),K,SIDE,N,T)
        Q_R = 0.0d0
    ELSEIF (BDRY_TYPE(SIDE,K).EQ.'W  ') THEN!Solid wall boundary
        U_R = 0.0d0
        Q_R = 0.0d0
    ELSEIF (BDRY_TYPE(SIDE,K).EQ.'O  ') THEN !Outflow boundary
        U_R = 0.0d0
        CALL U_QN_BC_OUTFLOW(Q_R,X(V1,K),Y(V1,K),X(V2,K),Y(V2,K),K,SIDE,N,T)   
    ENDIF

    END SUBROUTINE
