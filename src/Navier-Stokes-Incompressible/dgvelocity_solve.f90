    SUBROUTINE DGVELOCITY_SOLVE(U,F,T)
    USE SIZE,ONLY: NUM_EQN, NEL, NPMAX,NG,MG, NELMAX
    USE PARAM, ONLY: ITERMAX, TOLVEL, ITER_VEL, NU
    USE MESH, ONLY: JAC,DXDR,DXDS,DYDR,DYDS
    USE FIELDS, ONLY: AU, RHS, R, PP, R_DOT_R, &
                & R_DOT_Z_NEW, PP_DOT_APP, R_DOT_R_INIT
    USE TIMESTEP, ONLY: DIRK_A, DT
    IMPLICIT NONE
    INCLUDE "mpif.h"

    DOUBLE PRECISION,INTENT(OUT):: U(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION,INTENT(IN):: F(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION, INTENT(IN):: T
    
    INTEGER I, J, K, N, M, MM, IERROR

    !!Initial guess
    !$OMP WORKSHARE
    U = F
    !$OMP END WORKSHARE

    DO MM = 1, NUM_EQN
        !Compute RHS
        CALL DGVELOCITY_RHS(RHS,F,T,MM)

        !Compute residual
        CALL DGHELMHOLTZ(AU,U(:,:,MM),-1.0d0/(DIRK_A(1)*DT*NU),&
                            & NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

        !$OMP WORKSHARE
        R = RHS-AU
        PP = R
        !$OMP END WORKSHARE
        
        !$OMP MASTER 
        R_DOT_R = 0.0d0
        PP_DOT_APP = 0.0d0
        R_DOT_Z_NEW = 0.0d0
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_R) SCHEDULE(DYNAMIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO

        !$OMP MASTER
        CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        R_DOT_R_INIT = DSQRT(R_DOT_R)
        ITER_VEL = 0
        !$OMP END MASTER
        !$OMP BARRIER

        !........MAIN LOOP
        !Preconditioned Conjugate Gradient method
        DO  
            IF ((DSQRT(R_DOT_R)).LT. TOLVEL) EXIT

            CALL DGHELMHOLTZ(AU,PP,-1.0d0/(DIRK_A(1)*DT*NU),&
                                & NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

            !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:PP_DOT_APP) SCHEDULE(DYNAMIC)
            DO K = 1,NEL
                N = NG(K)
                M = MG(K)
                DO I = 0,N
                    DO J = 0,M
                        PP_DOT_APP = PP_DOT_APP + PP(I+(N+1)*J,K)*AU(I+(N+1)*J,K)
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO
            !$OMP MASTER
            CALL MPI_ALLREDUCE(PP_DOT_APP, PP_DOT_APP, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            !$OMP END MASTER
            !$OMP BARRIER

            !$OMP WORKSHARE
            U(:,:,MM) = U(:,:,MM) + (R_DOT_R/PP_DOT_APP)*PP
            R = R - (R_DOT_R/PP_DOT_APP)*AU
            !$OMP END WORKSHARE
            
            !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_Z_NEW) SCHEDULE(DYNAMIC)
            DO K = 1,NEL
                N = NG(K)
                M = MG(K)
                DO I = 0,N
                    DO J = 0,M
                        R_DOT_Z_NEW = R_DOT_Z_NEW + R(I+(N+1)*J,K)**2
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO
            !$OMP MASTER
            CALL MPI_ALLREDUCE(R_DOT_Z_NEW, R_DOT_Z_NEW, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            ITER_VEL = ITER_VEL + 1
            !$OMP END MASTER
            !$OMP BARRIER

            IF (ITER_VEL .GT. ITERMAX) THEN
                WRITE(*,*) "WARNING: ITERMAX Reached (ITER_VEL)"
                EXIT
            ENDIF

            !$OMP WORKSHARE
            PP = R + (R_DOT_Z_NEW/R_DOT_R)*PP
            !$OMP END WORKSHARE

            !$OMP MASTER 
            R_DOT_R = R_DOT_Z_NEW
            PP_DOT_APP = 0.0d0
            R_DOT_Z_NEW = 0.0d0
            !$OMP END MASTER     
            !$OMP BARRIER       
        ENDDO
    ENDDO
 
    END SUBROUTINE DGVELOCITY_SOLVE

!----------------------------------------------------------------------------------

    SUBROUTINE DGHELMHOLTZ(AU,U,SIGMA,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)
    USE SIZE, ONLY: NEL
    USE BASIS, ONLY: W
    USE FIELDS, ONLY: UQ_HOMOGENEOUS_BOUNDARY
    IMPLICIT NONE

    INTEGER,INTENT(IN):: NPMAX,NG(NEL),MG(NEL)
    DOUBLE PRECISION,INTENT(OUT):: AU(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: U(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: SIGMA

    DOUBLE PRECISION,INTENT(IN):: JAC(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DXDR(0:NPMAX,NEL), DXDS(0:NPMAX,NEL)
    DOUBLE PRECISION,INTENT(IN):: DYDR(0:NPMAX,NEL), DYDS(0:NPMAX,NEL)

    INTEGER I,J,K,N,M

    CALL DGPOISSON(AU,U,NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,&
                    & NPMAX,.FALSE.,UQ_HOMOGENEOUS_BOUNDARY)

    !$OMP DO PRIVATE(N,M,I,J,K)&
    !$OMP&     SCHEDULE(DYNAMIC)
    DO K =1,NEL
        N = NG(K)
        M = MG(K)
        DO I = 0,N
            DO J = 0,M
                AU(I+(N+1)*J,K) = AU(I+(N+1)*J,K) + &
                            & SIGMA*U(I+(N+1)*J,K)*W(I,N)*W(J,M)*JAC(I+(N+1)*J,K)
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE DGHELMHOLTZ
