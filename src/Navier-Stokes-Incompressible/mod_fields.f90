
MODULE FIELDS
IMPLICIT NONE

DOUBLE PRECISION, ALLOCATABLE::U(:,:,:)
DOUBLE PRECISION, ALLOCATABLE::P(:,:)    
DOUBLE PRECISION, ALLOCATABLE::VORT(:,:)

DOUBLE PRECISION, ALLOCATABLE:: UAUX1(:,:,:)
DOUBLE PRECISION, ALLOCATABLE:: UAUX2(:,:,:)

DOUBLE PRECISION, ALLOCATABLE::AU(:,:)
DOUBLE PRECISION, ALLOCATABLE::RHS(:,:)
DOUBLE PRECISION, ALLOCATABLE::DUDX(:,:), DUDY(:,:)
DOUBLE PRECISION, ALLOCATABLE::TAU(:)

DOUBLE PRECISION, ALLOCATABLE::R (:,:)
DOUBLE PRECISION, ALLOCATABLE::Z (:,:)
DOUBLE PRECISION, ALLOCATABLE::PP(:,:)

DOUBLE PRECISION R_DOT_R, R_DOT_Z, R_DOT_Z_NEW
DOUBLE PRECISION R_DOT_R_INIT, PP_DOT_APP, SS

DOUBLE PRECISION, ALLOCATABLE::U_ADAPT_STORAGE(:,:,:)
DOUBLE PRECISION, ALLOCATABLE::P_ADAPT_STORAGE(:,:)

CONTAINS
    SUBROUTINE ALLOCATE_FIELDS
    USE SIZE, ONLY: NUM_EQN, NMMAX, LEVEL, NELMAX, NPMAX
    USE PARAM, ONLY: IFADAPT,IFADAPT_PROJ
    IMPLICIT NONE

    ALLOCATE(U(0:NPMAX,NELMAX,NUM_EQN))   
    ALLOCATE(P(0:NPMAX,NELMAX))   
    ALLOCATE(VORT(0:NPMAX,NELMAX)) 

    ALLOCATE(UAUX1(0:NPMAX,NELMAX,NUM_EQN))  
    ALLOCATE(UAUX2(0:NPMAX,NELMAX,NUM_EQN))  

    ALLOCATE(AU(0:NPMAX,NELMAX))
    ALLOCATE(RHS(0:NPMAX,NELMAX))
    ALLOCATE(DUDX(0:NPMAX,NELMAX))
    ALLOCATE(DUDY(0:NPMAX,NELMAX))
    ALLOCATE(TAU(NELMAX))     

    ALLOCATE(R(0:NPMAX,NELMAX))
    ALLOCATE(Z(0:NPMAX,NELMAX))
    ALLOCATE(PP(0:NPMAX,NELMAX))

    IF (IFADAPT.OR.IFADAPT_PROJ) ALLOCATE(U_ADAPT_STORAGE(0:NPMAX,NELMAX,NUM_EQN))  
    IF (IFADAPT.OR.IFADAPT_PROJ) ALLOCATE(P_ADAPT_STORAGE(0:NPMAX,NELMAX))  

    END SUBROUTINE ALLOCATE_FIELDS

!---------------------------------------------------------------------------------------

    SUBROUTINE DEALLOCATE_FIELDS
    USE PARAM, ONLY: IFADAPT,IFADAPT_PROJ
    IMPLICIT NONE

    DEALLOCATE(U)   
    DEALLOCATE(P)   
    DEALLOCATE(VORT) 

    DEALLOCATE(UAUX1)  
    DEALLOCATE(UAUX2)  

    DEALLOCATE(AU)
    DEALLOCATE(RHS)
    DEALLOCATE(DUDX)
    DEALLOCATE(DUDY)
    DEALLOCATE(TAU)     

    DEALLOCATE(R)
    DEALLOCATE(Z)
    DEALLOCATE(PP)

    IF (IFADAPT.OR.IFADAPT_PROJ) DEALLOCATE(U_ADAPT_STORAGE)  
    IF (IFADAPT.OR.IFADAPT_PROJ) DEALLOCATE(P_ADAPT_STORAGE)  

    END SUBROUTINE DEALLOCATE_FIELDS

!---------------------------------------------------------------------------------------

    SUBROUTINE INIT_FIELDS
    !!-----------------------------------------------------------------------
    !!
    !!       Set Initial condition
    !!
    !!-----------------------------------------------------------------------
    USE SIZE, ONLY: NEL, NG, MG, NMAX,MMAX,NMIN,MMIN,LEVEL,RANK
    USE BASIS, ONLY: QUAD
    USE MESH, ONLY: X, Y
    USE PARAM, ONLY: IFADAPT_PROJ,IFADAPT
    USE TIMESTEP, ONLY: T
    IMPLICIT NONE

    INTEGER N,M,I,J,K, LV, ITER
    DOUBLE PRECISION XX, YY

    IF (RANK.EQ.0) WRITE(*,'(32A)',advance='no') "Computing initial projection...."
    T = 0.0d0

    !$OMP PARALLEL PRIVATE(LV,ITER) DEFAULT(SHARED)
    
    !$OMP DO PRIVATE(K,N,M,XX,YY,I,J) SCHEDULE(DYNAMIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K) 
        DO J=0,M
            DO I=0,N
                XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N)+1.0d0)
                YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M)+1.0d0)

                CALL INITIAL_CONDITION(U(I+(N+1)*J,K,:),P(I+(N+1)*J,K),XX,YY)
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    IF (IFADAPT_PROJ) THEN
        ITER = MAX(2*LEVEL,2*(NMAX-NMIN),2*(MMAX-MMIN))
        DO LV = 1,ITER
            CALL ADAPTMESH_VORT

            !Re-project
            !$OMP DO PRIVATE(K,N,M,XX,YY,I,J) SCHEDULE(DYNAMIC)
            DO K=1,NEL
                N = NG(K)
                M = MG(K) 
                DO J=0,M
                    DO I=0,N
                        XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N)+1.0d0)
                        YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M)+1.0d0)
                        CALL INITIAL_CONDITION(U(I+(N+1)*J,K,:),P(I+(N+1)*J,K),XX,YY)
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO
        ENDDO
    ENDIF

    CALL SET_TAU

    IF (IFADAPT.OR.IFADAPT_PROJ) THEN
        !$OMP WORKSHARE
        U_ADAPT_STORAGE = U
        P_ADAPT_STORAGE = P
        !$OMP END WORKSHARE
    ENDIF

    !$OMP END PARALLEL

    IF (RANK.EQ.0) WRITE(*,*) "done"
    IF (RANK.EQ.0) WRITE(*,*)

    END SUBROUTINE INIT_FIELDS

!-----------------------------------------------------------------------

    SUBROUTINE SET_TAU()
    !!----------------------------------------------------------------
        !!
        !!      Set tau paramter in cell K 
        !!      Taken from K. Shahbazi "An explicit expression for the         
        !!      penalty paramter in the internal penalty method"
        !!---------------------------------------------------------------
    USE SIZE, ONLY: NEL, NG,MG
    USE MESH, ONLY: SCAL
    IMPLICIT NONE

    INTEGER K,NN

    !$OMP DO PRIVATE(K,NN) SCHEDULE(DYNAMIC)
    DO K = 1, NEL
        NN = MAX(NG(K),MG(K))

        !This is a rough bound, there is a tighter one if you check wheither the side is a boundary
        !!!!! THIS ASSUMES PARALLELOGRAM CELLS
        TAU(K) = 0.5d0*DBLE((NN+1)*(NN+2))*(SCAL(1,K)+SCAL(2,K)+SCAL(3,K)+SCAL(4,K))/(2.0d0*SCAL(1,K)*SCAL(2,K))
    ENDDO
    !$OMP END DO

    END SUBROUTINE

!---------------------------------------------------------------------------------

    SUBROUTINE WRITE_FIELDS(PERCENT)
    USE SIZE, ONLY: RANK, NUM_EQN, NPMAX, NELMAX, NEL, NG, MG, NPROC 
    USE PARAM, ONLY: ITER_VEL, ITER_PRES
    USE BASIS, ONLY: QUAD_LOBATTO, INTERP_LOBATTO
    USE MESH, ONLY: X,Y
    USE TIMESTEP, ONLY: T, TSTEP
    USE PARAM, ONLY: MAXTIME, MAXSTEPS, FRAME
    IMPLICIT NONE
    INCLUDE "mpif.h"

    REAL, INTENT(IN):: PERCENT

    INTEGER I,J,K,IEL
    INTEGER N,M,IJ,II,JJ

    INTEGER IERROR, STATS(MPI_STATUS_SIZE), PROC, ELEM

    DOUBLE PRECISION XX, YY, U_INTERP(NUM_EQN), P_INTERP, VORT_INTERP

    CHARACTER(LEN=16)FILENAME

    INTEGER NEL_LOCAL
    INTEGER,ALLOCATABLE :: NG_LOCAL(:), MG_LOCAL(:)
    DOUBLE PRECISION,ALLOCATABLE :: U_LOCAL(:,:,:)
    DOUBLE PRECISION,ALLOCATABLE :: P_LOCAL(:,:)
    DOUBLE PRECISION,ALLOCATABLE :: VORT_LOCAL(:,:)
    DOUBLE PRECISION,ALLOCATABLE :: X_LOCAL(:,:), Y_LOCAL(:,:)

    FRAME = FRAME +1

    IF (RANK.EQ.0) THEN !Only root process writes
        !.....WRITE TO SCREEN
        WRITE(*, '(70A,''('' I0 '') t = '' F0.4 ''  ('' F0.1 ''%), ITER_PRES = '', I0, '', ITER_VEL = '',I0, &
                    & ''      '')',ADVANCE='NO'), (CHAR(8),J=1,70),TSTEP,T,PERCENT, ITER_PRES, ITER_VEL
        IF (PERCENT.GE.100.0) WRITE(*,*)      

        !.....WRITE TO FILE
        WRITE(FILENAME,FMT='(''aoutput'',I5.5,''.dat'')') FRAME
        OPEN(18,FILE=FILENAME)

        WRITE(18,FMT='(''variables="X","Y",'')',advance='no')

        ALLOCATE(NG_LOCAL(NELMAX))
        ALLOCATE(MG_LOCAL(NELMAX))
        ALLOCATE(U_LOCAL(0:NPMAX,NELMAX,NUM_EQN))
        ALLOCATE(P_LOCAL(0:NPMAX,NELMAX))
        ALLOCATE(VORT_LOCAL(0:NPMAX,NELMAX))
        ALLOCATE(X_LOCAL(4,NELMAX))
        ALLOCATE(Y_LOCAL(4,NELMAX))

        NEL_LOCAL = NEL
        NG_LOCAL = NG
        MG_LOCAL = MG
        U_LOCAL = U
        P_LOCAL = P
        VORT_LOCAL = VORT
        X_LOCAL = X
        Y_LOCAL = Y
        
        
        DO K = 1,NUM_EQN
            WRITE(18,FMT='(''"U'',I1,''",'')',advance='no') K
        ENDDO
        WRITE(18,FMT='(''"P"'')',advance='no')
        WRITE(18,FMT='(''"VORT"'')') 

        ELEM = 1
        DO PROC = 0,NPROC-1
            IF (PROC.NE.0) THEN
                CALL MPI_RECV(NEL_LOCAL, 1, MPI_INTEGER, PROC, 0, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(NG_LOCAL, NEL_LOCAL, MPI_INTEGER, PROC, 1, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(MG_LOCAL, NEL_LOCAL, MPI_INTEGER, PROC, 2, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(U_LOCAL, (NPMAX+1)*NELMAX*NUM_EQN, MPI_DOUBLE_PRECISION, &
                                                    PROC, 3, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(P_LOCAL, (NPMAX+1)*NEL_LOCAL, MPI_DOUBLE_PRECISION, &
                                                        PROC, 4, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(VORT_LOCAL, (NPMAX+1)*NEL_LOCAL, MPI_DOUBLE_PRECISION, PROC, 5, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(X_LOCAL, 4*NEL_LOCAL, MPI_DOUBLE_PRECISION, PROC, 6, MPI_COMM_WORLD, STATS, IERROR)
                CALL MPI_RECV(Y_LOCAL, 4*NEL_LOCAL, MPI_DOUBLE_PRECISION, PROC, 7, MPI_COMM_WORLD, STATS, IERROR)
            ENDIF

            DO IEL=1,NEL_LOCAL
                N = NG_LOCAL(IEL)
                M = MG_LOCAL(IEL)

                WRITE(18,FMT='(''zone t="IEL'',I6,''",i='',I2,'',j='',I2, &
                             & '',SOLUTIONTIME='',F0.4)') ELEM,N+1,M+1,T
                ELEM = ELEM+1

                DO J=0,M
                    DO I=0,N
                        XX = X_LOCAL(1,IEL) + (X_LOCAL(2,IEL)-X_LOCAL(1,IEL))*0.5d0*(QUAD_LOBATTO(I,N)+1.0d0)
                        YY = Y_LOCAL(1,IEL) + (Y_LOCAL(3,IEL)-Y_LOCAL(1,IEL))*0.5d0*(QUAD_LOBATTO(J,M)+1.0d0)
                        IJ = I + (N+1)*J

                        U_INTERP = 0.0d0
                        P_INTERP = 0.0d0
                        VORT_INTERP=0.0d0
                        DO JJ = 0,M
                            DO II = 0,N
                                U_INTERP(:) = U_INTERP(:) & 
                                        + U_LOCAL(II+(N+1)*JJ,IEL,:)*INTERP_LOBATTO(II,I,N)*INTERP_LOBATTO(JJ,J,M)
                                P_INTERP = P_INTERP &
                                            + P_LOCAL(II+(N+1)*JJ,IEL)*INTERP_LOBATTO(II,I,N)*INTERP_LOBATTO(JJ,J,M)
                                VORT_INTERP = VORT_INTERP &
                                            + VORT_LOCAL(II+(N+1)*JJ,IEL)*INTERP_LOBATTO(II,I,N)*INTERP_LOBATTO(JJ,J,M)
                            ENDDO
                        ENDDO

                        WRITE(18,FMT='(2F20.12)',advance='no') XX, YY
                        DO K = 1,NUM_EQN
                            WRITE(18,FMT='(F20.12)',advance='no') U_INTERP(K)
                        ENDDO
                        WRITE(18,FMT='(F20.12)',advance='no') P_INTERP
                        WRITE(18,FMT='(F20.12)') VORT_INTERP
                    END DO
                END DO
            END DO
        ENDDO
        CLOSE(18)

        DEALLOCATE(NG_LOCAL)
        DEALLOCATE(MG_LOCAL)
        DEALLOCATE(U_LOCAL)
        DEALLOCATE(P_LOCAL)
        DEALLOCATE(VORT_LOCAL)
        DEALLOCATE(X_LOCAL)
        DEALLOCATE(Y_LOCAL)
    ELSE
        CALL MPI_SEND(NEL, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(NG, NEL, MPI_INTEGER, 0, 1, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(MG, NEL, MPI_INTEGER, 0, 2, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(U, (NPMAX+1)*NELMAX*NUM_EQN, MPI_DOUBLE_PRECISION, 0, 3, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(P, (NPMAX+1)*NEL, MPI_DOUBLE_PRECISION, 0, 4, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(VORT, (NPMAX+1)*NEL, MPI_DOUBLE_PRECISION, 0, 5, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(X, 4*NEL, MPI_DOUBLE_PRECISION, 0, 6, MPI_COMM_WORLD, IERROR)
        CALL MPI_SEND(Y, 4*NEL, MPI_DOUBLE_PRECISION, 0, 7, MPI_COMM_WORLD, IERROR)
    ENDIF

    END SUBROUTINE WRITE_FIELDS

!-----------------------------------------------------------------------------------------

    SUBROUTINE CALC_VORT()
    USE SIZE, ONLY: NG, MG, NEL
    USE MESH, ONLY: DXDR,DXDS,DYDR,DYDS, JAC
    USE BASIS, ONLY: DER
    IMPLICIT NONE   

    INTEGER N,M,I,J,KK,K

    DOUBLE PRECISION DUDR, DUDS, DVDR,DVDS

    !$OMP DO PRIVATE(N,M,DUDR,DUDS,DVDR,DVDS,KK,I,J,K) &
    !$OMP& SCHEDULE(STATIC)
    DO K=1,NEL
        N = NG(K)
        M = MG(K)

        !Compute CURL
        DO I = 0,N
            DO J = 0,M
                DUDR = 0.0d0
                DUDS = 0.0d0
                DVDR = 0.0d0
                DVDS = 0.0d0
                DO KK = 0,N
                    DUDR = DUDR + U(KK+(N+1)*J,K,1)*DER(I+(N+1)*KK,N)
                    DVDR = DVDR + U(KK+(N+1)*J,K,2)*DER(I+(N+1)*KK,N)
                ENDDO
                DO KK = 0,M
                    DUDS = DUDS + U(I+(N+1)*KK,K,1)*DER(J+(M+1)*KK,M)
                    DVDS = DVDS + U(I+(N+1)*KK,K,2)*DER(J+(M+1)*KK,M)
                ENDDO
                !!!VORT = V_x-U_y
                VORT(I+(N+1)*J,K) = (( DYDS(I+(N+1)*J,K)*DVDR-DYDR(I+(N+1)*J,K)*DVDS) &
                                &  -(-DXDS(I+(N+1)*J,K)*DUDR+DXDR(I+(N+1)*J,K)*DUDS) &
                                & )/JAC(I+(N+1)*J,K)
            ENDDO
        ENDDO
    ENDDO
    !$OMP ENDDO

    END SUBROUTINE

!-------------------------------------------------------------------------------------------

    SUBROUTINE UQ_HOMOGENEOUS_BOUNDARY(Q_R,Q_L,U_R,U_L,K,SIDE,N)
    !----------------------------------------------------------------
    !   Homogeneous U boundary conditions for use in DGPOISSON
    !---------------------------------------------------------------
    USE MESH, ONLY:BDRY_TYPE
    IMPLICIT NONE

    INTEGER,INTENT(IN):: N, K, SIDE
    DOUBLE PRECISION,INTENT(IN):: U_L(0:N), Q_L(0:N)
    DOUBLE PRECISION,INTENT(OUT):: U_R(0:N), Q_R(0:N)

    IF (BDRY_TYPE(SIDE,K).EQ.'O  ') THEN !Neumann
        U_R =  U_L
        Q_R = -Q_L
    ELSE !Homogenous Diriclet BCs
        U_R = -U_L 
        Q_R =  Q_L
    ENDIF

    END SUBROUTINE

!----------------------------------------------------------------------------------------

    SUBROUTINE PQ_HOMOGENEOUS_BOUNDARY(Q_R,Q_L,P_R,P_L,K,SIDE,N)
    !----------------------------------------------------------------
    !   Homogeneous P boundary conditions for use in DGPOISSON
    !---------------------------------------------------------------
    USE MESH, ONLY:BDRY_TYPE
    IMPLICIT NONE

    INTEGER,INTENT(IN):: N, K, SIDE
    DOUBLE PRECISION,INTENT(IN):: P_L(0:N), Q_L(0:N)
    DOUBLE PRECISION,INTENT(OUT):: P_R(0:N), Q_R(0:N)

    IF (BDRY_TYPE(SIDE,K).EQ.'O  ') THEN !Homogenous Dirichlet boundary for pressure
        P_R = -P_L
        Q_R =  Q_L
    ELSE !Homogenous Neumann BC
        P_R =  P_L 
        Q_R = -Q_L
    ENDIF

    END SUBROUTINE


!-------------------------------------------------------------------
!
!   Adaptation subroutines
!
!-------------------------------------------------------------

    SUBROUTINE SET_ADAPT_BACKUP()
    IMPLICIT NONE

    !$OMP WORKSHARE
    U_ADAPT_STORAGE = U
    P_ADAPT_STORAGE = P
    !$OMP END WORKSHARE

    END SUBROUTINE SET_ADAPT_BACKUP

!-------------------------------------------------------------------

    SUBROUTINE RESTORE_ADAPT_BACKUP()
    IMPLICIT NONE

    !$OMP WORKSHARE
    U = U_ADAPT_STORAGE
    P = P_ADAPT_STORAGE
    !$OMP END WORKSHARE

    END SUBROUTINE RESTORE_ADAPT_BACKUP

!-------------------------------------------------------------------


    SUBROUTINE PADAPT_FIELDS_X(K,STEP) 
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP
    USE MESH, ONLY: UPDATE_METRICS
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K, STEP 
    INTEGER I, J, KK, N, M 

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION P_TEMP(0:NPMAX)
    DOUBLE PRECISION B_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION BP_TEMP(0:NPMAX)

    N = NG(K)
    M = MG(K)

    U_TEMP = 0.0d0
    P_TEMP = 0.0d0
    B_TEMP = 0.0d0
    BP_TEMP = 0.0d0
    DO J = 0,M
        DO I = 0,N+STEP
            DO KK = 0,N
                U_TEMP(I+(N+1+STEP)*J,:) = U_TEMP(I+(N+1+STEP)*J,:) + INTERP(KK,I,N,N+STEP)*U(KK+(N+1)*J,K,:)
                P_TEMP(I+(N+1+STEP)*J) = P_TEMP(I+(N+1+STEP)*J) + INTERP(KK,I,N,N+STEP)*P(KK+(N+1)*J,K)
                B_TEMP(I+(N+1+STEP)*J,:) = B_TEMP(I+(N+1+STEP)*J,:) + INTERP(KK,I,N,N+STEP)*U_ADAPT_STORAGE(KK+(N+1)*J,K,:)
                BP_TEMP(I+(N+1+STEP)*J) = BP_TEMP(I+(N+1+STEP)*J) + INTERP(KK,I,N,N+STEP)*P_ADAPT_STORAGE(KK+(N+1)*J,K)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K,:) = U_TEMP
    P(0:NPMAX,K) = P_TEMP
    U_ADAPT_STORAGE(0:NPMAX,K,:) = B_TEMP
    P_ADAPT_STORAGE(0:NPMAX,K) = BP_TEMP

    NG(K) = N+STEP

    CALL UPDATE_METRICS(K) 

    END SUBROUTINE PADAPT_FIELDS_X

!---------------------------------------------------------------------------

    SUBROUTINE PADAPT_FIELDS_Y(K,STEP) 
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP
    USE MESH, ONLY: UPDATE_METRICS
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K, STEP
    INTEGER I, J, KK, N, M 

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION P_TEMP(0:NPMAX)
    DOUBLE PRECISION B_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION BP_TEMP(0:NPMAX)

    N = NG(K)
    M = MG(K)

    U_TEMP = 0.0d0
    P_TEMP = 0.0d0
    B_TEMP = 0.0d0
    BP_TEMP = 0.0d0
    DO J = 0,M+STEP
        DO I = 0,N
            DO KK = 0,M
                U_TEMP(I+(N+1)*J,:) = U_TEMP(I+(N+1)*J,:) + INTERP(KK,J,M,M+STEP)*U(I+(N+1)*KK,K,:)
                P_TEMP(I+(N+1)*J) = P_TEMP(I+(N+1)*J) + INTERP(KK,J,M,M+STEP)*P(I+(N+1)*KK,K)
                B_TEMP(I+(N+1)*J,:) = B_TEMP(I+(N+1)*J,:) + INTERP(KK,J,M,M+STEP)*U_ADAPT_STORAGE(I+(N+1)*KK,K,:)
                BP_TEMP(I+(N+1)*J) = BP_TEMP(I+(N+1)*J) + INTERP(KK,J,M,M+STEP)*P_ADAPT_STORAGE(I+(N+1)*KK,K)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K,:) = U_TEMP
    P(0:NPMAX,K) = P_TEMP
    U_ADAPT_STORAGE(0:NPMAX,K,:) = B_TEMP
    P_ADAPT_STORAGE(0:NPMAX,K) = BP_TEMP

    MG(K) = M+STEP

    CALL UPDATE_METRICS(K) 

    END SUBROUTINE PADAPT_FIELDS_Y

!------------------------------------------------------------------------------------------

    SUBROUTINE COARSEN_FIELDS_X(K1) 
    USE SIZE, ONLY: NG,MG,NPMAX,NMMAX, NUM_EQN
    USE BASIS, ONLY: W, INTERP, INTERP_LEFT, INTERP_RIGHT
    USE MESH, ONLY: NEIGHBOUR_ID
    IMPLICIT NONE

    INTEGER K1, K2, I, J,II,JJ, KK, N1, M1, N2, M2, N_MAX, M_MAX

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION U1(NUM_EQN,0:NMMAX), U2(NUM_EQN,0:NMMAX)
    DOUBLE PRECISION P_TEMP(0:NPMAX)
    DOUBLE PRECISION P1(0:NMMAX), P2(0:NMMAX)

    !Because of the way the cells are refined, and the fact that child 1 calls the coarsening,
    ! the cell to the right is the cell which coarsens with this one
    K2 = NEIGHBOUR_ID(2,1,K1)

    N1 = NG(K1)
    M1 = MG(K1)
    N2 = NG(K2)
    M2 = MG(K2)

    !The degree of the coarsened cell will be the max of the children cells
    N_MAX = MAX(N1,N2)
    M_MAX = MAX(M1,M2)

    !This projection assumes paralleogram cells. A more general projection should include the jacobians
    U_TEMP = 0.0d0
    P_TEMP = 0.0d0
    DO J = 0,M_MAX
        !Interpolate N_MAX points in Cell 1 along eta_j
        U1 = 0.0d0
        P1 = 0.0d0
        DO JJ=0,M1
            DO II = 0,N1
                DO KK = 0,N_MAX
                    U1(:,KK) = U1(:,KK) + U(II+(N1+1)*JJ,K1,:)*INTERP(II,KK,N1,N_MAX)*INTERP(JJ,J,M1,M_MAX)
                    P1(KK) = P1(KK) + P(II+(N1+1)*JJ,K1)*INTERP(II,KK,N1,N_MAX)*INTERP(JJ,J,M1,M_MAX)
                ENDDO
            ENDDO
        ENDDO
       
        !And in cell 2
        U2 = 0.0d0
        P2 = 0.0d0
        DO JJ=0,M2
            DO II = 0,N2
                DO KK = 0,N_MAX
                    U2(:,KK) = U2(:,KK) + U(II+(N2+1)*JJ,K2,:)*INTERP(II,KK,N2,N_MAX)*INTERP(JJ,J,M2,M_MAX)
                    P2(KK) = P2(KK) + P(II+(N2+1)*JJ,K2)*INTERP(II,KK,N2,N_MAX)*INTERP(JJ,J,M2,M_MAX)
                ENDDO
            ENDDO
        ENDDO

        DO I = 0,N_MAX
            DO KK = 0,N_MAX
                U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:) + U1(:,KK)*INTERP_LEFT(I,KK,N_MAX,N_MAX)*W(KK,N_MAX) &
                                                            & + U2(:,KK)*INTERP_RIGHT(I,KK,N_MAX,N_MAX)*W(KK,N_MAX)
                P_TEMP(I+(N_MAX+1)*J) = P_TEMP(I+(N_MAX+1)*J) + P1(KK)*INTERP_LEFT(I,KK,N_MAX,N_MAX)*W(KK,N_MAX) &
                                                            & + P2(KK)*INTERP_RIGHT(I,KK,N_MAX,N_MAX)*W(KK,N_MAX)
            ENDDO
            U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:)/(2.0d0*W(I,N_MAX))
            P_TEMP(I+(N_MAX+1)*J) = P_TEMP(I+(N_MAX+1)*J)/(2.0d0*W(I,N_MAX))
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U_TEMP
    P(0:NPMAX,K1) = P_TEMP

    NG(K1) = N_MAX
    MG(K1) = M_MAX

    NG(K2) = -1
    MG(K2) = -1

    END SUBROUTINE COARSEN_FIELDS_X

!-----------------------------------------------------------------------------------------------------

    SUBROUTINE COARSEN_FIELDS_Y(K1) 
    USE SIZE, ONLY: NG,MG,NPMAX,NMMAX,NUM_EQN
    USE BASIS, ONLY: W, INTERP, INTERP_LEFT, INTERP_RIGHT
    USE MESH, ONLY: NEIGHBOUR_ID
    IMPLICIT NONE

    INTEGER K1, K2, I, J,II,JJ, KK, N1, M1, N2, M2, N_MAX, M_MAX

    DOUBLE PRECISION U_TEMP(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION U1(NUM_EQN,0:NMMAX), U2(NUM_EQN,0:NMMAX)
    DOUBLE PRECISION P_TEMP(0:NPMAX)
    DOUBLE PRECISION P1(0:NMMAX), P2(0:NMMAX)

    !Because of the way the cells are refined, and the fact that child 1 calls the coarsening,
    ! the cell to the right is the cell which coarsens with this one
    K2 = NEIGHBOUR_ID(3,1,K1)

    N1 = NG(K1)
    M1 = MG(K1)
    N2 = NG(K2)
    M2 = MG(K2)

    !The degree of the coarsened cell will be the max of the children cells
    N_MAX = MAX(N1,N2)
    M_MAX = MAX(M1,M2)

    !This projection assumes paralleogram cells. A more general projection should include the jacobians
    U_TEMP = 0.0d0
    P_TEMP = 0.0d0
    DO I = 0,N_MAX
        !Interpolate N_MAX points in Cell 1 along xi_i
        U1 = 0.0d0
        P1 = 0.0d0
        DO JJ=0,M1
            DO II = 0,N1
                DO KK = 0,M_MAX
                    U1(:,KK) = U1(:,KK) + U(II+(N1+1)*JJ,K1,:)*INTERP(II,I,N1,N_MAX)*INTERP(JJ,KK,M1,M_MAX)
                    P1(KK) = P1(KK) + P(II+(N1+1)*JJ,K1)*INTERP(II,I,N1,N_MAX)*INTERP(JJ,KK,M1,M_MAX)
                ENDDO
            ENDDO
        ENDDO
       
        !And in cell 2
        U2 = 0.0d0
        P2 = 0.0d0
        DO JJ=0,M2
            DO II = 0,N2
                DO KK = 0,M_MAX
                    U2(:,KK) = U2(:,KK) + U(II+(N2+1)*JJ,K2,:)*INTERP(II,I,N2,N_MAX)*INTERP(JJ,KK,M2,M_MAX)
                    P2(KK) = P2(KK) + P(II+(N2+1)*JJ,K2)*INTERP(II,I,N2,N_MAX)*INTERP(JJ,KK,M2,M_MAX)
                ENDDO
            ENDDO
        ENDDO

        DO J = 0,M_MAX
            DO KK = 0,M_MAX
                U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:) + U1(:,KK)*INTERP_LEFT(J,KK,M_MAX,M_MAX)*W(KK,M_MAX) &
                                                            & + U2(:,KK)*INTERP_RIGHT(J,KK,M_MAX,M_MAX)*W(KK,M_MAX)
                P_TEMP(I+(N_MAX+1)*J) = P_TEMP(I+(N_MAX+1)*J) + P1(KK)*INTERP_LEFT(J,KK,M_MAX,M_MAX)*W(KK,M_MAX) &
                                                            & + P2(KK)*INTERP_RIGHT(J,KK,M_MAX,M_MAX)*W(KK,M_MAX)                                                            
            ENDDO
            U_TEMP(I+(N_MAX+1)*J,:) = U_TEMP(I+(N_MAX+1)*J,:)/(2.0d0*W(J,M_MAX))
            P_TEMP(I+(N_MAX+1)*J) = P_TEMP(I+(N_MAX+1)*J)/(2.0d0*W(J,M_MAX))
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U_TEMP
    P(0:NPMAX,K1) = P_TEMP

    NG(K1) = N_MAX
    MG(K1) = M_MAX

    NG(K2) = -1
    MG(K2) = -1

    END SUBROUTINE COARSEN_FIELDS_Y

!-------------------------------------------------------------------------------

    SUBROUTINE MOVE_FIELDS(K1,K2)
    USE SIZE, ONLY:NG,MG
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2

    NG(K2) = NG(K1)
    MG(K2) = MG(K1)

    U(:,K2,:) = U(:,K1,:)
    P(:,K2) = P(:,K1)

    NG(K1) = -1
    MG(K1) = -1

    END SUBROUTINE MOVE_FIELDS

!-------------------------------------------------------------------------------

    SUBROUTINE REFINE_FIELDS_X(K1,K2)
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP_LEFT, INTERP_RIGHT
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2
    INTEGER I, J, KK, N, M

    DOUBLE PRECISION U1(0:NPMAX,NUM_EQN), U2(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION P1(0:NPMAX), P2(0:NPMAX)
    DOUBLE PRECISION B1(0:NPMAX,NUM_EQN), B2(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION BP1(0:NPMAX), BP2(0:NPMAX)


    N = NG(K1)
    M = MG(K1)

    !Since we're splitting a cell, we can project through simple interpolation
    U1 = 0.0d0
    U2 = 0.0d0
    P1 = 0.0d0
    P2 = 0.0d0
    B1 = 0.0d0
    B2 = 0.0d0
    BP1 = 0.0d0
    BP2 = 0.0d0
    DO J = 0,M
        DO I = 0,N
            DO KK = 0,N
                U1(I+(N+1)*J,:) = U1(I+(N+1)*J,:) + U(KK+(N+1)*J,K1,:)*INTERP_LEFT(KK,I,N,N)
                U2(I+(N+1)*J,:) = U2(I+(N+1)*J,:) + U(KK+(N+1)*J,K1,:)*INTERP_RIGHT(KK,I,N,N)
                P1(I+(N+1)*J) = P1(I+(N+1)*J) + P(KK+(N+1)*J,K1)*INTERP_LEFT(KK,I,N,N)
                P2(I+(N+1)*J) = P2(I+(N+1)*J) + P(KK+(N+1)*J,K1)*INTERP_RIGHT(KK,I,N,N)
                B1(I+(N+1)*J,:) = B1(I+(N+1)*J,:) + U_ADAPT_STORAGE(KK+(N+1)*J,K1,:)*INTERP_LEFT(KK,I,N,N)
                B2(I+(N+1)*J,:) = B2(I+(N+1)*J,:) + U_ADAPT_STORAGE(KK+(N+1)*J,K1,:)*INTERP_RIGHT(KK,I,N,N)
                BP1(I+(N+1)*J) = BP1(I+(N+1)*J) + P_ADAPT_STORAGE(KK+(N+1)*J,K1)*INTERP_LEFT(KK,I,N,N)
                BP2(I+(N+1)*J) = BP2(I+(N+1)*J) + P_ADAPT_STORAGE(KK+(N+1)*J,K1)*INTERP_RIGHT(KK,I,N,N)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U1
    U(0:NPMAX,K2,:) = U2
    P(0:NPMAX,K1) = P1
    P(0:NPMAX,K2) = P2
    U_ADAPT_STORAGE(0:NPMAX,K1,:) = B1
    U_ADAPT_STORAGE(0:NPMAX,K2,:) = B2
    P_ADAPT_STORAGE(0:NPMAX,K1) = BP1
    P_ADAPT_STORAGE(0:NPMAX,K2) = BP2

    NG(K2) = N
    MG(K2) = M

    END SUBROUTINE REFINE_FIELDS_X

!-----------------------------------------------------------------------------

    SUBROUTINE REFINE_FIELDS_Y(K1,K2)
    USE SIZE, ONLY: NG,MG,NPMAX,NUM_EQN
    USE BASIS, ONLY: INTERP_LEFT, INTERP_RIGHT
    IMPLICIT NONE

    INTEGER,INTENT(IN):: K1, K2
    INTEGER I, J, KK, N, M

    DOUBLE PRECISION U1(0:NPMAX,NUM_EQN), U2(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION P1(0:NPMAX), P2(0:NPMAX)
    DOUBLE PRECISION B1(0:NPMAX,NUM_EQN), B2(0:NPMAX,NUM_EQN)
    DOUBLE PRECISION BP1(0:NPMAX), BP2(0:NPMAX)

    N = NG(K1)
    M = MG(K1)

    !Since we're splitting a cell, we can project through simple interpolation
    U1 = 0.0d0
    U2 = 0.0d0
    P1 = 0.0d0
    P2 = 0.0d0
    B1 = 0.0d0
    B2 = 0.0d0
    BP1 = 0.0d0
    BP2 = 0.0d0
    DO J = 0,M
        DO I = 0,N
            DO KK = 0,M
                U1(I+(N+1)*J,:) = U1(I+(N+1)*J,:) + U(I+(N+1)*KK,K1,:)*INTERP_LEFT(KK,J,M,M)
                U2(I+(N+1)*J,:) = U2(I+(N+1)*J,:) + U(I+(N+1)*KK,K1,:)*INTERP_RIGHT(KK,J,M,M)
                P1(I+(N+1)*J) = P1(I+(N+1)*J) + P(I+(N+1)*KK,K1)*INTERP_LEFT(KK,J,M,M)
                P2(I+(N+1)*J) = P2(I+(N+1)*J) + P(I+(N+1)*KK,K1)*INTERP_RIGHT(KK,J,M,M)
                B1(I+(N+1)*J,:) = B1(I+(N+1)*J,:) + U_ADAPT_STORAGE(I+(N+1)*KK,K1,:)*INTERP_LEFT(KK,J,M,M)
                B2(I+(N+1)*J,:) = B2(I+(N+1)*J,:) + U_ADAPT_STORAGE(I+(N+1)*KK,K1,:)*INTERP_RIGHT(KK,J,M,M)
                BP1(I+(N+1)*J) = BP1(I+(N+1)*J) + P_ADAPT_STORAGE(I+(N+1)*KK,K1)*INTERP_LEFT(KK,J,M,M)
                BP2(I+(N+1)*J) = BP2(I+(N+1)*J) + P_ADAPT_STORAGE(I+(N+1)*KK,K1)*INTERP_RIGHT(KK,J,M,M)
            ENDDO
        ENDDO
    ENDDO

    U(0:NPMAX,K1,:) = U1
    U(0:NPMAX,K2,:) = U2
    P(0:NPMAX,K1) = P1
    P(0:NPMAX,K2) = P2
    U_ADAPT_STORAGE(0:NPMAX,K1,:) = B1
    U_ADAPT_STORAGE(0:NPMAX,K2,:) = B2
    P_ADAPT_STORAGE(0:NPMAX,K1) = BP1
    P_ADAPT_STORAGE(0:NPMAX,K2) = BP2

    NG(K2) = N
    MG(K2) = M
 
    END SUBROUTINE REFINE_FIELDS_Y

END MODULE FIELDS

