
    SUBROUTINE ADAPTMESH_VORT()
    !---------------------------------------------------------------
    !    This is a specialized main adaptation subroutine which uses the vorticity
    !    to compute the error esimation. 
    !-----------------------------------------------------------------
    USE SIZE, ONLY: NEL, NUM_EQN, NG, MG
    USE PARAM, ONLY: NASTEPS
    USE TIMESTEP, ONLY: T, TSTEP
    USE FIELDS, ONLY: VORT, SET_ADAPT_BACKUP, RESTORE_ADAPT_BACKUP,CALC_VORT
    USE ADAPT
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IERROR, K

    !Error estimator on vorticity
    CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
    !CALL DGVORT(VORT,U,T)
    CALL CALC_VORT
    CALL ERROR_ESTIM_MAVRIPLIS(VORT,1)

    !Start with a refinement check
    CALL MARK_ELEMENTS_REFINE
    CALL REFINE_NONCONFORM_CHECK
    CALL MAX_ELEM_CHECK

    !$OMP MASTER
    ACCEPT_MESH = .TRUE.
    !$OMP END MASTER
    !$OMP BARRIER
    !$OMP DO REDUCTION(.AND.:ACCEPT_MESH)
    DO K = 1,NEL !Check if anything should refine
        IF (FLAG_P_ENRICH_X(K).OR.FLAG_P_ENRICH_Y(K)&
            &.OR.FLAG_H_REFINE_X(K).OR.FLAG_H_REFINE_Y(K)) ACCEPT_MESH = .FALSE.
    ENDDO
    !$OMP END DO
    !$OMP MASTER
    CALL MPI_ALLREDUCE(ACCEPT_MESH,ACCEPT_MESH,1,MPI_LOGICAL,&
                            & MPI_LAND,MPI_COMM_WORLD,IERROR)
    !$OMP END MASTER
    !$OMP BARRIER

    IF (ACCEPT_MESH) THEN !Mesh is acceptable at this timestep
        !Coarsen solution as much as possible
        DO 
            !Error estimator on vorticity
            CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
            !CALL DGVORT(VORT,U,T)
            CALL CALC_VORT
            CALL ERROR_ESTIM_MAVRIPLIS(VORT,1)

            CALL MARK_ELEMENTS_COARSEN
            CALL COARSEN_NONCONFORM_CHECK

            !$OMP MASTER
            ACCEPT_MESH = .TRUE.
            !$OMP END MASTER
            !$OMP BARRIER
            !$OMP DO REDUCTION(.AND.:ACCEPT_MESH)
            DO K = 1,NEL
                IF (FLAG_P_DIMINISH_X(K).OR.FLAG_P_DIMINISH_Y(K)&
                    &.OR.FLAG_H_COARSEN_X(K).OR.FLAG_H_COARSEN_Y(K)) ACCEPT_MESH = .FALSE.
            ENDDO
            !$OMP END DO

            !$OMP MASTER
            CALL MPI_ALLREDUCE(ACCEPT_MESH,ACCEPT_MESH,1,MPI_LOGICAL,&
                                    & MPI_LAND,MPI_COMM_WORLD,IERROR)
            !$OMP END MASTER
            !$OMP BARRIER

            IF (ACCEPT_MESH) EXIT

            CALL ADAPTMESH_COARSEN
        ENDDO 

        !$OMP MASTER
        ADAPT_START_TIME = T
        !$OMP END MASTER
        !$OMP BARRIER
    ELSE
        !Refine until the mesh is acceptable at this timestep.
        DO 
            !Error estimator on vorticity
            CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
            !CALL DGVORT(VORT,U,T)
            CALL CALC_VORT
            CALL ERROR_ESTIM_MAVRIPLIS(VORT,1)

            CALL MARK_ELEMENTS_REFINE
            CALL REFINE_NONCONFORM_CHECK
            CALL MAX_ELEM_CHECK

            !$OMP MASTER
            ACCEPT_MESH = .TRUE.
            !$OMP END MASTER
            !$OMP BARRIER
            !$OMP DO REDUCTION(.AND.:ACCEPT_MESH)
            DO K = 1,NEL !Check if anything should refine
                IF (FLAG_P_ENRICH_X(K).OR.FLAG_P_ENRICH_Y(K)&
                    &.OR.FLAG_H_REFINE_X(K).OR.FLAG_H_REFINE_Y(K)) ACCEPT_MESH = .FALSE.
            ENDDO
            !$OMP END DO
            !$OMP MASTER
            CALL MPI_ALLREDUCE(ACCEPT_MESH,ACCEPT_MESH,1,MPI_LOGICAL,&
                                    & MPI_LAND,MPI_COMM_WORLD,IERROR)
            !$OMP END MASTER
            !$OMP BARRIER

            IF (ACCEPT_MESH) EXIT
            
            CALL ADAPTMESH_REFINE
        ENDDO

        !Now go back and repeat the timestepping
        !$OMP MASTER
        T = ADAPT_START_TIME
        TSTEP = TSTEP - NASTEPS
        !$OMP END MASTER
        !$OMP BARRIER

        CALL RESTORE_ADAPT_BACKUP
    ENDIF

    CALL SET_ADAPT_BACKUP

    END SUBROUTINE ADAPTMESH_VORT

