MODULE SIZE
IMPLICIT NONE
INCLUDE "input.f90"

INTEGER RANK
INTEGER NEL, NDIM, NGROUPS,NPROC, NEL_TOTAL

INTEGER, ALLOCATABLE::NG(:),MG(:)

INTEGER,PARAMETER:: NPMAX = (NMAX+1)*(MMAX+1)-1
INTEGER,PARAMETER:: NMMAX = MAX(NMAX,MMAX)

CONTAINS 
    SUBROUTINE ALLOCATE_SIZE
    IMPLICIT NONE

    ALLOCATE(NG(NELMAX))
    ALLOCATE(MG(NELMAX))

    END SUBROUTINE ALLOCATE_SIZE

!--------------------------------------------------------------------
    
    SUBROUTINE DEALLOCATE_SIZE
    IMPLICIT NONE

    DEALLOCATE(NG)
    DEALLOCATE(MG)

    END SUBROUTINE DEALLOCATE_SIZE

!--------------------------------------------------------------------

    SUBROUTINE INIT_SIZE
    IMPLICIT NONE

    NG = N
    MG = M

    END SUBROUTINE INIT_SIZE 

END MODULE SIZE