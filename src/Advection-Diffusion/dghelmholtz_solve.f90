    
    SUBROUTINE DGHELMHOLTZ_SOLVE(U,RHS,T,DT)
    USE SIZE, ONLY: NPMAX, NELMAX, NEL, NUM_EQN, NG, MG
    USE PARAM, ONLY: ITERMAX, TOLVEL, ITER_VEL
    USE MESH, ONLY: JAC,DXDR,DXDS,DYDR,DYDS
    USE BASIS, ONLY: W
    USE TIMESTEP, ONLY: DIRK_A
    USE FIELDS, ONLY: AU, R, PP, Z, &
                & R_DOT_R, R_DOT_R_INIT, PP_DOT_APP, &
                & R_DOT_Z, R_DOT_Z_NEW
    USE MULTIGRID, ONLY: MULTIGRID_PRECONDITIONER
    IMPLICIT NONE
    INCLUDE "mpif.h"

    DOUBLE PRECISION,INTENT(INOUT):: U(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION,INTENT(INOUT):: RHS(0:NPMAX,NELMAX,NUM_EQN)
    DOUBLE PRECISION,INTENT(IN):: T, DT

    INTEGER MM, I, J, K, N ,M, IERROR

    DOUBLE PRECISION SIGMA

    CALL GET_SIGMA(SIGMA)

    !First, weight the RHS forcing
    DO MM =1,NUM_EQN
        !$OMP DO PRIVATE(N,M,I,J,K)&
        !$OMP&     SCHEDULE(DYNAMIC)
        DO K =1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    RHS(I+(N+1)*J,K,MM) = -RHS(I+(N+1)*J,K,MM) &
                                    & *W(I,N)*W(J,M)*JAC(I+(N+1)*J,K)/(DT*DIRK_A(1)*SIGMA)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
    ENDDO

    !Second, move any boundary contributons to the RHS
    DO MM =1,NUM_EQN    
        CALL DGPOISSON_RHS(R,MM,T)
        !$OMP WORKSHARE
        RHS(:,:,MM) = RHS(:,:,MM) + R
        !$OMP END WORKSHARE
    ENDDO

    !Then solve eqch equation via preconditioned conjugate gradient
    DO MM = 1,NUM_EQN
        !Compute residual
        CALL DGHELMHOLTZ(AU,U(:,:,MM),-1.0d0/(DT*DIRK_A(1)*SIGMA),&
                            & NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

        !$OMP WORKSHARE
        R = RHS(:,:,MM)-AU
        Z = R!0.0d0
        !$OMP END WORKSHARE
        !CALL MULTIGRID_PRECONDITIONER(Z,R,DGHELMHOLTZ)

        !$OMP WORKSHARE
        PP = Z
        !$OMP END WORKSHARE

        !$OMP MASTER
        R_DOT_Z = 0.0d0
        R_DOT_R = 0.0d0
        PP_DOT_APP = 0.0d0
        R_DOT_Z_NEW = 0.0d0
        !$OMP END MASTER
        !$OMP BARRIER

        !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:R_DOT_R) REDUCTION(+:R_DOT_Z) SCHEDULE(STATIC)
        DO K = 1,NEL
            N = NG(K)
            M = MG(K)
            DO I = 0,N
                DO J = 0,M
                    R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                    R_DOT_Z = R_DOT_Z + R(I+(N+1)*J,K)*Z(I+(N+1)*J,K)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP MASTER
        CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        CALL MPI_ALLREDUCE(R_DOT_Z, R_DOT_Z, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
        R_DOT_R_INIT = DSQRT(R_DOT_R)
        R_DOT_R = 0.0d0
        ITER_VEL = 0
        !$OMP END MASTER
        !$OMP BARRIER

        !........MAIN LOOP
        !Preconditioned Conjugate Gradient method
        DO
            CALL DGHELMHOLTZ(AU,PP,-1.0d0/(DT*DIRK_A(1)*SIGMA), &
                        & NG,MG,JAC,DXDR,DXDS,DYDR,DYDS,NPMAX)

            !$OMP DO PRIVATE(N,M,I,J) REDUCTION(+:PP_DOT_APP) SCHEDULE(STATIC)
            DO K = 1,NEL
                N = NG(K)
                M = MG(K)
                DO I = 0,N
                    DO J = 0,M
                        PP_DOT_APP = PP_DOT_APP + PP(I+(N+1)*J,K)*AU(I+(N+1)*J,K)
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO
            !$OMP MASTER
            CALL MPI_ALLREDUCE(PP_DOT_APP, PP_DOT_APP, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            !$OMP END MASTER
            !$OMP BARRIER

            !$OMP WORKSHARE
            U(:,:,MM) = U(:,:,MM) + (R_DOT_Z/PP_DOT_APP)*PP
            R = R - (R_DOT_Z/PP_DOT_APP)*AU
            !$OMP END WORKSHARE
            
            !Apply preconditioner
            !$OMP WORKSHARE
            Z=R !0.0d0
            !$OMP END WORKSHARE
            !CALL MULTIGRID_PRECONDITIONER(Z,R,DGHELMHOLTZ)

            !$OMP DO PRIVATE(N,M,I,J,K) REDUCTION(+:R_DOT_Z_NEW) REDUCTION(+:R_DOT_R) SCHEDULE(STATIC)
            DO K = 1,NEL
                N = NG(K)
                M = MG(K)
                DO I = 0,N
                    DO J = 0,M
                        R_DOT_R = R_DOT_R + R(I+(N+1)*J,K)**2
                        R_DOT_Z_NEW = R_DOT_Z_NEW + R(I+(N+1)*J,K)*Z(I+(N+1)*J,K)
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO
            !$OMP MASTER
            CALL MPI_ALLREDUCE(R_DOT_R, R_DOT_R, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            CALL MPI_ALLREDUCE(R_DOT_Z_NEW, R_DOT_Z_NEW, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, IERROR)
            ITER_VEL = ITER_VEL + 1
            !$OMP END MASTER
            !$OMP BARRIER

            IF ((DSQRT(R_DOT_R)).LT. TOLVEL) EXIT
            IF (ITER_VEL .GT. ITERMAX) STOP "ITERMAX Reached"

            !$OMP WORKSHARE
            PP = Z + (R_DOT_Z_NEW/R_DOT_Z)*PP
            !$OMP END WORKSHARE

            !$OMP MASTER
            R_DOT_Z = R_DOT_Z_NEW
            PP_DOT_APP = 0.0d0
            R_DOT_Z_NEW = 0.0d0
            R_DOT_R = 0.0d0
            !$OMP END MASTER  
            !$OMP BARRIER          
        ENDDO
    ENDDO

    END SUBROUTINE DGHELMHOLTZ_SOLVE
