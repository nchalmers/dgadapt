
    SUBROUTINE MAIN_LOOP
    IMPLICIT NONE

    !Implicit explicit time stepping
    CALL MAIN_LOOP_IMEX

    !OR

    !Explicit tim stepping
    !CALL MAIN_LOOP_EX

    END SUBROUTINE MAIN_LOOP

!-----------------------------------------------------------------------

    SUBROUTINE MAIN_LOOP_IMEX
    USE SIZE,ONLY: NUM_EQN, NELMAX, NPMAX, NMMAX, NG,MG, RANK
    USE PARAM, ONLY: MAXTIME, MAXSTEPS,FRAME,MAXFRAMES, IFADAPT,NASTEPS
    USE FIELDS, ONLY: U, WRITE_FIELDS,SET_TAU
    USE TIMESTEP, ONLY: T,DT, TSTEP,RK_A,RK_C, U_RK_STORAGE, &
                & RK_RHS_STORAGE, NUM_STAGES,  &
                & DIRK_A, IMEX_RK_TABLEAU, DIRK_RHS_STORAGE
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS, SHARE_TAU_BDRY_BUFFERS
    !$ USE OMP_LIB
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IERROR, I, S
    DOUBLE PRECISION CPU_START,CPU_FINAL, SIGMA
    DOUBLE PRECISION ERROR_U
    REAL PERCENT

    CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
    CALL SHARE_TAU_BDRY_BUFFERS

    !Implicit/Explicit time stepping
    CALL IMEX_RK_TABLEAU(4)

    CALL GET_SIGMA(SIGMA)

    ALLOCATE(U_RK_STORAGE(0:NPMAX,NELMAX,NUM_EQN,2))
    ALLOCATE(RK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,NUM_STAGES))
    ALLOCATE(DIRK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,NUM_STAGES))

    T = 0.0d0
    TSTEP = 0
    FRAME = 0
    PERCENT = 0.0

    IF (RANK.EQ.0) WRITE(*,'("Computing...")')
    CALL WRITE_FIELDS(0.0)

    !$OMP PARALLEL DEFAULT(SHARED)

    CALL CPU_TIME(CPU_START)
    !$ CPU_START = OMP_GET_WTIME()

    !.......MAIN LOOP
    DO WHILE( (T.LT.MAXTIME) .AND. &
               & ( (TSTEP.LT.MAXSTEPS).AND.(MAXSTEPS.NE.-1) ) ) 
        CALL CFL_CONDITION_IMEX
        !$OMP MASTER
        CALL MPI_ALLREDUCE(DT, DT, 1, MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, IERROR)
        IF (MAXTIME .GE. 0.0d0) THEN
            IF (T+DT .GT. MAXTIME) DT = MAXTIME - T
        ENDIF
        !$OMP END MASTER
        !$OMP BARRIER

        CALL DGCONVECT(RK_RHS_STORAGE(:,:,:,1),U,T)

        DO S=1,NUM_STAGES
            !Calculate rhs for linear solve
            !$OMP WORKSHARE
            U_RK_STORAGE(:,:,:,1) = U
            !$OMP END WORKSHARE
            DO I = 1,S
                !$OMP WORKSHARE
                U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) + DT*RK_A(I+(S-1)*NUM_STAGES)*RK_RHS_STORAGE(:,:,:,I)
                !$OMP END WORKSHARE
            ENDDO

            DO I = 1,S-1
                !$OMP WORKSHARE
                U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) + DT*SIGMA*DIRK_A(I+(S-1)*NUM_STAGES)*DIRK_RHS_STORAGE(:,:,:,I)
                !$OMP END WORKSHARE
            ENDDO

            CALL DGHELMHOLTZ_SOLVE(U_RK_STORAGE(:,:,:,2),U_RK_STORAGE(:,:,:,1),T+RK_C(S+1)*DT,DT)
            !U_RK_STORAGE(:,:,:,2) = U_RK_STORAGE(:,:,:,1)

            IF (S==NUM_STAGES) EXIT

            !Calculate convective term at U_i
            CALL DGCONVECT(RK_RHS_STORAGE(:,:,:,S+1),U_RK_STORAGE(:,:,:,2),T+RK_C(S+1)*DT)

            !Calculate diffusive term at U_i
            CALL DGDIFFUSION(DIRK_RHS_STORAGE(:,:,:,S),U_RK_STORAGE(:,:,:,2),T+RK_C(S+1)*DT)
        ENDDO

        !$OMP WORKSHARE
        U = U_RK_STORAGE(:,:,:,2)
        !$OMP END WORKSHARE

        !$OMP MASTER
        T = T + DT
        TSTEP = TSTEP+1
        IF (MAXTIME.LT.0) THEN
            PERCENT = REAL(TSTEP*100)/REAL(MAXSTEPS)
        ELSEIF (MAXSTEPS.LT.0) THEN
            PERCENT = REAL(T/MAXTIME)*100.0
        ELSE
            PERCENT = MAX(REAL(TSTEP*100)/REAL(MAXSTEPS),REAL(T/MAXTIME)*100)
        ENDIF

        IF (PERCENT.GE. REAL(FRAME*100)/REAL(MAXFRAMES)) CALL WRITE_FIELDS(PERCENT)
        !$OMP END MASTER
        !$OMP BARRIER
        IF (PERCENT.GE.100.0) EXIT

        !Adapt
        IF (IFADAPT.AND.MOD(TSTEP,NASTEPS).EQ.0) THEN
            CALL ADAPTMESH
            CALL SET_TAU(NG,MG)
            CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
            CALL SHARE_TAU_BDRY_BUFFERS
        ENDIF
    END DO

    CALL EVAL_ERROR(ERROR_U)
    !$OMP END PARALLEL

    CALL CPU_TIME(CPU_FINAL)
    !$ CPU_FINAL = OMP_GET_WTIME()
    IF (RANK.EQ.0) THEN
        WRITE(*,*)
        PRINT '("ERROR_U = ",E12.5)', ERROR_U
        PRINT '("Time = ",f8.3," seconds.")',CPU_FINAL-CPU_START
    ENDIF

    END SUBROUTINE MAIN_LOOP_IMEX

!-----------------------------------------------------------------------

    SUBROUTINE MAIN_LOOP_EX
    USE SIZE,ONLY: NUM_EQN, NELMAX, NPMAX, NMMAX, NG,MG, RANK
    USE PARAM, ONLY: MAXTIME, MAXSTEPS,FRAME,MAXFRAMES,IFADAPT,NASTEPS
    USE FIELDS, ONLY: U, WRITE_FIELDS,SET_TAU
    USE TIMESTEP, ONLY: T,DT, TSTEP,RK_A,RK_B,RK_C, U_RK_STORAGE, &
                & RK_RHS_STORAGE, NUM_STAGES, BUTCHER_TABLEAU, &
                & DIRK_RHS_STORAGE
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS, SHARE_TAU_BDRY_BUFFERS
    !$ USE OMP_LIB
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER K, M, IERROR
    DOUBLE PRECISION CPU_START,CPU_FINAL,SIGMA
    DOUBLE PRECISION ERROR_U
    REAL PERCENT

    CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
    CALL SHARE_TAU_BDRY_BUFFERS

    !Explicit time stepping
    CALL BUTCHER_TABLEAU(NMMAX)

    ALLOCATE(U_RK_STORAGE(0:NPMAX,NELMAX,NUM_EQN,1))
    ALLOCATE(RK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,NUM_STAGES))
    ALLOCATE(DIRK_RHS_STORAGE(0:NPMAX,NELMAX,NUM_EQN,1))

    T = 0.0d0
    TSTEP = 0
    FRAME = 0
    PERCENT = 0.0

    CALL GET_SIGMA(SIGMA)

    IF (RANK.EQ.0) WRITE(*,'("Computing...")')
    CALL WRITE_FIELDS(0.0)

    !.......MAIN LOOP
    CALL CPU_TIME(CPU_START)
    !$ CPU_START = OMP_GET_WTIME()

    !$OMP PARALLEL DEFAULT(SHARED)
    DO WHILE( (T.LT.MAXTIME) .AND. &
               & ( (TSTEP.LT.MAXSTEPS).AND.(MAXSTEPS.NE.-1) ) ) 
        CALL CFL_CONDITION_EXPLICIT     !.......Compute stable time step
        !$OMP MASTER
        CALL MPI_ALLREDUCE(DT, DT, 1, MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, IERROR)
        IF (MAXTIME.GE.0.0d0) THEN
            IF (T+DT .GT. MAXTIME) DT = MAXTIME - T
        ENDIF
        !$OMP END MASTER
        !$OMP BARRIER

        DO K=1,NUM_STAGES
            !Compute the intermediate U to eval the RHS at
            !$OMP WORKSHARE
            U_RK_STORAGE(:,:,:,1) = U
            !$OMP END WORKSHARE
            DO M = 1,K-1 !K-1 is assumed since these are explicit RK methods
                IF (RK_A((K-1)*NUM_STAGES + M).NE.0.0d0) THEN !Don't do unnecessary adds
                    !$OMP WORKSHARE
                    U_RK_STORAGE(:,:,:,1) = U_RK_STORAGE(:,:,:,1) + &
                                        & DT*RK_A((K-1)*NUM_STAGES + M)*RK_RHS_STORAGE(:,:,:,M)
                    !$OMP END WORKSHARE
                ENDIF
            ENDDO

            !Compute RHS at this stage
            !Advection terms
            CALL DGCONVECT(RK_RHS_STORAGE(:,:,:,K),U_RK_STORAGE(:,:,:,1),T+RK_C(K)*DT)
            !Diffusion terms
            CALL DGDIFFUSION(DIRK_RHS_STORAGE(:,:,:,1),U_RK_STORAGE(:,:,:,1),T+RK_C(K)*DT)
        
            !Add together
            !$OMP WORKSHARE
            RK_RHS_STORAGE(:,:,:,K) = RK_RHS_STORAGE(:,:,:,K) + SIGMA*DIRK_RHS_STORAGE(:,:,:,1)
            !$OMP END WORKSHARE
        ENDDO

        !FINAL STAGE
        DO K = 1,NUM_STAGES
            IF (RK_B(K).NE.0.0d0) THEN !Don't do unnecessary adds
                !$OMP WORKSHARE
                U = U + DT*RK_B(K)*RK_RHS_STORAGE(:,:,:,K)
                !$OMP END WORKSHARE
            ENDIF
        ENDDO

        !$OMP MASTER
        T = T + DT
        TSTEP = TSTEP+1
        IF (MAXTIME.LT.0) THEN
            PERCENT = REAL(TSTEP*100)/REAL(MAXSTEPS)
        ELSEIF (MAXSTEPS.LT.0) THEN
            PERCENT = REAL(T/MAXTIME)*100.0
        ELSE
            PERCENT = MAX(REAL(TSTEP*100)/REAL(MAXSTEPS),REAL(T/MAXTIME)*100)
        ENDIF

        IF (PERCENT.GE. REAL(FRAME*100)/REAL(MAXFRAMES)) CALL WRITE_FIELDS(PERCENT)
        !$OMP END MASTER
        !$OMP BARRIER
        IF (PERCENT.GE.100.0) EXIT

        !Adapt
        IF (IFADAPT.AND.MOD(TSTEP,NASTEPS).EQ.0) THEN
            CALL ADAPTMESH
            CALL SET_TAU(NG,MG)
            CALL SHARE_ORDER_BDRY_BUFFERS(NG,MG)
            CALL SHARE_TAU_BDRY_BUFFERS
        ENDIF
    END DO

    CALL EVAL_ERROR(ERROR_U)
    !$OMP END PARALLEL

    CALL CPU_TIME(CPU_FINAL)
    !$ CPU_FINAL = OMP_GET_WTIME()
    IF (RANK.EQ.0) THEN
        WRITE(*,*)
        PRINT '("ERROR_U = ",E12.5)', ERROR_U
        PRINT '("Time = ",f8.3," seconds.")',CPU_FINAL-CPU_START
    ENDIF

    END SUBROUTINE MAIN_LOOP_EX


    SUBROUTINE EVAL_ERROR(ERROR_U)
    USE FIELDS, ONLY: U
    USE BASIS, ONLY: W, QUAD,INTERP
    USE SIZE, ONLY: NG, MG, NEL,NUM_EQN
    USE MESH, ONLY: X, Y, QUADMAP
    USE TIMESTEP, ONLY: T
    IMPLICIT NONE

    INTEGER K, I, J, II,JJ, N, M
    DOUBLE PRECISION XX, YY
    DOUBLE PRECISION UU_NUM(NUM_EQN)
    DOUBLE PRECISION UU(NUM_EQN)

    DOUBLE PRECISION DXDR,DXDS,DYDR,DYDS, JAC

    DOUBLE PRECISION ERROR_U

    !$OMP MASTER
    ERROR_U = 0.0d0
    !$OMP END MASTER

    !$OMP DO PRIVATE(K,N,M,XX,YY,I,J,II,JJ,UU_NUM,UU,DXDR,DXDS,DYDR,DYDS,JAC) &
    !$OMP&  REDUCTION(+:ERROR_U)
    DO K=1,NEL
        N = NG(K)
        M = MG(K) 
        DO J=0,M+1
            DO I=0,N+1
                XX = X(1,K) + (X(2,K)-X(1,K))*0.5d0*(QUAD(I,N+1)+1.0d0)
                YY = Y(1,K) + (Y(4,K)-Y(1,K))*0.5d0*(QUAD(J,M+1)+1.0d0)

                CALL QUADMAP(DXDR,DXDS, DYDR,DYDS, &
                             QUAD(I,N+1),QUAD(J,M+1),X(:,K),Y(:,K))    
                JAC = DXDR*DYDS - DXDS*DYDR

                CALL EXACT_SOLUTION(UU,XX,YY,T)

                UU_NUM = 0.0d0
                DO JJ = 0,M
                    DO II = 0,N
                        UU_NUM = UU_NUM + U(II+(N+1)*JJ,K,:)*INTERP(II,I,N,N+1)*INTERP(JJ,J,M,M+1)
                    ENDDO
                ENDDO

                DO II = 1,NUM_EQN
                    ERROR_U = ERROR_U + DABS(UU_NUM(II)-UU(II))*W(I,N+1)*W(J,M+1)*JAC
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO

    END SUBROUTINE EVAL_ERROR
