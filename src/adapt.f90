
    SUBROUTINE ADAPTMESH()
    !---------------------------------------------------------------
    !    This is the main adaptation subroutine. 
    !
    !    In the first part we apply p-adaptation to all cells that 
    !    are flagged for it. Nothing speacial is required here as we 
    !    we currently don't enforce a p-nonconformity degree. 
    !
    !    We then check h nonconformity which involves looping through
    !    the cells from highest to lowest level and checking their 
    !    neighbours' flags.
    !
    !    Once degree 2 non-conformity has been checked we assemble 
    !    a list of elements to coarsen and coarsen these elements in
    !    parallel. We then fill any hole in the solution arrays created
    !    by coarsening cells.
    !
    !    Finally we assemble a list of cells to refine, first checking 
    !    if too many cells wish to refine and removing cells from the 
    !    refinement list if necessary. We then refine these elements in 
    !    parallel
    !-----------------------------------------------------------------
    USE SIZE, ONLY: NEL, NUM_EQN, NG, MG
    USE PARAM, ONLY: NASTEPS
    USE TIMESTEP, ONLY: T, TSTEP
    USE FIELDS, ONLY: U, SET_ADAPT_BACKUP, RESTORE_ADAPT_BACKUP
    USE ADAPT
    USE COMMS, ONLY: SHARE_ORDER_BDRY_BUFFERS
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IERROR, K

    CALL ERROR_ESTIM_MAVRIPLIS(U,NUM_EQN)
    !CALL ERROR_ESTIM
    !CALL ERROR_ESTIM_GRAD

    !Start with a refinement check
    CALL MARK_ELEMENTS_REFINE
    CALL REFINE_NONCONFORM_CHECK
    CALL MAX_ELEM_CHECK

    !$OMP MASTER
    ACCEPT_MESH = .TRUE.
    !$OMP END MASTER
    !$OMP BARRIER
    !$OMP DO REDUCTION(.AND.:ACCEPT_MESH)
    DO K = 1,NEL !Check if anything should refine
        IF (FLAG_P_ENRICH_X(K).OR.FLAG_P_ENRICH_Y(K)&
            &.OR.FLAG_H_REFINE_X(K).OR.FLAG_H_REFINE_Y(K)) ACCEPT_MESH = .FALSE.
    ENDDO
    !$OMP END DO
    !$OMP MASTER
    CALL MPI_ALLREDUCE(ACCEPT_MESH,ACCEPT_MESH,1,MPI_LOGICAL,&
                            & MPI_LAND,MPI_COMM_WORLD,IERROR)
    !$OMP END MASTER
    !$OMP BARRIER

    IF (ACCEPT_MESH) THEN !Mesh is acceptable at this timestep
        !Coarsen solution as much as possible
        DO 
            CALL ERROR_ESTIM_MAVRIPLIS(U,NUM_EQN)
            !CALL ERROR_ESTIM
            !CALL ERROR_ESTIM_GRAD

            CALL MARK_ELEMENTS_COARSEN
            CALL COARSEN_NONCONFORM_CHECK

            !$OMP MASTER
            ACCEPT_MESH = .TRUE.
            !$OMP END MASTER
            !$OMP BARRIER
            !$OMP DO REDUCTION(.AND.:ACCEPT_MESH)
            DO K = 1,NEL
                IF (FLAG_P_DIMINISH_X(K).OR.FLAG_P_DIMINISH_Y(K)&
                    &.OR.FLAG_H_COARSEN_X(K).OR.FLAG_H_COARSEN_Y(K)) ACCEPT_MESH = .FALSE.
            ENDDO
            !$OMP END DO

            !$OMP MASTER
            CALL MPI_ALLREDUCE(ACCEPT_MESH,ACCEPT_MESH,1,MPI_LOGICAL,&
                                    & MPI_LAND,MPI_COMM_WORLD,IERROR)
            !$OMP END MASTER
            !$OMP BARRIER

            IF (ACCEPT_MESH) EXIT

            CALL ADAPTMESH_COARSEN
        ENDDO 

        !$OMP MASTER
        ADAPT_START_TIME = T
        !$OMP END MASTER
        !$OMP BARRIER
    ELSE
        !Refine until the mesh is acceptable at this timestep.
        DO 
            CALL ERROR_ESTIM_MAVRIPLIS(U,NUM_EQN)
            !CALL ERROR_ESTIM
            !CALL ERROR_ESTIM_GRAD

            CALL MARK_ELEMENTS_REFINE
            CALL REFINE_NONCONFORM_CHECK
            CALL MAX_ELEM_CHECK

            !$OMP MASTER
            ACCEPT_MESH = .TRUE.
            !$OMP END MASTER
            !$OMP BARRIER
            !$OMP DO REDUCTION(.AND.:ACCEPT_MESH)
            DO K = 1,NEL !Check if anything should refine
                IF (FLAG_P_ENRICH_X(K).OR.FLAG_P_ENRICH_Y(K)&
                    &.OR.FLAG_H_REFINE_X(K).OR.FLAG_H_REFINE_Y(K)) ACCEPT_MESH = .FALSE.
            ENDDO
            !$OMP END DO
            !$OMP MASTER
            CALL MPI_ALLREDUCE(ACCEPT_MESH,ACCEPT_MESH,1,MPI_LOGICAL,&
                                    & MPI_LAND,MPI_COMM_WORLD,IERROR)
            !$OMP END MASTER
            !$OMP BARRIER

            IF (ACCEPT_MESH) EXIT
            
            CALL ADAPTMESH_REFINE
        ENDDO

        !Now go back and repeat the timestepping
        !$OMP MASTER
        T = ADAPT_START_TIME
        TSTEP = TSTEP - NASTEPS
        !$OMP END MASTER
        !$OMP BARRIER

        CALL RESTORE_ADAPT_BACKUP
    ENDIF

    CALL SET_ADAPT_BACKUP

    END SUBROUTINE ADAPTMESH

!-----------------------------------------------------------------------------------------------------------------

    SUBROUTINE ADAPTMESH_REFINE
    USE SIZE, ONLY: NEL,NELMAX, LEVEL
    USE MESH, ONLY: REFINE_METRICS_X, REFINE_METRICS_Y
    USE FIELDS, ONLY: REFINE_FIELDS_X, REFINE_FIELDS_Y, &
                    & PADAPT_FIELDS_X, PADAPT_FIELDS_Y
    USE ADAPT
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER K, LV

    !Start with p adaptation, nothing special required
    !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
    DO K =1,NEL
        IF (FLAG_P_ENRICH_X(K).EQV..TRUE.)   CALL PADAPT_FIELDS_X(K,1)
        IF (FLAG_P_ENRICH_Y(K).EQV..TRUE.)   CALL PADAPT_FIELDS_Y(K,1)
    ENDDO
    !$OMP ENDDO

    !$OMP MASTER
    ALLOCATE(REFINE_LIST_X(NUM_NEW_CELLS))
    ALLOCATE(REFINE_LIST_Y(NUM_NEW_CELLS))
    ALLOCATE(REFINE_LIST_LEVEL_X(NUM_NEW_CELLS))
    ALLOCATE(REFINE_LIST_LEVEL_Y(NUM_NEW_CELLS))
    ALLOCATE(REFINE_POSITION_X(NUM_NEW_CELLS))
    ALLOCATE(REFINE_POSITION_Y(NUM_NEW_CELLS))
    NUM_CELLS_REFINE_X = 0
    NUM_CELLS_REFINE_Y = 0
    COUNT = NEL + 1
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO PRIVATE(K)
    DO K=1,NEL
        IF ((FLAG_H_REFINE_X(K).EQV..TRUE.).AND.(FLAG_H_REFINE_Y(K).EQV..TRUE.)) THEN
            IF (LEVEL_Y(K).LT.LEVEL_X(K)) THEN
                !Cell will refine in y first
                !$OMP CRITICAL
                NUM_CELLS_REFINE_Y = NUM_CELLS_REFINE_Y + 1
                REFINE_LIST_Y(NUM_CELLS_REFINE_Y) = K
                REFINE_LIST_LEVEL_Y(NUM_CELLS_REFINE_Y) = LEVEL_Y(K)
                REFINE_POSITION_Y(NUM_CELLS_REFINE_Y) = COUNT
                COUNT = COUNT+1

                NUM_CELLS_REFINE_X = NUM_CELLS_REFINE_X + 2
                REFINE_LIST_X(NUM_CELLS_REFINE_X-1) = K
                REFINE_LIST_X(NUM_CELLS_REFINE_X)   = COUNT-1
                REFINE_LIST_LEVEL_X(NUM_CELLS_REFINE_X-1) = LEVEL_X(K)
                REFINE_LIST_LEVEL_X(NUM_CELLS_REFINE_X)   = LEVEL_X(K)
                REFINE_POSITION_X(NUM_CELLS_REFINE_X-1) = COUNT
                REFINE_POSITION_X(NUM_CELLS_REFINE_X)   = COUNT+1
                COUNT = COUNT+2
                !$OMP END CRITICAL
            ELSE
                !Cell will refine in x first
                !$OMP CRITICAL
                NUM_CELLS_REFINE_X = NUM_CELLS_REFINE_X + 1
                REFINE_LIST_X(NUM_CELLS_REFINE_X) = K
                REFINE_LIST_LEVEL_X(NUM_CELLS_REFINE_X) = LEVEL_X(K)
                REFINE_POSITION_X(NUM_CELLS_REFINE_X) = COUNT
                COUNT = COUNT+1

                NUM_CELLS_REFINE_Y = NUM_CELLS_REFINE_Y + 2
                REFINE_LIST_Y(NUM_CELLS_REFINE_Y-1) = K
                REFINE_LIST_Y(NUM_CELLS_REFINE_Y)   = COUNT-1
                REFINE_LIST_LEVEL_Y(NUM_CELLS_REFINE_Y-1) = LEVEL_Y(K)
                REFINE_LIST_LEVEL_Y(NUM_CELLS_REFINE_Y)   = LEVEL_Y(K)
                REFINE_POSITION_Y(NUM_CELLS_REFINE_Y-1) = COUNT
                REFINE_POSITION_Y(NUM_CELLS_REFINE_Y)   = COUNT+1
                COUNT = COUNT+2
                !$OMP END CRITICAL
            ENDIF
        ELSEIF (FLAG_H_REFINE_X(K).EQV..TRUE.) THEN
            !$OMP CRITICAL
            NUM_CELLS_REFINE_X = NUM_CELLS_REFINE_X + 1
            REFINE_LIST_X(NUM_CELLS_REFINE_X) = K
            REFINE_LIST_LEVEL_X(NUM_CELLS_REFINE_X) = LEVEL_X(K)
            REFINE_POSITION_X(NUM_CELLS_REFINE_X) = COUNT
            COUNT = COUNT+1
            !$OMP END CRITICAL
        ELSEIF (FLAG_H_REFINE_Y(K).EQV..TRUE.) THEN
            !$OMP CRITICAL
            NUM_CELLS_REFINE_Y = NUM_CELLS_REFINE_Y + 1
            REFINE_LIST_Y(NUM_CELLS_REFINE_Y) = K
            REFINE_LIST_LEVEL_Y(NUM_CELLS_REFINE_Y) = LEVEL_Y(K)
            REFINE_POSITION_Y(NUM_CELLS_REFINE_Y) = COUNT
            COUNT = COUNT+1
            !$OMP END CRITICAL
        ENDIF
    ENDDO    
    !$OMP ENDDO

    !$OMP MASTER
    NUM_INDEX_LOCAL(:) = 0
    !$OMP END MASTER
    !$OMP BARRIER

    !Refine
    DO LV = 0,LEVEL-1
        !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
        DO K=1,NUM_CELLS_REFINE_X
            IF (REFINE_LIST_LEVEL_X(K) .EQ. LV) THEN
                CALL REFINE_FIELDS_X(REFINE_LIST_X(K),REFINE_POSITION_X(K))
                CALL REFINE_ADAPT_TREE_X(REFINE_LIST_X(K),REFINE_POSITION_X(K))
                CALL REFINE_METRICS_X(REFINE_LIST_X(K),REFINE_POSITION_X(K))
                !$OMP CRITICAL
                CALL UPDATE_CONECTIVITY_REFINE_X(REFINE_LIST_X(K),REFINE_POSITION_X(K))
                !$OMP END CRITICAL
            ENDIF
        ENDDO
        !$OMP ENDDO
        CALL UPDATE_GLOBAL_CONNECTIVITY_REFINE()
        !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
        DO K=1,NUM_CELLS_REFINE_Y
            IF (REFINE_LIST_LEVEL_Y(K) .EQ. LV) THEN
                CALL REFINE_FIELDS_Y(REFINE_LIST_Y(K),REFINE_POSITION_Y(K))
                CALL REFINE_ADAPT_TREE_Y(REFINE_LIST_Y(K),REFINE_POSITION_Y(K))
                CALL REFINE_METRICS_Y(REFINE_LIST_Y(K),REFINE_POSITION_Y(K))
                !$OMP CRITICAL
                CALL UPDATE_CONECTIVITY_REFINE_Y(REFINE_LIST_Y(K),REFINE_POSITION_Y(K))
                !$OMP END CRITICAL
            ENDIF
        ENDDO
        !$OMP ENDDO
        CALL UPDATE_GLOBAL_CONNECTIVITY_REFINE()
    ENDDO

    !$OMP MASTER 
    DEALLOCATE(REFINE_LIST_X)
    DEALLOCATE(REFINE_LIST_Y)
    DEALLOCATE(REFINE_LIST_LEVEL_X)
    DEALLOCATE(REFINE_LIST_LEVEL_Y)
    DEALLOCATE(REFINE_POSITION_X)
    DEALLOCATE(REFINE_POSITION_Y)
    NEL = NEL + NUM_NEW_CELLS
    !$OMP END MASTER
    !$OMP BARRIER

    END SUBROUTINE ADAPTMESH_REFINE

!-----------------------------------------------------------------------------

    SUBROUTINE ADAPTMESH_COARSEN
    USE SIZE, ONLY: NEL,NELMAX, LEVEL, NG
    USE MESH, ONLY: NEIGHBOUR_ID, &
                    & COARSEN_METRICS_X, COARSEN_METRICS_Y, &
                    & MOVE_METRICS
    USE FIELDS, ONLY: COARSEN_FIELDS_X, COARSEN_FIELDS_Y, &
                    & PADAPT_FIELDS_X, PADAPT_FIELDS_Y, &
                    & MOVE_FIELDS
    USE ADAPT
    IMPLICIT NONE

    INTEGER K, LV

    !Start with p adaptation, nothing special required
    !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
    DO K =1,NEL
        IF (FLAG_P_DIMINISH_X(K).EQV..TRUE.) CALL PADAPT_FIELDS_X(K,-1)
        IF (FLAG_P_DIMINISH_Y(K).EQV..TRUE.) CALL PADAPT_FIELDS_Y(K,-1)
    ENDDO
    !$OMP ENDDO

    !First count the number of cells coarsening
    !$OMP MASTER
    NUM_CELLS_COARSEN_X = 0
    NUM_CELLS_COARSEN_Y = 0
    NUM_REMOVED_CELLS = 0
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO PRIVATE(K)
    DO K=1,NEL
        IF ((FLAG_H_COARSEN_X(K).EQV..TRUE.).AND.(FLAG_H_COARSEN_Y(K).EQV..TRUE.)) THEN
            IF ((MOD(TREEINDEX_X(K),2).EQ.1).AND.(MOD(TREEINDEX_Y(K),2).EQ.1)) THEN
                IF (LEVEL_Y(K).GT.LEVEL_X(K)) THEN
                    !cells will coarsen in y first
                    !$OMP CRITICAL
                    NUM_CELLS_COARSEN_Y = NUM_CELLS_COARSEN_Y+2                 
                    NUM_CELLS_COARSEN_X = NUM_CELLS_COARSEN_X+1
                    NUM_REMOVED_CELLS = NUM_REMOVED_CELLS + 3
                    !$OMP END CRITICAL
                ELSE
                    !cells will coarsen in x first
                    !$OMP CRITICAL
                    NUM_CELLS_COARSEN_X = NUM_CELLS_COARSEN_X+2
                    NUM_CELLS_COARSEN_Y = NUM_CELLS_COARSEN_Y+1                 
                    NUM_REMOVED_CELLS = NUM_REMOVED_CELLS + 3
                    !$OMP END CRITICAL
                ENDIF
            ENDIF
        ELSEIF ((FLAG_H_COARSEN_X(K).EQV..TRUE.).AND.(MOD(TREEINDEX_X(K),2).EQ.1)) THEN
            !$OMP CRITICAL
            NUM_CELLS_COARSEN_X = NUM_CELLS_COARSEN_X +1
            NUM_REMOVED_CELLS = NUM_REMOVED_CELLS +1
            !$OMP END CRITICAL
        ELSEIF ((FLAG_H_COARSEN_Y(K).EQV..TRUE.).AND.(MOD(TREEINDEX_Y(K),2).EQ.1)) THEN
            !$OMP CRITICAL
            NUM_CELLS_COARSEN_Y = NUM_CELLS_COARSEN_Y +1
            NUM_REMOVED_CELLS = NUM_REMOVED_CELLS +1
            !$OMP END CRITICAL
        ENDIF
    ENDDO
    !$OMP ENDDO

    !We now need to build some arrays to facilitate coarsening in parallel. ----------------------------------------
    !$OMP MASTER
    ALLOCATE(COARSEN_LIST_X(NUM_CELLS_COARSEN_X))
    ALLOCATE(COARSEN_LIST_Y(NUM_CELLS_COARSEN_Y))
    ALLOCATE(COARSEN_LIST_LEVEL_X(NUM_CELLS_COARSEN_X))
    ALLOCATE(COARSEN_LIST_LEVEL_Y(NUM_CELLS_COARSEN_Y))
    ALLOCATE(REMOVED_CELL_LIST(NUM_REMOVED_CELLS))
    COUNT = 0
    COUNT_X = 0
    COUNT_Y = 0
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO PRIVATE(K)
    DO K=1,NEL
        IF ((FLAG_H_COARSEN_X(K).EQV..TRUE.).AND.(FLAG_H_COARSEN_Y(K).EQV..TRUE.)) THEN
            IF ((MOD(TREEINDEX_X(K),2).EQ.1).AND.(MOD(TREEINDEX_Y(K),2).EQ.1)) THEN
                IF (LEVEL_Y(K).GT.LEVEL_X(K)) THEN
                    !cells will coarsen in y first
                    !$OMP CRITICAL
                    COUNT_Y = COUNT_Y+2
                    COARSEN_LIST_Y(COUNT_Y-1) = K
                    COARSEN_LIST_Y(COUNT_Y  ) = NEIGHBOUR_ID(2,1,K)
                    COARSEN_LIST_LEVEL_Y(COUNT_Y-1) = LEVEL_Y(K)
                    COARSEN_LIST_LEVEL_Y(COUNT_Y  ) = LEVEL_Y(K)

                    COUNT_X = COUNT_X+1
                    COARSEN_LIST_X(COUNT_X) = K
                    COARSEN_LIST_LEVEL_X(COUNT_X) = LEVEL_X(K)
                    !$OMP END CRITICAL
                ELSE
                    !cells will coarsen in x first
                    !$OMP CRITICAL
                    COUNT_X = COUNT_X + 2
                    COARSEN_LIST_X(COUNT_X-1) = K
                    COARSEN_LIST_X(COUNT_X  ) = NEIGHBOUR_ID(3,1,K)
                    COARSEN_LIST_LEVEL_X(COUNT_X-1) = LEVEL_X(K)
                    COARSEN_LIST_LEVEL_X(COUNT_X  ) = LEVEL_X(K)

                    COUNT_Y = COUNT_Y + 1
                    COARSEN_LIST_Y(COUNT_Y) = K
                    COARSEN_LIST_LEVEL_Y(COUNT_Y) = LEVEL_Y(K)
                    !$OMP END CRITICAL
                ENDIF
            ELSE
                !$OMP CRITICAL
                COUNT = COUNT + 1
                REMOVED_CELL_LIST(COUNT) = K
                !$OMP END CRITICAL
            ENDIF
        ELSEIF (FLAG_H_COARSEN_X(K).EQV..TRUE.) THEN
            IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN
                !$OMP CRITICAL
                COUNT_X = COUNT_X +1
                COARSEN_LIST_X(COUNT_X) = K
                COARSEN_LIST_LEVEL_X(COUNT_X) = LEVEL_X(K)
                !$OMP END CRITICAL
            ELSE
                !$OMP CRITICAL
                COUNT = COUNT + 1
                REMOVED_CELL_LIST(COUNT) = K
                !$OMP END CRITICAL
            ENDIF
        ELSEIF (FLAG_H_COARSEN_Y(K).EQV..TRUE.) THEN
            IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN
                !$OMP CRITICAL
                COUNT_Y = COUNT_Y + 1
                COARSEN_LIST_Y(COUNT_Y) = K
                COARSEN_LIST_LEVEL_Y(COUNT_Y) = LEVEL_Y(K)
                !$OMP END CRITICAL
            ELSE
                !$OMP CRITICAL
                COUNT = COUNT + 1
                REMOVED_CELL_LIST(COUNT) = K
                !$OMP END CRITICAL
            ENDIF
        ENDIF
    ENDDO
    !$OMP ENDDO

    !$OMP MASTER
    NUM_INDEX_LOCAL(:) = 0
    NUM_REMOVED_INDEX_LOCAL(:) = 0
    !$OMP END MASTER
    !$OMP BARRIER

    !---------------------Coarsen cells-----------------------------------
    DO LV = LEVEL,1,-1
        !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
        DO K=1,NUM_CELLS_COARSEN_X
            IF (COARSEN_LIST_LEVEL_X(K) .EQ. LV) THEN
                CALL COARSEN_FIELDS_X(COARSEN_LIST_X(K))
                CALL COARSEN_ADAPT_TREE_X(COARSEN_LIST_X(K))
                CALL COARSEN_METRICS_X(COARSEN_LIST_X(K))
                !$OMP CRITICAL
                CALL UPDATE_CONECTIVITY_COARSEN_X(COARSEN_LIST_X(K))
                !$OMP END CRITICAL
            ENDIF
        ENDDO
        !$OMP ENDDO
        CALL UPDATE_GLOBAL_CONNECTIVITY_COARSEN()
        !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
        DO K=1,NUM_CELLS_COARSEN_Y
            IF (COARSEN_LIST_LEVEL_Y(K) .EQ. LV) THEN
                CALL COARSEN_FIELDS_Y(COARSEN_LIST_Y(K))
                CALL COARSEN_ADAPT_TREE_Y(COARSEN_LIST_Y(K))
                CALL COARSEN_METRICS_Y(COARSEN_LIST_Y(K))
                !$OMP CRITICAL
                CALL UPDATE_CONECTIVITY_COARSEN_Y(COARSEN_LIST_Y(K))
                !$OMP END CRITICAL
            ENDIF
        ENDDO
        !$OMP ENDDO
        CALL UPDATE_GLOBAL_CONNECTIVITY_COARSEN()
    ENDDO

    !----------------------Move Cells-----------------------------------------

    !After coarsening, we need to rearrange the solution arrays to remove holes. 

    !$OMP MASTER
    DEALLOCATE(COARSEN_LIST_X)
    DEALLOCATE(COARSEN_LIST_Y)
    DEALLOCATE(COARSEN_LIST_LEVEL_X)
    DEALLOCATE(COARSEN_LIST_LEVEL_Y)
    ALLOCATE(CELL_MOVE_START(NUM_REMOVED_CELLS))
    ALLOCATE(CELL_MOVE_END(NUM_REMOVED_CELLS))
    NUM_MOVED_CELLS = 0
    COUNT = 1
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC) 
    DO K = NEL,NEL- NUM_REMOVED_CELLS+1,-1
        IF (NG(K).EQ. -1) CYCLE !Cell already removed

        !$OMP CRITICAL
        NUM_MOVED_CELLS = NUM_MOVED_CELLS +1
        CELL_MOVE_START(NUM_MOVED_CELLS) = K
        DO
            IF (.NOT.(REMOVED_CELL_LIST(COUNT).GT.(NEL-NUM_REMOVED_CELLS))) EXIT
            COUNT = COUNT+1
        ENDDO
        CELL_MOVE_END(NUM_MOVED_CELLS) = REMOVED_CELL_LIST(COUNT)
        COUNT = COUNT+1
        !$OMP END CRITICAL
    ENDDO
    !$OMP ENDDO

    !$OMP DO PRIVATE(K) SCHEDULE(DYNAMIC)
    DO K = 1,NUM_MOVED_CELLS
        CALL MOVE_FIELDS(CELL_MOVE_START(K),CELL_MOVE_END(K))
        CALL MOVE_ADAPT_TREE(CELL_MOVE_START(K),CELL_MOVE_END(K))
        CALL MOVE_METRICS(CELL_MOVE_START(K),CELL_MOVE_END(K))
        !$OMP CRITICAL
        CALL UPDATE_CONECTIVITY_MOVE(CELL_MOVE_START(K),CELL_MOVE_END(K))
        !$OMP END CRITICAL
    ENDDO
    !$OMP ENDDO

    !Next we fill holes in the local indexing along global boundaries. 
    CALL UPDATE_GLOBAL_CONNECTIVITY_MOVE()

    !$OMP MASTER
    DEALLOCATE(CELL_MOVE_START)
    DEALLOCATE(CELL_MOVE_END)
    DEALLOCATE(REMOVED_CELL_LIST)
    NEL = NEL - NUM_REMOVED_CELLS
    !$OMP END MASTER
    !$OMP BARRIER

    END SUBROUTINE ADAPTMESH_COARSEN

!------------------------------------------------------------------------------

    SUBROUTINE REFINE_NONCONFORM_CHECK
    USE SIZE, ONLY: NEL,NELMAX, LEVEL
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, & 
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_TYPE, &
                    & COARSEN_METRICS_X, COARSEN_METRICS_Y, &
                    & MOVE_METRICS, &
                    & REFINE_METRICS_X, REFINE_METRICS_Y
    USE FIELDS, ONLY: SET_ADAPT_BACKUP, RESTORE_ADAPT_BACKUP, &
                    & PADAPT_FIELDS_X, PADAPT_FIELDS_Y, &
                    & COARSEN_FIELDS_X, COARSEN_FIELDS_Y, &
                    & MOVE_FIELDS, &
                    & REFINE_FIELDS_X, REFINE_FIELDS_Y
    USE COMMS, ONLY: SHARE_FLAGS
    USE ADAPT
    IMPLICIT NONE

    INTEGER K, K_R, LV, SIDE


    DO LV=LEVEL,0,-1
        !We loop through cells from most refined to least. We check 
        ! non-conformity by flagging a cell for refinement if it has a neighbour 
        ! of one level higher that will refine. 

        !Share refinement/coarsening flags along interprocess boundaries 
        CALL SHARE_FLAGS

        !$OMP DO PRIVATE(K,K_R,SIDE) SCHEDULE(DYNAMIC)
        DO K=1,NEL
            !x -direction
            IF ((LEVEL_X(K) .EQ. LV).AND.(FLAG_H_REFINE_X(K).EQV..FALSE.)) THEN
                IF (BDRY_TYPE(1,K).EQ.'SP ') THEN
                    IF ((FLAG_H_REFINE_X(NEIGHBOUR_ID(1,1,K) ).EQV..TRUE.) .OR.&
                        &(FLAG_H_REFINE_X(NEIGHBOUR_ID(1,2,K)).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_X(K) = .TRUE.
                    ENDIF
                ELSEIF (BDRY_TYPE(1,K).EQ.'GSP') THEN
                    K_R  = NEIGHBOUR_ID(1,1,K) !Get local index of boundary edge
                    SIDE = NEIGHBOUR_SIDE_ID(1,1,K) !Get global side number

                    !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                    IF ((BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) .OR.&
                        &(BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_X(K) = .TRUE.
                    ENDIF
                ENDIF
                IF (BDRY_TYPE(3,K).EQ.'SP ') THEN
                    IF ((FLAG_H_REFINE_X(NEIGHBOUR_ID(3,1,K)).EQV..TRUE.).OR.&
                        &(FLAG_H_REFINE_X(NEIGHBOUR_ID(3,2,K) ).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_X(K) = .TRUE.
                    ENDIF
                ELSEIF (BDRY_TYPE(3,K).EQ.'GSP') THEN
                    K_R  = NEIGHBOUR_ID(3,1,K) !Get local index of boundary edge
                    SIDE = NEIGHBOUR_SIDE_ID(3,1,K) !Get global side number

                    !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                    IF ((BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) .OR.&
                        &(BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_X(K) = .TRUE.
                    ENDIF
                ENDIF
            ENDIF
            !y-direction
            IF ((LEVEL_Y(K) .EQ. LV).AND.(FLAG_H_REFINE_Y(K).EQV..FALSE.)) THEN
                IF (BDRY_TYPE(2,K).EQ.'SP ') THEN
                    IF ((FLAG_H_REFINE_Y(NEIGHBOUR_ID(2,1,K)).EQV..TRUE.) .OR.&
                        &(FLAG_H_REFINE_Y(NEIGHBOUR_ID(2,2,K)).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_Y(K) = .TRUE.
                    ENDIF
                ELSEIF (BDRY_TYPE(2,K).EQ.'GSP') THEN
                    K_R  = NEIGHBOUR_ID(2,1,K) !Get local index of boundary edge
                    SIDE = NEIGHBOUR_SIDE_ID(2,1,K) !Get global side number

                    !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                    IF ((BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) .OR.&
                        &(BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_Y(K) = .TRUE.
                    ENDIF
                ENDIF
                IF (BDRY_TYPE(4,K).EQ.'SP ') THEN
                    IF ((FLAG_H_REFINE_Y(NEIGHBOUR_ID(4,1,K)).EQV..TRUE.).OR.&
                        &(FLAG_H_REFINE_Y(NEIGHBOUR_ID(4,2,K)).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_Y(K) = .TRUE.
                    ENDIF
                ELSEIF (BDRY_TYPE(4,K).EQ.'GSP') THEN
                    K_R  = NEIGHBOUR_ID(4,1,K) !Get local index of boundary edge
                    SIDE = NEIGHBOUR_SIDE_ID(4,1,K) !Get global side number

                    !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                    IF ((BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) .OR.&
                        &(BDRY_BUFFER_FLAG_H_REFINE(&
                            &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.)) THEN
                        FLAG_H_REFINE_Y(K) = .TRUE.
                    ENDIF
                ENDIF
            ENDIF
        ENDDO
        !$OMP ENDDO
    ENDDO

    END SUBROUTINE REFINE_NONCONFORM_CHECK

!------------------------------------------------------------------------------

    SUBROUTINE COARSEN_NONCONFORM_CHECK
    USE SIZE, ONLY: NEL,NELMAX, LEVEL
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, & 
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_TYPE, &
                    & COARSEN_METRICS_X, COARSEN_METRICS_Y, &
                    & MOVE_METRICS, &
                    & REFINE_METRICS_X, REFINE_METRICS_Y
    USE FIELDS, ONLY: SET_ADAPT_BACKUP, RESTORE_ADAPT_BACKUP, &
                    & PADAPT_FIELDS_X, PADAPT_FIELDS_Y, &
                    & COARSEN_FIELDS_X, COARSEN_FIELDS_Y, &
                    & MOVE_FIELDS, &
                    & REFINE_FIELDS_X, REFINE_FIELDS_Y
    USE COMMS, ONLY: SHARE_FLAGS
    USE ADAPT
    IMPLICIT NONE

    INTEGER K, K_R, K_R_X, K_R_Y, LV
    INTEGER SIDE, SIDE_X, SIDE_Y
    INTEGER SISTER_IND

    DO LV=LEVEL,0,-1
        !We loop through cells from most refined to least. 

        ! We check non-conformity for coarsening by first checking if both cells 
        ! to coarsen are active, then checking if the cell has a neighbour of one 
        ! level of refinment higher isn't also coarsening

        CALL SHARE_FLAGS

        !$OMP DO PRIVATE (K,K_R,SIDE,SISTER_IND) 
        DO K=1,NEL
            !x -direction
            IF ((LEVEL_X(K) .EQ. LV).AND.(FLAG_H_COARSEN_X(K).EQV..TRUE.)) THEN
                IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN 
                    SISTER_IND = TREEINDEX_X(K) + 1
                    K_R = NEIGHBOUR_ID(2,1,K)
                ELSE 
                    SISTER_IND = TREEINDEX_X(K) - 1
                    K_R = NEIGHBOUR_ID(4,1,K)
                ENDIF

                !Check if other child is active, if not then unflag cell for coarsening
                IF (    (ROOTINDEX(K_R)  .NE.ROOTINDEX(K)) .OR.&
                        &TREEINDEX_Y(K_R).NE.TREEINDEX_Y(K).OR.&
                        &TREEINDEX_X(K_R).NE.SISTER_IND) THEN !Sister not present
                    FLAG_H_COARSEN_X(K) = .FALSE.
                ELSE !Sister is present, so check non-conformity
                    IF (BDRY_TYPE(1,K).EQ.'SP ') THEN
                        IF (FLAG_H_COARSEN_X(NEIGHBOUR_ID(1,1,K)).EQV..FALSE.) FLAG_H_COARSEN_X(K)=.FALSE.
                    ELSEIF (BDRY_TYPE(1,K).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(1,1,K) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(1,1,K) !Get global side number

                        !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                        IF (BDRY_BUFFER_FLAG_H_COARSEN(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..FALSE.) FLAG_H_COARSEN_X(K)=.FALSE.
                    ENDIF
                    IF (BDRY_TYPE(3,K).EQ.'SP ') THEN
                        IF (FLAG_H_COARSEN_X(NEIGHBOUR_ID(3,1,K)).EQV..FALSE.) FLAG_H_COARSEN_X(K)=.FALSE.
                    ELSEIF (BDRY_TYPE(3,K).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(3,1,K) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(3,1,K) !Get global side number

                        !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                        IF (BDRY_BUFFER_FLAG_H_COARSEN(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..FALSE.) FLAG_H_COARSEN_X(K)=.FALSE.
                    ENDIF
                ENDIF
            ENDIF

            !y-direction
            IF ((LEVEL_Y(K) .EQ. LV).AND.(FLAG_H_COARSEN_Y(K).EQV..TRUE.)) THEN
                IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN 
                    SISTER_IND = TREEINDEX_Y(K) + 1
                    K_R = NEIGHBOUR_ID(3,1,K)
                ELSE 
                    SISTER_IND = TREEINDEX_Y(K) - 1
                    K_R = NEIGHBOUR_ID(1,1,K)
                ENDIF

                !Check if other child is active, if not then unflag cell for coarsening
                IF (    (ROOTINDEX(K_R)  .NE.ROOTINDEX(K)) .OR.&
                        &TREEINDEX_X(K_R).NE.TREEINDEX_X(K).OR.&
                        &TREEINDEX_Y(K_R).NE.SISTER_IND) THEN !Sister not present
                    FLAG_H_COARSEN_Y(K) = .FALSE.
                ELSE !Sister is present so check non-conformity
                    IF (BDRY_TYPE(2,K).EQ.'SP ') THEN
                        IF (FLAG_H_COARSEN_Y(NEIGHBOUR_ID(2,1,K) ).EQV..FALSE.) FLAG_H_COARSEN_Y(K) = .FALSE.
                    ELSEIF (BDRY_TYPE(2,K).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(2,1,K) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(2,1,K) !Get global side number

                        !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                        IF (BDRY_BUFFER_FLAG_H_COARSEN(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..FALSE.) FLAG_H_COARSEN_Y(K)=.FALSE.
                    ENDIF
                    IF (BDRY_TYPE(4,K).EQ.'SP ') THEN
                        IF (FLAG_H_COARSEN_Y(NEIGHBOUR_ID(4,1,K)).EQV..FALSE.) FLAG_H_COARSEN_Y(K) = .FALSE.
                    ELSEIF (BDRY_TYPE(4,K).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(4,1,K) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(4,1,K) !Get global side number

                        !BDRY_BUFFER_NEIGHBOUR_ID maps the local index to the index of the neighbour on the other side of the boundary
                        IF (BDRY_BUFFER_FLAG_H_COARSEN(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..FALSE.) FLAG_H_COARSEN_Y(K)=.FALSE.
                    ENDIF
                ENDIF
            ENDIF
        ENDDO
        !$OMP ENDDO

        ! Next, cells can only coarsen if every cell in the parent agrees
        ! So check with all children cells and remove coarsen flag if there is disagreement

        ! This will guarantee that from this point on if a cell is flagged for coarsening
        ! then child 1 in that group is active and also flagged. Child 1 is the only cell
        ! allowed to do global memory writes.

        !$OMP DO PRIVATE(K,SIDE)
        DO K =1,NEL            
            !x direction
            IF ((LEVEL_X(K).EQ.LV).AND.(FLAG_H_COARSEN_X(K).EQV..TRUE.)) THEN
                IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN 
                    SIDE = 2
                ELSE 
                    SIDE = 4
                ENDIF

                !Both children are active, check if they agree to coarsen
                IF ((FLAG_H_COARSEN_X(NEIGHBOUR_ID(SIDE,1,K)).EQV..FALSE.)) FLAG_H_COARSEN_X(K) = .FALSE.
            ENDIF

            !y-direction
            IF ((LEVEL_Y(K).EQ.LV).AND.(FLAG_H_COARSEN_Y(K).EQV..TRUE.)) THEN
                IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN 
                    SIDE = 3
                ELSE 
                    SIDE = 1
                ENDIF

                !Both children are active, check if they agree to coarsen
                IF ((FLAG_H_COARSEN_Y(NEIGHBOUR_ID(SIDE,1,K)).EQV..FALSE.)) FLAG_H_COARSEN_Y(K) = .FALSE.
            ENDIF
        ENDDO
        !$OMP ENDDO

        !Some conflicts can still exist after cells agree to coarsen, namely some cells will
        ! want to coarsen in x and y when it isn't possible to do both. We have to check for these 
        ! and resolve the conflicts. For the sake of non-conformity, we will preserve the
        ! coarsening for the higher level elements. If lvl_x == lvl_y then we'll pick the most 
        ! benificial coarsening direction.

        !$OMP DO PRIVATE(K,SIDE_X,SIDE_Y,K_R_X,K_R_Y) 
        DO K =1,NEL
            IF (((LEVEL_X(K).EQ.LV).AND.(LEVEL_Y(K).GE.LV)).AND.&
                &((FLAG_H_COARSEN_X(K).EQV..TRUE.).AND.(FLAG_H_COARSEN_Y(K).EQV..TRUE.))) THEN
                !This cell wants to coarsen in both x and y. lvl_y >= lvl_x so we'll usually preserve 
                ! the coarsening in the y-direction
                IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN 
                    SIDE_X = 2
                ELSE 
                    SIDE_X = 4
                ENDIF

                IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN 
                    SIDE_Y = 3
                ELSE 
                    SIDE_Y = 1
                ENDIF

                K_R_X = NEIGHBOUR_ID(SIDE_X,1,K)
                K_R_Y = NEIGHBOUR_ID(SIDE_Y,1,K)

                IF ((FLAG_H_COARSEN_X(K_R_Y).EQV..FALSE.).AND.(FLAG_H_COARSEN_Y(K_R_X).EQV..TRUE.)) THEN 
                    !All 4 cells want to y coarsen but only 2 want to x coarsen. 
                    IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN !Only x child 1 performs the write
                        !Preserve y coarsening
                        FLAG_H_COARSEN_X(K) = .FALSE.
                        FLAG_H_COARSEN_X(K_R_X) = .FALSE.
                    ENDIF
                ELSEIF ((FLAG_H_COARSEN_X(K_R_Y).EQV..TRUE.).AND.(FLAG_H_COARSEN_Y(K_R_X).EQV..FALSE.)) THEN 
                    !All 4 cells want to x coarsen but only 2 want to y coarsen. 
                    IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN !Only y child 1 performs the write
                        IF (LEVEL_Y(K).GT.LV) THEN !Cell is more refined in y. Preserve y coarsening
                            FLAG_H_COARSEN_X(K) = .FALSE.
                            FLAG_H_COARSEN_X(K_R_X) = .FALSE.
                            FLAG_H_COARSEN_X(K_R_Y) = .FALSE.
                            FLAG_H_COARSEN_X(NEIGHBOUR_ID(SIDE_X,1,K_R_Y)) = .FALSE.
                        ELSE !Cell is the same level in x and y. Preserve x coarsening
                            FLAG_H_COARSEN_Y(K) = .FALSE.
                            FLAG_H_COARSEN_Y(K_R_Y) = .FALSE.
                        ENDIF
                    ENDIF
                ELSEIF ((FLAG_H_COARSEN_X(K_R_Y).EQV..FALSE.).AND.(FLAG_H_COARSEN_Y(K_R_X).EQV..FALSE.)) THEN 
                    !Only the current cells wants to x and y coarsen, but that isn't possible. 
                    IF (LEVEL_Y(K).GT.LV) THEN !Cell is more refined in y. Preserve y coarsening
                        FLAG_H_COARSEN_X(K) = .FALSE.
                        FLAG_H_COARSEN_X(K_R_X) = .FALSE.
                    ELSE !Cell is the same level in x and y.
                        !Find the most benificial direction
                        IF (ERROR_ESTIM_X(K) .LE. ERROR_ESTIM_Y(K)) THEN !x coarsening wins
                            FLAG_H_COARSEN_Y(K) = .FALSE.
                            FLAG_H_COARSEN_Y(K_R_Y) =.FALSE.
                        ELSE !y coarsening wins
                            FLAG_H_COARSEN_X(K) = .FALSE.
                            FLAG_H_COARSEN_X(K_R_X) = .FALSE.
                        ENDIF
                    ENDIF
                ENDIF
                ! If FLAG_H_COARSEN_X(K_R_Y)=T and FLAG_H_COARSEN_Y(K_R_X)=T then the cell will coarsen 
                !   in both x and y, which is ok.
            ELSEIF (((LEVEL_Y(K).EQ.LV).AND.(LEVEL_X(K).GE.LV)).AND.&
                &((FLAG_H_COARSEN_X(K).EQV..TRUE.).AND.(FLAG_H_COARSEN_Y(K).EQV..TRUE.))) THEN
                !This cell wants to coarsen in both x and y. lvl_x >= lvl_y so we'll usually preserve 
                ! the coarsening in the x-direction
                IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN 
                    SIDE_X = 2
                ELSE 
                    SIDE_X = 4
                ENDIF

                IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN 
                    SIDE_Y = 3
                ELSE 
                    SIDE_Y = 1
                ENDIF

                K_R_X = NEIGHBOUR_ID(SIDE_X,1,K)
                K_R_Y = NEIGHBOUR_ID(SIDE_Y,1,K)

                IF ((FLAG_H_COARSEN_X(K_R_Y).EQV..FALSE.).AND.(FLAG_H_COARSEN_Y(K_R_X).EQV..TRUE.)) THEN 
                    !All 4 cells want to y coarsen but only 2 want to x coarsen. 
                    IF (MOD(TREEINDEX_X(K),2).EQ.1) THEN !Only x child 1 performs the write
                        IF (LEVEL_X(K).GT.LV) THEN !Cell is more refined in x. Preserve x coarsening
                            FLAG_H_COARSEN_Y(K) = .FALSE.
                            FLAG_H_COARSEN_Y(K_R_X) = .FALSE.
                            FLAG_H_COARSEN_Y(K_R_Y) = .FALSE.
                            FLAG_H_COARSEN_Y(NEIGHBOUR_ID(SIDE_X,1,K_R_Y)) = .FALSE.
                        ELSE !Cell is the same level in x and y. Preserve y coarsening
                            FLAG_H_COARSEN_X(K) = .FALSE.
                            FLAG_H_COARSEN_X(K_R_X) = .FALSE.
                        ENDIF
                    ENDIF
                ELSEIF ((FLAG_H_COARSEN_X(K_R_Y).EQV..TRUE.).AND.(FLAG_H_COARSEN_Y(K_R_X).EQV..FALSE.)) THEN 
                    !All 4 cells want to x coarsen but only 2 want to y coarsen. 
                    IF (MOD(TREEINDEX_Y(K),2).EQ.1) THEN !Only y child 1 performs the write
                        !Preserve x coarsening
                        FLAG_H_COARSEN_Y(K) = .FALSE.
                        FLAG_H_COARSEN_Y(K_R_Y) = .FALSE.
                    ENDIF
                ELSEIF ((FLAG_H_COARSEN_X(K_R_Y).EQV..FALSE.).AND.(FLAG_H_COARSEN_Y(K_R_X).EQV..FALSE.)) THEN 
                    !Only the current cells wants to x and y coarsen, but that isn't possible. 
                    IF (LEVEL_X(K).GT.LV) THEN !Cell is more refined in x. Preserve x coarsening
                        FLAG_H_COARSEN_Y(K) = .FALSE.
                        FLAG_H_COARSEN_Y(K_R_Y) = .FALSE.
                    ELSE !Cell is the same level in x and y.
                        !Find the most benificial direction
                        IF (ERROR_ESTIM_X(K) .LE. ERROR_ESTIM_Y(K)) THEN !x coarsening wins
                            FLAG_H_COARSEN_Y(K) = .FALSE.
                            FLAG_H_COARSEN_Y(K_R_Y) =.FALSE.
                        ELSE !y coarsening wins
                            FLAG_H_COARSEN_X(K) = .FALSE.
                            FLAG_H_COARSEN_X(K_R_X) = .FALSE.
                        ENDIF
                    ENDIF
                ENDIF
                ! if FLAG_H_COARSEN_X(K_R_Y)=T and FLAG_H_COARSEN_Y(K_R_X)=T then the cell will coarsen 
                !   in both x and y, which is ok.
            ENDIF
        ENDDO
        !$OMP ENDDO
    ENDDO

    END SUBROUTINE COARSEN_NONCONFORM_CHECK

!-------------------------------------------------------------------------------

    SUBROUTINE MAX_ELEM_CHECK
    USE SIZE, ONLY: NEL,NELMAX, LEVEL
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, & 
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_TYPE, &
                    & COARSEN_METRICS_X, COARSEN_METRICS_Y, &
                    & MOVE_METRICS, &
                    & REFINE_METRICS_X, REFINE_METRICS_Y
    USE FIELDS, ONLY: SET_ADAPT_BACKUP, RESTORE_ADAPT_BACKUP, &
                    & PADAPT_FIELDS_X, PADAPT_FIELDS_Y, &
                    & COARSEN_FIELDS_X, COARSEN_FIELDS_Y, &
                    & MOVE_FIELDS, &
                    & REFINE_FIELDS_X, REFINE_FIELDS_Y
    USE COMMS, ONLY: SHARE_FLAGS
    USE ADAPT
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER IERROR

    INTEGER K, K1, K_R
    INTEGER SIDE, TEMP
    DOUBLE PRECISION ERR1, ERR2

    !First, count the number of cells that wish to h-refine

    !$OMP MASTER
    NUM_CELLS_REFINE = 0
    NUM_NEW_CELLS = 0
    !$OMP END MASTER
    !$OMP BARRIER

    !$OMP DO 
    DO K=1,NEL
        IF ((FLAG_H_REFINE_X(K).EQV..TRUE.).AND.(FLAG_H_REFINE_Y(K).EQV..TRUE.)) THEN
            !Refine to 4 cells
            !$OMP CRITICAL
            NUM_CELLS_REFINE = NUM_CELLS_REFINE + 2
            NUM_NEW_CELLS = NUM_NEW_CELLS + 3
            !$OMP END CRITICAL
        ELSEIF ((FLAG_H_REFINE_X(K).EQV..TRUE.).OR.(FLAG_H_REFINE_Y(K).EQV..TRUE.)) THEN
            !$OMP CRITICAL
            NUM_CELLS_REFINE = NUM_CELLS_REFINE +1
            NUM_NEW_CELLS = NUM_NEW_CELLS + 1
            !$OMP END CRITICAL
        ENDIF
    ENDDO
    !$OMP ENDDO

    !If too many cells wish to refine, we need to prioritize. 
    !THE IDEA: We want to find cells to remove from the refinement list
    !           but we dont want to remove a cell if it would violate 
    !           degree 2 non-conformity. So, the first thing we do is sort 
    !           our list of elements that wish to refine from smallest
    !           error estimator to largest (we use a parallel bubble sort).
    !           Starting at the beginning of the list we remove a cell only 
    !           if it's allowed, then start at the beginning of the list again
    !           until we have removed enough cells.

    IF ((NEL+NUM_NEW_CELLS).GT.NELMAX) THEN !Adding too many elements
        !We need the list of cells that wish to refine and how they are refining. 
        !$OMP MASTER
        ALLOCATE(REFINE_LIST(NUM_CELLS_REFINE))
        ALLOCATE(REFINE_DIRECTION(NUM_CELLS_REFINE))
        COUNT = 0
        !$OMP END MASTER
        !$OMP BARRIER

        !First, make a list of the cells that wish to refine, and along what direction.
        !$OMP DO
        DO K=1,NEL
            IF ((FLAG_H_REFINE_X(K).EQV..TRUE.).AND.(FLAG_H_REFINE_Y(K).EQV..TRUE.)) THEN
                !$OMP CRITICAL
                COUNT = COUNT + 2
                REFINE_LIST(COUNT-1) = K
                REFINE_LIST(COUNT  ) = K
                REFINE_DIRECTION(COUNT-1) = 1
                REFINE_DIRECTION(COUNT  ) = 2
                !$OMP END CRITICAL
            ELSEIF (FLAG_H_REFINE_X(K).EQV..TRUE.) THEN
                !$OMP CRITICAL
                COUNT = COUNT + 1
                REFINE_LIST(COUNT) = K
                REFINE_DIRECTION(COUNT) = 1
                !$OMP END CRITICAL
            ELSEIF (FLAG_H_REFINE_Y(K).EQV..TRUE.) THEN
                !$OMP CRITICAL
                COUNT = COUNT + 1
                REFINE_LIST(COUNT) = K
                REFINE_DIRECTION(COUNT) = 2
                !$OMP END CRITICAL
            ENDIF
        ENDDO    
        !$OMP ENDDO

        !Sort the refinement list by error estimator
        DO K = 0,NUM_CELLS_REFINE-2
            IF (MOD(K,2).EQ.0) THEN
                !$OMP DO PRIVATE(K1,ERR1,ERR2,TEMP) SCHEDULE(DYNAMIC)
                DO K1=0,(NUM_CELLS_REFINE/2)-1
                    IF (REFINE_DIRECTION(2*K1+1).EQ.1) THEN
                        ERR1 = ERROR_ESTIM_X(REFINE_LIST(2*K1+1))
                    ELSE
                        ERR1 = ERROR_ESTIM_Y(REFINE_LIST(2*K1+1))
                    ENDIF
                    IF (REFINE_DIRECTION(2*K1+2).EQ.1) THEN
                        ERR2 = ERROR_ESTIM_X(REFINE_LIST(2*K1+2))
                    ELSE
                        ERR2 = ERROR_ESTIM_Y(REFINE_LIST(2*K1+2))
                    ENDIF

                    IF (ERR2.LT.ERR1) THEN !Swap
                        TEMP = REFINE_LIST(2*K1+1) 
                        REFINE_LIST(2*K1+1) = REFINE_LIST(2*K1+2)
                        REFINE_LIST(2*K1+2) = TEMP

                        TEMP = REFINE_DIRECTION(2*K1+1) 
                        REFINE_DIRECTION(2*K1+1) = REFINE_DIRECTION(2*K1+2)
                        REFINE_DIRECTION(2*K1+2) = TEMP
                    ENDIF
                ENDDO
                !$OMP ENDDO
            ELSE
                !$OMP DO PRIVATE(K1,ERR1,ERR2,TEMP) SCHEDULE(DYNAMIC)
                DO K1=0,((NUM_CELLS_REFINE-1)/2)-1
                    IF (REFINE_DIRECTION(2*K1+2).EQ.1) THEN
                        ERR1 = ERROR_ESTIM_X(REFINE_LIST(2*K1+2))
                    ELSE
                        ERR1 = ERROR_ESTIM_Y(REFINE_LIST(2*K1+2))
                    ENDIF
                    IF (REFINE_DIRECTION(2*K1+3).EQ.1) THEN
                        ERR2 = ERROR_ESTIM_X(REFINE_LIST(2*K1+3))
                    ELSE
                        ERR2 = ERROR_ESTIM_Y(REFINE_LIST(2*K1+3))
                    ENDIF

                    IF (ERR2.LT.ERR1) THEN !Swap
                        TEMP = REFINE_LIST(2*K1+2) 
                        REFINE_LIST(2*K1+2) = REFINE_LIST(2*K1+3)
                        REFINE_LIST(2*K1+3) = TEMP

                        TEMP = REFINE_DIRECTION(2*K1+2) 
                        REFINE_DIRECTION(2*K1+2) = REFINE_DIRECTION(2*K1+3)
                        REFINE_DIRECTION(2*K1+3) = TEMP
                    ENDIF
                ENDDO
                !$OMP ENDDO
            ENDIF
        ENDDO
    ENDIF


    !Remove cells until NEL+NUM_NEW_CELLS <= NELMAX on all processors
    DO
        !$OMP MASTER
        IF ((NEL+NUM_NEW_CELLS).GT.NELMAX) THEN
            FLAG_LOCAL_CELL_OVERFLOW = .TRUE.
        ELSE 
            FLAG_LOCAL_CELL_OVERFLOW = .FALSE.
        ENDIF
        CALL MPI_ALLREDUCE(FLAG_LOCAL_CELL_OVERFLOW,FLAG_CELL_OVERFLOW,1,MPI_LOGICAL,&
                            & MPI_LOR,MPI_COMM_WORLD,IERROR)
        !$OMP END MASTER
        !$OMP BARRIER

        IF (FLAG_CELL_OVERFLOW.EQV..FALSE.) EXIT

        CALL SHARE_FLAGS

        !$OMP MASTER
        IF ((NEL+NUM_NEW_CELLS).GT.NELMAX) THEN      
            !Loop through the cells to refine in order of their error estimator 
            ! and remove the first cell that's allowed
            DO K = 1,NUM_CELLS_REFINE
                IF (REFINE_LIST(K).EQ.-1) CYCLE !Element already removed from list
                
                K1 = REFINE_LIST(K)

                IF (REFINE_DIRECTION(K).EQ.1) THEN !x-refinement     
                    !Check if there is a neighbour of a level higher that wishes to refine  
                    IF (BDRY_TYPE(1,K1).EQ.'SP ') THEN
                        IF (FLAG_H_REFINE_X(NEIGHBOUR_ID(1,1,K1)).EQV..TRUE.) CYCLE
                        IF (FLAG_H_REFINE_X(NEIGHBOUR_ID(1,2,K1)).EQV..TRUE.) CYCLE
                    ELSEIF (BDRY_TYPE(1,K1).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(1,1,K1) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(1,1,K1) !Get global side number
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) CYCLE
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.) CYCLE
                    ENDIF

                    IF (BDRY_TYPE(3,K1).EQ.'SP ') THEN
                        IF (FLAG_H_REFINE_X(NEIGHBOUR_ID(3,1,K1)).EQV..TRUE.) CYCLE 
                        IF (FLAG_H_REFINE_X(NEIGHBOUR_ID(3,2,K1)).EQV..TRUE.) CYCLE 
                    ELSEIF (BDRY_TYPE(3,K1).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(3,1,K1) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(3,1,K1) !Get global side number
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) CYCLE
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.) CYCLE
                    ENDIF
                ELSE !y-refinement
                    IF (BDRY_TYPE(2,K1).EQ.'SP ') THEN
                        IF (FLAG_H_REFINE_Y(NEIGHBOUR_ID(2,1,K1)).EQV..TRUE.) CYCLE
                        IF (FLAG_H_REFINE_Y(NEIGHBOUR_ID(2,2,K1)).EQV..TRUE.) CYCLE 
                    ELSEIF (BDRY_TYPE(2,K1).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(2,1,K1) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(2,1,K1) !Get global side number
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) CYCLE
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.) CYCLE
                    ENDIF

                    IF (BDRY_TYPE(4,K1).EQ.'SP ') THEN
                        IF (FLAG_H_REFINE_Y(NEIGHBOUR_ID(4,1,K1)).EQV..TRUE.) CYCLE
                        IF (FLAG_H_REFINE_Y(NEIGHBOUR_ID(4,2,K1)).EQV..TRUE.) CYCLE 
                    ELSEIF (BDRY_TYPE(4,K1).EQ.'GSP') THEN
                        K_R  = NEIGHBOUR_ID(4,1,K1) !Get local index of boundary edge
                        SIDE = NEIGHBOUR_SIDE_ID(4,1,K1) !Get global side number
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1),SIDE).EQV..TRUE.) CYCLE
                        IF (BDRY_BUFFER_FLAG_H_REFINE(&
                                &BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2),SIDE).EQV..TRUE.) CYCLE
                    ENDIF
                ENDIF

                !If we made it this far, this cell is safe to remove
                IF ((FLAG_H_REFINE_X(K1).EQV..TRUE.).AND.(FLAG_H_REFINE_Y(K1).EQV..TRUE.)) THEN
                    NUM_NEW_CELLS = NUM_NEW_CELLS - 2
                ELSE
                    NUM_NEW_CELLS = NUM_NEW_CELLS - 1
                ENDIF

                REFINE_LIST(K) = -1

                IF (REFINE_DIRECTION(K).EQ.1) THEN
                    FLAG_H_REFINE_X(K1)=.FALSE.
                ELSE
                    FLAG_H_REFINE_Y(K1)=.FALSE.
                ENDIF

                !If we're not overflowing anymore, free the lists
                IF ((NEL+NUM_NEW_CELLS).LE.NELMAX) THEN   
                    DEALLOCATE(REFINE_LIST)
                    DEALLOCATE(REFINE_DIRECTION)
                ENDIF

                EXIT
            ENDDO
        ENDIF
        !$OMP END MASTER
        !$OMP BARRIER
    ENDDO

    END SUBROUTINE MAX_ELEM_CHECK

!-------------------------------------------------------------------------------

    SUBROUTINE UPDATE_CONECTIVITY_COARSEN_X(K1)
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, BDRY_TYPE, BDRY_DATA, &
                  & BDRY_BUFFER_ELEM_MAP, BDRY_BUFFER_NEIGHBOUR_ID
    USE ADAPT, ONLY: NUM_INDEX_LOCAL, GLOBAL_REFINE_LIST, INDEX_LIST_LOCAL, &
                    & NUM_REMOVED_INDEX_LOCAL, REMOVED_INDEX_LIST
    IMPLICIT NONE

    INTEGER K1, K2, SIDE_R, K_R


    K2 = NEIGHBOUR_ID(2,1,K1)

    BDRY_TYPE(2,K1) = BDRY_TYPE(2,K2)
    BDRY_DATA(:,2,K1) = BDRY_DATA(:,2,K2)
    NEIGHBOUR_ID(2,:,K1) = NEIGHBOUR_ID(2,:,K2) 
    NEIGHBOUR_SIDE_ID(2,:,K1) = NEIGHBOUR_SIDE_ID(2,:,K2) 
    IF (BDRY_TYPE(2,K2).EQ.'SP ') THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)
        K_R    = NEIGHBOUR_ID(2,1,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1

        SIDE_R = NEIGHBOUR_SIDE_ID(2,2,K2)
        K_R    = NEIGHBOUR_ID(2,2,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
    ELSEIF ((BDRY_TYPE(2,K2).EQ.'E  ').OR.(BDRY_TYPE(2,K2).EQ.'J  ')) THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)
        K_R    = NEIGHBOUR_ID(2,1,K2)

        IF (NEIGHBOUR_ID(SIDE_R,1,K_R) .EQ. K2) THEN
            NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        ELSE
            NEIGHBOUR_ID(SIDE_R,2,K_R) = K1
        ENDIF
    ELSEIF ((BDRY_TYPE(2,K2).EQ.'GE ').OR.(BDRY_TYPE(2,K2).EQ.'GJ ').OR.&
                & (BDRY_TYPE(2,K2).EQ.'GSP')) THEN
        K_R    = NEIGHBOUR_ID(2,1,K2)       !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)  !Global side number

        BDRY_BUFFER_ELEM_MAP(K_R,SIDE_R) = K1 
    ENDIF

    IF (BDRY_TYPE(1,K1).EQ.'J  ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(1,K1) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K1)
        K_R    = NEIGHBOUR_ID(1,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        NEIGHBOUR_ID(SIDE_R,2,K_R) = 0
    ELSEIF (BDRY_TYPE(1,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(1,K1) = 'SP '

        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K1)
        K_R    = NEIGHBOUR_ID(1,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 0.5d0

        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K2)
        K_R    = NEIGHBOUR_ID(1,1,K2)

        NEIGHBOUR_ID(1,2,K1) = K_R
        NEIGHBOUR_SIDE_ID(1,2,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 1.0d0
    ELSEIF ((BDRY_TYPE(1,K1).EQ.'GJ ').OR.(BDRY_TYPE(1,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !Removed element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 1  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(1,1,K1),SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(1,1,K2),SIDE_R,1)   !Exterior index of cell

        NUM_REMOVED_INDEX_LOCAL(SIDE_R) = NUM_REMOVED_INDEX_LOCAL(SIDE_R)+1
        REMOVED_INDEX_LIST(NUM_REMOVED_INDEX_LOCAL(SIDE_R),SIDE_R) = NEIGHBOUR_ID(1,1,K2)
    ENDIF

    IF (BDRY_TYPE(3,K1).EQ.'J  ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(3,K1) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K1)
        K_R    = NEIGHBOUR_ID(3,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        NEIGHBOUR_ID(SIDE_R,2,K_R) = 0
    ELSEIF (BDRY_TYPE(3,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(3,K1) = 'SP '

        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K1)
        K_R    = NEIGHBOUR_ID(3,1,K1)

        NEIGHBOUR_ID(3,2,K1) = K_R
        NEIGHBOUR_SIDE_ID(3,2,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 1.0d0

        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)
        K_R    = NEIGHBOUR_ID(3,1,K2)

        NEIGHBOUR_ID(3,1,K1) = K_R
        NEIGHBOUR_SIDE_ID(3,1,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 0.5d0
    ELSEIF ((BDRY_TYPE(3,K1).EQ.'GJ ').OR.(BDRY_TYPE(3,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !Removed element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 3  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(3,1,K2),SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(3,1,K1),SIDE_R,1)   !Exterior index of cell

        NUM_REMOVED_INDEX_LOCAL(SIDE_R) = NUM_REMOVED_INDEX_LOCAL(SIDE_R)+1
        REMOVED_INDEX_LIST(NUM_REMOVED_INDEX_LOCAL(SIDE_R),SIDE_R) = NEIGHBOUR_ID(3,1,K1)
    ENDIF

    END SUBROUTINE

!-------------------------------------------------------------------------------

    SUBROUTINE UPDATE_CONECTIVITY_COARSEN_Y(K1)
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, BDRY_TYPE, BDRY_DATA, &
                    BDRY_BUFFER_ELEM_MAP, BDRY_BUFFER_NEIGHBOUR_ID
    USE ADAPT, ONLY: NUM_INDEX_LOCAL, GLOBAL_REFINE_LIST, INDEX_LIST_LOCAL, &
                    & NUM_REMOVED_INDEX_LOCAL, REMOVED_INDEX_LIST
    IMPLICIT NONE

    INTEGER K1, K2, SIDE_R, K_R

    K2 = NEIGHBOUR_ID(3,1,K1)

    !Update boundary data
    BDRY_TYPE(3,K1) = BDRY_TYPE(3,K2)
    BDRY_DATA(:,3,K1) = BDRY_DATA(:,3,K2)
    NEIGHBOUR_ID(3,:,K1) = NEIGHBOUR_ID(3,:,K2) 
    NEIGHBOUR_SIDE_ID(3,:,K1) = NEIGHBOUR_SIDE_ID(3,:,K2) 
    IF (BDRY_TYPE(3,K2).EQ.'SP ') THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)
        K_R    = NEIGHBOUR_ID(3,1,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1

        SIDE_R = NEIGHBOUR_SIDE_ID(3,2,K2)
        K_R    = NEIGHBOUR_ID(3,2,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
    ELSEIF ((BDRY_TYPE(3,K2).EQ.'E  ').OR.(BDRY_TYPE(3,K2).EQ.'J  ')) THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)
        K_R    = NEIGHBOUR_ID(3,1,K2)

        IF (NEIGHBOUR_ID(SIDE_R,1,K_R) .EQ. K2) THEN
            NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        ELSE
            NEIGHBOUR_ID(SIDE_R,2,K_R) = K1
        ENDIF
    ELSEIF ((BDRY_TYPE(3,K2).EQ.'GE ').OR.(BDRY_TYPE(3,K2).EQ.'GJ ').OR.&
                & (BDRY_TYPE(3,K2).EQ.'GSP')) THEN
        K_R    = NEIGHBOUR_ID(3,1,K2)       !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)  !Global side number

        BDRY_BUFFER_ELEM_MAP(K_R,SIDE_R) = K1
    ENDIF

    IF (BDRY_TYPE(2,K1).EQ.'J  ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(2,K1) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K1)
        K_R    = NEIGHBOUR_ID(2,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        NEIGHBOUR_ID(SIDE_R,2,K_R) = 0
    ELSEIF (BDRY_TYPE(2,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(2,K1) = 'SP '

        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K1)
        K_R    = NEIGHBOUR_ID(2,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 0.5d0

        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)
        K_R    = NEIGHBOUR_ID(2,1,K2)

        NEIGHBOUR_ID(2,2,K1) = K_R
        NEIGHBOUR_SIDE_ID(2,2,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 1.0d0
    ELSEIF ((BDRY_TYPE(2,K1).EQ.'GJ ').OR.(BDRY_TYPE(2,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !Removed element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 2  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(2,1,K1),SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(2,1,K2),SIDE_R,1)   !Exterior index of cell

        NUM_REMOVED_INDEX_LOCAL(SIDE_R) = NUM_REMOVED_INDEX_LOCAL(SIDE_R)+1
        REMOVED_INDEX_LIST(NUM_REMOVED_INDEX_LOCAL(SIDE_R),SIDE_R) = NEIGHBOUR_ID(2,1,K2)
    ENDIF

    IF (BDRY_TYPE(4,K1).EQ.'J  ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(4,K1) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K1)
        K_R    = NEIGHBOUR_ID(4,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        NEIGHBOUR_ID(SIDE_R,2,K_R) = 0
    ELSEIF (BDRY_TYPE(4,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(4,K1) = 'SP '

        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K1)
        K_R    = NEIGHBOUR_ID(4,1,K1)

        NEIGHBOUR_ID(4,2,K1) = K_R
        NEIGHBOUR_SIDE_ID(4,2,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 1.0d0

        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K2)
        K_R    = NEIGHBOUR_ID(4,1,K2)

        NEIGHBOUR_ID(4,1,K1) = K_R
        NEIGHBOUR_SIDE_ID(4,1,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'J  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        BDRY_DATA(5,SIDE_R,K_R) = 0.5d0
    ELSEIF ((BDRY_TYPE(4,K1).EQ.'GJ ').OR.(BDRY_TYPE(4,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !Removed element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 4  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(4,1,K2),SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                        & = BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(4,1,K1),SIDE_R,1)   !Exterior index of cell

        NUM_REMOVED_INDEX_LOCAL(SIDE_R) = NUM_REMOVED_INDEX_LOCAL(SIDE_R)+1
        REMOVED_INDEX_LIST(NUM_REMOVED_INDEX_LOCAL(SIDE_R),SIDE_R) = NEIGHBOUR_ID(4,1,K1)
    ENDIF

    END SUBROUTINE

!---------------------------------------------------------------------------------------------------

    SUBROUTINE UPDATE_GLOBAL_CONNECTIVITY_COARSEN()
    USE MESH, ONLY: GLOBAL_NEIGHBOUR_ID, GLOBAL_NEIGHBOUR_SIDE_ID, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, NEIGHBOUR_ID, &
                    & BDRY_BUFFER_ELEM_MAP, BDRY_BUFFER_SIDE_MAP, &
                    & BDRY_TYPE, BDRY_DATA, BDRY_BUFFER_NEIGHBOUR_ID 
    USE ADAPT,ONLY: NUM_INDEX_LOCAL, NUM_INDEX, INDEX_LIST, &
                    &  INDEX_LIST_LOCAL, GLOBAL_REFINE_LIST
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER SIDE, ISIDE, L, K1, K2, K_R, IND1, IND2
    INTEGER REQUEST(8), IERROR
    INTEGER STATS(MPI_STATUS_SIZE,8)

    !This subroutine preforms the connectivity changes along the interprocess boundaries
    
    !During refinement at this level, several arrays have been built. INDEX_LIST_LOCAL stores 
    ! the exterior indexes of the cells along the global edges which will coarsen. 
    ! We share these arrays in order apply their changes locally.

    REQUEST = MPI_REQUEST_NULL

    !$OMP MASTER
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(NUM_INDEX_LOCAL(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE,&
                        & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(NUM_INDEX(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE),&
                        & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)
    !$OMP BARRIER

    !$OMP MASTER
    DO SIDE=1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(INDEX_LIST_LOCAL(:,:,SIDE), 3*NUM_INDEX_LOCAL(SIDE),&
                            & MPI_INTEGER, GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE+4,&
                            & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(INDEX_LIST(:,:,SIDE), 3*NUM_INDEX(SIDE),&
                            & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE)+4,&
                            & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)
    !$OMP BARRIER

    !Apply exterior changes first  
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP DO PRIVATE(IND1,IND2,K1,K2,ISIDE,L) SCHEDULE(DYNAMIC)
            DO L = 1,NUM_INDEX(SIDE)
                IND1 = INDEX_LIST(1,L,SIDE) !Local index of cell whose neighbour is coarsening

                !Get element and side numbers 
                K1  = BDRY_BUFFER_ELEM_MAP(IND1,SIDE)
                ISIDE = BDRY_BUFFER_SIDE_MAP(IND1,SIDE)

                IF (BDRY_TYPE(ISIDE,K1).EQ.'GE ') THEN !Conforming edge becomes non-conforming
                    IND2 = INDEX_LIST(2,L,SIDE) !Local index of second cell whose neighbour is coarsening
                    K2   = BDRY_BUFFER_ELEM_MAP(IND2,SIDE)

                    BDRY_TYPE(ISIDE,K1) = 'GJ '
                    BDRY_TYPE(ISIDE,K2) = 'GJ '

                    BDRY_DATA(5,ISIDE,K2) = 1.0d0
                    BDRY_DATA(5,ISIDE,K1) = 0.5d0

                    BDRY_BUFFER_NEIGHBOUR_ID(IND2,SIDE,1) = BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1)
                ELSEIF (BDRY_TYPE(ISIDE,K1).EQ.'GSP') THEN !non-conforming edge becomes conforming
                    BDRY_TYPE(ISIDE,K1) = 'GE '
                    BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1) = BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,2)
                ENDIF
            ENDDO
            !$OMP ENDDO
        ENDIF
    ENDDO
    
    !Apply local changes  
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP DO PRIVATE(K1,K2,K_R,ISIDE,L) SCHEDULE(DYNAMIC)
            DO L = 1,NUM_INDEX_LOCAL(SIDE)
                K1    = GLOBAL_REFINE_LIST(1,L,SIDE) !Element index
                K2    = GLOBAL_REFINE_LIST(2,L,SIDE) !Removed element index
                ISIDE = GLOBAL_REFINE_LIST(3,L,SIDE) !side

                IF (BDRY_TYPE(ISIDE,K1).EQ.'GJ ') THEN !Non-conforming edge becomes conforming
                    BDRY_TYPE(ISIDE,K1) = 'GE '

                    IF ((ISIDE.EQ.3).OR.(ISIDE.EQ.4)) THEN
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K1),SIDE) = -1
                        K_R = NEIGHBOUR_ID(ISIDE,1,K2) 
                        NEIGHBOUR_ID(ISIDE,1,K1) = K_R
                        BDRY_BUFFER_ELEM_MAP(K_R,SIDE) = K1
                    ELSE
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = -1
                    ENDIF
                ELSEIF (BDRY_TYPE(ISIDE,K1).EQ.'GE ') THEN !Conforming edge becomes non-conforming
                    BDRY_TYPE(ISIDE,K1) = 'GSP'

                    IF ((ISIDE.EQ.3).OR.(ISIDE.EQ.4)) THEN
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K1),SIDE) = -1
                        K_R = NEIGHBOUR_ID(ISIDE,1,K2) 
                        BDRY_BUFFER_ELEM_MAP(K_R,SIDE) = K1

                        BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2) = &
                                            & BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(ISIDE,1,K1),SIDE,1)
                        NEIGHBOUR_ID(ISIDE,1,K1) = K_R 
                    ELSE
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = -1
                        K_R = NEIGHBOUR_ID(ISIDE,1,K1) 
                        BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2) = &
                                            & BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(ISIDE,1,K2),SIDE,1)
                    ENDIF
                ENDIF
            ENDDO
            !$OMP ENDDO
        ENDIF
    ENDDO

    !$OMP MASTER
    NUM_INDEX_LOCAL(:) = 0
    !$OMP END MASTER
    !$OMP BARRIER

    END SUBROUTINE


!-------------------------------------------------------------------------------

    SUBROUTINE UPDATE_CONECTIVITY_MOVE(K1,K2)
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, BDRY_TYPE,BDRY_DATA, &
                    & BDRY_BUFFER_ELEM_MAP
    IMPLICIT NONE

    INTEGER K1, K2, SIDE, SIDE_R, K_R

    !Mesh data
    BDRY_TYPE(:,K2) = BDRY_TYPE(:,K1)
    BDRY_DATA(:,:,K2) = BDRY_DATA(:,:,K1)
    NEIGHBOUR_ID(:,:,K2) = NEIGHBOUR_ID(:,:,K1)
    NEIGHBOUR_SIDE_ID(:,:,K2) = NEIGHBOUR_SIDE_ID(:,:,K1)

    !Update connectivity
    DO SIDE =1,4
        SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,1,K1)
        K_R    = NEIGHBOUR_ID(SIDE,1,K1)
        IF (BDRY_TYPE(SIDE,K1).EQ.'SP ') THEN
            NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

            SIDE_R = NEIGHBOUR_SIDE_ID(SIDE,2,K1)
            K_R    = NEIGHBOUR_ID(SIDE,2,K1)

            NEIGHBOUR_ID(SIDE_R,1,K_R) = K2
        ELSEIF ((BDRY_TYPE(SIDE,K1).EQ.'E  ').OR.(BDRY_TYPE(SIDE,K1).EQ.'J  ')) THEN
            IF (NEIGHBOUR_ID(SIDE_R,1,K_R) .EQ. K1) THEN
                NEIGHBOUR_ID(SIDE_R,1,K_R) = K2
            ELSE
                NEIGHBOUR_ID(SIDE_R,2,K_R) = K2
            ENDIF
        ELSEIF ((BDRY_TYPE(SIDE,K1).EQ.'GSP').OR.(BDRY_TYPE(SIDE,K1).EQ.'GE ').OR.&
                &(BDRY_TYPE(SIDE,K1).EQ.'GJ ')) THEN
            BDRY_BUFFER_ELEM_MAP(K_R,SIDE_R) = K2
        ENDIF
    ENDDO
    
    END SUBROUTINE

!---------------------------------------------------------------------------

    SUBROUTINE UPDATE_GLOBAL_CONNECTIVITY_MOVE()
    USE MESH, ONLY: GLOBAL_NEIGHBOUR_ID, GLOBAL_NEIGHBOUR_SIDE_ID, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, NEIGHBOUR_ID, &
                    & BDRY_BUFFER_ELEM_MAP, BDRY_BUFFER_SIDE_MAP, &
                    & BDRY_TYPE, BDRY_BUFFER_NEIGHBOUR_ID, &
                    & BDRY_BUFFER_SENDSIZE, BDRY_BUFFER_RECVSIZE
    USE ADAPT,ONLY: NUM_REMOVED_INDEX_LOCAL, NUM_REMOVED_INDEX, &
                    & NUM_MOVED_INDEX_LOCAL, NUM_MOVED_INDEX, &
                    & REMOVED_INDEX_LIST, &
                    & COUNT, MOVED_INDEX_LIST_LOCAL, MOVED_INDEX_LIST
    IMPLICIT NONE
    INCLUDE "mpif.h"
 
    INTEGER SIDE, ISIDE, L, K1, IND1, IND2
    INTEGER REQUEST(8), IERROR
    INTEGER STATS(MPI_STATUS_SIZE,8) 

    REQUEST = MPI_REQUEST_NULL

    !This subroutine reorders local indexing to remove holes created by
    ! coarsening along interprocess boundaries

    !$OMP MASTER
    NUM_MOVED_INDEX_LOCAL = 0
    !$OMP END MASTER
    !$OMP BARRIER

    !Frst build the start and finish arrays
    DO SIDE =1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP MASTER
            COUNT = 1
            !$OMP END MASTER
            !$OMP BARRIER
            !$OMP DO PRIVATE(K1) SCHEDULE(DYNAMIC) 
            DO K1 = BDRY_BUFFER_SENDSIZE(SIDE),&
                 & BDRY_BUFFER_SENDSIZE(SIDE)-NUM_REMOVED_INDEX_LOCAL(SIDE)+1,-1
                IF (BDRY_BUFFER_ELEM_MAP(K1,SIDE).EQ.-1) CYCLE !Index already removed

                !$OMP CRITICAL
                DO
                    IF (REMOVED_INDEX_LIST(COUNT,SIDE).LE.&
                        &(BDRY_BUFFER_SENDSIZE(SIDE)-NUM_REMOVED_INDEX_LOCAL(SIDE))) EXIT
                    COUNT = COUNT+1
                ENDDO
                NUM_MOVED_INDEX_LOCAL(SIDE) = NUM_MOVED_INDEX_LOCAL(SIDE)+1
                MOVED_INDEX_LIST_LOCAL(1,NUM_MOVED_INDEX_LOCAL(SIDE),SIDE) &
                                        & = BDRY_BUFFER_NEIGHBOUR_ID(K1,SIDE,1)     !Exterior index of start
                MOVED_INDEX_LIST_LOCAL(2,NUM_MOVED_INDEX_LOCAL(SIDE),SIDE) &
                                        & = BDRY_BUFFER_NEIGHBOUR_ID(K1,SIDE,2)     !Second exterior index of start, if needed
                MOVED_INDEX_LIST_LOCAL(3,NUM_MOVED_INDEX_LOCAL(SIDE),SIDE) = K1     !Local index of start
                MOVED_INDEX_LIST_LOCAL(4,NUM_MOVED_INDEX_LOCAL(SIDE),SIDE) = REMOVED_INDEX_LIST(COUNT,SIDE)  !Local index of end
                COUNT = COUNT+1
                !$OMP END CRITICAL
            ENDDO
            !$OMP ENDDO
        ENDIF
    ENDDO

    !Share these arrays

    !$OMP MASTER
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(NUM_REMOVED_INDEX_LOCAL(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE,&
                        & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(NUM_REMOVED_INDEX(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE),&
                        & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)

    !$OMP MASTER
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(NUM_MOVED_INDEX_LOCAL(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE,&
                        & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(NUM_MOVED_INDEX(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE),&
                        & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)
    !$OMP BARRIER

    !$OMP MASTER
    DO SIDE=1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(MOVED_INDEX_LIST_LOCAL(:,:,SIDE), 4*NUM_MOVED_INDEX_LOCAL(SIDE),&
                            & MPI_INTEGER, GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE+4,&
                            & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(MOVED_INDEX_LIST(:,:,SIDE), 4*NUM_MOVED_INDEX(SIDE),&
                            & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE)+4,&
                            & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)
    !$OMP BARRIER


    !Exterior reordering
    DO SIDE =1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP DO PRIVATE(L,K1,ISIDE,IND1,IND2) SCHEDULE(DYNAMIC)
            DO L = 1,NUM_MOVED_INDEX(SIDE)
                IND1 = MOVED_INDEX_LIST(1,L,SIDE)

                K1    = BDRY_BUFFER_ELEM_MAP(IND1,SIDE)
                ISIDE = BDRY_BUFFER_SIDE_MAP(IND1,SIDE)

                IF (BDRY_TYPE(ISIDE,K1).EQ.'GJ ') THEN
                    IND2 = MOVED_INDEX_LIST(2,L,SIDE)
                    BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1) = MOVED_INDEX_LIST(4,L,SIDE)    
                    BDRY_BUFFER_NEIGHBOUR_ID(IND2,SIDE,1) = MOVED_INDEX_LIST(4,L,SIDE)
                ELSEIF (BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1).EQ.MOVED_INDEX_LIST(3,L,SIDE)) THEN
                    BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1) = MOVED_INDEX_LIST(4,L,SIDE)
                ELSE
                    BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,2) = MOVED_INDEX_LIST(4,L,SIDE)
                ENDIF
            ENDDO
            !$OMP ENDDO
        ENDIF
    ENDDO

    !Local reordering
    DO SIDE =1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP DO PRIVATE(L,K1,ISIDE,IND1,IND2) SCHEDULE(DYNAMIC)
            DO L = 1,NUM_MOVED_INDEX_LOCAL(SIDE)
                IND1 = MOVED_INDEX_LIST_LOCAL(3,L,SIDE)
                IND2 = MOVED_INDEX_LIST_LOCAL(4,L,SIDE)

                K1    = BDRY_BUFFER_ELEM_MAP(IND1,SIDE)
                ISIDE = BDRY_BUFFER_SIDE_MAP(IND1,SIDE)

                NEIGHBOUR_ID(ISIDE,1,K1) = IND2
                BDRY_BUFFER_ELEM_MAP(IND2,SIDE) = K1
                BDRY_BUFFER_SIDE_MAP(IND2,SIDE) = ISIDE
                BDRY_BUFFER_NEIGHBOUR_ID(IND2,SIDE,:) = BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,:)
            ENDDO
            !$OMP ENDDO

            !$OMP MASTER
            BDRY_BUFFER_SENDSIZE(SIDE) = BDRY_BUFFER_SENDSIZE(SIDE)-NUM_REMOVED_INDEX_LOCAL(SIDE)
            BDRY_BUFFER_RECVSIZE(SIDE) = BDRY_BUFFER_RECVSIZE(SIDE)-NUM_REMOVED_INDEX(SIDE)
            !$OMP END MASTER
            !$OMP BARRIER
        ENDIF
    ENDDO

    END SUBROUTINE

!-------------------------------------------------------------------------------

    SUBROUTINE UPDATE_CONECTIVITY_REFINE_X(K1,K2)
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, BDRY_TYPE, BDRY_DATA, &
                    & BDRY_BUFFER_ELEM_MAP, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_BUFFER_SENDSIZE
    USE ADAPT, ONLY: NUM_INDEX_LOCAL, GLOBAL_REFINE_LIST, INDEX_LIST_LOCAL
    IMPLICIT NONE

    INTEGER K1, K2, SIDE_R, K_R

    !Update boundary data
    BDRY_TYPE(2,K2) = BDRY_TYPE(2,K1)
    NEIGHBOUR_ID(2,:,K2) = NEIGHBOUR_ID(2,:,K1)
    NEIGHBOUR_SIDE_ID(2,:,K2) = NEIGHBOUR_SIDE_ID(2,:,K1)
    BDRY_DATA(:,2,K2) = BDRY_DATA(:,2,K1)
    IF (BDRY_TYPE(2,K2).EQ.'SP ') THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)
        K_R    = NEIGHBOUR_ID(2,1,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        SIDE_R = NEIGHBOUR_SIDE_ID(2,2,K2)
        K_R    = NEIGHBOUR_ID(2,2,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        NEIGHBOUR_ID(2,2,K1) = 0
    ELSEIF ((BDRY_TYPE(2,K2).EQ.'E  ').OR.(BDRY_TYPE(2,K2).EQ.'J  ')) THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)
        K_R    = NEIGHBOUR_ID(2,1,K2)

        IF (NEIGHBOUR_ID(SIDE_R,1,K_R) .EQ. K1) THEN
            NEIGHBOUR_ID(SIDE_R,1,K_R) = K2
        ELSE
            NEIGHBOUR_ID(SIDE_R,2,K_R) = K2
        ENDIF
    ELSEIF ((BDRY_TYPE(2,K2).EQ.'GE ').OR.(BDRY_TYPE(2,K2).EQ.'GJ ').OR.&
                & (BDRY_TYPE(2,K2).EQ.'GSP')) THEN
        K_R    = NEIGHBOUR_ID(2,1,K2)       !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K2)  !Global side number

        BDRY_BUFFER_ELEM_MAP(K_R,SIDE_R) = K2
    ENDIF

    BDRY_TYPE(2,K1) = 'E  '
    BDRY_TYPE(4,K2) = 'E  '
    NEIGHBOUR_ID(2,1,K1) = K2
    NEIGHBOUR_SIDE_ID(2,1,K1) = 4
    NEIGHBOUR_ID(4,1,K2) = K1
    NEIGHBOUR_SIDE_ID(4,1,K2) = 2

    IF (BDRY_TYPE(1,K1).EQ.'SP ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(1,K1) = 'E  '
        BDRY_TYPE(1,K2) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K1)
        K_R    = NEIGHBOUR_ID(1,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1

        SIDE_R = NEIGHBOUR_SIDE_ID(1,2,K1)
        K_R    = NEIGHBOUR_ID(1,2,K1)

        NEIGHBOUR_ID(1,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(1,1,K2) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        NEIGHBOUR_ID(1,2,K1) = 0
    ELSEIF (BDRY_TYPE(1,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(1,K1) = 'J  '
        BDRY_TYPE(1,K2) = 'J  '

        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K1)
        K_R    = NEIGHBOUR_ID(1,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'SP '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2
        NEIGHBOUR_SIDE_ID(SIDE_R,1,K_R) = 1
        NEIGHBOUR_ID(SIDE_R,2,K_R) = K1
        NEIGHBOUR_SIDE_ID(SIDE_R,2,K_R) = 1

        NEIGHBOUR_ID(1,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(1,1,K2) = SIDE_R
        BDRY_DATA(5,1,K2) = 0.5d0
        BDRY_DATA(5,1,K1) = 1.0d0
    ELSEIF ((BDRY_TYPE(1,K1).EQ.'GSP').OR.(BDRY_TYPE(1,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        K_R    = NEIGHBOUR_ID(1,1,K1)      !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(1,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        BDRY_BUFFER_SENDSIZE(SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R)+1

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !New element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 1  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R) !Local index of new cell
        INDEX_LIST_LOCAL(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,2)   !Exterior index of second cell
    ELSE
        BDRY_TYPE(1,K2) = BDRY_TYPE(1,K1)
        NEIGHBOUR_ID(1,:,K2) = NEIGHBOUR_ID(1,:,K1)
        NEIGHBOUR_SIDE_ID(1,:,K2) = NEIGHBOUR_SIDE_ID(1,:,K1)
        BDRY_DATA(:,1,K2) = BDRY_DATA(:,1,K1)
    ENDIF


    IF (BDRY_TYPE(3,K1).EQ.'SP ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(3,K1) = 'E  '
        BDRY_TYPE(3,K2) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K1)
        K_R    = NEIGHBOUR_ID(3,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        NEIGHBOUR_ID(3,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(3,1,K2) = SIDE_R

        SIDE_R = NEIGHBOUR_SIDE_ID(3,2,K1)
        K_R    = NEIGHBOUR_ID(3,2,K1)

        NEIGHBOUR_ID(3,1,K1) = K_R
        NEIGHBOUR_SIDE_ID(3,1,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1

        NEIGHBOUR_ID(3,2,K1) = 0
    ELSEIF (BDRY_TYPE(3,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(3,K1) = 'J  '
        BDRY_TYPE(3,K2) = 'J  '

        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K1)
        K_R    = NEIGHBOUR_ID(3,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'SP '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        NEIGHBOUR_SIDE_ID(SIDE_R,1,K_R) = 3
        NEIGHBOUR_ID(SIDE_R,2,K_R) = K2
        NEIGHBOUR_SIDE_ID(SIDE_R,2,K_R) = 3

        NEIGHBOUR_ID(3,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(3,1,K2) = SIDE_R
        BDRY_DATA(5,3,K2) = 1.0d0
        BDRY_DATA(5,3,K1) = 0.5d0
    ELSEIF ((BDRY_TYPE(3,K1).EQ.'GSP').OR.(BDRY_TYPE(3,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        K_R    = NEIGHBOUR_ID(3,1,K1)      !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        BDRY_BUFFER_SENDSIZE(SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R)+1

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !New element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 3  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R) !Local index of new cell
        INDEX_LIST_LOCAL(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,2)   !Exterior index of second cell
    ELSE
        BDRY_TYPE(3,K2) = BDRY_TYPE(3,K1)
        NEIGHBOUR_ID(3,:,K2) = NEIGHBOUR_ID(3,:,K1)
        NEIGHBOUR_SIDE_ID(3,:,K2) = NEIGHBOUR_SIDE_ID(3,:,K1)
        BDRY_DATA(:,3,K2) = BDRY_DATA(:,3,K1)
    ENDIF

    END SUBROUTINE

!-------------------------------------------------------------------------------

    SUBROUTINE UPDATE_CONECTIVITY_REFINE_Y(K1,K2)
    USE MESH, ONLY: NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, BDRY_TYPE, BDRY_DATA, &
                    & BDRY_BUFFER_ELEM_MAP, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, BDRY_BUFFER_SENDSIZE
    USE ADAPT, ONLY: NUM_INDEX_LOCAL, GLOBAL_REFINE_LIST, INDEX_LIST_LOCAL
    IMPLICIT NONE

    INTEGER K1, K2, SIDE_R, K_R

    !Update boundary data
    BDRY_TYPE(3,K2) = BDRY_TYPE(3,K1)
    NEIGHBOUR_ID(3,:,K2) = NEIGHBOUR_ID(3,:,K1)
    NEIGHBOUR_SIDE_ID(3,:,K2) = NEIGHBOUR_SIDE_ID(3,:,K1)
    BDRY_DATA(:,3,K2) = BDRY_DATA(:,3,K1)
    IF (BDRY_TYPE(3,K2).EQ.'SP ') THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)
        K_R    = NEIGHBOUR_ID(3,1,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        SIDE_R = NEIGHBOUR_SIDE_ID(3,2,K2)
        K_R    = NEIGHBOUR_ID(3,2,K2)

        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        NEIGHBOUR_ID(3,2,K1) = 0
    ELSEIF ((BDRY_TYPE(3,K2).EQ.'E  ').OR.(BDRY_TYPE(3,K2).EQ.'J  ')) THEN
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)
        K_R    = NEIGHBOUR_ID(3,1,K2)

        IF ((BDRY_TYPE(3,K2).EQ.'J  ').AND.(INT(BDRY_DATA(5,3,K2)).EQ.1)) THEN
            NEIGHBOUR_ID(SIDE_R,2,K_R) = K2
        ELSE
            NEIGHBOUR_ID(SIDE_R,1,K_R) = K2
        ENDIF
    ELSEIF ((BDRY_TYPE(3,K2).EQ.'GE ').OR.(BDRY_TYPE(3,K2).EQ.'GJ ').OR.&
                & (BDRY_TYPE(3,K2).EQ.'GSP')) THEN
        K_R    = NEIGHBOUR_ID(3,1,K2)       !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(3,1,K2)  !Global side number

        BDRY_BUFFER_ELEM_MAP(K_R,SIDE_R) = K2
    ENDIF

    BDRY_TYPE(3,K1) = 'E  '
    BDRY_TYPE(1,K2) = 'E  '
    NEIGHBOUR_ID(3,1,K1) = K2
    NEIGHBOUR_SIDE_ID(3,1,K1) = 1
    NEIGHBOUR_ID(1,1,K2) = K1
    NEIGHBOUR_SIDE_ID(1,1,K2) = 3

    IF (BDRY_TYPE(2,K1).EQ.'SP ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(2,K1) = 'E  '
        BDRY_TYPE(2,K2) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K1)
        K_R    = NEIGHBOUR_ID(2,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1

        SIDE_R = NEIGHBOUR_SIDE_ID(2,2,K1)
        K_R    = NEIGHBOUR_ID(2,2,K1)

        NEIGHBOUR_ID(2,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(2,1,K2) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        NEIGHBOUR_ID(2,2,K1) = 0
    ELSEIF (BDRY_TYPE(2,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(2,K1) = 'J  '
        BDRY_TYPE(2,K2) = 'J  '

        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K1)
        K_R    = NEIGHBOUR_ID(2,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'SP '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2
        NEIGHBOUR_SIDE_ID(SIDE_R,1,K_R) = 2
        NEIGHBOUR_ID(SIDE_R,2,K_R) = K1
        NEIGHBOUR_SIDE_ID(SIDE_R,2,K_R) = 2

        NEIGHBOUR_ID(2,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(2,1,K2) = SIDE_R
        BDRY_DATA(5,2,K2) = 0.5d0
        BDRY_DATA(5,2,K1) = 1.0d0
    ELSEIF ((BDRY_TYPE(2,K1).EQ.'GSP').OR.(BDRY_TYPE(2,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        K_R    = NEIGHBOUR_ID(2,1,K1)      !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(2,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        BDRY_BUFFER_SENDSIZE(SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R)+1

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !New element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 2  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R) !Local index of new cell
        INDEX_LIST_LOCAL(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,2)   !Exterior index of second cell
    ELSE
        BDRY_TYPE(2,K2) = BDRY_TYPE(2,K1)
        NEIGHBOUR_ID(2,:,K2) = NEIGHBOUR_ID(2,:,K1)
        NEIGHBOUR_SIDE_ID(2,:,K2) = NEIGHBOUR_SIDE_ID(2,:,K1)
        BDRY_DATA(:,2,K2) = BDRY_DATA(:,2,K1)
    ENDIF

    IF (BDRY_TYPE(4,K1).EQ.'SP ') THEN !Non-conforming edge becomes conforming
        BDRY_TYPE(4,K1) = 'E  '
        BDRY_TYPE(4,K2) = 'E  '

        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K1)
        K_R    = NEIGHBOUR_ID(4,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K2

        NEIGHBOUR_ID(4,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(4,1,K2) = SIDE_R

        SIDE_R = NEIGHBOUR_SIDE_ID(4,2,K1)
        K_R    = NEIGHBOUR_ID(4,2,K1)

        NEIGHBOUR_ID(4,1,K1) = K_R
        NEIGHBOUR_SIDE_ID(4,1,K1) = SIDE_R

        BDRY_TYPE(SIDE_R,K_R) = 'E  '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1

        NEIGHBOUR_ID(4,2,K1) = 0
    ELSEIF (BDRY_TYPE(4,K1).EQ.'E  ') THEN !Conforming edge becomes non-conforming
        BDRY_TYPE(4,K1) = 'J  '
        BDRY_TYPE(4,K2) = 'J  '

        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K1)
        K_R    = NEIGHBOUR_ID(4,1,K1)

        BDRY_TYPE(SIDE_R,K_R) = 'SP '
        NEIGHBOUR_ID(SIDE_R,1,K_R) = K1
        NEIGHBOUR_SIDE_ID(SIDE_R,1,K_R) = 4
        NEIGHBOUR_ID(SIDE_R,2,K_R) = K2
        NEIGHBOUR_SIDE_ID(SIDE_R,2,K_R) = 4

        NEIGHBOUR_ID(4,1,K2) = K_R
        NEIGHBOUR_SIDE_ID(4,1,K2) = SIDE_R
        BDRY_DATA(5,4,K2) = 1.0d0
        BDRY_DATA(5,4,K1) = 0.5d0
    ELSEIF ((BDRY_TYPE(4,K1).EQ.'GSP').OR.(BDRY_TYPE(4,K1).EQ.'GE ')) THEN  !Assemble data into buffers
        K_R    = NEIGHBOUR_ID(4,1,K1)      !Local index
        SIDE_R = NEIGHBOUR_SIDE_ID(4,1,K1) !Global side

        NUM_INDEX_LOCAL(SIDE_R) = NUM_INDEX_LOCAL(SIDE_R)+1 !Size of buffer

        BDRY_BUFFER_SENDSIZE(SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R)+1

        GLOBAL_REFINE_LIST(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K1 !Element index
        GLOBAL_REFINE_LIST(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = K2 !New element index
        GLOBAL_REFINE_LIST(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = 4  !side

        INDEX_LIST_LOCAL(1,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,1)   !Exterior index of cell
        INDEX_LIST_LOCAL(2,NUM_INDEX_LOCAL(SIDE_R),SIDE_R) = BDRY_BUFFER_SENDSIZE(SIDE_R) !Local index of new cell
        INDEX_LIST_LOCAL(3,NUM_INDEX_LOCAL(SIDE_R),SIDE_R)  &
                                & = BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE_R,2)   !Exterior index of second cell
    ELSE
        BDRY_TYPE(4,K2) = BDRY_TYPE(4,K1)
        NEIGHBOUR_ID(4,:,K2) = NEIGHBOUR_ID(4,:,K1)
        NEIGHBOUR_SIDE_ID(4,:,K2) = NEIGHBOUR_SIDE_ID(4,:,K1)
        BDRY_DATA(:,4,K2) = BDRY_DATA(:,4,K1)
    ENDIF

    END SUBROUTINE

!---------------------------------------------------------------------------

    SUBROUTINE UPDATE_GLOBAL_CONNECTIVITY_REFINE()
    USE MESH, ONLY: GLOBAL_NEIGHBOUR_ID, GLOBAL_NEIGHBOUR_SIDE_ID, &
                    & BDRY_BUFFER_NEIGHBOUR_ID, NEIGHBOUR_SIDE_ID, &
                    & NEIGHBOUR_ID, &
                    & BDRY_BUFFER_ELEM_MAP, BDRY_BUFFER_SIDE_MAP, &
                    & BDRY_TYPE, BDRY_DATA, BDRY_BUFFER_NEIGHBOUR_ID, &
                    & BDRY_BUFFER_RECVSIZE
    USE ADAPT,ONLY: NUM_INDEX_LOCAL, NUM_INDEX, INDEX_LIST, &
                    &  INDEX_LIST_LOCAL, GLOBAL_REFINE_LIST
    IMPLICIT NONE
    INCLUDE "mpif.h"

    INTEGER SIDE, ISIDE, L, K1, K2, K_R, IND1, IND2
    INTEGER REQUEST(8), IERROR
    INTEGER STATS(MPI_STATUS_SIZE,8) 

    !This subroutine preforms the connectivity changes along the interprocess boundaries
    
    !During refinement at this level, several arrays have been built. INDEX_LIST_LOCAL stores 
    ! the exterior indexes of the cells along the global edges which have refined. 
    ! We share these arrays in order apply the connectivity changes locally.

    REQUEST = MPI_REQUEST_NULL

    !$OMP MASTER
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(NUM_INDEX_LOCAL(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE,&
                        & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(NUM_INDEX(SIDE), 1, &
                        & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE),&
                        & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)
    !$OMP BARRIER

    REQUEST = MPI_REQUEST_NULL

    !$OMP MASTER
    DO SIDE=1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            CALL MPI_ISEND(INDEX_LIST_LOCAL(:,:,SIDE), 3*NUM_INDEX_LOCAL(SIDE),&
                            & MPI_INTEGER, GLOBAL_NEIGHBOUR_ID(SIDE)-1, SIDE+4,&
                            & MPI_COMM_WORLD, REQUEST(SIDE), IERROR)
            CALL MPI_IRECV(INDEX_LIST(:,:,SIDE), 3*NUM_INDEX(SIDE),&
                            & MPI_INTEGER,GLOBAL_NEIGHBOUR_ID(SIDE)-1, GLOBAL_NEIGHBOUR_SIDE_ID(SIDE)+4,&
                            & MPI_COMM_WORLD, REQUEST(SIDE+4),IERROR)
        ENDIF
    ENDDO
    !$OMP END MASTER

    CALL MPI_WAITALL(8,REQUEST,STATS,IERROR)
    !$OMP BARRIER

    !Apply exterior changes first  
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP DO PRIVATE(IND1,IND2, K1,K2,ISIDE,L) SCHEDULE(DYNAMIC)
            DO L = 1,NUM_INDEX(SIDE)
                IND1 = INDEX_LIST(1,L,SIDE) !Local index of cell whose neighbour is refining

                !Get element and side numbers 
                K1    = BDRY_BUFFER_ELEM_MAP(IND1,SIDE)
                ISIDE = BDRY_BUFFER_SIDE_MAP(IND1,SIDE)

                IF (BDRY_TYPE(ISIDE,K1).EQ.'GE ') THEN !Conforming edge becomes non-conforming
                    BDRY_TYPE(ISIDE,K1) = 'GSP'

                    BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,2) = BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1)
                    BDRY_BUFFER_NEIGHBOUR_ID(IND1,SIDE,1) = INDEX_LIST(2,L,SIDE) !Exterior index of new cell
                ELSEIF (BDRY_TYPE(ISIDE,K1).EQ.'GJ ') THEN !non-conforming edge becomes conforming
                    IND2 = INDEX_LIST(3,L,SIDE) !Local index of cell whose neighbour is refining
                    K2  = BDRY_BUFFER_ELEM_MAP(IND2,SIDE)

                    BDRY_TYPE(ISIDE,K1) = 'GE '
                    BDRY_TYPE(ISIDE,K2) = 'GE '

                    BDRY_BUFFER_NEIGHBOUR_ID(IND2,SIDE,1) = INDEX_LIST(2,L,SIDE) !Exterior index of new cell
                ENDIF
            ENDDO
            !$OMP ENDDO
        ENDIF
    ENDDO
    
    !Apply local changes  
    DO SIDE = 1,4
        IF (GLOBAL_NEIGHBOUR_ID(SIDE).NE.0) THEN
            !$OMP DO PRIVATE(K1,K2,K_R,ISIDE,L) SCHEDULE(DYNAMIC)
            DO L = 1,NUM_INDEX_LOCAL(SIDE)
                K1    = GLOBAL_REFINE_LIST(1,L,SIDE) !Element index
                K2    = GLOBAL_REFINE_LIST(2,L,SIDE) !New element index
                ISIDE = GLOBAL_REFINE_LIST(3,L,SIDE) !side

                IF (BDRY_TYPE(ISIDE,K1).EQ.'GSP') THEN !Non-conforming edge becomes conforming
                    BDRY_TYPE(ISIDE,K1) = 'GE '
                    BDRY_TYPE(ISIDE,K2) = 'GE '

                    K_R = NEIGHBOUR_ID(ISIDE,1,K1)      !Local index

                    NEIGHBOUR_SIDE_ID(ISIDE,1,K2) = SIDE

                    IF ((ISIDE.EQ.1).OR.(ISIDE.EQ.2)) THEN
                        NEIGHBOUR_ID(ISIDE,1,K2) = INDEX_LIST_LOCAL(2,L,SIDE)  !New local index
                        !Update local index mapping
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = K2
                        BDRY_BUFFER_SIDE_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = ISIDE
                        BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(ISIDE,1,K2),SIDE,1) = &
                                            & BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2) 
                    ELSE
                        NEIGHBOUR_ID(ISIDE,1,K2) = K_R
                        NEIGHBOUR_ID(ISIDE,1,K1) = INDEX_LIST_LOCAL(2,L,SIDE) !New local index
                        !Update local index mapping
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = K2
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K1),SIDE) = K1
                        BDRY_BUFFER_SIDE_MAP(NEIGHBOUR_ID(ISIDE,1,K1),SIDE) = ISIDE
                        BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(ISIDE,1,K1),SIDE,1) = &
                                            & BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,2) 
                    ENDIF    
                ELSEIF (BDRY_TYPE(ISIDE,K1).EQ.'GE ') THEN !Conforming edge becomes non-conforming
                    BDRY_TYPE(ISIDE,K1) = 'GJ '
                    BDRY_TYPE(ISIDE,K2) = 'GJ '

                    K_R = NEIGHBOUR_ID(ISIDE,1,K1)      !Local index

                    NEIGHBOUR_SIDE_ID(ISIDE,1,K2) = SIDE

                    IF ((ISIDE.EQ.3).OR.(ISIDE.EQ.4)) THEN
                        NEIGHBOUR_ID(ISIDE,1,K2) = K_R 
                        NEIGHBOUR_ID(ISIDE,1,K1) = INDEX_LIST_LOCAL(2,L,SIDE) !New local index
                        BDRY_DATA(5,ISIDE,K2) = 1.0d0
                        BDRY_DATA(5,ISIDE,K1) = 0.5d0

                        !Update local index mapping
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = K2
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K1),SIDE) = K1
                        BDRY_BUFFER_SIDE_MAP(NEIGHBOUR_ID(ISIDE,1,K1),SIDE) = ISIDE
                        BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(ISIDE,1,K1),SIDE,1) = &
                                            & BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1) 
                    ELSE
                        NEIGHBOUR_ID(ISIDE,1,K2) = INDEX_LIST_LOCAL(2,L,SIDE) !New local index
                        BDRY_DATA(5,ISIDE,K1) = 1.0d0
                        BDRY_DATA(5,ISIDE,K2) = 0.5d0

                        !Update local index mapping
                        BDRY_BUFFER_ELEM_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = K2
                        BDRY_BUFFER_SIDE_MAP(NEIGHBOUR_ID(ISIDE,1,K2),SIDE) = ISIDE
                        BDRY_BUFFER_NEIGHBOUR_ID(NEIGHBOUR_ID(ISIDE,1,K2),SIDE,1) = &
                                            & BDRY_BUFFER_NEIGHBOUR_ID(K_R,SIDE,1) 
                    ENDIF
                ENDIF
            ENDDO
            !$OMP ENDDO
        ENDIF
    ENDDO

    !$OMP MASTER
    BDRY_BUFFER_RECVSIZE(:) = BDRY_BUFFER_RECVSIZE(:)+NUM_INDEX(:)
    NUM_INDEX_LOCAL(:) = 0
    !$OMP END MASTER
    !$OMP BARRIER

    END SUBROUTINE