#!/bin/bash
# MOAB/Torque submission script for SciNet GPC (hybrid job)
#
#PBS -l nodes=8:ppn=8,walltime=0:15:00 -q debug
#PBS -N taylor_vortex
 
# load modules (must match modules used for compilation)
module load intel openmpi
 
# DIRECTORY TO RUN - $PBS_O_WORKDIR is directory job was submitted from
cd $PBS_O_WORKDIR
 
# SET THE NUMBER OF THREADS PER PROCESS:
export OMP_NUM_THREADS=4
 
# EXECUTION COMMAND; -np = nodes*processes_per_nodes; --byhost forces a round robin of nodes.

mv input_1024cell_2group input
mpirun -np 2 --bynode main
wait
mv input input_1024cell_2group

mv input_1024cell_4group input
mpirun -np 4 --bynode main
wait
mv input input_1024cell_4group

mv input_1024cell_8group input
mpirun -np 8 --bynode main
wait
mv input input_1024cell_8group

mv input_1024cell_16group input
mpirun -np 16 --bynode main
wait
mv input input_1024cell_16group

