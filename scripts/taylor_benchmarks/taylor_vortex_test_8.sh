#!/bin/bash
# MOAB/Torque submission script for SciNet GPC (hybrid job)
#
#PBS -l nodes=1:ppn=8,walltime=0:6:00 -q debug
#PBS -N taylor_vortex_8
 
# load modules (must match modules used for compilation)
module load intel openmpi
 
# DIRECTORY TO RUN - $PBS_O_WORKDIR is directory job was submitted from
cd $PBS_O_WORKDIR
 
# SET THE NUMBER OF THREADS PER PROCESS:
export OMP_NUM_THREADS=2
 
# EXECUTION COMMAND; -np = nodes*processes_per_nodes; --byhost forces a round robin of nodes.

mv input_8 input
mpirun -np 8 --bynode main
wait
mv input input_8

